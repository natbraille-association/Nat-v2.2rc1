/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package outils;

import gestionnaires.GestionnaireErreur;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import nat.Nat;

/**
 * Trouve le chemin d'accès à Libre/OpenOffice.org sur une machine Windows qui
 * utilise NAT, et prévient l'utilisateur si cette application n'est pas
 * installée
 * @author Raphaël Mina
 * raphael.mina@gmail.Com
 */
public class Path {

    /** Le gestionnaire d'erreur de la classe*/
    private GestionnaireErreur gest;
    /** L'utilitaire Windows pour accéder à la base de registre */
    private static final String REGQUERY_UTIL = "reg query ";
    /** Attribut de la clé de la base de registre */
    private static final String REGSTR_TOKEN = "REG_SZ";
    /** La racine de LibreOffice.org dans la base de registre sous Windows*/
    private static final String LibreOfficeRoot = "HKLM\\Software\\LibreOffice\\LibreOffice\\";
    /** La racine de LibreOffice.org dans la base de registre sous Windows 64bits*/
    private static final String LibreOfficeRoot64 = "HKLM\\Software\\Wow6432Node\\LibreOffice\\LibreOffice\\";
    /** La racine d'OpenOffice.org dans la base de registre sous Windows*/
    private static final String OpenOfficeRoot = "HKLM\\Software\\OpenOffice.org\\OpenOffice.org\\";
    /** La racine d'OpenOffice.org dans la base de registre sous Windows 64bits*/
    private static final String OpenOfficeRoot64 = "HKLM\\Software\\Wow6432Node\\OpenOffice.org\\OpenOffice.org\\";
    /** Tableau regroupant l'ensemble des regkeys de libre/openoffice. Peut être étendu.*/
    private static final String [] OfficeRegKeys = {LibreOfficeRoot, OpenOfficeRoot,LibreOfficeRoot64,OpenOfficeRoot64};
    /** La clé de registre d'Open/LibreOffice installé sur la machine*/
    private static String InstalledOfficeRegKey = "";
    /** La version d'Open/LibreOffice LA PLUS RECENTE installée sur la machine*/
    private static String InstalledOfficeVersion = "";
    /** Le nom d'Open/LibreOffice Le PLUS RECENT installée sur la machine*/
    private static String InstalledOfficeName = "";
    /** Flux d'entrée */
    private InputStream is;
    /** Flux d'écriture de chaîne de caractères*/
    private StringWriter sw;

    /**
     * Constructeur
     * @param g instance de GestionnaireErreur
     */
    public Path(GestionnaireErreur g) {gest = g;}

    /**
     * lit le flux d'entrée et l'écrit dans {@link #sw}
     */
    public void lire()
    {
        try {
            int c;
            while ((c = is.read()) != -1) {
                sw.write(c);
            }
        } catch (IOException e) {gest.afficheMessage("\nErreur d'entrée sortie lors de la lecture du flux dans Path", Nat.LOG_SILENCIEUX);}
    }

    /**
     * @return {@link #sw} convertit en String
     */
    public String getResult() {return sw.toString();}

    /**
     * Construit les flux
     * @param inputS le flux d'entrée pour {@link #is}
     */
    public void setParameters(InputStream inputS)
    {
        is = inputS;
        sw = new StringWriter();
    }
    
    /**
     * Sous Windows, cherche dans le registre
     * si la clé pathOffice existe et
     * la renvoie ; sinon chaine vide
     * @param pathOffice the registry key to find
     * @return the openoffice version or null if not found
     */
    private String checkRegKey (String pathOffice)
    {
    	Process process;
		try {
			process = Runtime.getRuntime().exec(pathOffice);
	        this.setParameters(process.getInputStream());
	        this.lire();
	        process.waitFor();
	        return this.getResult();
		} catch (Exception e) {
			gest.afficheMessage("Erreur lors de la récupération du numéro de version de "+pathOffice, Nat.LOG_NORMAL);
            return "";
		}
    }

    /**
     * Détecte et renvoie le nom de version d'open office
     * Initialise InstalledOfficeVersion, InstalledOfficeName et InstalledOfficeRegKey
     */
    public void getOfficeNameAndVersion()
    {
    	try
        {
            /*Numéro de la version, stocké sous forme de double afin de
             * permettre des comparaisons facilement
             */
        	/* PRIORITE EST DONNEE A LIBREOFFICE SUR OPENOFFICE */
            String result = "";
            String OfficeRegPattern="";
            double version ;
            int i=0;
            while (result.isEmpty() && i<OfficeRegKeys.length ) {
            	result=checkRegKey(REGQUERY_UTIL + OfficeRegKeys[i]);
            	if (!result.isEmpty())
            	{
            		InstalledOfficeRegKey=OfficeRegKeys[i];
            		if (InstalledOfficeRegKey.contains("LibreOffice"))
            		{OfficeRegPattern="LibreOffice"; InstalledOfficeName="Libre Office";}
            		else {OfficeRegPattern="OpenOffice.org"; InstalledOfficeName="Open Office";}
            	}
            	i++;
            }
            
            if (result.isEmpty()) {return ;} 
            
            /*Curseur permettant de se déplacer dans la chaîne. On le position
             * d'abord au premier "OpenOffice.org" ou "LibreOffice" trouvé, car c'est cette
             * ligne qui est la première intéressante
             */
            int p = result.indexOf("\n", result.indexOf(OfficeRegPattern));

            // On extrait ce qui suit le dernier "\" de la ligne
            String versionString = result.substring(result.lastIndexOf("\\", p) + 1, p);

            /*Si le caractère extrait est un saut de ligne, on l'ignore,
             * c'est à dire que l'on déclare son numéro de version à 0
             */

            if (versionString.charAt(0) == 13) {//13 = saut de ligne
                version = 0;
            } else {
                version = Double.parseDouble(versionString);
            }

            //On se place au début de la prochaine ligne
            p = result.indexOf("HKEY", p);

            /*On récupère la dernière version d'OpenOffice installée
             * en parcourant tout la chaîne de caractères
             */

            while (p != -1) {

                /*Pour chaque ligne contenant la chaîne "HKEY", on extrait
                 * la partie de la chaîne contenue entre le dernier "\" et le
                 * caractère LF
                 */
                versionString = result.substring(result.lastIndexOf("\\", result.indexOf("\n", p)) + 1, result.indexOf("\n", p) - 1);

                // De nouveau, si on a extrait une chaîne vide, on l'ignore
                if (versionString.charAt(0) == 13) {
                    versionString = "0";
                }

                /*Si la version trouvée à ce stade est plus récente que la
                 * dernière trouvée, on actualise le numéro de version
                 */
                version = (Double.parseDouble(versionString) > version ? Double.parseDouble(versionString) : version);

                /*Avancement du curseur au prochain début de ligne commençant
                 * par HKEY. S'il n'y en a pas, p prend la valeur -1 et on sort
                 * de la boucle
                 */
                p = result.indexOf("HKEY", p + 1);
            }

            InstalledOfficeVersion= String.valueOf(version);
            return ;
            

        } catch (Exception e) 
        {
        	gest.afficheMessage("Erreur lors de la récupération du numéro de version d'open/libre office", Nat.LOG_NORMAL);
            return ;
        }
    }

    /**
     * @return le chemin de l'exécutable d'open/libre office
     */
    public String getOOPath() {
        try {
            //Récupération du numéro de version
        	getOfficeNameAndVersion();

            // Si OpenOffice n'est pas installé
            if (InstalledOfficeVersion.isEmpty()) {
                return "";
            }
            gest.afficheMessage("\n*** "+InstalledOfficeName+" version "+InstalledOfficeVersion+
                    " détecté", Nat.LOG_VERBEUX);

            /*try{
            Process process = Runtime.getRuntime().exec(REGQUERY_UTIL + InstalledOfficeRegKey + InstalledOfficeVersion + "\\ /s");
            //On donne à l'objet Path le flux sortant du process
            this.setParameters(process.getInputStream());
            this.lire();
            process.waitFor();
            }
            catch(Exception e){
                gest.afficheMessage("Erreur lors de la recherche du chemin "+
                        "d'accès à OpenOffice.org. Essayez de convertir "+
                        "manuellement votre document d'entrée",Nat.LOG_VERBEUX);}*/

            String result = checkRegKey(REGQUERY_UTIL + InstalledOfficeRegKey + InstalledOfficeVersion + "\\ /s");
            //Récupération de la ligne contenant le path :
            result = result.substring(result.indexOf("Path"), result.indexOf("\n", result.indexOf("Path")));
            //Parsage de la ligne : on ne conserve que le chemin d'accès
            result = result.substring(result.indexOf(REGSTR_TOKEN) + REGSTR_TOKEN.length() + (" ").length());

            /*Remplacement des \ par des \\ pour le traitement java correct et
             * suppression des sauts de ligne et retours chariots.
             */
            result=result.replace("\\","\\\\");
            result=result.replace("\n","");
            result=result.replace("\r","");
            return result;

        } catch (Exception e) {
            return "";
        }
    }
}
