/*
 * Trace assistant
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package outils.data;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import outils.FileToolKit;
/**
 * This class is designed to convert mtables (mathml) and Arrays in various formats
 * @author bruno
 *
 */
public class TableConverter
{
	/** Constant representing the tmp file location used to load the String*/
	public static final String TMP_TABLE_FILE = "/tmp/testTable.xml";
	/**
	 * Convert a mtable into a 2D ArrayList
	 * @param f the file containing the mtable
	 * @return the 2D ArrayList representing the given mtable
	 */
	public static ArrayList<ArrayList<String>> mTable2Array(File f)
	{
		//Array to be returned
		ArrayList<ArrayList<String>> ret = new ArrayList<ArrayList<String>>();
		
		//Builders and fabrics
    	DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
    	fabrique.setValidating(false);
		DocumentBuilder constructeur;
        try
        {
            constructeur = fabrique.newDocumentBuilder();
            //parsing the s string
            org.w3c.dom.Document doc = constructeur.parse(new File("/tmp/testTable.xml"));
		    //setting doc properties
		    doc.setXmlVersion("1.0");
		    doc.setXmlStandalone(true);
		    
		    //get the mtable node
		    Node root = doc.getElementsByTagName("mtable").item(0);
		    //get the mtr nodes
		    NodeList rows = root.getChildNodes();
		    System.out.println("rows="+rows.getLength());
		    for(int i=0; i<rows.getLength();i++)
		    {
		    	Node mtr = rows.item(i);
		    	System.out.println(mtr.getNodeName()+";"+mtr.getLocalName());
		    	if(mtr.getNodeName().equals("mtr"))
		    	{
		    		ArrayList<String> al_row = new ArrayList<String>();
		    		NodeList cols = mtr.getChildNodes();
		    		//adding content
		    		System.out.println("cols:"+cols.getLength());
		    		for(int j=0; j<cols.getLength();j++)
		    		{
		    			Node mtd = cols.item(j);
		    			System.out.println(mtd.getNodeName()+";"+mtd.getLocalName());
		    			if(mtd.getNodeName().equals("mtd")){al_row.add(mtd.getTextContent().replaceAll("[\n\t ]", ""));}
		    		}
		    		ret.add(al_row);
		    	}
		    }
        }
        catch(ParserConfigurationException pce)
        {
        	pce.printStackTrace();
        }
        catch (SAXException se)
        {
	        se.printStackTrace();
        }
        catch (IOException ioe)
        {
	        ioe.printStackTrace();
        }
        return ret;
	}
	
	/**
	 * Convert a mtable into a 2D ArrayList
	 * @param s the string containning the mtable
	 * @return the 2D ArrayList representing the given mtable
	 */
	public static ArrayList<ArrayList<String>> mTable2Array(String s)
	{
		FileToolKit.saveStrToFile(s, TableConverter.TMP_TABLE_FILE, "UTF-8");
		return mTable2Array(new File(TableConverter.TMP_TABLE_FILE));
	}
	
	/** 
	 * Convert a 2D ArrayList in a linearized string
	 * @param t the 2D ArrayList to be converted
	 * @param sep the separator used between cells
	 * @param end the line separator
	 * @param empty replacement for empty cells
	 * @return a linearisation of t
	 */
	public static String lineariseTab(ArrayList<ArrayList<String>> t, String sep, String end, String empty)
	{
		StringBuffer ret = new StringBuffer("");
		//add rows
	    for(ArrayList<String>r:t)
		{
	    	//add cells
			for(String str:r)
			{
				if(str.isEmpty()){ret.append(empty+sep);}
				else{ret.append(str+sep);}
			}
			if(t.lastIndexOf(r)!=t.size()-1){ret.append(end);}
		}
		return ret.toString();
	}
	
	/**
	 * Convert a 2D ArrayList in a MathMl String
	 * The String contains only the mtable (no xhtml headers)
	 * @param t the 2D ArrayList to convert
	 * @return the mtable without headers
	 */
	public static String Array2mTable(ArrayList<ArrayList<String>> t)
	{
		String ret ="";
		try
		{
			//Creating a new Document
			DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
			DocumentBuilder constructeur= fabrique.newDocumentBuilder();
		    Document doc = constructeur.newDocument();
		    doc.setXmlVersion("1.1");
		    doc.setXmlStandalone(true);
		    
		    //create root
		    Element racine = doc.createElement("mtable");
		    //add rows
		    for(ArrayList<String>r:t)
			{
		    	//add cells
		    	Element row = doc.createElement("mtr");
				for(String str:r)
				{
					Element cell = doc.createElement("mtd");
					cell.setTextContent(str);
					row.appendChild(cell);
				}
				racine.appendChild(row);
			}
		    doc.appendChild(racine);
		    //save file
		    //configuration de la transformation
			DOMSource in = new DOMSource(doc);
			StringWriter swResu = new StringWriter();
			StreamResult out = new StreamResult(swResu);
			//Création du transformeur
			TransformerFactory transformFactory = TransformerFactory.newInstance();
			// lire le style
			Transformer transform = transformFactory.newTransformer();
			transform.setOutputProperty(OutputKeys.METHOD, "xml");
			transform.setOutputProperty(OutputKeys.INDENT, "yes");
			transform.transform(in,out);
			
		    ret = swResu.getBuffer().toString();		    
		}
        catch (ParserConfigurationException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (TransformerConfigurationException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (TransformerException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
		return ret;
	}
	
	/**
	 * Only for testing
	 * @param a arguments
	 */
	public static void main(String [] a)
	{
		String s = 
			"<mtable><mtr><mtd><msub><mi>CH</mi><mn>3</mn></msub></mtd>"+
			"<mtd><mtext>-</mtext></mtd><mtd><mi>CH</mi></mtd><mtd><mtext>-</mtext></mtd>"+
			"<mtd><msub><mi>CH</mi><mn>3</mn></msub></mtd></mtr>"+
			"<mtr><mtd><mtext/></mtd><mtd><mtext/></mtd><mtd><mtext>|</mtext></mtd>"+
			"<mtd><mtext/></mtd><mtd><mtext/></mtd></mtr>"+
			"<mtr><mtd><mtext/></mtd><mtd><mtext/></mtd><mtd><mi>OH</mi></mtd><mtd><mtext/>"+
			"</mtd><mtd><mtext/></mtd></mtr></mtable>";
		ArrayList<ArrayList<String>> al = mTable2Array(s);
		for(ArrayList<String>r:al)
		{
			for(String str:r){System.out.print(str);}
			System.out.println();
		}
	    System.out.println("********** TABLE********\n"+Array2mTable(al));
	    
	    System.out.println("**********LINEARISE*****\n"+lineariseTab(al," ", "`@ ","´,"));
	}
}
