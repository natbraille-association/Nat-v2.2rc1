/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package outils.regles;

import nat.transcodeur.Ambiguity;


/**
 * Classe de regrouppement des règles portant sur l'abréviation de mot (Locution, symboles, signes)
 * Ces règles génèrent les listes de mots et leurs abréviations
 * @author bruno
 *
 */
public abstract class RegleMot extends Regle implements Comparable<RegleMot>
{
	/** Mot(s) en noir */
	protected String noir;
	/** transcription */
	protected String braille;
	/**
	 * Il this rule may produce an ambiguity from braille to black text
	 */
	protected Ambiguity ambBl = null;
	
	/**
	 * Il this rule may produce an ambiguity from  black to braille text
	 */
	protected Ambiguity ambBr = null;
	
	/**
	 * @param d description
	 * @param r référence
	 * @param n mot(s) en noir
	 * @param b transcription en braille
	 * @param a true si règle active
	 * @param p true si règle pédagogique
	 * @param aBl the black to braille ambiguity
	 * @param aBr the braille to black ambiguity
	 */
	public RegleMot(String d, String r, String n, String b, boolean a, boolean p, Ambiguity aBr, Ambiguity aBl)
	{
		super(d,r,a,p);
		noir = n;
		braille = b;
		ambBr = aBr;
		ambBl = aBl;
	}
	
	/**
	 * Renvoie le(s) mot(s) en noir
	 * @return {@link #noir}
	 */
	public String getNoir()
	{
		String ret = noir;
		if(ambBl!=null)
		{
			ret="if($blackToBraille) then ('["+ambBl.getDescription()+"|"+ambBl.getAvant()+"|"+braille+"|"+ambBl.getApres()+"|";
			for(String s:ambBl.getPropositions()){ret+= s+"|";}
			ret+="]') else "+ "'"+noir+"'";
			System.out.println(ambBl);
		}
		return ret;
	}
	/**
	 * Renvoie la transcription de {@link #noir} en braille
	 * @return {@link #braille}
	 */
	public String getBraille()
	{
		String ret = braille;
		if(ambBr!=null)
		{
			ret="if(not($blackToBraille)) then '"+braille+"' else ('["+ambBr.getDescription()+"|"+ambBr.getAvant()+"|"+noir+"|"+ambBr.getApres()+"|";
			for(String s:ambBr.getPropositions()){ret+= s+"|";}
			ret+="]')";
		}
		return ret;
	}
	
	/**
	 * Tri des mots par longueur
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(RegleMot rm) {
		return rm.getNoir().length()-noir.length();
	}
	
	/**
     * Set the ambiguity
     * @param a the ambiguity to set
     * @param blackToBraille true if {@link #ambBl} to set, false if {@link #ambBr}
     */
    public void setAmbiguity(Ambiguity a, boolean blackToBraille)
    {
    	if(blackToBraille){ambBr = a;}
    	else{ambBl = a;}
    }
    
    /**
     * The {@link #ambBr} (Black to Braille)
     * @return {@link #ambBr}
     */
    public Ambiguity getAmbBr(){return ambBr;}
    
    /**
     * The {@link #ambBl} (Braille tp black)
     * @return {@link #ambBl}
     */
    public Ambiguity getAmbBl(){return ambBl;}
}
