/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package outils.regles;

import nat.transcodeur.Ambiguity;

/**
 * Classe premettant de représenter une règle de type "Locution";
 * Les règles de locution font correspondre plusieurs mots à un seul ensemble de signes braille.
 * Les locutions sont invariantes et ne se composent pas avec d'autres règles
 * @author bruno
 *
 */
public class RegleLocution extends RegleMot
{	
	/**
	 * Constructeur
	 * @param n la locution en noir
	 * @param b la transcription en braille
	 * @param a true si règle active
	 * @param p true si règle pédagogique
	 * @param aBl the black to braille ambiguity
	 * @param aBr the braille to black ambiguity
	 */
	public RegleLocution(String n, String b, boolean a, boolean p, Ambiguity aBr, Ambiguity aBl)
	{
		super("Locution", "IV",n,b,a,p,aBr,aBl);
	}
	
	/**
	 * Redéfinition de {@link outils.regles.Regle#toString()}
	 * @see outils.regles.Regle#toString()
	 */
	@Override
	public String toString()
	{
		return description + " ("+reference+"): "+ noir + " est transcrit par " + braille; 
	}

	/**
	 * Renvoie vrai si r est une RegleLocution et que les attributs noir sont égaux
	 */
	@Override
	public boolean equals(Object r)
	{
		return (r instanceof RegleLocution) && ((RegleLocution)r).noir.equals(noir);
	}

	/**
	 * @see outils.regles.Regle#getXML()
	 */
	@Override
	public String getXML()
	{
		String a = actif ? " actif=\"true\"" : "actif=\"false\"";
		String p = peda ? " peda=\"true\"" : "";
		String amBr = ambBr != null ? "\t\t<ambBr>\n"+ambBr.getXML("\t\t\t")+"\t\t</ambBr>\n" : "";
		String amBl = ambBl != null ? "\t\t<ambBl>\n"+ambBl.getXML("\t\t\t")+"\t\t</ambBl>\n" : "";
		return "\t<locution " + a +p+">\n" +
				"\t\t<noir>"+noir+"</noir>\n" +
				"\t\t<braille>" + braille + "</braille>\n" +
				amBr + amBl +
				"\t</locution>\n";
	}
}
