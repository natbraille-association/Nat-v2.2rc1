/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package outils;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import java.io.FileInputStream;
import nat.ConfigNat;
//import nat.Nat;
import nat.OptNames;
import java.util.Arrays;
import java.util.List;
import outils.FileToolKit;

/**
 * Restorer for system configurations
 * 
 * @author vivien
 * 
 */

public class SystemConfRestorer {

	/**
	 * get files in system conf directory
	 * 
	 * @return list of files
	 */
	private static List<File> getSystemConfFiles() {
		File originalSysConfDir = new File(
				ConfigNat.getSystemConfigFilterFolder());
		// File[] sysFiles = originalSysConfDir.listFiles();
		List<File> list = Arrays.asList(originalSysConfDir.listFiles());
		return list;
	}

	/**
	 * gets the basename of a file
	 * 
	 * @param f
	 *            the File
	 * @return File basename
	 */
	public static String basename(File f) {
		String ret = "";
		int lastSep = f.getPath().lastIndexOf(File.separator);
		if (lastSep > 0) {
			ret = f.getPath().substring(lastSep);
		}
		return ret;
	}
	
	/**
	 * copies the directory of the abrege config files
	 * located in system directory config to user config directory
	 * 
	 * @param f the abrege config directory
	 * @return true if the target directory has been made
	 * 
	 */
	private static boolean copieleDiRectory(File f){
		
		File newDirectory = new File(ConfigNat.getUserConfigFilterFolder() + basename(f));
		boolean allOk=false;
		try {
			allOk = newDirectory.mkdir();
			List<File> list = Arrays.asList(f.listFiles());
			for (File nf : list){
				String baseName = basename(nf);
				if (nf.isFile()) 
					FileToolKit.copyFile(f.getCanonicalPath()+baseName,
							newDirectory.getCanonicalPath()+baseName);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return allOk;
	}
	
	/**
	 * copies a file to user configration directory if it's a system config.
	 * 
	 * @param f
	 *            the file
	 * @param force
	 *            if true, overwrites if f already exists
	 * @return true if success
	 */
	private static boolean pushSystemConfFile(File f, boolean force) {
		boolean cannotReplace = false;

		if (!f.isDirectory()) {
			try {

				// load <f> as a property file
				Properties conf = new Properties();
				FileInputStream fis = new FileInputStream(f);
				conf.load(fis);
				fis.close();

				// ensures it's a config file
				if (conf.containsKey(OptNames.fi_is_sys_config)) {
					File newFile = new File(
							ConfigNat.getUserConfigFilterFolder() + basename(f));

					if ((!newFile.exists()) || force) {
						FileToolKit.copyFile(f.getPath(), newFile.getPath());
						// conf.store(new
						// FileOutputStream(newFile),"configuration copiee");
					} else {
						cannotReplace = true;
					}
				}
			} catch (java.io.FileNotFoundException e) {
				// System.out.println("the system configuration file "+f+" does not exists");
			} catch (java.io.IOException e) {
				// System.out.println("the system configuration file "+f+" is not a property file");
			}
		} else {
			/*si le directory commence par . on considère que c'est
			un dossier système genre .svn et on le recopie pas */
			if (!f.getName().startsWith(".")) {copieleDiRectory(f);}
		}
		return cannotReplace;
	}

	/**
	 * restore all config files
	 * 
	 * @param force
	 *            overwrites if file already exists
	 */
	public static void restoreNonInteractive(boolean force) {
		for (File a : getSystemConfFiles()) {
			if (pushSystemConfFile(a, force)) {
				// System.out.println("cannot replace");
			}
		}
	}

}
