/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package outils.emboss;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.PrintService;
//import javax.print.ServiceUI;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.Sides;


import nat.ConfigNat;
import nat.Nat;
import gestionnaires.GestionnaireErreur;
/**
 * Gère l'embossage avec un périphérique d'impression installé sur la machine
 * @author bruno
 *
 */
public class EmbosseurInstalle extends Embosseur
{
	/** 
	 * Constructeur
	 * @param f L'adresse du fichier à embosser
	 * @param g une instance de {@link GestionnaireErreur}
	*/
	public EmbosseurInstalle(String f, GestionnaireErreur g){super(f, g);}
	/**
	 * Rédéfinition de {@link Embosseur#Embosser()}
	 * <p>Recherche parmis les périphériques installés le périphérique d'embossage à utiliser</p>
	 * <p>Tente un embossage en Text/Plain</p>
	 */
	@Override
	public void Embosser()
	{
		gest.afficheMessage("\nEmbossage", Nat.LOG_NORMAL);
		gest.afficheMessage("\n**Recherche des imprimantes", Nat.LOG_VERBEUX);
		// recherche de l'embosseuse sélectionnée
		boolean trouve = false;
		
		PrintService[] pservices = PrintServiceLookup.lookupPrintServices(null, null);
		if (pservices.length > 0)
		{
			//il y a des imprimantes
			int i =0;
			while (!trouve && i < pservices.length)
			{	
				if (pservices[i].getName().compareTo(ConfigNat.getCurrentConfig().getPrintservice())==0)
				{
					trouve = true;
				}
				else {i++;}
			} //i est l'imprimante choisie
			if (trouve)
			{
				PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
				aset.add(new Copies(1));
				aset.add(MediaSizeName.ISO_A4);
				aset.add(Sides.DUPLEX);
				PrintService service = pservices[i];
				//PrintService service =  ServiceUI.printDialog(null, 50, 50, pservices, pservices[i], null, aset);
				if (service != null)
				{
					gest.afficheMessage("\n**Sélection de l'imprimante " + service.getName(), Nat.LOG_VERBEUX);
					
					FileInputStream fis = null;
					try {
					   fis = new FileInputStream(fichier);
					}
					catch (FileNotFoundException ffne)
					{
						gest.afficheMessage("\nErreur, le fichier "+fichier+" n'a pas pu être lu", Nat.LOG_SILENCIEUX);
						ffne.printStackTrace();
					}
					if (!(fis==null))
					{
						gest.afficheMessage("\n**Création du document", Nat.LOG_VERBEUX);
						DocFlavor [] sdf = service.getSupportedDocFlavors();
						
						gest.afficheMessage("\n*** Liste des DocFlavor supportés:",Nat.LOG_DEBUG);
						for (int j=0; j< sdf.length; j++)
						{
							gest.afficheMessage("\n - " + sdf[j],Nat.LOG_VERBEUX);
						}
						
						DocFlavor fisFormat = DocFlavor.INPUT_STREAM.TEXT_PLAIN_HOST;						
						if(!service.isDocFlavorSupported(fisFormat))
						{
							gest.afficheMessage("\n**Avertissement: impossible d'utiliser le DocFlavor spécifié, remplacement par un DocFlavor plus générique... ", Nat.LOG_VERBEUX);
							fisFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
						}
						if(service.isDocFlavorSupported(fisFormat))
						{
							Doc document = new SimpleDoc(fis, fisFormat, null); 
							//PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
							try
							{
								gest.afficheMessage("\n**Impression de "+fichier, Nat.LOG_VERBEUX);
								DocPrintJob job = service.createPrintJob();
								job.print(document, aset);
							}
							catch (PrintException e)
							{
								gest.afficheMessage("\n** Erreur lors de l'impression", Nat.LOG_NORMAL);
								e.printStackTrace();
							}
							catch (Exception e)
							{
								gest.afficheMessage("\n** Erreur d'impression", Nat.LOG_NORMAL);
								e.printStackTrace();
							}
						}
						else
						{
							gest.afficheMessage("\n** Erreur: l'imprimante séléctionnée ne peut imprimer en mode texte via java", Nat.LOG_NORMAL);
						}
					}
					else
					{
						gest.afficheMessage("\nErreur lors de la création du fichier d'impression", Nat.LOG_NORMAL);
					}
				}
				else{ gest.afficheMessage("\nErreur: Imprimante non compatible", Nat.LOG_SILENCIEUX);}
			}
			gest.afficheMessage("\n--Fin de la procédure d'impression", Nat.LOG_NORMAL);
		}
	}
	/**
	 * Rédéfinition de {@link Embosseur#Embosser(String)}
	 * <p>Met à jour l'adresse du fichier à embosser</p>
	 * <p>Appel de {@link #Embosser()}</p>
	 * @param f l'adresse du fichier à embosser
	 */
	@Override
	public void Embosser(String f)
	{
		fichier = f;
		Embosser();
	}	
}
