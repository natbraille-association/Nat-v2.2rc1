/*
 * Trace assistant
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package outils.emboss;

/**
 * Cette classe configure l'embossage d'un document
 * @author bruno
 *
 */
public class Embossage
{
	/** constante représentant la page actuelle */
	public static final int CURRENT = 1;
	/** constante représentant toutes les pages du document */
	public static final int ALL = 2;
	/** constante représentant "aucune page sélectionnée"*/
	public static final int NONE = 0;
	
	/** Nombre d'exemplaires à embosser */
	private int nbExemplaires = 1;
	/** tableau des pages à embosser */
	private boolean[] pages;
	/** numéro de la page actuelle (commence à 0)*/
	private int pageActu = 0;
	
	/**
	 * Construit un objet embossage
	 * @param p adresse du tableau des pages
	 * @param pa page actuellement sélectionnée
	 * @param nbEx nombre d'exemplaires à embosser
	 */
	public Embossage(boolean[] p, int pa, int nbEx)
	{
		pages = p;
		pageActu=  pa;
		nbExemplaires = nbEx;
	}
	
	/**
	 * Construit un objet embosssage
	 * Le nombre d'exemplaire {@link #nbExemplaires} est fixé à 1
	 * @param p adresse du tableau de pages à embosser
	 * @param pa page actuellement sélectionnée
	 */
	public Embossage(boolean[] p,int pa)
	{
		pages=p;
		pageActu= pa;
		nbExemplaires = 1;
	}
	
	/**
	 * Construit un objet embosssage
	 * Le nombre d'exemplaire {@link #nbExemplaires} est fixé à 1
	 * La page actuelle {@link #pageActu} est fixée à 0
	 * @param p adresse du tableau de pages à embosser
	 */
	public Embossage(boolean[] p)
	{
		pages=p;
		pageActu= 0;
		nbExemplaires = 1;
	}

	/**
     * @param nbEx the {@link #nbExemplaires} to set
     */
    public void setNbExemplaires(int nbEx)
    {
	    if(nbEx>0){nbExemplaires = nbEx;}
	    else{nbExemplaires = 1;}
    }

	/**
     * @return the {@link #nbExemplaires}
     */
    public int getNbExemplaires()
    {
	    return nbExemplaires;
    }

	/**
     * @return the {@link #pages} to emboss
     */
    public boolean[] getPages(){return pages;}

	/**
     * @param pa the {@link #pageActu} to set
     */
    public void setPageActu(int pa){pageActu = pa;}

	/**
     * @return the pageActu {@link #pageActu}
     */
    public int getPageActu(){return pageActu;}

	/**
	 * Sélectionne 
	 * <ul>
     * 		<li>{@link #CURRENT}: la page actuelle {@link #pageActu}</li>
     *	</ul>
     * @param c constante représentant l'action de sélection à réaliser
     */
    public void select(int c)
    {
    	switch(c)
    	{
    		case CURRENT:
    		{
    			int i=0;
				for(i=0; i<pageActu-1;i++){pages[i]=false;}
				pages[i]=true;
				for(i=pageActu;i<pages.length;i++){pages[i]=false;}
				break;
    		}
    		case ALL:
    		{
    			for(int i=0;i<pages.length;i++){pages[i]=true;}
    			break;
    		}
    		case NONE:
    		{
    			for(int i=0;i<pages.length;i++){pages[i]=false;}
    		}
    	}
    }
    
    /**
     * Réalise la sélection des pages à embosser à partir de la chaines s
     * @param s la chaine contenant les motifs
     */
    public void select(String s)
    {
    	//initialisation à false
		select(Embossage.NONE);
		//parsage de la chaine des motifs
		String [] motifs = s.split(";");
    	for(int i =0; i<motifs.length;i++)
		{
			String [] sousMotifs= motifs[i].split("-");
			if (sousMotifs.length == 1) //un seul nombre
			{
				int p = -1;
				try{p = Integer.parseInt(sousMotifs[0]) - 1;}
				catch(NumberFormatException nfe){nfe.printStackTrace();}//erreur de conversion
				
				if(p>-1 && p < pages.length){pages[p]=true;}
			}
			if (sousMotifs.length == 2)
			{
				int p1 =-1, p2 = -1;
				try
				{
					p1 = Integer.parseInt(sousMotifs[0]) - 1;
					p2 = Integer.parseInt(sousMotifs[1]) - 1;
				}
				catch(NumberFormatException nfe){nfe.printStackTrace();}//erreur de conversion
				if(p1>-1 && p1 < pages.length && p2>-1 && p2 < pages.length && p1<=p2)
				{
					for(int j= p1; j<= p2;j++){pages[j]=true;}
				}
			}
		}
    }
}
