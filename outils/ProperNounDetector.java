/*
 * Nat Braille
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * http://natbraille.free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package outils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import nat.ConfigNat;
import nat.Transcription;
import net.sf.saxon.Controller;
import net.sf.saxon.event.Emitter;
import net.sf.saxon.event.MessageEmitter;

/**
 * This class provides methods to check the converted xml file and search for proper nouns
 * It updates the user list of proper nouns 
 * @author bruno
 *
 */
public class ProperNounDetector
{
	/** The adress of the xsl file containing the algorithm for proper name detection*/
	public static final String PROPERNAME_XSL = ConfigNat.getInstallFolder()+"xsl/propername_detection.xsl";

	/**
	 * check the converted xml file and search for proper nouns
	 * updates the user list of proper nouns 
	 * @param allPunct true if adding ; and : to the liste of final punctuations
	 * @return ArrayList of <String, Boolean, Boolean> (word, active, check)
	 */
	public static ArrayList<ArrayList<Object>> extractProperNouns(boolean allPunct)
	{
		ArrayList<String> words = new ArrayList<String>();
		ArrayList<ArrayList<Object>> d = new ArrayList<ArrayList<Object>>();
		try
        {
	        BufferedReader raf = new BufferedReader(new InputStreamReader(new FileInputStream(ConfigNat.getFichierIntegral()),"UTF-8"));
	        String ligne=raf.readLine();
			while(ligne!=null)
			{
				String[] exep = ligne.split(" ");
				if(exep.length==3)
				{
					String mot = exep[0];
					Boolean auto = new Boolean(Boolean.parseBoolean(exep[1]));
					Boolean ask = new Boolean(Boolean.parseBoolean(exep[2]));
					ArrayList<Object> a = new ArrayList<Object>();
					
					a.add(mot);
					words.add(mot);
					a.add(auto);
					a.add(ask);
					
					d.add(a);
				}
				ligne=raf.readLine();
			}

			ArrayList<Object> a = new ArrayList<Object>();
			a.add("");
			a.add(Boolean.FALSE);
			a.add(Boolean.FALSE);
			d.add(a);
			
			
			raf.close();
        }
        catch (UnsupportedEncodingException e){e.printStackTrace();}
        catch (FileNotFoundException e)
        {
        	System.out.println("filenotfound");
        	ArrayList<Object> a = new ArrayList<Object>();
        	a.add("");
        	a.add(Boolean.FALSE);
        	a.add(Boolean.FALSE);
        	d.add(a);
        	e.printStackTrace();
        }
        catch (IOException e){e.printStackTrace();}
		
        //executing algorithm
        try 
		{
			//Création du document source
			DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
			DocumentBuilder constructeur= fabrique.newDocumentBuilder();
		    Document doc = constructeur.parse(new File(Transcription.fTempXML));
		    doc.setXmlVersion("1.1");
		    doc.setXmlStandalone(true);
			
			//Création du transformeur
			TransformerFactory transformFactory = TransformerFactory.newInstance();
			StreamSource styleSource = new StreamSource(new File(PROPERNAME_XSL));
			// lire le style
			Transformer transform = transformFactory.newTransformer(styleSource);
			transform.setParameter("allPonct", allPunct);
			transform.setOutputProperty(OutputKeys.METHOD, "text");
			
			
			//configuration de la transformation
			DOMSource in = new DOMSource(doc);
			StringWriter swResu = new StringWriter();
			StreamResult out = new StreamResult(swResu);
			
			//pour récupérer les xsl:message
			Controller control = (Controller)transform;
			control.setMessageEmitter(new MessageEmitter());
			Emitter mesgEm = (Emitter) control.getMessageEmitter();
			
			StringWriter swMesg = new StringWriter();
			mesgEm.setWriter(swMesg);
			
			//transformation
			transform.transform(in, out);
			
			//utilisation des résultats
			String resu = swResu.getBuffer().toString();
			String list[] = resu.split(";");
			String listOK[] = list[0].split(" ");
			String listVERIF[] = list[1].split(" ");
			String listNOK[] = list[2].split(" ");
			//insertion dans la liste si pas de doublon
			for(String s: listOK)
			{
				if(!words.contains(s) && !s.trim().equals(""))
				{
					ArrayList<Object> a = new ArrayList<Object>();
		        	a.add(s);
		        	a.add(Boolean.TRUE);
		        	a.add(Boolean.FALSE);
		        	d.add(a);
				}
			}
			for(String s: listVERIF)
			{
				if(!words.contains(s) && !s.trim().equals(""))
				{
					ArrayList<Object> a = new ArrayList<Object>();
		        	a.add(s);
		        	a.add(Boolean.TRUE);
		        	a.add(Boolean.TRUE);
		        	d.add(a);
				}
			}
			for(String s: listNOK)
			{
				if(!words.contains(s) && !s.trim().equals(""))
				{
					ArrayList<Object> a = new ArrayList<Object>();
		        	a.add(s);
		        	a.add(Boolean.FALSE);
		        	a.add(Boolean.FALSE);
		        	d.add(a);
				}
			}
		}
		catch (NullPointerException e)  
		{
			e.printStackTrace();
		}
        catch (ArrayIndexOutOfBoundsException e){
        	//TODO
        	System.err.println("Liste vide ou mal formée");
        	e.printStackTrace();
        }
        catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        catch (SAXException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (IOException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        return d;
	}
}
