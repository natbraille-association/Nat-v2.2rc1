    :��une cons�quence de
      l'effet de �serre
      :��terres en voie
       de �disparition
  :��si la plan�te se r�-
chauffe, le niveau des mers
montera. �doucement, mais s5-
rement. �et si �a d�borde pour
de bon? �les 3les, les deltas,
les estuaires seront aux pre-
mi�res loges. �or ces r�gions
sont souvent tr�s peupl�es.
�que �faire?
  �question: au ��xxi^�me
(`21^�me) si�cle, quel sera le
point commun entre un �hollan-
dais, un �bangladais et un ha-
bitant des 3les �marshall dans
le �pacifique?
  �r�sultat. �les zozos qui
ont r�pondu �a ou �b, ont un
gage: porter un sombrero avec
thermom�tre int�gr�! �les
autres ont les f�licitations
du jury. �car aux derni�res
nouvelles scientifiques, il
semble bien que la temp�rature
moyenne de l'atmosph�re va
augmenter dans les prochaines
d�cennies (:�voir encadr� p.
�`9). �et une des cons�quences
sur laquelle les savants de
tout poil tombent d'accord,
c'est bien une mont�e des
eaux.


         �du monde au
        bord de l'eau


  �le hic, c'est que l'homme
s'est install� dans les deltas
et � l'embouchure des fleuves,
en bord de mer et d'oc�an.
�pour jouir du commerce flu-
vial et maritime, pour �chap-
per � la surpopulation des r�-
gions int�rieures (comme en
�hollande ou au �bangla-    `2
desh), pour la bronzette et le
bain marin, ou par amour de
l'azur bleut�. �alors si l'eau
monte... �certes, direz-vous,
tout le monde ne sera pas tou-
ch� (:�voir carte p. �`8), et
pour les intr�pides qui ont
construit leur bicoque au ras
des flots il n'y a pas le feu
au lac. �la mont�e des eaux
sera lente (en moyenne de `2 �
`4 mm chaque ann�e), ils au-
ront le temps de r�agir.
�pourtant --imaginons que
l'eau soit plus haute de `1 m-
- ils risquent fort de se
mouiller les pantoufles car
l'eau d�bordera plus souvent
et engendrera plus de d�g1ts.
  �pour comprendre, prenons
deux exemples. �en `1953, le
long des c4tes hollandaises,
une terrible temp2te fit mon-
ter l'eau de `4 m au-dessus de
son niveau normal. �la      `3
mer du �nord brisa des digues,
inonda `15'000 ha de polders
(ces terres artificiellement
gagn�es par l'homme sur la
mer) et fit `1'800 morts. �un
tel �v�nement est exception-
nel; la probabilit� qu'il se
produise n'est que de `1 fois
tous les `250 ans. �mais si le
niveau marin �tait plus haut
de `1 m (et en supposant que
l'on n'ait pas augment� la
taille des digues), il suffi-
rait alors d'une �l�vation de
`3 m pour retrouver les m2mes
conditions. �or une "surcote"
de `3 m est beaucoup plus pro-
bable: elle a lieu environ `1
fois tous les `50 ans!
�deuxi�me exemple: en `1982,
le cyclone �isaac inonda `23
km^2 de l'3le de �tongatapu
(royaume du �tonga, dans le
�pacifique) et toucha `33���
de ses `67'000 habitants.   `4
�si la mer montait de `1 m, le
m2me cyclone inonderait `37
km^2, et `45��� de la popula-
tion se retrouverait les pieds
dans l'eau. �face � la mont�e
des eaux, mieux vaut donc pr�-
voir que voir venir. �mais que
faire? �pour simplifier, di-
sons qu'il existe trois solu-
tions: se prot�ger, fuir ou
s'adapter.


    �am�nagements co5teux


  �la premi�re de ces solu-
tions est celle choisie par
des pays industrialis�s. �le
long de leurs `353 km de
c4tes, les �hollandais luttent
depuis des si�cles contre la
mer. �un combat vital puisque
`27��� du territoire se situe
en dessous du niveau ma-    `5
rin! �les dunes, sur `254 km,
forment une barri�re natu-
relle. �sur ces `254 km, `96
km sont maintenus par des �pis
qui retiennent le sable. �des
digues d�fendent `34 km c4-
tiers, et des constructions
diverses `27 km. �enfin, l'en-
semble de ces c4tes et les `38
km de plages basses restantes
sont �galement prot�g�es par
des dunes artificielles. �le
sable est pr�lev� `2 ou `3 km
� l'int�rieur des terres et
remplace celui emport� par les
vagues.
  �les pays riches se sont
�galement lanc�s dans des
constructions plus complexes
et au co5t... tr�s sal�! �en
`1982, une barri�re a �t� �ri-
g�e sur la �tamise pour prot�-
ger �londres de la mont�e des
eaux provenant de la mer et
provoqu�e par les tem-      `6
p2tes. �depuis sa construc-
tion, cette barri�re a �t�
ferm�e plus d'une trentaine de
fois, d�s que l'on pr�voyait
que le niveau de l'eau arrive-
rait � moins de `45 cm des
structures de d�fense du
centre londonien. �mais la
�thames �barrier est une folie
financi�re puisque sa
construction a co5t� `1 mil-
liard d'euros et que son co5t
de maintenance et d'utilisa-
tion est de `6 millions d'eu-
ros par an. �cet investisse-
ment s'av�rera-t-il un jour
judicieux? �oui, si une tem-
p2te similaire � celle de
`1966 qui ravagea le port de
�hambourg se reproduisait.
�pour la ville de �londres,
les d�g1ts occasionn�s par une
telle catastrophe --estim�s �
`29 milliards d'euros-- se-
raient alors �vit�s.        `7
  ��d'abord, �moins �polluer!
  �en `1997, � �kyoto, une
conf�rence internationale a
fix� une r�duction moyenne de
`5,2��� des �missions de gaz �
effet de serre d'ici � `2010.
�cette mesure ne s'applique
qu'aux pays industrialis�s
qui, ��tats-unis en t2te, sont
les principaux responsables
puisqu'ils �mettent `90��� de
ces gaz. �apr�s la conf�rence
de �la �haye, ce mois-ci,
l'�europe et le �japon sui-
vront tr�s certainement leur
engagement. �mais les ��tats-
unis et d'autres nations an-
glo-saxonnes tra3nent les
pieds de peur de ralentir leur
croissance �conomique. �r�sul-
tat: faute d'argent pour se
prot�ger, les pays pauvres
risquent de subir les cons�-
quences de la pollution des
pays riches. �en d'autres   `8
termes, ils pourraient 2tre
les dindons mouill�s d'une
farce --pas vraiment dr4le--
que leurs auront faite les
pays riches.


      �philippe �monges


  �remerciements � �roland
�paskoff, professeur �m�rite
de l'universit� �louis-�lu-
mi�re de �lyon; et �anne de la
�vega-�leinert, de l'universi-
t� du �middlesex.

       �avis de fi�vre
          plan�taire

  �la terre est emmitoufl�e
dans un manteau de gaz et
c'est tr�s bien ainsi. �sans
le gaz carbonique (��co?`2),
la vapeur d'eau, le m�-     `9
thane et quelques autres gaz
naturellement pr�sents dans
l'atmosph�re, le thermom�tre
afficherait en moyenne `-20�o
�c! �en effet, � la mani�re
d'une serre de jardin, ces gaz
laissent passer les rayons so-
laires et pi�gent en retour la
chaleur due aux rayonnements
infrarouges �mis par la �terre
chauff�e. �gr1ce � cet "effet
de serre", il r�gne sur notre
plan�te une moyenne tr�s sup-
portable de `!15�o �c.
  �mais l'homme a rompu l'�-
quilibre naturel. �en br5lant
charbon et p�trole, usines,
voitures et habitations
crachent toujours plus de
��co?`2. ��levages et rizi�res
produisent de fortes quantit�s
de m�thane, gaz produit en mi-
lieux priv�s d'oxyg�ne (intes-
tins d'animaux, mar�cages na-
turels ou artificiels).    `10
�ces gaz, rejet�s chaque ann�e
en plus grande quantit� dans
l'atmosph�re, augmentent l'ef-
fet de serre naturel. �au
point d'engendrer un r�chauf-
fement plan�taire? �tr�s pro-
bablement, r�pondent les
scientifiques, qui restent
prudents. �car si la temp�ra-
ture moyenne a d�j� augment�
de `0,5�o �c au cours du
��xx^�me si�cle, il est diffi-
cile de prouver que cette aug-
mentation est davantage due �
l'impact des activit�s hu-
maines qu'� un cycle clima-
tique naturel. �qu'en serait-
il d'ici � `2100? �si les
�missions de gaz � effet de
serre ne sont pas r�duites, le
mercure pourrait grimper de
`!1�o �c � `!3,5�o �c. �les
cons�quences seraient alors
d�sastreuses: mont�e du niveau
des mers et des oc�ans     `11
mais �galement d�r�glement g�-
n�ral du climat, augmentation
du nombre de temp2tes et d'ou-
ragan, d�sertification accrue
dans certaines r�gions, inon-
dations dans d'autres...


















                           `12

