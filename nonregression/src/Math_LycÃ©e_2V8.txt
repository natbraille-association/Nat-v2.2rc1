Math�matiques niveau lyc�e : s�rie 2

<math>
   <semantics>
      <mi>f</mi>
      
   </semantics>
</math>est la fonction d�finie sur <math>
   <semantics>
      <mrow>
         <mrow><mo>]</mo> <mrow>
            <mn>0</mn><mo>;</mo><mo>+</mo><mi>&infin;</mi>
         </mrow> <mo>[</mo></mrow>
      </mrow>
      
   </semantics>
</math> par<math>
   <semantics>
      <mrow>
         <mi>f</mi><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mfrac>
            <mrow>
               <mo>ln</mo><mrow><mo>(</mo>
                  <mi>x</mi>
               <mo>)</mo></mrow>
               
            </mrow>
            <mrow>
               <msqrt>
                  <mi>x</mi>
               </msqrt>
               
            </mrow>
         </mfrac>
         
      </mrow>
      
   </semantics>
</math>.
<math>
   <semantics>
      <mi>&Cscr;</mi>
      
   </semantics>
</math>est sa courbe repr�sentative dans un rep�re orthonormal.

1. a) �tudier le sens de variation de<math>
   <semantics>
      <mi>f</mi>
      
   </semantics>
</math>.
b) �tudier les limites de <math>
   <semantics>
      <mi>f</mi>
      
   </semantics>
</math> en 0 et en <math>
   <semantics>
      <mrow>
         <mo>+</mo><mi>&infin;</mi>
      </mrow>
      
   </semantics>
</math> ; dresser le tableau de variation de<math>
   <semantics>
      <mi>f</mi>
      
   </semantics>
</math>.
c) Tracer la courbe<math>
   <semantics>
      <mi>&Cscr;</mi>
      
   </semantics>
</math>.
2. On d�signe par <math>
   <semantics>
      <mi>T</mi>
      
   </semantics>
</math> la tangente � la courbe <math>
   <semantics>
      <mi>&Cscr;</mi>
      
   </semantics>
</math> au point d'abscisse 1.
a) D�terminer une �quation de<math>
   <semantics>
      <mi>T</mi>
      
   </semantics>
</math>.
b) On note <math>
   <semantics>
      <mi>g</mi>
      
   </semantics>
</math> la fonction d�finie sur <math>
   <semantics>
      <mrow>
         <mrow><mo>]</mo> <mrow>
            <mn>0</mn><mo>;</mo><mo>+</mo><mi>&infin;</mi>
         </mrow> <mo>[</mo></mrow>
      </mrow>
      
   </semantics>
</math> par : <math>
   <semantics>
      <mrow>
         <mi>g</mi><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mrow><mo>(</mo>
            <mrow>
               <mi>x</mi><mo>-</mo><mn>1</mn>
            </mrow>
         <mo>)</mo></mrow>
         <mo>-</mo><mi>f</mi><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         
      </mrow>
      
   </semantics>
</math>
Calculer <math>
   <semantics>
      <mrow>
         <mi>g</mi><mo>'</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         
      </mrow>
      
   </semantics>
</math> et v�rifier que : <math>
   <semantics>
      <mrow>
         <mi>g</mi><mo>'</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mfrac>
            <mn>1</mn>
            <mrow>
               <mn>2</mn><msup>
                  <mi>x</mi>
                  <mrow>
                     <mfrac>
                        <mn>3</mn>
                        <mn>2</mn>
                     </mfrac>
                     
                  </mrow>
               </msup>
               
            </mrow>
         </mfrac>
         <mrow><mo>[</mo> <mrow>
            <mo>ln</mo><mrow><mo>(</mo>
               <mi>x</mi>
            <mo>)</mo></mrow>
            <mo>+</mo><mn>2</mn><mrow><mo>(</mo>
               <mrow>
                  <msup>
                     <mi>x</mi>
                     <mrow>
                        <mfrac>
                           <mn>3</mn>
                           <mn>2</mn>
                        </mfrac>
                        
                     </mrow>
                  </msup>
                  <mo>-</mo><mn>1</mn>
               </mrow>
            <mo>)</mo></mrow>
            
         </mrow> <mo>]</mo></mrow>
      </mrow>
      
   </semantics>
</math>
Calculer <math>
   <semantics>
      <mrow>
         <mi>g</mi><mo>'</mo><mrow><mo>(</mo>
            <mn>1</mn>
         <mo>)</mo></mrow>
         
      </mrow>
      
   </semantics>
</math> et �tudier le signe de <math>
   <semantics>
      <mrow>
         <mi>g</mi><mo>'</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         
      </mrow>
      
   </semantics>
</math> sur chacun des intervalles <math>
   <semantics>
      <mrow>
         <mrow><mo>]</mo> <mrow>
            <mn>0</mn><mo>;</mo><mn>1</mn>
         </mrow> <mo>[</mo></mrow>
      </mrow>
      
   </semantics>
</math> et<math>
   <semantics>
      <mrow>
         <mrow><mo>]</mo> <mrow>
            <mn>1</mn><mo>;</mo><mo>+</mo><mi>&infin;</mi>
         </mrow> <mo>[</mo></mrow>
      </mrow>
      
   </semantics>
</math>.
c) Calculer <math>
   <semantics>
      <mrow>
         <mi>g</mi><mrow><mo>(</mo>
            <mn>1</mn>
         <mo>)</mo></mrow>
         
      </mrow>
      
   </semantics>
</math>et, � l'aide du sens de variation de<math>
   <semantics>
      <mi>g</mi>
      
   </semantics>
</math>, �tudier le signe de<math>
   <semantics>
      <mrow>
         <mi>g</mi><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         
      </mrow>
      
   </semantics>
</math>.
En d�duire la position de <math>
   <semantics>
      <mi>&Cscr;</mi>
      
   </semantics>
</math> par rapport �<math>
   <semantics>
      <mi>T</mi>
      
   </semantics>
</math>. Tracer<math>
   <semantics>
      <mi>T</mi>
      
   </semantics>
</math>.

SOLUTION

1) a) <math>
   <semantics>
      <mi>f</mi>
      
   </semantics>
</math>est d�rivable sur <math>
   <semantics>
      <mrow>
         <mrow><mo>]</mo> <mrow>
            <mn>0</mn><mo>;</mo><mo>+</mo><mi>&infin;</mi>
         </mrow> <mo>[</mo></mrow>
      </mrow>
      
   </semantics>
</math> et <math>
   <semantics>
      <mrow>
         <mi>f</mi><mo>'</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mfrac>
            <mrow>
               <mfrac>
                  <mn>1</mn>
                  <mi>x</mi>
               </mfrac>
               <mo>&times;</mo><msqrt>
                  <mi>x</mi>
               </msqrt>
               <mo>-</mo><mfrac>
                  <mrow>
                     <mo>ln</mo><mrow><mo>(</mo>
                        <mi>x</mi>
                     <mo>)</mo></mrow>
                     
                  </mrow>
                  <mrow>
                     <mn>2</mn><msqrt>
                        <mi>x</mi>
                     </msqrt>
                     
                  </mrow>
               </mfrac>
               
            </mrow>
            <mi>x</mi>
         </mfrac>
         <mo>=</mo><mfrac>
            <mrow>
               <mn>2</mn><mo>-</mo><mo>ln</mo><mrow><mo>(</mo>
                  <mi>x</mi>
               <mo>)</mo></mrow>
               
            </mrow>
            <mrow>
               <mn>2</mn><mi>x</mi><msqrt>
                  <mi>x</mi>
               </msqrt>
               
            </mrow>
         </mfrac>
         
      </mrow>
      
   </semantics>
</math>
<math>
   <semantics>
      <mrow>
         <mn>2</mn><mo>-</mo><mo>ln</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>&gt;</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math>si <math>
   <semantics>
      <mrow>
         <mi>x</mi><mo>&amp;lt;</mo><msup>
            <mi>e</mi>
            <mn>2</mn>
         </msup>
         
      </mrow>
      
   </semantics>
</math>

b) <math>
   <semantics>
      <mrow>
         <munder>
            <mrow>
               <mo>lim</mo>
            </mrow>
            <mrow>
               <mi>x</mi><mo>&rarr;</mo><mn>0</mn>
            </mrow>
         </munder>
         <mo>ln</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mo>-</mo><mi>&infin;</mi>
      </mrow>
      
   </semantics>
</math>et <math>
   <semantics>
      <mrow>
         <munder>
            <mrow>
               <mo>lim</mo>
            </mrow>
            <mrow>
               <mi>x</mi><mo>&rarr;</mo><mn>0</mn>
            </mrow>
         </munder>
         <msqrt>
            <mi>x</mi>
         </msqrt>
         <mo>=</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math> et <math>
   <semantics>
      <mrow>
         <msqrt>
            <mi>x</mi>
         </msqrt>
         <mo>&gt;</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math> et donc <math>
   <semantics>
      <mrow>
         <munder>
            <mrow>
               <mo>lim</mo>
            </mrow>
            <mrow>
               <mi>x</mi><mo>&rarr;</mo><mn>0</mn>
            </mrow>
         </munder>
         <mi>f</mi><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mo>-</mo><mi>&infin;</mi>
      </mrow>
      
   </semantics>
</math>

On remarque que pour<math>
   <semantics>
      <mrow>
         <mi>x</mi><mo>&gt;</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math>, <math>
   <semantics>
      <mrow>
         <mi>f</mi><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mn>2</mn><mfrac>
            <mrow>
               <mi>l</mi><mi>n</mi><mrow><mo>(</mo>
                  <mrow>
                     <msqrt>
                        <mi>x</mi>
                     </msqrt>
                     
                  </mrow>
               <mo>)</mo></mrow>
               
            </mrow>
            <mrow>
               <msqrt>
                  <mi>x</mi>
               </msqrt>
               
            </mrow>
         </mfrac>
         
      </mrow>
      
   </semantics>
</math> et on pose<math>
   <semantics>
      <mrow>
         <mi>X</mi><mo>=</mo><msqrt>
            <mi>x</mi>
         </msqrt>
         
      </mrow>
      
   </semantics>
</math>.
<math>
   <semantics>
      <mrow>
         <munder>
            <mrow>
               <mo>lim</mo>
            </mrow>
            <mrow>
               <mi>X</mi><mo>&rarr;</mo><mo>+</mo><mi>&infin;</mi>
            </mrow>
         </munder>
         <mi>X</mi><mo>=</mo><mo>+</mo><mi>&infin;</mi>
      </mrow>
      
   </semantics>
</math>et <math>
   <semantics>
      <mrow>
         <munder>
            <mrow>
               <mo>lim</mo>
            </mrow>
            <mrow>
               <mi>X</mi><mo>&rarr;</mo><mo>+</mo><mi>&infin;</mi>
            </mrow>
         </munder>
         <mfrac>
            <mrow>
               <mo>ln</mo><mrow><mo>(</mo>
                  <mi>X</mi>
               <mo>)</mo></mrow>
               
            </mrow>
            <mi>X</mi>
         </mfrac>
         <mo>=</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math>
Donc : <math>
   <semantics>
      <mrow>
         <munder>
            <mrow>
               <mo>lim</mo>
            </mrow>
            <mrow>
               <mi>x</mi><mo>&rarr;</mo><mo>+</mo><mi>&infin;</mi>
            </mrow>
         </munder>
         <mi>f</mi><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math>

Tableau de variation
<math>
   <semantics>
      <mrow>
         <mtable>
            <mtr>
               <mtd>
                  <mi>x</mi>
               </mtd>
               <mtd>
                  <mrow>
                     <mn>0</mn><mtext>&quad;</mtext><mtext>&ThickSpace;</mtext><mtext>&thinsp;</mtext>
                  </mrow>
               </mtd>
               <mtd>
                  <mrow></mrow>
               </mtd>
               <mtd>
                  <mrow>
                     <msup>
                        <mi>e</mi>
                        <mn>2</mn>
                     </msup>
                     
                  </mrow>
               </mtd>
               <mtd>
                  <mrow></mrow>
               </mtd>
               <mtd>
                  <mrow>
                     <mo>+</mo><mi>&infin;</mi>
                  </mrow>
               </mtd>
            </mtr>
            <mtr>
               <mtd>
                  <mrow>
                     <mi>f</mi><mo>'</mo><mrow><mo>(</mo>
                        <mi>x</mi>
                     <mo>)</mo></mrow>
                     
                  </mrow>
               </mtd>
               <mtd>
                  <mrow>
                     <mo>&par;</mo><mtext>&quad;</mtext><mtext>&ThickSpace;</mtext><mtext>&thinsp;</mtext>
                  </mrow>
               </mtd>
               <mtd>
                  <mo>+</mo>
               </mtd>
               <mtd>
                  <mn>0</mn>
               </mtd>
               <mtd>
                  <mo>-</mo>
               </mtd>
               <mtd>
                  <mrow></mrow>
               </mtd>
            </mtr>
            <mtr>
               <mtd>
                  <mrow>
                     <mi>f</mi><mrow><mo>(</mo>
                        <mi>x</mi>
                     <mo>)</mo></mrow>
                     
                  </mrow>
               </mtd>
               <mtd>
                  <mrow>
                     <mo>&par;</mo><mo>-</mo><mi>&infin;</mi>
                  </mrow>
               </mtd>
               <mtd>
                  <mo>&nearr;</mo> 
               </mtd>
               <mtd>
                  <mrow>
                     <mfrac>
                        <mn>2</mn>
                        <mi>e</mi>
                     </mfrac>
                     
                  </mrow>
               </mtd>
               <mtd>
                  <mo>&searr;</mo> 
               </mtd>
               <mtd>
                  <mn>0</mn>
               </mtd>
            </mtr>
            
         </mtable>
         
      </mrow>
      
   </semantics>
</math>

2. a) Une �quation de <math>
   <semantics>
      <mi>T</mi>
      
   </semantics>
</math> est<math>
   <semantics>
      <mrow>
         <mi>y</mi><mo>=</mo><mi>f</mi><mrow><mo>(</mo>
            <mn>1</mn>
         <mo>)</mo></mrow>
         <mo>+</mo><mi>f</mi><mo>'</mo><mrow><mo>(</mo>
            <mn>1</mn>
         <mo>)</mo></mrow>
         <mrow><mo>(</mo>
            <mrow>
               <mi>x</mi><mo>-</mo><mn>1</mn>
            </mrow>
         <mo>)</mo></mrow>
         
      </mrow>
      
   </semantics>
</math>, soit<math>
   <semantics>
      <mrow>
         <mi>y</mi><mo>=</mo><mi>x</mi><mo>-</mo><mn>1</mn>
      </mrow>
      
   </semantics>
</math>.
b) Pour <math>
   <semantics>
      <mrow>
         <mi>x</mi><mo>&gt;</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math> :
<math>
   <semantics>
      <mrow>
         <mi>g</mi><mo>'</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mn>1</mn><mo>-</mo><mi>f</mi><mo>'</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mn>1</mn><mo>-</mo><mfrac>
            <mrow>
               <mn>2</mn><mo>-</mo><mo>ln</mo><mrow><mo>(</mo>
                  <mi>x</mi>
               <mo>)</mo></mrow>
               
            </mrow>
            <mrow>
               <mn>2</mn><mi>x</mi><msqrt>
                  <mi>x</mi>
               </msqrt>
               
            </mrow>
         </mfrac>
         <mo>=</mo><mfrac>
            <mn>1</mn>
            <mrow>
               <mn>2</mn><mi>x</mi><msqrt>
                  <mi>x</mi>
               </msqrt>
               
            </mrow>
         </mfrac>
         <mrow><mo>[</mo> <mrow>
            <mn>2</mn><mi>x</mi><msqrt>
               <mi>x</mi>
            </msqrt>
            <mo>-</mo><mn>2</mn><mo>+</mo><mo>ln</mo><mrow><mo>(</mo>
               <mi>x</mi>
            <mo>)</mo></mrow>
            
         </mrow> <mo>]</mo></mrow>
      </mrow>
      
   </semantics>
</math>
<math>
   <semantics>
      <mrow>
         <mi>g</mi><mo>'</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>=</mo><mfrac>
            <mn>1</mn>
            <mrow>
               <mn>2</mn><msup>
                  <mi>x</mi>
                  <mrow>
                     <mfrac>
                        <mn>3</mn>
                        <mn>2</mn>
                     </mfrac>
                     
                  </mrow>
               </msup>
               
            </mrow>
         </mfrac>
         <mrow><mo>[</mo> <mrow>
            <mn>2</mn><mrow><mo>(</mo>
               <mrow>
                  <msup>
                     <mi>x</mi>
                     <mrow>
                        <mfrac>
                           <mn>3</mn>
                           <mn>2</mn>
                        </mfrac>
                        
                     </mrow>
                  </msup>
                  <mo>-</mo><mn>1</mn>
               </mrow>
            <mo>)</mo></mrow>
            <mo>+</mo><mo>ln</mo><mrow><mo>(</mo>
               <mi>x</mi>
            <mo>)</mo></mrow>
            
         </mrow> <mo>]</mo></mrow>
      </mrow>
      
   </semantics>
</math>
<math>
   <semantics>
      <mrow>
         <mi>g</mi><mo>'</mo><mrow><mo>(</mo>
            <mn>1</mn>
         <mo>)</mo></mrow>
         <mo>=</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math>
Pour<math>
   <semantics>
      <mrow>
         <mi>x</mi><mo>&gt;</mo><mn>1</mn>
      </mrow>
      
   </semantics>
</math>, <math>
   <semantics>
      <mrow>
         <msup>
            <mi>x</mi>
            <mrow>
               <mfrac>
                  <mn>3</mn>
                  <mn>2</mn>
               </mfrac>
               
            </mrow>
         </msup>
         <mo>&gt;</mo><mn>1</mn>
      </mrow>
      
   </semantics>
</math> et<math>
   <semantics>
      <mrow>
         <mo>ln</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>&gt;</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math>, donc<math>
   <semantics>
      <mrow>
         <mi>g</mi><mo>'</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>&gt;</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math>.
Pour<math>
   <semantics>
      <mrow>
         <mn>0</mn><mo>&amp;lt;</mo><mi>x</mi><mo>&amp;lt;</mo><mn>1</mn>
      </mrow>
      
   </semantics>
</math>, <math>
   <semantics>
      <mrow>
         <msup>
            <mi>x</mi>
            <mrow>
               <mfrac>
                  <mn>3</mn>
                  <mn>2</mn>
               </mfrac>
               
            </mrow>
         </msup>
         <mo>&amp;lt;</mo><mn>1</mn>
      </mrow>
      
   </semantics>
</math> et <math>
   <semantics>
      <mrow>
         <mo>ln</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>&amp;lt;</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math> donc<math>
   <semantics>
      <mrow>
         <mi>g</mi><mo>'</mo><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>&amp;lt;</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math>.

c) <math>
   <semantics>
      <mrow>
         <mi>g</mi><mrow><mo>(</mo>
            <mn>1</mn>
         <mo>)</mo></mrow>
         <mo>=</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math>
<math>
   <semantics>
      <mi>g</mi>
      
   </semantics>
</math>est d�croissante sur <math>
   <semantics>
      <mrow>
         <mrow><mo>]</mo> <mrow>
            <mn>0</mn><mo>;</mo><mn>1</mn>
         </mrow> <mo>]</mo></mrow>
      </mrow>
      
   </semantics>
</math> et croissante sur<math>
   <semantics>
      <mrow>
         <mrow> <mo>[</mo> <mrow>
            <mn>1</mn><mo>;</mo><mo>+</mo><mi>&infin;</mi>
         </mrow> <mo>[</mo> </mrow>
      </mrow>
      
   </semantics>
</math>, donc pour tout r�el <math>
   <semantics>
      <mi>x</mi>
      
   </semantics>
</math> de<math>
   <semantics>
      <mrow>
         <mrow><mo>]</mo> <mrow>
            <mn>0</mn><mo>;</mo><mo>+</mo><mi>&infin;</mi>
         </mrow> <mo>[</mo></mrow>
      </mrow>
      
   </semantics>
</math>,<math>
   <semantics>
      <mrow>
         <mi>g</mi><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         <mo>&ge;</mo><mn>0</mn>
      </mrow>
      
   </semantics>
</math>.
Alors, pour tout r�el <math>
   <semantics>
      <mi>x</mi>
      
   </semantics>
</math> de<math>
   <semantics>
      <mrow>
         <mrow><mo>]</mo> <mrow>
            <mn>0</mn><mo>;</mo><mo>+</mo><mi>&infin;</mi>
         </mrow> <mo>[</mo></mrow>
      </mrow>
      
   </semantics>
</math>, <math>
   <semantics>
      <mrow>
         <mi>x</mi><mo>-</mo><mn>1</mn><mo>&ge;</mo><mi>f</mi><mrow><mo>(</mo>
            <mi>x</mi>
         <mo>)</mo></mrow>
         
      </mrow>
      
   </semantics>
</math>, donc la tangente <math>
   <semantics>
      <mi>T</mi>
      
   </semantics>
</math> est au-dessus de la courbe <math>
   <semantics>
      <mi>&Cscr;</mi>
      
   </semantics>
</math> sur<math>
   <semantics>
      <mrow>
         <mrow><mo>]</mo> <mrow>
            <mn>0</mn><mo>;</mo><mo>+</mo><mi>&infin;</mi>
         </mrow> <mo>[</mo></mrow>
      </mrow>
      
   </semantics>
</math>.





