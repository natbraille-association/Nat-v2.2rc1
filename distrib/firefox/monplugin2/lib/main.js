/*
 * local natbraille plugin for firefox
 *   Copyright (C) 2012 Vivien Guillet, Bruno Mascret
 *
 * This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var pageMod = require("page-mod");
const data = require("self").data;
var tabs = require("tabs");
var simpleprefs = require("simple-prefs");

/*
 * these values can be configured by setting mozilla prefs : resp
 *  extensions.natbraille.natwrapperpath
 *  extensions.natbraille.configurationpath
 */

var natWrapperPathOptionName = "extensions.natbraille.natwrapperpath";
var natConfigurationOptionsName = "extensions.natbraille.configurationpath";

/*
 * generic function binding
 */

// bind method to object for callbacks
function bind(toObject, methodName){
    return function(param){toObject[methodName](param)}
}

/* 
 * preferences
 */

function getNatWrapperPath(){
    return simpleprefs.prefs[natWrapperPathOptionName];
}
function getNatConfigurationPath(){
    return simpleprefs.prefs[natConfigurationOptionsName];

}

//console.log("NAT"+"["+getNatWrapperPath()+"]");
//console.log("NAT"+"["+getNatConfigurationPath()+"]");

/*
 * writes @data to a temporary file
 * calls @onSucess  with the path to the temporary file
 * calls @onFailure without argument
 */
const {Cc,Ci,Cr,components} = require("chrome");

Components.utils.import("resource://gre/modules/NetUtil.jsm");  
Components.utils.import("resource://gre/modules/FileUtils.jsm");  


function writeTmpFile(data,onSuccess,onFailure){
    var file = Components.classes["@mozilla.org/file/directory_service;1"].  
        getService(Components.interfaces.nsIProperties).  
        get("TmpD", Components.interfaces.nsIFile);  
    file.append("suggestedName.web");  
    file.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0666);  

    var ostream = FileUtils.openSafeFileOutputStream(file)  
    var converter = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"].  
        createInstance(Components.interfaces.nsIScriptableUnicodeConverter);  
    converter.charset = "UTF-8";  
    var istream = converter.convertToInputStream(data);  
    
    NetUtil.asyncCopy(istream, ostream, function(status) {  
	if (!Components.isSuccessCode(status)) {  
	    onFailure();
	    return;
	}  
	onSuccess(file.path);
    });  
}

/*
 * runs local @programme with @args 
 * calls @onSucess  with process end details
 * calls @onFailure with process end details
 */
function lanceex(programme,args,onSuccess,onFailure) {
    var file = Components.classes["@mozilla.org/file/local;1"]  
        .createInstance(Components.interfaces.nsILocalFile);  
    file.initWithPath(programme);  


    var process = Components.classes["@mozilla.org/process/util;1"]  
        .createInstance(Components.interfaces.nsIProcess);  
    process.init(file);     

    // attends !
    process.runAsync(args, args.length, {
	observe : function(subject,topic,data) {
	    if (topic == "process-finished") {
		onSuccess("detaille du reussissage");
	    }  else if (topic == "process-failed"){
		onFailure("detaille du ratage");
	    }
	}
    });
}

function callnat(srcFileName,destFileName,onSuccess,onFailure){
    
    var bin = getNatWrapperPath();
    var config = getNatConfigurationPath();
    
    var args = [
	"--from "+srcFileName, 
	"--to "+destFileName,
	"--config "+config,
	"-m "
    ];

    console.log("callnat::call programm: "+bin);
    console.log("callnat::call programm with: "+args);
    lanceex(bin,args,onSuccess,onFailure);
}

/*
 * reads file @filename 
 * calls @onSucess  with the filename data
 * calls @onFailure without argument
 */
function readfile(filename,onSuccess,onFailure){
    var file = Components.classes["@mozilla.org/file/local;1"].
        createInstance(Components.interfaces.nsILocalFile);
    file.initWithPath(filename);
    var channel = NetUtil.newChannel(file);
    
    channel.contentType = "text/html";

    NetUtil.asyncFetch(channel, function(inputStream, status) {
	if (!Components.isSuccessCode(status)) {  
	    onFailure();
	    return;  
	}  

	var charset = "UTF-8";  
	const replacementChar = Components.interfaces.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER;  
	var is = Components.classes["@mozilla.org/intl/converter-input-stream;1"]  
            .createInstance(Components.interfaces.nsIConverterInputStream);  
	is.init(inputStream, charset, 1024, replacementChar);  

	var data = "";
	var str = {};  

	while (is.readString(4096, str) != 0) {  
	    data = data + str.value;
	}
	is.close();
	onSuccess(data);
    });
}

/*
 * NatWorker
 */
function NatWorker(worker,htmlData){
    this.worker = worker;
    this.htmlData = htmlData;
    
    // read dest
    this.onDestFileReadSuccess = function(data){
	console.log(this+":"+"CB::onDestFileReadSuccess");
	console.log(this+":"+"CB::     post message to the main window");
	this.worker.postMessage(data);
	
    };
    this.onDestFileReadFailure = function(){
	console.log(this+":"+"CB::onDestFileReadFailure");
	this.worker.postMessage("onDestFileReadFailure");
    };

    // call nat
    this.onNatTransSuccess = function(details){
	console.log(this+":"+"CB::onNatTransSuccess");
	console.log(this+":"+"         details:"+details);
	readfile(this.destFileName,bind(this,"onDestFileReadSuccess"),bind(this,"onDestFileReadFailure"));
    };
    this.onNatTransFailure = function(details){
	console.log(this+":"+"CB::onNatTransFailure");
	this.worker.postMessage("onNatTransFailure");
    };

    // write source
    this.onSrcFileWriteSuccess = function(fileName){
	console.log(this+":"+"CB::onSrcFileWriteSuccess");
	this.srcFileName = fileName;
	this.destFileName = fileName+".nat";
	console.log(this+":"+"     srcfilename:"+this.srcFileName);
	console.log(this+":"+"    destfilename:"+this.destFileName);
	callnat(this.srcFileName,this.destFileName,bind(this,"onNatTransSuccess"),bind(this,"onNatTransFailure"));
    };
    this.onSrcFileWriteFailure = function(){
	console.log(this+":"+"CB::onSrcFileWriteFailure");
	this.worker.postMessage("onSrcFileWriteFailure");
    };

    this.run = function (){
	console.log(this+":"+"::run");
	writeTmpFile(this.htmlData,bind(this,"onSrcFileWriteSuccess"),bind(this,"onSrcFileWriteFailure"));
    };
}

/*
 * PageMod Hook
 */

var theMessage = "<div style=\"position:absolute;"
    + "left : 0px; "
    + "right : 0px; "
    + "top : 0px; "
    + "bottom : 0px; "
    + "border : 0px; "
    + "padding : 0px;" 
    + "margin : 0px; "
    + "opacity : 0.9;" 
    + "background-color : black;    \"> "
    + "<p style=\""
    + "height:100%;	"
    + "border : solid black 10px;"
    + "font-family : sans-serif;"
    + "font-size : 20px;"
    + "padding : 0px;"
    + "margin : 0px;"
    + "line-height:500px;"
    + "text-align:center;"
    + "color : white;\"> "
    + "    Running Nat <blink>.</blink>"
    + "    </p>"
    + "    </div>";


pageMod.PageMod({
    include: ["*"],
    contentScriptWhen: 'end',
    contentScript: ["postMessage(document.body.innerHTML);",
		    "onMessage = function onMessage(message) {" +
		    "      document.body.innerHTML = message;"+
		    "};",
		   ],
    onAttach: function onAttach(worker) {
	console.log("Attaching content scripts")
	worker.on('message', function(htmlData) {	    
	    worker.postMessage(theMessage+htmlData);
	    worker.postMessage(theMessage);
	    var natWorker = new NatWorker(worker,htmlData);
	    natWorker.run();
	});
    }
});

//tabs.open("http://handy.univ-lyon1.fr");

