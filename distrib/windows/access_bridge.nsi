!include "x64.nsh"
!include "FileFunc.nsh"
!include "StrFunc.nsh"

Name "Java Access Bridge Control Center"
OutFile "jab_control.exe"
RequestExecutionLevel user

# uncomment the following line to make the installer silent by default.
SilentInstall silent ;comment this pour voir les messagebox


;variables d'acces � java
  !define JAVA_RUNTIME "Software\JavaSoft\Java Runtime Environment"
  !define REG_KEY_VERSION "CurrentVersion"
  !define JAVA_HOME "JavaHome"
  
  ${StrStr} ; mandatory first call of function
  
 Function .onInit
;${If} ${RunningX64}
;MessageBox MB_OK "running on x64"
;${Else}
;MessageBox MB_OK "running on x32"
;${EndIf} 
${GetParameters} $0
${StrStr} $1 $0 "/enable" ; pour enabler le jab en mode compl�tement silencieux
;StrCmp $1 "" 0 +3 false means $1 not empty = means /enable found in command line options
FunctionEnd

Section
  StrCmp $1 "" 0 yes ;enable without asking
  MessageBox MB_YESNOCANCEL "Cliquez sur $\"Oui$\" pour activer le Java Access Bridge.$\nCliquez sur $\"Non$\" pour d�sactiver le Java Access Bridge.$\n Cliquez sur $\"Annuler$\" pour annuler." IDNO no IDYES yes
	StrCpy $R2 "cancel"
	goto Fini
	no:
	  StrCpy $R2 "disable"
	  Goto done
	yes:
	  StrCpy $R2 "enable"
	  Goto done
  done:
  
  ${If} ${RunningX64}
  SetRegView 64 ;detection of 64bit jvms
  ReadRegStr $R7 HKLM "${JAVA_RUNTIME}" "${REG_KEY_VERSION}"
  ReadRegStr $R7 HKLM "${JAVA_RUNTIME}\$R7" "${JAVA_HOME}"
  DetailPrint "JRE64 d�tect�e ici : $R7"
  ${EndIf}
  SetRegView 32 ;detection of 32bit jvms
  ReadRegStr $R8 HKLM "${JAVA_RUNTIME}" "${REG_KEY_VERSION}"
  ReadRegStr $R8 HKLM "${JAVA_RUNTIME}\$R8" "${JAVA_HOME}"
  DetailPrint "JRE32 d�tect�e ici : $R8"
  
  ;filename will be enable_accessbridge.bat or disable_accessbridge.bat
  FileOpen $5 "$TEMP\$R2_accessbridge.bat" w
  StrCmp $R7 "" no64 ;do we have a 64bit jvm ?
  FileWrite $5 "@echo Activation du Java Accessbridge 64 bits...$\r$\n"
  FileWrite $5 "$\"$R7\bin\jabswitch.exe$\" /$R2 $\r$\n" ; we write a new line
  no64:
  StrCmp $R8 "" no32 ;do we have a 32bit jvm ?
  FileWrite $5 "@echo Activation du Java Accessbridge 32 bits...$\r$\n"
  FileWrite $5 "$\"$R8\bin\jabswitch.exe$\" /$R2 $\r$\n" ; we write a new line
  no32:
  StrCmp $1 "" 0 +2 ;no pause if /enable in command line
  FileWrite $5 "@pause$\r$\n"
  FileClose $5
  ExecWait "$TEMP\$R2_accessbridge.bat"
  Fini:
SectionEnd  