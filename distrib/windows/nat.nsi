;NSIS Modern User Interface
;NAT braille installation script
;Written by Frederic Schwebel, Vivien Guillet and Bruno Mascret
;from Basic Example Script
;Written by Joost Verburg


;--------------------------------
;Include Modern UI

!include "MUI2.nsh"
!include "FontReg.nsh"
!include "FontName.nsh"
!include "WordFunc.nsh"

;--------------------------------
;General

  ;Details montres
  ShowInstDetails show
  
  ;Name and file
  Name "NatBraille 2.1rc2"
  OutFile "Nat-installer-v2-1rc2.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES\Nat"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\Nat" ""

  ;Request application privileges for Windows Vista
  RequestExecutionLevel admin

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "..\..\licence.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "French"


;--------------------------------
; .onInit searches for JRE and quits if none is discovered and user decides to quit
Function .onInit
  ;jre detection
  !define JAVA_RUNTIME "Software\JavaSoft\Java Runtime Environment"
  !define REG_KEY_VERSION "CurrentVersion"
  !define JAVA_HOME "JavaHome"
	
	SetRegView 64 ;detection of 64bit jvms
	ReadRegStr $R7 HKLM "${JAVA_RUNTIME}" "${REG_KEY_VERSION}"
  ReadRegStr $R7 HKLM "${JAVA_RUNTIME}\$R7" "${JAVA_HOME}"
  DetailPrint "JRE64 d�tect�e ici : $R7"
	SetRegView 32 ;detection of 32bit jvms
  ReadRegStr $R8 HKLM "${JAVA_RUNTIME}" "${REG_KEY_VERSION}"
  ReadRegStr $R8 HKLM "${JAVA_RUNTIME}\$R8" "${JAVA_HOME}"
  DetailPrint "JRE32 d�tect�e ici : $R8"
  StrCmp $R7 "" 0 +4
  StrCmp $R8 "" 0 +3
	MessageBox MB_YESNO|MB_ICONEXCLAMATION "Aucune machine java 32 ou 64 bits n'a �t� d�tect�e sur votre ordinateur.$\n\
				Natbraille ne fonctionnera pas sans Java. Vous pourrez installer Java apr�s mais $\n\
				il est vivement recommand� de le faire avant, surtout si vous �tes non-voyant.$\n\
				Voulez-vous malgr� tout installer NatBraille ?" IDYES +2
  Abort  
FunctionEnd
;--------------------------------
;Installer Sections

Section "Nat" SecMain


	;--------------------------------
	;Question pour la suite de l'install
	MessageBox MB_YESNO "Des non-voyants vont-ils utiliser cette installation de NAT ?" IDYES true1 IDNO false1
	true1:
	  StrCpy $R2 "aveugle"
	  StrCmp $R7 "" 0 +4
		StrCmp $R8 "" 0 +3
	  MessageBox MB_OK|MB_ICONEXCLAMATION "Comme Java n'est pas pr�sent sur votre ordinateur, le Java Access Bridge$\n\
				ne peut pas �tre activ� � l'installation. Vous devrez$\nle faire apr�s avoir install� Java en allant dans$\n\
				le menu D�marrer / NAT / Contr�le du Java Access Bridge."
	  Goto next1
	  ;java access bridge activation if jre has been detected
	  FileOpen $5 "$TEMP\enable_accessbridge.bat" w
	  FileWrite $5 "@echo Activation du Java Accessbridge...$\r$\n"
	  FileWrite $5 "$\"$R7\bin\jabswitch.exe$\" /enable $\r$\n" ; we write a new line
		FileWrite $5 "$\"$R8\bin\jabswitch.exe$\" /enable $\r$\n" 
	  ;FileWrite $5 "@pause$\r$\n"
	  FileClose $5
	  ExecWait "$TEMP\enable_accessbridge.bat"
	  Goto next1
	false1:
	  StrCpy $R2 "voyant"
	next1:

  SetOutPath "$INSTDIR\aide"
  File /r /x *.svn "..\..\aide\*.*" 
    
  SetOutPath "$INSTDIR\configurations"	
  File "..\..\configurations\*.cfg"
  File /r /x *.svn "..\..\configurations\abrege"
  File /r /x *.svn "..\..\configurations\kommer"
  
  SetOutPath "$INSTDIR"
  ; plus de .class depuis le .jar File /r /x *.svn "..\..\*.class"
  File /r /x *.svn "..\..\*.jar"
  File /r /x *.svn "..\..\*.png"
  File /r /x *.svn "..\..\*.au"
  File /r /x *.svn "..\..\*.ttf"
  File /r /x *.svn "..\..\*.txt"
  File /r /x *.svn "..\..\*.ico"
  
  SetOutPath "$INSTDIR\writer2latex"
  File /r "..\..\writer2latex\*.xml"
  
  SetOutPath "$INSTDIR\xsl"
  File /r /x *.svn "..\..\xsl\*.*" 

  ;s�curit� : fichiers jaws et polices copi�es dans tmp pour les r�cup�rer si jamais mal install�es
  SetOutPath "$INSTDIR\tmp"   
  File "..\..\licence.txt"
  File "*.jbt"
  File "*.jcf"
  File "..\fonts\*.ttf"
  
  SetOutPath "$INSTDIR\documents"
  File /x *.svn "..\..\documents\*.*"

  SetOutPath "$INSTDIR\text"
  File /r /x *.svn "..\..\text\*.properties"
  
  SetOutPath "$INSTDIR\gnu\getopt"
  File /r /x *.svn "..\..\gnu\getopt\*.properties"

  
  SetOutPath "$INSTDIR"   
  File "..\..\nat-launch-windows.bat"
  File "..\..\licence.txt"
  ;File "..\..\nat.exe"
  ;bien avoir compil� le nsis "access_bridge.nsi" pour avoir l'exe suivant
  File "jab_control.exe" 
  File "..\..\scenarii.cfg.initial"
  StrCmp $R2 "aveugle" gui_aveugle gui_voyant
  gui_aveugle:
	File "/oname=nat-gui.conf-initial" "guidefaultconfigs\nat-gui.conf-initial-aveugle"
	;MessageBox MB_OK "N'oubliez pas d'installer le Java Access Bridge 2.0.1 pour utiliser NAT.$\nCe logiciel est t�l�chargeable depuis le site tr�s accessible$\nhttp://sonobraille.free.fr section t�l�chargement."
	Goto fin_gui
  gui_voyant:
    File "/oname=nat-gui.conf-initial" "guidefaultconfigs\nat-gui.conf-initial-voyant"
  fin_gui:
  
  ;Store installation folder
  WriteRegStr HKCU "Software\Nat" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  
  CreateDirectory "$SMPROGRAMS\NAT"
  SetOutPath "$INSTDIR" ; inutile, c'est d�j� le cas mais c'est pour �tre s�r que le r�pertoire de travail du raccourci sera le bon
  CreateShortCut "$SMPROGRAMS\NAT\NAT pour Windows.lnk" "%windir%\system32\javaw.exe" \
	"-Xss20000k -Xmx256m -splash:ui/nat_splash.png -jar natbraille.jar -g" "$INSTDIR\iconeNat.ico" 0 SW_SHOWNORMAL
  CreateShortCut "$SMPROGRAMS\NAT\NAT avec console.lnk" "$INSTDIR\nat-launch-windows.bat"
  CreateShortCut "$SMPROGRAMS\NAT\Contr�le du Java Access Bridge.lnk" "$INSTDIR\jab_control.exe"
  CreateShortCut "$SMPROGRAMS\NAT\Supprimer NAT.lnk" "$INSTDIR\Uninstall.exe"
  
SectionEnd

Section "Polices braille" SecPolice
  StrCpy $FONT_DIR $FONTS
 
  !insertmacro RemoveTTFFont 'BrailleTBFr2007INSHEAb_thermo.ttf'
  !insertmacro RemoveTTFFont 'braille_tbfr2007_b.ttf'
  !insertmacro RemoveTTFFont 'braille_tbfr2007_b-2.ttf'
  !insertmacro RemoveTTFFont 'LouisLouis.ttf'
  !insertmacro RemoveTTFFont 'LouisLouisThermo.ttf'
  !insertmacro InstallTTFFont '..\fonts\BrailleTBFr2007INSHEAb_thermo.ttf'
  !insertmacro InstallTTFFont '..\fonts\braille_tbfr2007_b-2.ttf'
  !insertmacro InstallTTFFont '..\fonts\LouisLouis.ttf'
  !insertmacro InstallTTFFont '..\fonts\LouisLouisThermo.ttf'
  !insertmacro InstallTTFFont '..\fonts\DejaVuSans.ttf'
  
  SendMessage ${HWND_BROADCAST} ${WM_FONTCHANGE} 0 0 /TIMEOUT=5000
SectionEnd

Function InstallezyJaws
;MessageBox MB_OK "installation dans $R6"
SetOutPath "$R6"
File "BFU_U_utf8.jbt"
FunctionEnd

!define JAWS7_REG_DIR_CONFIG "Software\Freedom Scientific\JAWS\7.0"
!define JAWS8_REG_DIR_CONFIG "Software\Freedom Scientific\JAWS\8.0"
!define JAWS9_REG_DIR_CONFIG "Software\Freedom Scientific\JAWS\9.0"
!define JAWS10_REG_DIR_CONFIG "Software\Freedom Scientific\JAWS\10.0"
!define JAWS11_REG_DIR_CONFIG "Software\Freedom Scientific\JAWS\11.0"
!define JAWS12_REG_DIR_CONFIG "Software\Freedom Scientific\JAWS\12.0"
!define JAWS13_REG_DIR_CONFIG "Software\Freedom Scientific\JAWS\13.0"
!define JAWS14_REG_DIR_CONFIG "Software\Freedom Scientific\JAWS\14.0"
!define REG_KEY_JAWSDIR "Target"
var yajaws

Function JawsDetection
;same function used to detect 32 and 64bits versions of jaws
  ReadRegStr $R6 HKLM "${JAWS7_REG_DIR_CONFIG}" "${REG_KEY_JAWSDIR}"
  StrCmp $R6 "" jaws8
  DetailPrint "Jaws 7 d�tect� sur votre syst�me."
  StrCpy $yajaws "oui"
  Call InstallezyJaws

  jaws8:
  ReadRegStr $R6 HKLM "${JAWS8_REG_DIR_CONFIG}" "${REG_KEY_JAWSDIR}"
  StrCmp $R6 "" jaws9
  DetailPrint "Jaws 8 d�tect� sur votre syst�me."
  StrCpy $yajaws "oui"
  Call InstallezyJaws
  
  jaws9:
  ReadRegStr $R6 HKLM "${JAWS9_REG_DIR_CONFIG}" "${REG_KEY_JAWSDIR}"
  StrCmp $R6 "" jaws10
  DetailPrint "Jaws 9 d�tect� sur votre syst�me."
  StrCpy $yajaws "oui"
  Call InstallezyJaws
  
  jaws10:
  ReadRegStr $R6 HKLM "${JAWS10_REG_DIR_CONFIG}" "${REG_KEY_JAWSDIR}"
  StrCmp $R6 "" jaws11
  DetailPrint "Jaws 10 d�tect� sur votre syst�me."
  StrCpy $yajaws "oui"
  Call InstallezyJaws
  
  jaws11:
  ReadRegStr $R6 HKLM "${JAWS11_REG_DIR_CONFIG}" "${REG_KEY_JAWSDIR}"
  StrCmp $R6 "" jaws12
  DetailPrint "Jaws 11 d�tect� sur votre syst�me."
  StrCpy $yajaws "oui"
  Call InstallezyJaws
  
  jaws12:
  ReadRegStr $R6 HKLM "${JAWS12_REG_DIR_CONFIG}" "${REG_KEY_JAWSDIR}"
  StrCmp $R6 "" jaws13
  DetailPrint "Jaws 12 d�tect� sur votre syst�me."
  StrCpy $yajaws "oui"
  Call InstallezyJaws
  
  jaws13:
  ReadRegStr $R6 HKLM "${JAWS13_REG_DIR_CONFIG}" "${REG_KEY_JAWSDIR}"
  StrCmp $R6 "" jaws14
  DetailPrint "Jaws 13 d�tect� sur votre syst�me."
  StrCpy $yajaws "oui"
  Call InstallezyJaws
	
	jaws14:
  ReadRegStr $R6 HKLM "${JAWS14_REG_DIR_CONFIG}" "${REG_KEY_JAWSDIR}"
  StrCmp $R6 "" finjaws
  DetailPrint "Jaws 14 d�tect� sur votre syst�me."
  StrCpy $yajaws "oui"
  Call InstallezyJaws
	
  finjaws:
FunctionEnd

Section "Table braille fran�aise UTF8 pour Jaws" SecJaws
  
  StrCpy $yajaws "non"
  SetRegView 64
  Call JawsDetection
  SetRegView 32
	Call JawsDetection
  StrCmp $yajaws "non" nojaws
  ;si $r6="" aucun jaws
  MessageBox MB_OK "Table braille install�e.$\nPour avoir acc�s au braille interne de NAT (choix � faire lors d'ambigu�t�s, �dition du document transcrit...),$\nil faut installer cette table : allez dans Jaws, barre de menu Options, Braille..., zone de liste d�roulante table braille$\net choisir BFU_U_utf8. Si vous utilisez BFU_U (table fran�aise standard), $\nvous n'aurez aucun probl�me avec BFU_U_utf8 qui peut devenir votre table par d�faut."
  nojaws:
SectionEnd

Section "Configurations pour DBT" SecDBT
  ;SetOutPath "$INSTDIR\configurations"	
  ;File "/oname=windows-dbt107.cfg" "..\..\configurations\windows-dbt107.cfg.initial"
  
  ReadRegStr $R1 HKLM "Software\Duxbury Systems, Inc." "LatestDBT"
  StrCmp $R1 "" nondetecte32 detecte
  nondetecte32:
	ReadRegStr $R1 HKLM "Software\Wow6432Node\Duxbury Systems, Inc." "LatestDBT"
  StrCmp $R1 "" nondetecte64 detecte
	nondetecte64:
  MessageBox MB_OK "DBT 10.7 ou sup�rieur non d�tect� sur votre syst�me.$\nLa configuration sera install�e mais vous devrez pr�ciser o� est DBT dans les options d'interface."
  StrCpy $R1 "Entrez ici le chemin d'acc�s � DBT 10.7 ou sup�rieur."
  Goto finDBT
  detecte:
  DetailPrint "DBT d�tect� ici : $R1"
	${WordReplace} $R1 "\" "\\" "+*" $R3 ; il faut doubler les slash
	DetailPrint $R3
  finDBT:
  ;mise � jour des confs dbt avec le chemin de dbt
  FileOpen $4 "$INSTDIR\configurations\8-windows-dbt107.cfg" a
  FileSeek $4 0 END
  FileWrite $4 "$\r$\n" ; we write a new line
  FileWrite $4 "ui-editor-external="
  FileWrite $4 "$R3"
  FileWrite $4 "$\r$\n" ; we write an extra line
  FileClose $4 ; and close the file
	
	FileOpen $3 "$INSTDIR\configurations\11-mep-dbt107.cfg" a
  FileSeek $3 0 END
  FileWrite $3 "$\r$\n" ; we write a new line
  FileWrite $3 "ui-editor-external="
  FileWrite $3 "$R3"
  FileWrite $3 "$\r$\n" ; we write an extra line
  FileClose $3 ; and close the file
SectionEnd

Section /o "-Int�gration � l'explorateur" SecShell ; section cach�e
  WriteRegStr HKCR "*\shell\Transcrire avec NAT\command" "" "$\"$INSTDIR\nat-launch-win-gf.bat$\"  $\"%1$\""
  FileOpen $4 "$INSTDIR\nat-launch-win-gf.bat"  w
  FileWrite $4 "@echo OFF$\r$\n"
  FileWrite $4 "set courant=%CD%$\r$\n"
  FileWrite $4 "cd $\"$INSTDIR$\"$\r$\n"
  StrCpy $R5 $INSTDIR 2
  FileWrite $4 "$R5$\r$\n"
  FileWrite $4 "set JAVAOPTIONS=-Xss20000k -Xmx256m -classpath .;lib/jeuclid-core-3.1.9.jar;lib/jeuclid-fop-3.1.9.jar;lib/xercesImpl-2.9.1.jar;lib/commons-io-1.4.jar;lib/jodconverter-2.2.2.jar;lib/jurt-3.0.1.jar;lib/slf4j-api-1.5.6.jar;lib/unoil-3.0.1.jar;lib/juh-3.0.1.jar;lib/ridl-3.0.1.jar;lib/slf4j-jdk14-1.5.6.jar;lib/xstream-1.3.1.jar;lib/jhall.jar;lib/saxon9.jar;lib/saxon9-dom.jar;lib/saxon9he.jar"
  FileWrite $4 "$\r$\n"
  FileWrite  $4 "set FROM=%1$\r$\n"
  FileWrite  $4 "java %JAVAOPTIONS% nat/Nat -g -f %FROM%$\r$\n"
  FileWrite $4 "cd $\"%CD%$\"$\r$\n"
  FileClose $4
SectionEnd

!define MT4_REG_DIR_CONFIG "Software\Design Science\DSMT4\Config"
!define MT5_REG_DIR_CONFIG "Software\Design Science\DSMT5\Config"
!define MT6_REG_DIR_CONFIG "Software\Design Science\DSMT6\Config"
!define REG_KEY_TRANSLATORDIR "TranslatorDir"

!define MT4_REG_DIR_DIRECTORIES "Software\Design Science\DSMT4\Directories"
!define MT5_REG_DIR_DIRECTORIES "Software\Design Science\DSMT5\Directories"
!define MT6_REG_DIR_DIRECTORIES "Software\Design Science\DSMT6\Directories"
!define REG_KEY_PROGDIR "ProgDir"

Function Installezy
;MessageBox MB_OK "installation dans $R0"
SetOutPath "$R0"
File "mathtype\MathML (BraMaNet2).tdl"
FunctionEnd

Section "Convertisseur pour MathType" SecMathType
; mathtype 4

ReadRegStr $R0 HKLM "${MT4_REG_DIR_CONFIG}" "${REG_KEY_TRANSLATORDIR}"
StrCmp $R0 "" mt4_bis
DetailPrint "MathType 4 d�tect� sur votre syst�me."
Call Installezy
Goto mt5

mt4_bis:
ReadRegStr $R0 HKLM "${MT4_REG_DIR_DIRECTORIES}" "${REG_KEY_PROGDIR}"
StrCmp $R0 "" mt5
StrCpy $R0 "$R0\Translators"
DetailPrint "MathType 4 d�tect� sur votre syst�me."
Call Installezy

; mathtype 5
mt5:
ReadRegStr $R0 HKLM "${MT5_REG_DIR_CONFIG}" "${REG_KEY_TRANSLATORDIR}"
StrCmp $R0 "" mt5_bis
DetailPrint "MathType 5 d�tect� sur votre syst�me."
Call Installezy
Goto mt6

mt5_bis:
ReadRegStr $R0 HKLM "${MT5_REG_DIR_DIRECTORIES}" "${REG_KEY_PROGDIR}"
StrCmp $R0 "" mt6
DetailPrint "MathType 5 d�tect� sur votre syst�me."
StrCpy $R0 "$R0\Translators"
Call Installezy

; mathtype 6
mt6:
ReadRegStr $R0 HKLM "${MT6_REG_DIR_CONFIG}" "${REG_KEY_TRANSLATORDIR}"
StrCmp $R0 "" mt6_bis
DetailPrint "MathType 6 d�tect� sur votre syst�me."
Call Installezy
Goto done

mt6_bis:
ReadRegStr $R0 HKLM "${MT6_REG_DIR_DIRECTORIES}" "${REG_KEY_PROGDIR}"
StrCmp $R0 "" done
DetailPrint "MathType 6 d�tect� sur votre syst�me."
StrCpy $R0 "$R0\Translators"
Call Installezy

done:

SectionEnd
Section "Raccourcis sur le bureau" SecRaccourcis

 SetOutPath "$INSTDIR"
 CreateShortCut $DESKTOP\NAT.lnk "%windir%\system32\javaw.exe" \
	"-Xss20000k -Xmx256m -splash:ui/nat_splash.png -jar natbraille.jar -g" "$INSTDIR\iconeNat.ico" 0

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecMain ${LANG_FRENCH} "Programme principal"
  LangString DESC_SecDBT ${LANG_FRENCH} "Configurations sp�ciales pour export vers DBT 10.7 ou plus. Ne fonctionnera pas si DBT 10.7 ou plus n'est pas install�."
  LangString DESC_SecPolice ${LANG_FRENCH} "Police LouisLouis (Braille) pour les tables CBFr1252, DuxCBFr1252 et UTF8 ;  Police Braille INSHEA r�alis�e par Marc Ollier (INSHEA) pour la nouvelle table tbfr2007."
  LangString DESC_SecMathType ${LANG_FRENCH} "Convertisseurs vers le MathML pour MathType"
  LangString DESC_SecShell ${LANG_FRENCH} "Ouvrir avec Nat dans le menu contextuel de l'explorateur"
  LangString DESC_SecRaccourcis ${LANG_FRENCH} "Raccourci sur le Bureau"
  LangString DESC_SecJaws ${LANG_FRENCH} "Table braille pour Jaws prenant en compte les caract�res braille UTF8. Si vous utilisez la table braille fran�aise BFU, vous pouvez la remplacer par celle-ci sans crainte."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SecMain} $(DESC_SecMain)
  !insertmacro MUI_DESCRIPTION_TEXT ${SecDBT} $(DESC_SecDBT)
  !insertmacro MUI_DESCRIPTION_TEXT ${SecPolice} $(DESC_SecPolice)
  !insertmacro MUI_DESCRIPTION_TEXT ${SecRaccourcis} $(DESC_SecRaccourcis)
  !insertmacro MUI_DESCRIPTION_TEXT ${SecMathType} $(DESC_SecMathType)
  !insertmacro MUI_DESCRIPTION_TEXT ${SecShell} $(DESC_SecShell)
  !insertmacro MUI_DESCRIPTION_TEXT ${SecJaws} $(DESC_SecJaws)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  RMDir /r "$INSTDIR\aide"
  RMDir /r "$INSTDIR\gestionnaires"
  RMDir /r "$INSTDIR\joptsimple"
  RMDir /r "$INSTDIR\nat"
  RMDir /r "$INSTDIR\outils"
  RMDir /r "$INSTDIR\config"
  RMDir /r "$INSTDIR\formula"
  RMDir /r "$INSTDIR\jaxe"
  RMDir /r "$INSTDIR\ui"
  RMDir /r "$INSTDIR\lib"
  RMDir /r "$INSTDIR\tmp"
  RMDir /r "$INSTDIR\writer2latex"
  RMDir /r "$INSTDIR\xsl"
  RMDir /r "$INSTDIR\org"
  RMDir /r "$INSTDIR\configurations"
  RMDir /r "$INSTDIR\winos"
  RMDir /r "$INSTDIR\distrib"
  RMDir /r "$INSTDIR\documents"
  RMDir /r "$INSTDIR\text"
  RMDir /r "$INSTDIR\gnu\getopt"
  RMDir /r "$INSTDIR\gnu"
  RMDir /r "$INSTDIR\doc"
  RMDir /r "$INSTDIR\nonregression"

  
  Delete "$INSTDIR\tmp*.*"
  Delete "$INSTDIR\nat-launch-windows.bat"
  Delete "$INSTDIR\nat-launch-win-gf.bat"
  Delete "$INSTDIR\nat.exe"
  Delete "$INSTDIR\jab_control.exe"
  Delete "$INSTDIR\nat-gui.conf-initial"
  Delete "$INSTDIR\licence.txt"
  Delete "$INSTDIR\natbraille.jar"
  Delete "$INSTDIR\enable_accessbridge.bat"
  Delete "$INSTDIR\disable_accessbridge.bat"
  Delete "$INSTDIR\iconeNat.ico"
  Delete "$INSTDIR\scenarii.cfg.initial"
  
  Delete "$DESKTOP\NAT.lnk"
  
  Delete "$INSTDIR\Uninstall.exe"  
  
  DeleteRegKey HKCR "*\shell\Transcrire avec NAT" 
  RMDir /r $SMPROGRAMS\NAT
   
  DeleteRegKey /ifempty HKCU "Software\Nat"
  

SectionEnd
