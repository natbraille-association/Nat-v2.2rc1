#!/usr/bin/perl
use POSIX;
use strict;
use warnings;
use Getopt::Std;


my %opt_name = (
    'a' => '<d> horizontal dot to dot distance',
    'b' => '<d> vertical dot to dot distance',
    'c' => '<d> horizontal cell to cell distance',
    'e' => '<d> dot diameter',
    'f' => '<d> rep dot diameter (mm)',
    's' => 'circle:<d>|diamond dot shape',
    't' => 'thermo font',
    'x' => 'entity filename',
       
);

sub optToString {
    my $opt = shift;
    my @resu;
    foreach (sort keys %opt_name){
	my $cho = "  -".$_." ".$opt_name{$_}." : ";
	if ($opt->{$_}){
	    push @resu,  $cho.' '.$opt->{$_};
	} else {
	    push @resu,  $cho.' '."?";
	}
    }

    return join("\n",@resu);
}

sub erreurString {
    my $opt = shift;

    my @resu;
    
    push @resu, "usage : \n mkbrlsfd \n",
    push @resu, optToString($opt);
    push @resu,   "

   | a |      |e|
-  *   *       *   *         
b                            
-  *   *       *   *          
                             
   *   *       *   *
   |      c    |

";
    
    return join("\n",@resu);
}



sub xunicode_to_num {
    my $rc = shift;
    $rc =~ s/&#//;
    $rc =~ s/;//;
    return $rc;
}

#
# character creation
#

sub make_diamond {
    my ($x,$y,$r) = @_;
    my @splineSet;

    push  @splineSet, int($x-$r), int($y), 'm 4'."\n";
    push  @splineSet, int($x), int($y+$r), 'l 4'."\n";
    push  @splineSet, int($x+$r), int($y), 'l 4'."\n";
    push  @splineSet, int($x), int($y-$r), 'l 4'."\n";
    push  @splineSet, int($x-$r), int($y), 'l 4'."\n";
    return join(" ",@splineSet);
}

sub make_rond {
    my ($x,$y,$r,$round) = @_;

    my @splineSet;
    my $c;
    if (!$round) {
	$c = $r/2 + $r/20;
    } else {
	$c = $r*$round;
    }

    push @splineSet, (map {int($_)} ($x-$r, $y)),'m 4',"\n";
    push @splineSet, (map {int($_)}($x-$r, $y+$c,
				    $x-$c, $y+$r,
				    $x,$y+ $r)),'c 4',"\n";
    push @splineSet, (map {int($_)}($x+$c,$y+$r,
				    $x+$r,$y+$c,
				    $x+$r,$y)),'c 4',"\n";
    push @splineSet, (map {int($_)}($x+$r,$y-$c,
				    $x+$c,$y-$r,
				    $x,$y-$r)), 'c 4',"\n";
    push @splineSet, (map {int($_)}($x-$c,$y-$r,
				     $x-$r,$y-$c,
				    $x-$r,$y)),'c 4';

    return join(" ",@splineSet);
}

my $height;

sub make_cell {
    my $pt = shift;
    my %opt = %{(shift)};
    my $width = 1000;

    my $a = 2.5;      # horizontal dot to dot
    my $b = 2.5;      # vertical dot to dot
    my $c = 6;        # cell to cell w
    my $e = 1.2;      # dot diameter
#    my $d = 11;       # line to line h
    my $d = (2 * $b + $e);

    my $thermofont = ($opt{'t'} =~ /(oui|yes)/);


    my $f;
    $a = $opt{'a'} || (2.5);
    $b = $opt{'b'} || (2.5);
    $c = $opt{'c'} || (6);
    $e = $opt{'e'} || (1.2);
    $f = $opt{'f'} || (0.9);


    my $shape_name = $opt{'shape'};

    

    my $scale = ($width / $c);
    $height = $d * $scale;
 
        

    my $pt_intro = join("\n",
			"Width: ".($width),
			"Height : ".$height,
			"VWidth: 0 ",
			"Flags: W",
			"LayerCount: 2",
			"Fore",
			"SplineSet");

    my $pt_outro = join("\n",
			"EndSplineSet",
			"EndChar\n");
    
    my @l;

    push @l, $pt_intro;

    my @positions  = (

	[$scale * ($c - $a) / 2, $scale * ((($d - (2 * $b)) /  2) + (2 * $b))],
	[$scale * ($c - $a) / 2, $scale * ((($d - (2 * $b)) /  2) + $b)],
	[$scale * ($c - $a) / 2, $scale * (($d - (2 * $b)) /  2 )],
	

	[$scale * ((($c - $a) / 2)+$a), $scale * ((($d - (2 * $b)) /  2) + (2 * $b))],	
	[$scale * ((($c - $a) / 2)+$a), $scale * ((($d - (2 * $b)) /  2) + $b)],
	[$scale * ((($c - $a) / 2)+$a), $scale * ($d - (2 * $b)) /  2 ],


	);

    foreach (1 .. 6) {
	my $x =  ($positions[$_-1]->[0]);
	my $y =  ($positions[$_-1]->[1]);
	my $r = $scale * $e / 2;
	if ($pt =~ /$_/){
	    if ($shape_name =~ /circle/){
		push @l, make_rond($x,$y,$r,$opt{'round'});
	    } else {
		push @l, make_diamond($x,$y,$r,$f);
	    }
	}  else {
	    if (!$thermofont){
		if ($shape_name =~ /circle/){
		    push @l, make_rond($x,$y,$r*$f,$opt{'round'});
		} else {
		    push @l, make_diamond($x,$y,$r*$f,$f);
		}
	    }
	}
	
    }
    
    push @l, $pt_outro;
    return join("\n",@l);
}

#
# arguments
#

my %opts;
getopt('abceftxs', \%opts);

#
# mandatory
#

foreach (qw/a b c e x/){
    if (!$opts{$_}){
	print STDERR '!parameter <'.$_.'> not found'."\n\n";
	print STDERR erreurString(\%opts);
	die;
    }
}

#
# bricolage 
#

$opts{'t'} = $opts{'t'} || 'no'; # pas thermo par défaut
$opts{'s'} = $opts{'s'} || 'circle'; 
$opts{'f'} = $opts{'f'} || 0.5; # rap size petit point

my @p = split(':',$opts{'s'});
$opts{'shape'} = $p[0];
if ($p[1]){
    $opts{'round'} = $p[1];
} else {
    $opts{'round'} = (22/40);
}

#print STDERR optToString(\%opts);


open A, $opts{'x'};
my @table = <A>;
close A;

# decode entities;
my %entities;
foreach (@table){
    if ($_ =~ /<!ENTITY\s*(.*)\s"(.*)"/){
	push @{$entities{$1}}, $2;
    }
}

my $count  = 0; 
my @infos;
foreach my $carac (keys %entities){
    my $version = 0;
    foreach my $code (@{$entities{$carac}}){
	push @infos,  "StartChar: ".$carac.'_'.$version;
	my $a = xunicode_to_num($code);
 	push @infos,  "Encoding: ".join(" ",$a,$a,$count);
#	print STDERR $carac." ".$code."\n";
	push @infos,  make_cell($carac,\%opts);
 	$version++;
	$count++;
    }
}
unshift @infos, "BeginChars: ".$count." ".$count."\n";
push @infos, ("EndChars","EndSplineFont","\n");

unshift @infos, "SplineFontDB: 3.0
FontName: Untitled1
FullName: Untitled1
FamilyName: Untitled1
Weight: Medium
Copyright: Created by vivien,,, with FontForge 2.0 (http://fontforge.sf.net)
UComments: \"2011-3-17: Created.\" 
Version: 001.000
ItalicAngle: 0
UnderlinePosition: -100
UnderlineWidth: 50
Ascent: ".$height."
Descent: 200
LayerCount: 2
Layer: 0 0 \"Arri+AOgA-re\"  1
Layer: 1 0 \"Avant\"  0
NeedsXUIDChange: 1
XUID: [1021 848 1751021855 6123354]
OS2Version: 0
OS2_WeightWidthSlopeOnly: 0
OS2_UseTypoMetrics: 1
CreationTime: 1300361182
ModificationTime: 1300361546
OS2TypoAscent: 0
OS2TypoAOffset: 1
OS2TypoDescent: 0
OS2TypoDOffset: 1
OS2TypoLinegap: 0
OS2WinAscent: 0
OS2WinAOffset: 1
OS2WinDescent: 0
OS2WinDOffset: 1
HheadAscent: 0
HheadAOffset: 1
HheadDescent: 0
HheadDOffset: 1
OS2Vendor: 'PfEd'
DEI: 91125
Encoding: Custom
UnicodeInterp: none
NameList: Adobe Glyph List
DisplaySize: -24
AntiAlias: 1
FitToEm: 1
WinInfo: 0 32 20
";

print join("\n",@infos);
print "\n";


