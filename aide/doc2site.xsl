<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "../xsl/mmlents/windob.dtd">
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xhtml="http://www.w3.org/1999/xhtml">

<xsl:output 
  method="html"
  encoding="UTF-8"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  indent="yes" />

<xsl:template match="/">
	<xsl:apply-templates select="xhtml:html/xhtml:body/xhtml:div[@id='corps']"/>
</xsl:template>

<xsl:template match="@*|*|processing-instruction()|comment()" priority="-1">
	<xsl:copy>
		<xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()"/>
	</xsl:copy>
</xsl:template>

<xsl:template match="xhtml:a[@href]">
	<xsl:copy>
		<xsl:for-each select="@*">
			<xsl:choose>
				<xsl:when test="local-name(.)='href' and (not(starts-with(.,'http'))) and (contains(.,'.html'))">
					<xsl:attribute name="href" select="concat('?lang=fr&amp;article=aide/',substring-before(.,'.html'),'.php',substring-after(.,'.html'))"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<xsl:apply-templates select="*|text()|processing-instruction()|comment()"/>
	</xsl:copy>
</xsl:template>

<xsl:template match="xhtml:img">
	<xsl:copy>
		<xsl:copy-of select="@*" />
		<xsl:attribute name="class" select="'helpimage'" />
	</xsl:copy>
</xsl:template>

<xsl:template match="xhtml:html/xhtml:body/xhtml:div[@id='corps']">
	<xsl:apply-templates select="*[not(self::xhtml:div[@align])]|text()|processing-instruction()|comment()"/>
</xsl:template>

</xsl:stylesheet>