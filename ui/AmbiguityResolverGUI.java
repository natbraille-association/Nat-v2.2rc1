/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;

import ui.dialog.DialogueAmbiguity;

import nat.ConfigNat;
import nat.transcodeur.Ambiguity;
import nat.transcodeur.AmbiguityResolverUI;

/**
 * @author bruno
 *
 */
public class AmbiguityResolverGUI implements AmbiguityResolverUI
{    
	/** liste des ambiguïtés déjà résolues */
	private ArrayList<Ambiguity> lAmbAuto = new ArrayList<Ambiguity>();
	/** longueur de la fenêtre principale*/
	private int lgRel = 0;
	/** hauteur de la fenêtre principale*/
	private int htRel = 0;
	/**
	 * Constructeur; met à jour les infos pour centrer les DialogueAmbiguity
	 */
	public AmbiguityResolverGUI()
	{
		lgRel = ConfigNat.getCurrentConfig().getWidthPrincipal();
		htRel = ConfigNat.getCurrentConfig().getHeightPrincipal();	
	}
	/**
	 * @see nat.transcodeur.AmbiguityResolverUI#resolve(nat.transcodeur.Ambiguity)
	 */
	@Override
	public void resolve(Ambiguity amb)
	{
		if(lAmbAuto.contains(amb))
		{
			amb.setSolution(lAmbAuto.get(lAmbAuto.indexOf(amb)).getSolution());
		}
		else
		{
			DialogueAmbiguity da = new DialogueAmbiguity(amb,lAmbAuto);
			Dimension screenSize;
			Dimension size = da.getSize();
			
			if(ConfigNat.getCurrentConfig().getCentrerFenetre())
			{
				screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			}
			else
			{
				screenSize = new Dimension(lgRel,htRel);
			}
			int y = screenSize.height/2 - size.height/2;
			int x = screenSize.width/2 - size.width/2;
			da.setLocation(x, y);
			da.setVisible(true);
		}
	}

}
