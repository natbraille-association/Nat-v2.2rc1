/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ui;

import gestionnaires.GestionnaireErreur;

import java.awt.Dimension;
import java.awt.Toolkit;
import ui.dialog.DialogueListe;

import nat.ConfigNat;
import nat.transcodeur.IVBLoaderUI;

/**
 * This class provide an IVBLoaderUI instance for graphical running mode
 * @author bruno
 *
 */
public class IVBLoaderGUI implements IVBLoaderUI
{    
	/** longueur de la fenêtre principale*/
	private int lgRel = 0;
	/** hauteur de la fenêtre principale*/
	private int htRel = 0;
	/** Error manager instance */
	private GestionnaireErreur gest = null;
	/**
	 * Constructeur; met à jour les infos pour centrer les DialogueAmbiguity
	 */
	public IVBLoaderGUI()
	{
		lgRel = ConfigNat.getCurrentConfig().getWidthPrincipal();
		htRel = ConfigNat.getCurrentConfig().getHeightPrincipal();	
	}
	/**
	 * @see nat.transcodeur.AmbiguityResolverUI#resolve(nat.transcodeur.Ambiguity)
	 */
	@Override
	public void launch()
	{
		DialogueListe dl = new DialogueListe(ConfigNat.getFichierIVB(), DialogueListe.IVB_LIST, gest);
		Dimension screenSize;
		Dimension size = dl.getSize();
		
		if(ConfigNat.getCurrentConfig().getCentrerFenetre())
		{
			screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		}
		else
		{
			screenSize = new Dimension(lgRel,htRel);
		}
		int y = screenSize.height/2 - size.height/2;
		int x = screenSize.width/2 - size.width/2;
		dl.setLocation(x, y);
		dl.setVisible(true);
	}
	/**
	 * @see nat.transcodeur.ProperNounLoaderUI#setGestionnaireErreur(gestionnaires.GestionnaireErreur)
	 */
    @Override
    public void setGestionnaireErreur(GestionnaireErreur g)
    {
	    gest = g;
	    
    }
}
