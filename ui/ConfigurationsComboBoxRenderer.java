/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret, Frédérick Schwebel, Vivien Guillet
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.ListCellRenderer;
import javax.swing.JList;
import javax.swing.ImageIcon;

import ui.accessibility.Language;

/**
 * Classe décrivant le rendu du JComboBox de la fenêtre principale de NAT contenant 
 * les configurations possibles
 * @see FenetrePrinc
 * @author Vivien, Bruno
 *
 */
public class ConfigurationsComboBoxRenderer extends JLabel implements ListCellRenderer<ConfigurationsListItem>
{ 
	/** Textual contents */
    private Language texts = new Language("FenetrePrinc");
	
    /** Pour la sérialisation, non utilisé */
    private static final long serialVersionUID = 1L;
    /** ImageIcon pour les configutrations système */
    private ImageIcon iconSys = new ImageIcon("ui/icon/system-run.png");
    /** ImageIcon pour les configurations de l'utilisateur */
    private ImageIcon iconUsr = new ImageIcon("ui/icon/gtk-edit.png");
    /** ImageIcon pour les scenarii */ //de l'utilisateur */
    private ImageIcon iconScen = new ImageIcon("ui/icon/go-jump.png");
    /** ImageIcon pour les scenarii utilisateur */
    private ImageIcon iconUserScen = new ImageIcon("ui/icon/go-jump-user.png");
	
    /** Constructeur */
    public ConfigurationsComboBoxRenderer()
    {
    	setOpaque(true);
		setHorizontalAlignment(LEFT);
		setVerticalAlignment(CENTER);
    }
    /**
     * Renvoie le rendu pour une configuration donnée
     * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
     */
    @Override
    public Component getListCellRendererComponent( JList<? extends ConfigurationsListItem> list,
            ConfigurationsListItem cli,
						  int index,
						  boolean isSelected,
						  boolean cellHasFocus)
    {
		if (cli != null)
		{
		    if (isSelected)
		    {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
		    }
		    else
		    {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
		    }
		    
		    ImageIcon icon = (cli.getIsSystem())?iconSys:iconUsr;
		    String pet = (cli.getName() + " ("+cli.getInfos()+")");
		    if(cli.isStep())
		    {
		    	icon=(cli.getIsSystem())?iconScen:iconUserScen;
		    	pet = cli.getInfos();
		    }
		    setIcon(icon);
		    String accessNom="";
		    String accessDesc="";
		    if(cli.getIsSystem())
		    {
		    	accessNom = texts.getText("systemtablename")+" " + cli.getName();
		    	accessDesc = texts.getText("systemtabledesc");
		    }
		    else
		    {
		    	accessNom = texts.getText("privatetablename")+" " + cli.getName();
		    	accessDesc = texts.getText("privatetabledesc");
		    }
		    getAccessibleContext().setAccessibleDescription(accessDesc);
		    getAccessibleContext().setAccessibleName(accessNom);
		    setToolTipText(accessDesc);
		    
		    if (icon != null) {	setText(pet);}
		}
		return this;
    }
}
