/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package ui.config;

import gestionnaires.GestionnaireErreur;
import gestionnaires.GestionnaireMajTabBraille;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.io.IOException;
//import javax.swing.ImageIcon;

import nat.ConfigNat;
import nat.Nat;
import nat.Scenario;
import ui.FenetrePrinc;
import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;

/**
 * Fenêtre graphique de configuration des options de NAT
 * @author bruno
 *
 */
public class Configuration extends JFrame implements ActionListener, WindowListener, ComponentListener, ChangeListener, KeyListener
{
	/** Textual contents */
	private static Language texts = new Language("Configuration");
	
    /** Pour la sérialisation (non utilisé) */
    private static final long serialVersionUID = 1L;
    /** titre onglet 4 (indice 3) en cas de MEP activée */
    private final String TAB3_TITLE_MEP = texts.getText("headinglevel");
    /** titre onglet 4 (indice 3) en cas de MEP désactivée */
    private final String TAB3_TITLE_NOMEP = texts.getText("chainstitle");
    /** JTabbedPane contenant les onglets de configuration */
    private JTabbedPane onglets = new JTabbedPane();
    /** Onglet de configuration générale */
    private ConfGeneral panGen;
    /** Onglet MEP Avancée (Niveaux de titres OU chaînes à rajouter) */
    private ConfMiseEnPageAvancee panMepAvancee;
    /** liste de tous les onglets de configuration */
    private ArrayList<OngletConf> listOnglets = new ArrayList<OngletConf>();
    /** Début du titre de la configuration*/
    private JLabel titreConfig1 = new JLabel("<html><h3>"
			  + texts.getText("configlabel")
		      + ((ConfigNat.getCurrentConfig().getIsSysConfig())?texts.getText("system"):"") 
		      + " : "
    		  + "</h3></html>");
    /** Fin du titre de la configuration*/
    private JLabel titreConfig2 = new JLabel("");
				
    /** JTextField indiquant la configuration éditée */
    private JTextField lTitreConfig = new JTextField (ConfigNat.getCurrentConfig().getName(),20);										
					      
    //private JLabel lInfosConfig  = new JLabel ("<b>"+ConfigNat.getCurrentConfig().getInfos() +"</b>");											
    /** JPanel du titre */
    private JPanel panelTitre = new JPanel();
    /** JPanel conteannt les boutons d'enregistrement */
    private JPanel lesBoutons = new JPanel();
    /** Annule les modifications */
    private JButton btAnnuler = new JButton(texts.getText("cancel"),new ImageIcon("ui/icon/edit-undo.png"));
    /** Enregistre les modifications dans la configuration courante */
    private JButton btEnregistrer = new JButton(texts.getText("save"),new ImageIcon("ui/icon/document-save.png"));
    /** Enregistre les options dans une nouvelle configuration */
    private JButton btEnregistrerSous = new JButton(texts.getText("saveas"),new ImageIcon("ui/icon/document-save-as.png"));
    /** Bouton (invisible) pour maximiser la fenêtre */
    /* Passe à l'étape suivante/
    private JButton btNextStep = new JButton(new ImageIcon("ui/icon/go-next.png"));
    /* Passe à l'étape précédente /
    private JButton btPreviousStep = new JButton(new ImageIcon("ui/icon/go-previous.png"));*/
    /** Spinner pour les étapes de scénario*/
    private JSpinner jsStep;
    /** supprime l'étape */
    private JButton btRemoveStep = new JButton(new ImageIcon("ui/icon/list-remove.png"));
    /** ajoute une étape */
    private JButton btPropagate = new JButton(new ImageIcon("ui/icon/next.png"));
    /** propagates modifications if the configuration is a scenario step */
    private JButton btAddStep = new JButton(new ImageIcon("ui/icon/list-add.png"));
    /** Instance de la fenêtre principale de NAT qui a construit l'instance de Configuration */
    private FenetrePrinc fPrinc;
    /** une instance de {@link gestionnaires.GestionnaireMajTabBraille}*/
    private GestionnaireMajTabBraille gmtb;
    /** scénario de la config, s'il existe, null sinon */
    private Scenario scen = null;
    
    /** textes pour la fenêtre Configuration*/
    private Language ctexts = new Language("Configuration");
    /**
     * Constructeur
     * @param fp la fenêtre principale appelant le constructeur
     */
    public Configuration(FenetrePrinc fp)
    {
		super(texts.getText("title"));
		getAccessibleContext().setAccessibleName(texts.getText("titlename"));
	   	
		fPrinc = fp;
		addWindowListener(this);
		addComponentListener(this);
		
		//chargement du scénario si scénario
		if(ConfigNat.getCurrentConfig().getScenarioName()!="")
		{
			String path = new File(ConfigNat.getCurrentConfig().getFichierConf()).getParent();
			scen = new Scenario(path,fp.getGestErreur());
			fp.getGestErreur().afficheMessage("Scénario "+scen.getName(), Nat.LOG_DEBUG);
		}
		
		//Ajout d'une action pour quitter la fenêtre avec echap
		Action annuleAction = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
            public void actionPerformed(ActionEvent e) {dispose();}};
			
		Action selectOnglet0 = new OngletAction(0);
		Action selectOnglet1 = new OngletAction(1);
		Action selectOnglet2 = new OngletAction(2);
		Action selectOnglet3 = new OngletAction(3);
		Action selectOnglet4 = new OngletAction(4);
		Action selectOnglet5 = new OngletAction(5);
		Action selectOnglet6 = new OngletAction(6);
		Action selectOnglet7 = new OngletAction(7);
		
		//input map de l'onglet quand il a le focus
		InputMap im = onglets.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		//ajout des actions à cette inputmap
		im.put(KeyStroke.getKeyStroke("ESCAPE"), "annuleAction");
		im.put(KeyStroke.getKeyStroke('1'), "ong0");
		//im.put(KeyStroke.getKeyStroke('g'), "ong0");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.ALT_DOWN_MASK), "ong0");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.ALT_DOWN_MASK), "ong0");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.CTRL_DOWN_MASK), "ong0");
		im.put(KeyStroke.getKeyStroke('2'), "ong1");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.ALT_DOWN_MASK), "ong1");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.CTRL_DOWN_MASK), "ong1");
		im.put(KeyStroke.getKeyStroke('3'), "ong2");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_3, InputEvent.ALT_DOWN_MASK), "ong2");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_3, InputEvent.CTRL_DOWN_MASK), "ong2");
		im.put(KeyStroke.getKeyStroke('4'), "ong3");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_4, InputEvent.ALT_DOWN_MASK), "ong3");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_4, InputEvent.CTRL_DOWN_MASK), "ong3");
		im.put(KeyStroke.getKeyStroke('5'), "ong4");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_5, InputEvent.ALT_DOWN_MASK), "ong4");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_5, InputEvent.CTRL_DOWN_MASK), "ong4");
		im.put(KeyStroke.getKeyStroke('6'), "ong5");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_6, InputEvent.ALT_DOWN_MASK), "ong5");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_6, InputEvent.CTRL_DOWN_MASK), "ong5");
		im.put(KeyStroke.getKeyStroke('7'), "ong6");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_7, InputEvent.ALT_DOWN_MASK), "ong6");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_7, InputEvent.CTRL_DOWN_MASK), "ong6");
		im.put(KeyStroke.getKeyStroke('8'), "ong7");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_8, InputEvent.ALT_DOWN_MASK), "ong7");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_8, InputEvent.CTRL_DOWN_MASK), "ong7");
		//ajout de l'action à la liste des ActionMap de 'onglet
		ActionMap am = onglets.getActionMap();
		am.put("annuleAction", annuleAction);
		am.put("ong0", selectOnglet0);
		am.put("ong1", selectOnglet1);
		am.put("ong2", selectOnglet2);
		am.put("ong3", selectOnglet3);
		am.put("ong4", selectOnglet4);
		am.put("ong5", selectOnglet5);
		am.put("ong6", selectOnglet6);
		am.put("ong7", selectOnglet7);
			
		chargeOnglets();
		
		btAnnuler.addActionListener(this);
		Context cBtAnnuler = new Context("z","Button","cancel",texts);
		new ContextualHelp(btAnnuler,"guiOptions",cBtAnnuler);
		//btAnnuler.setMnemonic(KeyEvent.VK_ESCAPE);
		btAnnuler.setMnemonic('z');
			
		//if ((!(ConfigNat.getCurrentConfig().getIsSysConfig())) && (ConfigNat.getCurrentConfig().getFichierConf() != null))
		if (ConfigNat.getCurrentConfig().getFichierConf() != null)
		{
	        btEnregistrer.addActionListener(this);
	    } 
		else {btEnregistrer.setEnabled(false);}
		
		Context cBtEnregistrer = new Context("s","Button","save",texts);
		new ContextualHelp(btEnregistrer,"guiOptions",cBtEnregistrer);
		btEnregistrer.setMnemonic('s');
			
		btEnregistrerSous.addActionListener(this);
		Context cBtEnregistrerSous = new Context("n","Button","saveas",texts);
		new ContextualHelp(btEnregistrerSous,"guiOptions",cBtEnregistrerSous);
		btEnregistrerSous.setMnemonic('n');
		
		lesBoutons.add(btEnregistrer);
		lesBoutons.add(btEnregistrerSous);
		lesBoutons.add(btAnnuler);
		
		//ajout d'un scrollpane aux onglets pour éviter les pb de résolution sur petites résolutions d'écran
		JScrollPane scrollRes = new JScrollPane (onglets);
		scrollRes.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollRes.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		//mise en page générale
		
		
		//TODO :faire 2 panneaux différents dans des classes externes
		if(scen != null)
		{
			jsStep = new JSpinner(new SpinnerNumberModel(scen.getCurrentStep(), 1, scen.getSize(), 1));
			jsStep.addChangeListener(this);
			jsStep.addKeyListener(this); //pour F11 plein écran mais marche pas
			/*btPreviousStep.addActionListener(this);
			btNextStep.addActionListener(this);*/
			btRemoveStep.addActionListener(this);
			btAddStep.addActionListener(this);
			btPropagate.addActionListener(this);
			panelTitre.add(new JLabel("Scénario: "+ scen.getPrettyName()));
			//panelTitre.add(btPreviousStep);
			titreConfig1.setText("étape ");
			panelTitre.add(titreConfig1);
			//panelTitre.add(btNextStep);
			panelTitre.add(jsStep);
			titreConfig2.setText(" de " + scen.getSize());
			panelTitre.add(titreConfig2);
			
			if(!ConfigNat.getCurrentConfig().getIsSysConfig())
			{
				panelTitre.add(btRemoveStep);
				panelTitre.add(btAddStep);
				panelTitre.add(btPropagate);
			}
		}
		else
		{
			titreConfig1.setLabelFor(lTitreConfig);
			panelTitre.add(titreConfig1);
			Context cLTitreConfig = new Context("","TextField","config",texts);
			new ContextualHelp(lTitreConfig,"guiOptions",cLTitreConfig);
			lTitreConfig.addKeyListener(this); //pour F11 plein écran
			lTitreConfig.setCaretPosition(0);
			panelTitre.add(lTitreConfig);
		}

		add("North", panelTitre);
		add("Center",scrollRes);
		add("South",lesBoutons);
		if(ConfigNat.getCurrentConfig().getMemoriserFenetre())
	    {
	    	int x= ConfigNat.getCurrentConfig().getWidthOptions();
	    	int y=ConfigNat.getCurrentConfig().getHeightOptions();
	    	if(x+y != 0){setPreferredSize(new Dimension(x,y));}
	    }
		if(ConfigNat.getCurrentConfig().getCentrerFenetre())
	    {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension size = this.getPreferredSize();
			screenSize.height = screenSize.height/2;
			screenSize.width = screenSize.width/2;
			size.height = size.height/2;
			size.width = size.width/2;
			int y = screenSize.height - size.height;
			int x = screenSize.width - size.width;
			setLocation(x, y);
	    }
		
	
    }
    /**
     * charge les onglets et éventuellement MAJ de {@link #titreConfig1} et  {@link #titreConfig2}
     */
    private void chargeOnglets()
    {
    	int selected = 0;
    	if(onglets.getSelectedIndex()>0){selected = onglets.getSelectedIndex();}
    	//TODO cf+haut l'histoire des deux panneaux
    	if(scen!=null)
    	{
    		titreConfig1.setText("étape ");
    		titreConfig2.setText(" de " + scen.getSize());
    	}
    	listOnglets.clear();
    	onglets.removeAll();
    	ConfEmbossage panConfEmbossage = new ConfEmbossage();
		panGen = new ConfGeneral(this);
		panMepAvancee = new ConfMiseEnPageAvancee();
		
		listOnglets.add(panConfEmbossage);
		listOnglets.add(new ConfConversion());
		listOnglets.add(new ConfTranscription(this));
		listOnglets.add(new ConfMiseEnPage(this));
		listOnglets.add(panMepAvancee);
		listOnglets.add(new ConfPostTraitement());//,this);
		listOnglets.add(new ConfInterface());
		listOnglets.add(new ConfAvance());
		listOnglets.add(new ConfAccessibilite());
		listOnglets.add(panGen);
			
		gmtb = new GestionnaireMajTabBraille (panGen.getComboTables(), panConfEmbossage.getComboTables());
		onglets.addTab(ctexts.getText("general"),new ImageIcon("ui/icon/document-properties.png"),panGen,ctexts.getText("generalttt"));
		onglets.setMnemonicAt(0, 'g');
		onglets.addTab(ctexts.getText("transcription"),new ImageIcon("ui/icon/stock_script.png"),listOnglets.get(2),ctexts.getText("transcriptionttt"));
		//onglets.setMnemonicAt(1, '2');
		onglets.addTab(ctexts.getText("layout"), new ImageIcon("ui/icon/document-page-setup.png"),listOnglets.get(3),ctexts.getText("layoutttt"));
		//onglets.setMnemonicAt(2, '3');
		String titreOnglet4;
		if (ConfigNat.getCurrentConfig().getMep()) {titreOnglet4 = TAB3_TITLE_MEP;}
		else {titreOnglet4 = TAB3_TITLE_NOMEP;}
		onglets.addTab(titreOnglet4, new ImageIcon("ui/icon/document-page-setup-advanced.png"),listOnglets.get(4),ctexts.getText("headinglevelttt"));
		//onglets.setMnemonicAt(3, '4');
		onglets.addTab(ctexts.getText("embossing"),new ImageIcon("ui/icon/stock_print-setup.png"),panConfEmbossage,ctexts.getText("embossingttt"));
		//onglets.setMnemonicAt(4, '5');
		onglets.addTab(ctexts.getText("interface"),new ImageIcon("ui/icon/gnome-settings-ui-behavior.png"),listOnglets.get(6),ctexts.getText("interfacettt"));
		//onglets.setMnemonicAt(5, '6');
		/*if(!(ConfigNat.getCurrentConfig().getNiveauLog() < Nat.LOG_DEBUG))
		    {*/
		onglets.addTab(ctexts.getText("advanced"),new ImageIcon("ui/icon/stock_form-properties.png"),listOnglets.get(7),ctexts.getText("advancedttt"));
			//onglets.setMnemonicAt(6, '7');
		    //}
		//onglets.addTab("Conversion",new ImageIcon("ui/icon/stock_filter-data-by-criteria.png"),listOnglets.get(1),"Regroupe les options des convertisseurs");
		onglets.addTab(ctexts.getText("accessibility"),new ImageIcon("ui/icon/preferences-desktop-accessibility.png"),listOnglets.get(8),ctexts.getText("accessibilityttt"));
		onglets.addChangeListener(this);
		onglets.setSelectedIndex(selected);
		lTitreConfig.setText(ConfigNat.getCurrentConfig().getName());
	    
    }
	/** Méthode d'accès en lecture à {@link #gmtb}
     * @return {@link #gmtb} 
     **/
    public GestionnaireMajTabBraille getGmtb (){return gmtb ;}
    /**
     * ferme la fenêtre et indique à {@link #fPrinc} que la fenêtre est fermée
     * @see java.awt.Window#dispose()
     */
    @Override
	public void dispose()
    {
		super.dispose();
		fPrinc.setOptionsOuvertes(false);
    }
    /**
     * Gère les actions sur les boutons d'enregistrement
     * @see #btAnnuler
     * @see #btEnregistrer
     * @see #btEnregistrerSous
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent evt)
    {
		if (evt.getSource() == btAnnuler)
	    {
			this.dispose();
			try{this.finalize();}
			catch (Throwable e){e.printStackTrace();}
	    }
		else if (evt.getSource() == btEnregistrer)
		{
			if (saveAll())
		    {
				//String nomConf = ConfigNat.getCurrentConfig().getShortFichierConf();
				ConfigNat.getCurrentConfig().setConfVersion(Nat.CONFS_VERSION);
				//dès qu'on enregistre une conf système, elle ne l'est plus
				ConfigNat.getCurrentConfig().setIsSysConfig(false);
				ConfigNat.getCurrentConfig().saveFilterConf(ConfigNat.getCurrentConfig().getFichierConf());
				fPrinc.chargeConfigurations();
				if (ConfigNat.getCurrentConfig().getSortieAuto())
					{fPrinc.setSortieAuto(ConfigNat.getCurrentConfig().isReverseTrans());}
				this.dispose();
				try {this.finalize();}
				catch (Throwable e){e.printStackTrace();}
		    }
	    }
		else if(evt.getSource()==btEnregistrerSous)
	    {	
			if(scen==null){enregistrerConfig();}//une simple conf
			else{enregistrerScenario();}
			
	    }
		else if (evt.getSource()==panGen.getMepBox())
		{
			/*if (panGen.getMepBox().isSelected())
				{getOnglets().setTitleAt(3, TAB3_TITLE_MEP);}
			else
				{getOnglets().setTitleAt(3, TAB3_TITLE_NOMEP);}*/
			//écriture équivalente
			getOnglets().setTitleAt(3,panGen.getMepBox().isSelected()?TAB3_TITLE_MEP:TAB3_TITLE_NOMEP);
			panMepAvancee.enableTabComponents(panGen.getMepBox().isSelected());
	    }
		/*
		else if(evt.getSource()==btNextStep)
		{
			//System.out.println(scen);
			scen.setStep(scen.getCurrentStep()+1);
			ConfigNat.charger(scen.getCurrentStepPath());
			//System.out.println(scen);
			chargeOnglets();
			//repaint();
		}
		else if(evt.getSource()==btPreviousStep)
		{
			//System.out.println(scen);
			scen.setStep(scen.getCurrentStep()-1);
			ConfigNat.charger(scen.getCurrentStepPath());
			//System.out.println(scen);
			chargeOnglets();
			//repaint();
		}*/
		else if(evt.getSource()==btRemoveStep){removeStep();}
		else if (evt.getSource()==btAddStep){addStep();}
		//TODO uncomment when ready else if (evt.getSource()==btPropagate){propagate();}
    }
    /**
	 * launch the dialog to propagate some of the modifications to next steps
	 */
    private void propagate()
    {
    	new PropagateFrame(scen);
    }
	/**
     * Enregistre un scénario: copie des fichiers et renommage du scen-name dans chaque config 
     */
    private void enregistrerScenario()
    {
    	//on demande le nom du scénario
		String nomNewScen = (String)JOptionPane.showInputDialog(this,texts.getText("newscenname"),
				texts.getText("currentscen")+" : "+scen.getName(), 
				JOptionPane.PLAIN_MESSAGE,null,null,scen.getName());
		//on vérifie qu'elle n'existe pas déjà
		int reponse = JOptionPane.YES_OPTION ;
		if ((nomNewScen != null) && (nomNewScen.length() > 0) && (reponse == JOptionPane.YES_OPTION))
	    {    
			try
		    {
				//création du répertoire
				File f = new File(ConfigNat.getUserConfigFilterFolder() + "1-scen");
				int j=1;
				while(f.exists())
				{
					f=new File(ConfigNat.getUserConfigFilterFolder() + j+"-scen");
					j++;
				}
				f.mkdir();
				String scen_path = f.getCanonicalPath();
				Scenario newScen = Scenario.createNewScen(scen_path, fPrinc.getGestErreur());
				newScen.setPrettyName(nomNewScen);
				//copie des fichiers
				for(int i=1; i<=scen.getSize();i++)
				{
					/*newScen.setStep(i+1);
					newScen.insertStep(scen.getEtape(scen.getSize()-i), 1);
					ConfigNat.getCurrentConfig().setFichierConf(newScen.getEtape(1));
					ConfigNat.getCurrentConfig().setScenarioName(newScen.getName());
					ConfigNat.getCurrentConfig().setName(newScen.getName()+"-"+(scen.getSize()-i));
					ConfigNat.getCurrentConfig().setIsSysConfig(false);
					ConfigNat.getCurrentConfig().setConfVersion(Nat.CONFS_VERSION);
					System.out.println(scen.getEtape(scen.getSize()-i)+"\n"+
							newScen.getEtape(1)+"\n"+
							ConfigNat.getCurrentConfig().getFichierConf());
					ConfigNat.getCurrentConfig().saveFilterConf(ConfigNat.getCurrentConfig().getFichierConf());
					*/
					newScen.addStep(scen.getEtape(i));
					ConfigNat.charger(newScen.getEtape(i));
					ConfigNat.getCurrentConfig().setFichierConf(newScen.getEtape(i));
					ConfigNat.getCurrentConfig().setScenarioName(newScen.getName());
					ConfigNat.getCurrentConfig().setName(newScen.getName()+"-"+(i));
					ConfigNat.getCurrentConfig().setIsSysConfig(false);
					ConfigNat.getCurrentConfig().setConfVersion(Nat.CONFS_VERSION);
					ConfigNat.getCurrentConfig().saveFilterConf(ConfigNat.getCurrentConfig().getFichierConf());
				}
				newScen.setStep(scen.getCurrentStep());
				ConfigNat.charger(newScen.getCurrentStepPath());
				scen=newScen;
				
				fPrinc.chargeConfigurations();//ajout de la nouvelle conf
				if (ConfigNat.getCurrentConfig().getSortieAuto())
				{fPrinc.setSortieAuto(ConfigNat.getCurrentConfig().isReverseTrans());}
				this.dispose();
				try{this.finalize();}
				catch (Throwable e){e.printStackTrace();}
				
		    }
			catch (IOException ioe)
		    {
				JOptionPane.showMessageDialog(this,
						texts.getText("bugwhilesaving"),
						texts.getText("error"),JOptionPane.ERROR_MESSAGE);
		    }
	    }
    }
    
    /**
     * Enregistrer la configuration en demandant un nom
     */
    private void enregistrerConfig()
    {
    	String nomConf;
		if (ConfigNat.getCurrentConfig().getFichierConf() != null ){nomConf = lTitreConfig.getText();}
		/** {nomConf = ConfigNat.getCurrentConfig().getShortFichierConf();} */
		else {nomConf = texts.getText("default");}

		//System.out.println("Enregistrere sous / ancienne : "+nomConf);
		//on demande le nom de la nouvelle config
		String nomNewConf = (String)JOptionPane.showInputDialog(this,texts.getText("newconfname"),
				texts.getText("currentconf")+" : "+nomConf, JOptionPane.PLAIN_MESSAGE,null,null,nomConf);
		
		//on vérifie qu'elle n'existe pas déjà
		int reponse = JOptionPane.YES_OPTION ;
		if ((new File(ConfigNat.getUserConfigFolder()+nomNewConf).exists()) && (nomNewConf != null))
	    {
			reponse = JOptionPane.showConfirmDialog(this, texts.getText("theconfiguration")+nomNewConf+texts.getText("alreadyexists"),
					texts.getText("confirm"),	JOptionPane.YES_NO_OPTION);
	    }

		if ((nomNewConf != null) && (nomNewConf.length() > 0) && (reponse == JOptionPane.YES_OPTION))
	    {    
			try
		    {
				String cpn = (new File(ConfigNat.getUserConfigFilterFolder() + nomNewConf).getCanonicalPath());				
				ConfigNat.getCurrentConfig().setFichierConf(cpn);
    			    
				if (saveAll())
			    {
					ConfigNat.getCurrentConfig().setIsSysConfig(false);
					ConfigNat.getCurrentConfig().setName(nomNewConf);
					ConfigNat.getCurrentConfig().setConfVersion(Nat.CONFS_VERSION);
					ConfigNat.getCurrentConfig().saveFilterConf(ConfigNat.getCurrentConfig().getFichierConf());
					//				ConfigNat.getCurrentConfig().sauvegarder();

					fPrinc.chargeConfigurations();//ajout de la nouvelle conf
					if (ConfigNat.getCurrentConfig().getSortieAuto())
					{fPrinc.setSortieAuto(ConfigNat.getCurrentConfig().isReverseTrans());}
					this.dispose();
					try{this.finalize();}
					catch (Throwable e){e.printStackTrace();}
			    }
		    }
			catch (IOException ioe)
		    {
				JOptionPane.showMessageDialog(this,
						texts.getText("bugwhilesaving"),
						texts.getText("error"),JOptionPane.ERROR_MESSAGE);
		    }
	    }
    }
    
    /** supprime l'étape actuelle, après demande de confirmation */
    private void removeStep()
    {
    	//demande de confirmation
    	int reponse = JOptionPane.showConfirmDialog(this, texts.getText("deleteStep")+scen.getCurrentStep(),
					texts.getText("confirm"),	JOptionPane.YES_NO_OPTION);
    	if(reponse == JOptionPane.YES_OPTION)
    	{
    		scen.removeStep(scen.getCurrentStep());
    		ConfigNat.charger(scen.getCurrentStepPath());
    		jsStep.setModel(new SpinnerNumberModel(scen.getCurrentStep(), 1, scen.getSize(), 1));
    		chargeOnglets();
    		
    	}
    }
    
    /** ajoute une étape (par copie de l'étape courante) */
    private void addStep()
    {
    	scen.insertStep(ConfigNat.getCurrentConfig().getFichierConf(),scen.getCurrentStep());
    	jsStep.setModel(new SpinnerNumberModel(scen.getCurrentStep(), 1, scen.getSize(), 1));
    	chargeOnglets();
    }
    
	/** Méthode redéfinie de ComponentListener
	 * Ne fait rien
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentHidden(ComponentEvent arg0){/*do nothing*/}
	/** Méthode redéfinie de ComponentListener
	 * Ne fait rien
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentMoved(ComponentEvent arg0){/*do nothing*/}
	/** Méthode redéfinie de ComponentListener
	 * Ne fait rien
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentShown(ComponentEvent arg0){/*do nothing*/}
	/** Méthode redéfinie de ComponentListener
	 * Mis à jour de l'affichage lors du redimensionement
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentResized(ComponentEvent arg0)
	{
		if (getExtendedState()==Frame.MAXIMIZED_BOTH)
		{
		ConfigNat.getCurrentConfig().setMaximizedOptions(true);
		}
		else
		{
		ConfigNat.getCurrentConfig().setWidthOptions(getWidth());
		ConfigNat.getCurrentConfig().setHeightOptions(getHeight());
		ConfigNat.getCurrentConfig().setMaximizedOptions(false);
		}
		repaint();
	}

    /** ne fait rien
     * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
     */
    @Override
    public void windowActivated(WindowEvent arg0) {/*do nothing*/}
    /** ne fait rien
     * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
     */
    @Override
    public void windowClosed(WindowEvent arg0) {/*do nothing*/}
    /** 
     * Indique à {@link #fPrinc} que la fenêtre des options est fermée
     * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
     */
    @Override
    public void windowClosing(WindowEvent arg0)
    {
    	fPrinc.setOptionsOuvertes(false);
    	if(scen!=null){fPrinc.chargeConfigurations();}
    }
    /** ne fait rien
     * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
     */
    @Override
    public void windowDeactivated(WindowEvent arg0) {/*do nothing*/}
    /** ne fait rien
     * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
     */
    @Override
    public void windowDeiconified(WindowEvent arg0) {/*do nothing*/}
    /** ne fait rien
     * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
     */
    @Override
    public void windowIconified(WindowEvent arg0) {/*do nothing*/}
    /** ne fait rien
     * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
     */
    @Override
    public void windowOpened(WindowEvent arg0) {/*do nothing*/}
    /**
     * Sauvegarde tous les onglets de configuration contenus dans {@link #listOnglets}
     * @return true si les sauvegardes se sont bien déroulées, false sinon
     */
    private boolean saveAll()
    {
		boolean retour = true;
		int i= 0;
		while(i<listOnglets.size()&&retour)
	   	{
			retour = listOnglets.get(i).enregistrer();
			i++;
	    }
		fPrinc.loadTexts();
		return retour;
    }
    /** 
     * Renvoie le gestionnaire d'erreur utilisé dans la fenêtre principale 
     * @return le gestionnaire d'erreur utilisé dans la fenêtre principale
     * */
	public GestionnaireErreur getGestErreur() {return fPrinc.getGestErreur();}
	
	/**
	 * Accès aux onglets
	 * @return les onglets
	 */
	protected JTabbedPane getOnglets() {return onglets;}

	/**
	 * classe interne pour les actions de positionnement sur onglet
	 * @author bruno
	 *
	 */
	private class OngletAction extends AbstractAction
	{
		/**pour la sérialisation (non utilisé)*/
		private static final long serialVersionUID = 1L;
		/** numéro de l'onglet*/
		private int numOnglet = 0;
		/**
		 * Constructeur
		 * @param i le numéro de l'onglet associé à l'action
		 */
		public OngletAction(int i)
		{
			numOnglet = i;
		}
		/**
		 * Sélectionne l'onglet n° {@link #numOnglet}
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e)
		{
			if(numOnglet<getOnglets().getComponentCount()){
				getOnglets().setSelectedIndex(numOnglet);
				//JPanel selectedTab = (JPanel) getOnglets().getSelectedComponent();
				//selectedTab.getComponent(7).requestFocus();
				}
		}
	}
	
	/** contextualhelp for the tabbed pane*/
	private ContextualHelp tabbedPaneHelp = new ContextualHelp(onglets);
	
	/**
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	@Override
	public void stateChanged(ChangeEvent ce) 
	{
		if(ce.getSource()==jsStep)
		{
			//ask for current config save
			int choice = JOptionPane.showConfirmDialog(this, texts.getText("saveStep"));
			if(choice != JOptionPane.CANCEL_OPTION)
			{
				if(choice == JOptionPane.YES_OPTION && saveAll())
				{
					ConfigNat.getCurrentConfig().setConfVersion(Nat.CONFS_VERSION);
					ConfigNat.getCurrentConfig().setIsSysConfig(false);
					ConfigNat.getCurrentConfig().saveFilterConf(ConfigNat.getCurrentConfig().getFichierConf());	
				}
				scen.setStep(((Integer)(jsStep.getValue())).intValue());
				ConfigNat.charger(scen.getCurrentStepPath());
				chargeOnglets();
			}
		}
		else if(onglets.hasFocus())
		{
			String newID="guiOptions";
			String[] paneHelpKeys = new String[2];
			String[] labels = {"Nom de l'onglet","Infobulle"};
			switch (onglets.getSelectedIndex())
			{
			case 1 : 
				newID="guiOptionsTranscription";
				paneHelpKeys[0]="transcription";
				paneHelpKeys[1]="transcriptionttt";
				break;
			case 2 : 
				newID="guiOptionsMiseEnPage";
				paneHelpKeys[0]="layout";
				paneHelpKeys[1]="layoutttt";
				break;
			case 3 : 
				newID="guiOptionsMiseEnPageAvancee";
				paneHelpKeys[0]="headinglevel";
				paneHelpKeys[1]="headinglevelttt";
				break;
			case 4 : 
				newID="guiOptionsEmbossage";
				paneHelpKeys[0]="embossing";
				paneHelpKeys[1]="embossingttt";
				break;
			case 5 : 
				newID="guiOptionsInterface";
				paneHelpKeys[0]="interface";
				paneHelpKeys[1]="interfacettt";
				break;
			case 6 : 
				newID="guiOptionsAvancees";
				paneHelpKeys[0]="advanced";
				paneHelpKeys[1]="advancedttt";
				break;
			case 7 : 
				newID="guiOptionsAccess";
				paneHelpKeys[0]="accessibility";
				paneHelpKeys[1]="accessibilityttt";
				break;
			default :
				newID="guiOptionsPrinc";
				paneHelpKeys[0]="general";
				paneHelpKeys[1]="generalttt";
			}
			tabbedPaneHelp.changeID(newID);
			tabbedPaneHelp.addLabels(labels, paneHelpKeys);
			if(onglets.getSelectedIndex()==3)
				tabbedPaneHelp.addLabel("Titre alternatif", "chainstitle");
		}
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		if (e.getKeyCode() == KeyEvent.VK_F11)
		{
			if (this.getExtendedState()==Frame.MAXIMIZED_BOTH)
	    	{this.setExtendedState(Frame.NORMAL);}
	    	else {this.setExtendedState(Frame.MAXIMIZED_BOTH);}
		}		
		}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
