/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret, Frédérick Schwebel, Vivien Guillet
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.config;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ui.accessibility.AccessibleListRenderer;
import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;
import ui.dialog.ConfStyle;
//import ui.dialog.ConfStyle;

import nat.ConfigNat;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


/**
 * Onglet de configuration de la mise en page
 * @see OngletConf
 * @author bruno
 *
 */
public class ConfMiseEnPage extends OngletConf implements ItemListener, ChangeListener, ActionListener
{
	/** Textual contents */
	private Language texts = new Language("ConfMiseEnPage");
	
	/** pour la sérialisation (non utilisé */
	private static final long serialVersionUID = 1L;
	
	//private JSpinner jsRetraitDebLigne;
	/** JCheckBox pour produire un saut de page */
	private JCheckBox jchKeepPageBreak = new JCheckBox(texts.getText("keeppbreak"));
	/** JCheckBox pour produire un saut de page */
	private JCheckBox jchMinPageBreak = new JCheckBox(texts.getText("minpbreak"));
	/** JSpinner minimum pour produire un saut de page */
	private JSpinner jsMinPageBreak;
	/** JSpinner minimum pour produire 3 lignes vides */
	private JSpinner jsMin3L;
	/** JSpinner minimum pour produire 2 lignes vides */
	private JSpinner jsMin2L;
	/** JSpinner minimum pour produire 1 ligne vide */
	private JSpinner jsMin1L;
	/** JSpinner longueur de la ligne braille */
	private JSpinner jsLongLigne;
	/** JLabel longueur de la ligne braille */
	private JLabel lLongLigne = new JLabel(texts.getText("lengthlabel"));
	/** JSpinner nombre de lignes par page */
	private JSpinner jsNbLigne;
	/** JLabel Nombre de lignes par page braille*/
	private JLabel lNbLigne = new JLabel(texts.getText("numberlabel"));
	/** JCheckBox ajouter un saut de page 
	private JCheckBox jchbSautFin = new JCheckBox(texts.getText("finalpb"));*/
	/** JCheckBox linéariser les structures de type table */
	private JCheckBox jchbLineariseTable = new JCheckBox(texts.getText("linetable"));
	/** JCheckBox preserve hyphens during reverse transcription */
	private JCheckBox jchbPreserveHyphen = new JCheckBox(texts.getText("preservehyphen"));
	/** JCheckBox preserve page break during reverse transcription */
	private JCheckBox jchbPreservePageBreak = new JCheckBox(texts.getText("preservepagebreak"));
	/** JSpinner nombre minimum de cellule pour rendre une table en 2D */
	private JSpinner jsMinCellLin;
	/** JLabel nombre minimum de cellule pour rendre une table en 2D */
	private JLabel ljsMinLin = new JLabel(texts.getText("minline"));
	/** JComboBox contenant les styles de numérotations possibles*/
	private JComboBox<Integer> jcbNumerotation;
	/** JLabel for {@link #jcbNumerotation}*/
	private JLabel lNumerotation = new JLabel(texts.getText("numtypelabel"));
	/** Tableau des numérotations possibles */
	private final String [] tabNumPossible = {texts.getText("nonum"),texts.getText("top1line"),texts.getText("topsharedline"),texts.getText("bottom1line"),texts.getText("bottomsharedline")};
	/** Constante d'accès représentant l'indice de "Aucune Numérotation" dans {@link #tabNumPossible}*/
	private final int NUM_AUCUN = 0;
	/** Constante d'accès représentant l'indice de "en haut sur une ligne partagée" dans {@link #tabNumPossible}*/
	private final int NUM_HB = 2;
	/** Constante d'accès représentant l'indice de "en haut seul sur une ligne" dans {@link #tabNumPossible}*/
	private final int NUM_HS = 1;
	/** Constante d'accès représentant l'indice de "en bas sur une ligne partagée" dans {@link #tabNumPossible}*/
	private final int NUM_BB = 4;
	/** Constante d'accès représentant l'indice de "en bas sur une ligne" dans {@link #tabNumPossible}*/
	private final int NUM_BS = 3;
	/** JSpinner numéroter à partir de...
	 *  JSpinner start numbering from...
	 */
	private JSpinner jspNumberingStartPage;
	/** JLabel for {@link #jspNumberingStartPage}*/
	private JLabel lJspNumberingStartPage = new JLabel(texts.getText("numberingStartPage"));
	/** JComboBox contenant les styles de gestion des lignes vides possibles*/
	private JComboBox<Integer> jcbLV;
	/** Tableau des gestions des lignes vides possibles */
	private final String [] tabLVPossible = {texts.getText("assource"),texts.getText("custom"),texts.getText("noempty"),texts.getText("sparse"),texts.getText("dense"), texts.getText("double")};
	/** Panneau masquable contenant les options personnalisées pour la gestion des lignes vides */
	private JPanel panLVPerso = new JPanel(); 
	/** Label pour le JSpinner 1 ligne en sortie*/
	private JLabel ljs1 = new JLabel(" "+texts.getText("emptylinesnumlabel"));
	/** Label pour le JSpinner 2 lignes en sortie*/
	private JLabel ljs2 = new JLabel(" "+texts.getText("emptylinesnum2label"));
	/** Label pour le JSpinner 3 lignes en sortie*/
	private JLabel ljs3 = new JLabel(" "+texts.getText("emptylinesnum3label"));
	/** Label pour le JSpinner saut de page*/
	private JLabel ljspb = new JLabel(" "+texts.getText("minspinlabel"));
	/* Nom du style pour la chimie 
	private JTextField jtfChimie = new JTextField();
	** Nom du style pour la poésie *
	private JTextField jtfPoesie = new JTextField();
	** Nom du style pour les note de transcripteur *
	private JTextField jtfNotesTr = new JTextField(); */
	/** Nom de la table des matières */
	private JTextField jtfIndexTableName = new JTextField();
	/** JCheckBox insertion d'une table des matières braille en fin de document */
	private JCheckBox jchbInsertIndex = new JCheckBox(texts.getText("insertIndex"));
	/** Bouton ouvrant le dialogue d'édition des styles */
	private JButton btEditStyle = new JButton(texts.getText("styleEdit"),new ImageIcon("ui/icon/gtk-edit.png"));
	/** la fenêtre appelante */
	private Configuration parent = null; 
	
	/**
	 * Constructeur de l'onglet Mise en page
	 * @param f la fenêtre configuration appelante, pour {@link #parent}
	 */
	public ConfMiseEnPage(Configuration f)
	{
		super();
		parent = f;
		// Works with NVDA. Doesn't work with JAWS
		getAccessibleContext().setAccessibleDescription(texts.getText("tabdesc"));
		getAccessibleContext().setAccessibleName(texts.getText("tabname"));

		/**********
		 * Préparation des composants
		 */
		
		/* Dimensions de la page */
		jsLongLigne = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getLongueurLigne(), 10, 1000, 1));
		Context cJsLongLigne = new Context("b","Spinner","length",texts);
		new ContextualHelp(jsLongLigne,"confmepsize",cJsLongLigne);
		
		lLongLigne.setLabelFor(jsLongLigne);
		lLongLigne.setDisplayedMnemonic('b');
		
		jsNbLigne = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getNbLigne(), 0, 1000, 1));
		Context cJsNbLigne = new Context("m","Spinner","number",texts);
		new ContextualHelp(jsNbLigne,"confmepsize",cJsNbLigne);
		
		jsNbLigne.addChangeListener(this);
		
		lNbLigne.setLabelFor(jsNbLigne);
		lNbLigne.setDisplayedMnemonic('m');
		
		/* Numérotation */
		Integer[] numberingIntArray = new Integer[tabNumPossible.length];
        Context cJcbNumerotation = new Context("p","ComboBox","numtype",texts);
		for (int i = 0; i < tabNumPossible.length; i++) {numberingIntArray[i] = new Integer(i);}
        AccessibleListRenderer numberingRenderer= new AccessibleListRenderer
        								("numbers.png",
        								cJcbNumerotation.getName(),
        								cJcbNumerotation.getDesc(),
        								cJcbNumerotation.getTTT(),
        								tabNumPossible);
        jcbNumerotation = new JComboBox<Integer>(numberingIntArray);
        new ContextualHelp(jcbNumerotation,"confmepnumber",cJcbNumerotation);
		jcbNumerotation.setRenderer(numberingRenderer);
		jcbNumerotation.addItemListener(this);
		
		lNumerotation.setLabelFor(jcbNumerotation);
		lNumerotation.setDisplayedMnemonic('p');
		
		jspNumberingStartPage = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getNumberingStartPage(), 1, 10000, 1));
		Context cJspNumberingStartPage = new Context("e","Spinner","numberingStartPage",texts);
		new ContextualHelp(jspNumberingStartPage,"numberingStartPage",cJspNumberingStartPage);
		
		lJspNumberingStartPage.setLabelFor(jspNumberingStartPage);
		lJspNumberingStartPage.setDisplayedMnemonic('e');
		
		/*Context cJcbNumeroteFirst = new Context("e","CheckBox","numfirst",texts);
		new ContextualHelp(jchbNumeroteFirst,"confmepnumber",cJcbNumeroteFirst);
		jchbNumeroteFirst.setSelected(ConfigNat.getCurrentConfig().getNumeroteFirst());
		jchbNumeroteFirst.setMnemonic('e');*/
		
		String numStyle = ConfigNat.getCurrentConfig().getNumerotation();
		if(numStyle.equals("'nn'")){jcbNumerotation.setSelectedIndex(NUM_AUCUN);}
		else if(numStyle.equals("'hb'")){jcbNumerotation.setSelectedIndex(NUM_HB);}
		else if(numStyle.equals("'hs'")){jcbNumerotation.setSelectedIndex(NUM_HS);}
		else if(numStyle.equals("'bs'")){jcbNumerotation.setSelectedIndex(NUM_BS);}
		else if(numStyle.equals("'bb'")){jcbNumerotation.setSelectedIndex(NUM_BB);}
		
		/* lignes vides */
		
	    //gestion perso
		ljs1.setDisplayedMnemonic('v');
		//ljs2.setDisplayedMnemonic('v');
		//ljs3.setDisplayedMnemonic('p');
		
		jsMin1L = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMepMinLigne1(), 1, 40, 1));
		ljs1.setLabelFor(jsMin1L);
		Context cJsMin1L = new Context("v","Spinner","emptylinesnum",texts);
		new ContextualHelp(jsMin1L,"confmepblanklines",cJsMin1L);
		jsMin1L.addChangeListener(this);
	    
	    jsMin2L = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMepMinLigne2(), 2, 40, 1));
	    ljs2.setLabelFor(jsMin2L);
		Context cJsMin2L = new Context(texts.getText("emptylinesnum2key"),"Spinner","emptylinesnum2",texts);
		new ContextualHelp(jsMin2L,"confmepblanklines",cJsMin2L);
	    jsMin2L.addChangeListener(this);
	    
	    jsMin3L = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMepMinLigne3(), 3, 40, 1));
	    ljs3.setLabelFor(jsMin3L);
		Context cJsMin3L = new Context(texts.getText("emptylinesnum3key"),"Spinner","emptylinesnum3",texts);
		new ContextualHelp(jsMin3L,"confmepblanklines",cJsMin3L);
	    jsMin3L.addChangeListener(this);
	    
		Context cJchMinPageBreak = new Context("u","CheckBox","minpbreak",texts);
		new ContextualHelp(jchMinPageBreak,"guiOptionsMiseEnPage",cJchMinPageBreak);
	    jchMinPageBreak.setSelected(ConfigNat.getCurrentConfig().getGeneratePB());
	    jchMinPageBreak.setMnemonic('u');
	    jchMinPageBreak.addItemListener(this);
	    
		Context cJchKeepPageBreak = new Context("o","CheckBox","keeppbreak",texts);
		new ContextualHelp(jchKeepPageBreak,"guiOptionsMiseEnPage",cJchKeepPageBreak);
		jchKeepPageBreak.setMnemonic('o');
		jchKeepPageBreak.setSelected(ConfigNat.getCurrentConfig().getKeepPageBreak());
	    //jchKeepPageBreak.addItemListener(this);
	    
	    jsMinPageBreak = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMepMinLignePB(), 4, 40, 1));
	    ljspb.setLabelFor(jsMinPageBreak);
	    Context cJsMinPageBreak = new Context(texts.getText("minpbreakkey"),"Spinner","minspin",texts);
	    new ContextualHelp(jsMinPageBreak,"guiOptionsMiseEnPage",cJsMinPageBreak);
		jsMinPageBreak.addChangeListener(this);
	    jsMinPageBreak.setEnabled(jchMinPageBreak.isSelected());
	    
	    //option des lv
	    Integer[] cbLVIntArray = new Integer[tabLVPossible.length];
	    Context cJcbLV = new Context("l","ComboBox","emptylines",texts);
		for (int i = 0; i < tabLVPossible.length; i++) {cbLVIntArray[i] = new Integer(i);}
        AccessibleListRenderer cbLVRenderer= new AccessibleListRenderer
        								("blank-lines.png",
        								cJcbLV.getName(),
        								cJcbLV.getDesc(),
        								cJcbLV.getTTT(),
        								tabLVPossible);
		jcbLV = new JComboBox<Integer>(cbLVIntArray);
		jcbLV.setRenderer(cbLVRenderer);
		new ContextualHelp(jcbLV,"confmepblanklines",cJcbLV);
		jcbLV.addItemListener(this);
		jcbLV.setSelectedIndex(ConfigNat.getCurrentConfig().getMepModelignes());
		JLabel lLignesVides = new JLabel(texts.getText("emptylineslabel"));
		lLignesVides.setDisplayedMnemonic('l');
		lLignesVides.setLabelFor(jcbLV);
	    
		// il a pas l'air d'être utilisé...
		/*
	    jchbSautFin.setSelected(ConfigNat.getCurrentConfig().getSautPageFin());
	    Context cJchbSautFin = new Context("f","CheckBox","finalpb",texts);
	    new ContextualHelp(jchbSautFin,"confmepblanklines",cJchbSautFin);
		jchbSautFin.setMnemonic('f');
		*/
	    
	    /* Options de mise en forme */
	    /*JLabel ljsRetraitDebLigne = new JLabel("Nombre d'espaces en début de paragraphe:");
	    ljsRetraitDebLigne.setDisplayedMnemonic('e');
		
	    jsRetraitDebLigne = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMepRetraitPar(), 0, 10, 1));
	    ljsRetraitDebLigne.setLabelFor(jsRetraitDebLigne);
	    jsRetraitDebLigne.getAccessibleContext().setAccessibleName("Liste déroulante taille du retrait en début de paragraphe");
	    jsRetraitDebLigne.getAccessibleContext().setAccessibleDescription("Sélectionner avec les flèches le nombre d'espace à insérer en début de paragraphe");
	    jsRetraitDebLigne.setToolTipText("nombre d'espaces à insérer en début de paragraphe");
	    */
				
	    jchbLineariseTable.setSelected(ConfigNat.getCurrentConfig().getLineariseTable());
	    Context cJchbLineariseTable = new Context("t","CheckBox","linetable",texts);
	    new ContextualHelp(jchbLineariseTable,"confmep2d",cJchbLineariseTable);
	    jchbLineariseTable.setMnemonic('t');
	    jchbLineariseTable.addItemListener(this);
	    
	    //A mettre après jchbLineariseTable.addItemListener(this);
	    ljsMinLin.setDisplayedMnemonic('d');
	    ljsMinLin.setEnabled(!jchbLineariseTable.isSelected());
		// A mettre après jchbLineariseTable.addItemListener(this);
		jsMinCellLin = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMinCellLin(), 0, 30, 1));
		ljsMinLin.setLabelFor(jsMinCellLin);
	    Context cJsMinCellLin = new Context("d","Spinner","minline",texts);
	    new ContextualHelp(jsMinCellLin,"confmep2d",cJsMinCellLin);
	    jsMinCellLin.setEnabled(!jchbLineariseTable.isSelected());
		
		/* tous ces champs ne servent plus, commentés pour l'instant
	    JLabel lPoesie = new JLabel(texts.getText("poetryStyleName"));
		lPoesie.setLabelFor(jtfPoesie);
		lPoesie.setDisplayedMnemonic('é');
		jtfPoesie.setText(ConfigNat.getCurrentConfig().getStylePoesie());
		
		JLabel lChimie = new JLabel(texts.getText("chemistryStyleName"));
		lChimie.setLabelFor(jtfChimie);
		lChimie.setDisplayedMnemonic('h');
		jtfChimie.setText(ConfigNat.getCurrentConfig().getStyleChimie());
		
		JLabel lNoteTr = new JLabel(texts.getText("transcriberStyleName"));
		lNoteTr.setLabelFor(jtfNotesTr);
		lNoteTr.setDisplayedMnemonic('é');
		jtfNotesTr.setText(ConfigNat.getCurrentConfig().getStyleNoteTr()); */
		
		JLabel lIndex = new JLabel(texts.getText("indexTableName"));
		lIndex.setLabelFor(jtfIndexTableName);
		lIndex.setDisplayedMnemonic('r');
		jtfIndexTableName.setText(ConfigNat.getCurrentConfig().getIndexTableName());
	    
		jchbInsertIndex.setSelected(ConfigNat.getCurrentConfig().getIndexTable());
	    Context cJchbInsertIndex = new Context("i","CheckBox","insertIndex",texts);
	    new ContextualHelp(jchbInsertIndex,"insertIndex",cJchbInsertIndex);
	    jchbInsertIndex.setMnemonic('i');
	    jchbInsertIndex.addItemListener(this);
	    
	    btEditStyle.addActionListener(this);
		btEditStyle.addItemListener(this);
		Context cBtEditStyle = new Context("y","Button","styleEdit",texts);
		btEditStyle.setMnemonic('y');
		new ContextualHelp(btEditStyle,"styleEdit",cBtEditStyle);
		
		jchbPreserveHyphen.setSelected(ConfigNat.getCurrentConfig().getPreserveHyphen());
		jchbPreserveHyphen.setMnemonic('q');
	    Context cJchbPreserveHyphen = new Context("q","CheckBox","preservehyphen",texts);
	    new ContextualHelp(jchbPreserveHyphen,"preservehyphen",cJchbPreserveHyphen);
	    
	    jchbPreservePageBreak.setSelected(ConfigNat.getCurrentConfig().getPreservePageBreakConv());
	    jchbPreservePageBreak.setMnemonic('c');
	    Context cJchbPreservePageBreak = new Context("c","CheckBox","preservepagebreak",texts);
	    new ContextualHelp(jchbPreservePageBreak,"preservepagebreak",cJchbPreservePageBreak);
		/*********
		 * Mise en page
		 */
		
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		JPanel panTr = new JPanel();
		panTr.setLayout(gbl);
		
		JLabel titre = new JLabel(texts.getText("title1"));	
		titre.getAccessibleContext().setAccessibleName(texts.getText("title1name"));
		
		gbc.anchor = GridBagConstraints.WEST;
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth=3;
		gbl.setConstraints(titre, gbc);
		panTr.add(titre);
		
		JLabel titre2 = new JLabel(texts.getText("title2"));
		titre2.getAccessibleContext().setAccessibleName(texts.getText("title2name"));
		gbc.gridy++;
		gbl.setConstraints(titre2, gbc);
		panTr.add(titre2);
		
		gbc.gridy++;
		gbc.gridx++;
		gbc.gridwidth=2;
		gbl.setConstraints(lLongLigne, gbc);
		panTr.add(lLongLigne);
		
		gbc.gridx+=2;
		gbc.gridwidth=1;
		gbl.setConstraints(jsLongLigne, gbc);
		panTr.add(jsLongLigne);
		
		gbc.gridx=1;
		gbc.gridy++;
		gbc.gridwidth=2;
		gbl.setConstraints(lNbLigne, gbc);
		panTr.add(lNbLigne);
		
		gbc.gridx+=2;
		gbc.gridwidth=1;
		gbl.setConstraints(jsNbLigne, gbc);
		panTr.add(jsNbLigne);
		
		//Numérotation
		gbc.gridwidth=3;
		gbc.gridx=0;
		gbc.gridy++;
		gbl.setConstraints(lNumerotation, gbc);
		panTr.add(lNumerotation);
		
		gbc.gridwidth=1;
		gbc.gridx+=3;
		gbl.setConstraints(jcbNumerotation, gbc);
		panTr.add(jcbNumerotation);
		
		gbc.gridwidth=2;
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(lJspNumberingStartPage, gbc);
		panTr.add(lJspNumberingStartPage);
		gbc.gridx+=2;
		gbc.gridwidth=1;
		panTr.add(jspNumberingStartPage,gbc);
		
		//lignes vides
		gbc.gridwidth=3;
		gbc.gridx=0;
		gbc.gridy++;
		gbl.setConstraints(lLignesVides, gbc);
		panTr.add(lLignesVides);
		
		gbc.gridwidth=1;
		gbc.gridx+=3;
		gbl.setConstraints(jcbLV, gbc);
		panTr.add(jcbLV);
		
		GridBagConstraints gbcLV = new GridBagConstraints();
		//gbcLV.insets = new Insets(3,3,3,3);
		GridBagLayout glLV = new GridBagLayout();
		
		panLVPerso.setLayout(glLV);
		
		gbcLV.gridx = 0;
		gbcLV.gridy = 0;
		gbcLV.gridwidth=1;
		panLVPerso.add(jsMin1L,gbcLV);
		gbcLV.gridy++;
		panLVPerso.add(jsMin2L,gbcLV);
		gbcLV.gridy++;
		panLVPerso.add(jsMin3L,gbcLV);
		
		gbcLV.anchor = GridBagConstraints.WEST;
		gbcLV.gridy=0;
		gbcLV.gridx++;
		gbcLV.gridwidth=2;
		panLVPerso.add(ljs1,gbcLV);
		gbcLV.gridy++;
		panLVPerso.add(ljs2,gbcLV);
		gbcLV.gridy++;
		panLVPerso.add(ljs3,gbcLV);
		
		gbc.gridwidth=3;
		gbc.gridheight=3;
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(panLVPerso, gbc);
		panTr.add(panLVPerso);
		
		//panneau pour la ligne page break
		JPanel pMPB = new JPanel();
		pMPB.add(jsMinPageBreak);
		pMPB.add(ljspb);
		
		gbc.gridwidth=2;
		gbc.gridheight=1;
		gbc.gridy +=4;
		gbc.gridx=1;
		
		panTr.add(jchKeepPageBreak,gbc);
		
		gbc.gridy++;
		gbc.gridwidth=2;
		panTr.add(jchMinPageBreak,gbc);
		
		gbc.gridx+=2;
		panTr.add(pMPB,gbc);
		
		//mise en forme
		JLabel titre4 = new JLabel(texts.getText("title4"));
		titre4.getAccessibleContext().setAccessibleName(texts.getText("title4name"));
		gbc.gridwidth=3;
		gbc.gridx=0;
		gbc.gridy++;
		gbc.gridheight=1;
		gbl.setConstraints(titre4, gbc);
		panTr.add(titre4);
		
		/*gbc.gridy++;
		gbc.gridx++;
		gbc.gridwidth=2;
		gbl.setConstraints(ljsRetraitDebLigne, gbc);
		panTr.add(ljsRetraitDebLigne);
		
		gbc.gridx=gbc.gridx+2;
		gbc.gridwidth=1;
		gbl.setConstraints(jsRetraitDebLigne, gbc);
		panTr.add(jsRetraitDebLigne);*/
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth=3;
		gbl.setConstraints(jchbLineariseTable, gbc);
		panTr.add(jchbLineariseTable);

		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth=2;
		gbl.setConstraints(ljsMinLin, gbc);
		panTr.add(ljsMinLin);
		gbc.gridwidth=1;
		gbc.gridx=3;
		gbl.setConstraints(jsMinCellLin, gbc);
		panTr.add(jsMinCellLin);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth=3;
		gbl.setConstraints(jchbInsertIndex, gbc);
		panTr.add(jchbInsertIndex);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth=2;
		gbl.setConstraints(lIndex, gbc);
		panTr.add(lIndex);
		gbc.gridwidth=1;
		gbc.gridx=3;
		gbl.setConstraints(jtfIndexTableName, gbc);
		panTr.add(jtfIndexTableName);
		
		gbc.gridy++;
		gbc.gridx=0;
		JLabel titre5 = new JLabel(texts.getText("title5"));
		titre5.getAccessibleContext().setAccessibleName(texts.getText("title5name"));
		panTr.add(titre5,gbc);
		
		gbc.gridx+=3;
		panTr.add(btEditStyle,gbc);
		
		/* tous ces champs ne servent plus, commentés pour l'instant
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth = 2;
		gbl.setConstraints(lPoesie, gbc);
		panTr.add(lPoesie);
		gbc.gridx+=2;
		gbc.gridwidth = 1;
		gbl.setConstraints(jtfPoesie, gbc);
		panTr.add(jtfPoesie);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth = 2;
		gbl.setConstraints(lChimie, gbc);
		panTr.add(lChimie);
		gbc.gridx+=2;
		gbc.gridwidth = 1;
		gbl.setConstraints(jtfChimie, gbc);
		panTr.add(jtfChimie);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth = 2;
		gbl.setConstraints(lNoteTr, gbc);
		panTr.add(lNoteTr);
		gbc.gridx+=2;
		gbc.gridwidth = 1;
		gbl.setConstraints(jtfNotesTr, gbc);
		panTr.add(jtfNotesTr); */
		
		gbc.gridy++;
		gbc.gridx=0;
		gbc.gridwidth=3;
		JLabel titre6 = new JLabel(texts.getText("title6"));
		titre6.getAccessibleContext().setAccessibleName(texts.getText("title6name"));
		panTr.add(titre6,gbc);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth=3;
		gbl.setConstraints(jchbPreserveHyphen, gbc);
		panTr.add(jchbPreserveHyphen);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(jchbPreservePageBreak, gbc);
		panTr.add(jchbPreservePageBreak);
		
		/*gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth=3;
		gbl.setConstraints(jchbSautFin, gbc);
		panTr.add(jchbSautFin);*/
		
		setLayout(new BorderLayout());
		add(panTr, BorderLayout.NORTH);
		
		//MAJ 
		changeLignes();
	}	

	/**
	 * Méthode redéfinie de ItemListener
	 * Masque les éléments utilisés dans la linéarisation suivant l'état de jchbLineariseTable
	 * Appel changeLignes() si la source est le combobox des lignes vides
	 * @param ie l'instance d'Itemevent
	 * @see #changeLignes()
	 */
	@Override
    public void itemStateChanged(ItemEvent ie) 
	{
		if(ie.getSource()==jcbLV){changeLignes();}
		
		//pour la linéarisation
		if(ie.getSource()==jchbLineariseTable)
		{
			if(!jchbLineariseTable.isSelected())
			{
				jsMinCellLin.setEnabled(true);
				ljsMinLin.setEnabled(true);
			}
			else
			{
				jsMinCellLin.setEnabled(false);
				ljsMinLin.setEnabled(false);
			}
		}
		
		// pour la numerotation des pages
		if(ie.getSource()==jcbNumerotation)
		{
			boolean activated = !(jcbNumerotation.getSelectedIndex()==NUM_AUCUN);
			jspNumberingStartPage.setEnabled(activated);
			lJspNumberingStartPage.setEnabled(activated);
		}
		
		// pour la génération de sauts de pages
		if (ie.getSource()==jchMinPageBreak)
		{
			jsMinPageBreak.setEnabled(jchMinPageBreak.isSelected());
		}
	}
	
	/**
	 * Active ou non les options avancées conernant les lignes vides, suivant que personnalisé est choisi ou non
	 *
	 */
	private void changeLignes()
	{
		if(jcbLV.getSelectedIndex()!=1)
		{
			jsMin1L.setEnabled(false);
			ljs1.setEnabled(false);
			jsMin2L.setEnabled(false);
			ljs2.setEnabled(false);
			jsMin3L.setEnabled(false);
			ljs3.setEnabled(false);
		}
		else
		{
			jsMin1L.setEnabled(true);
			ljs1.setEnabled(true);
			jsMin2L.setEnabled(true);
			ljs2.setEnabled(true);
			jsMin3L.setEnabled(true);
			ljs3.setEnabled(true);
		}
	}
	
	/**
	 * Redéfinie de ChangeListener
	 * Vérifie la cohérence des valeurs des JSpinner des lignes vides
	 * Permet 0 ou 10 pour lignes par page également
	 * @param ce ChangeEvent généré
	 */
	@Override
    public void stateChanged(ChangeEvent ce)
	{
		int v1 = ((Integer)jsMin1L.getValue()).intValue();
		int v2 = ((Integer)jsMin2L.getValue()).intValue();
		int v3 = ((Integer)jsMin3L.getValue()).intValue();
		int vpb = ((Integer)jsMinPageBreak.getValue()).intValue();
		
		Object source = ce.getSource();
		
		if (source== jsNbLigne)
			{
			int vnbl = ((Integer)jsNbLigne.getValue()).intValue();
			if (vnbl==9){jsNbLigne.setValue(0);}
			else if (vnbl==1){jsNbLigne.setValue(10);}
			}
		if (source == jsMin1L)
			{if(!(v1<v2)){jsMin2L.setValue(new Integer(v1+1));}}
		if (source == jsMin1L || source == jsMin2L)
			{if(!(v2<v3)){jsMin3L.setValue(new Integer(v2+1));}}
		if (source == jsMin1L || source == jsMin2L || source == jsMin3L)
			{if(!(v3<vpb)){jsMinPageBreak.setValue(new Integer(v3+1));}}
		
		if(v1>1)
		{
			if (source == jsMinPageBreak)
				{if(!(v3<vpb)){jsMin3L.setValue(new Integer(vpb-1));}}
			if (source == jsMinPageBreak || source == jsMin3L)
				{if(!(v2<v3)){jsMin2L.setValue(new Integer(v3-1));}}
			if (source == jsMinPageBreak || source == jsMin3L || source == jsMin2L)
				{if(!(v1<v2)){jsMin1L.setValue(new Integer(v2-1));}}
			
		}
	}
	
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	@Override
    public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer()
	 */
	@Override
    public boolean enregistrer()
	{
		boolean retour = true;
		ConfigNat.getCurrentConfig().setMepModelignes(jcbLV.getSelectedIndex());
		ConfigNat.getCurrentConfig().setMepMinLigne1(((Integer)jsMin1L.getValue()).intValue());
		ConfigNat.getCurrentConfig().setMepMinLigne2(((Integer)jsMin2L.getValue()).intValue());
		ConfigNat.getCurrentConfig().setMepMinLigne3(((Integer)jsMin3L.getValue()).intValue());
		ConfigNat.getCurrentConfig().setMepMinLignePB(((Integer)jsMinPageBreak.getValue()).intValue());
		ConfigNat.getCurrentConfig().setKeepPageBreak(jchKeepPageBreak.isSelected());
		ConfigNat.getCurrentConfig().setGeneratePB(jchMinPageBreak.isSelected());
		ConfigNat.getCurrentConfig().setLongueurLigne(((Integer)jsLongLigne.getValue()).intValue());
		ConfigNat.getCurrentConfig().setNbLigne(((Integer)jsNbLigne.getValue()).intValue());
		//ConfigNat.getCurrentConfig().setSautPageFin(jchbSautFin.isSelected());
		//ConfigNat.getCurrentConfig().setMepRetraitPar(((Integer)jsRetraitDebLigne.getValue()).intValue());
		ConfigNat.getCurrentConfig().setLineariseTable(jchbLineariseTable.isSelected());
		//ConfigNat.getCurrentConfig().sauvegarder();
		ConfigNat.getCurrentConfig().setMinCellLin(((Integer)jsMinCellLin.getValue()).intValue());
		switch (jcbNumerotation.getSelectedIndex())
		{
			case NUM_AUCUN:
			{
				ConfigNat.getCurrentConfig().setNumerotation("'nn'");
				break;
			}
			case NUM_HS:
			{
				ConfigNat.getCurrentConfig().setNumerotation("'hs'");
				break;
			}
			case NUM_HB:
			{
				ConfigNat.getCurrentConfig().setNumerotation("'hb'");
				break;
			}
			case NUM_BS:
			{
				ConfigNat.getCurrentConfig().setNumerotation("'bs'");
				break;
			}
			case NUM_BB:
			{
				ConfigNat.getCurrentConfig().setNumerotation("'bb'");
				break;
			}	
		}
		ConfigNat.getCurrentConfig().setNumberingStartPage(((Integer)jspNumberingStartPage.getValue()).intValue());
		/*ConfigNat.getCurrentConfig().setStyleChimie(jtfChimie.getText());
		ConfigNat.getCurrentConfig().setStylePoesie(jtfPoesie.getText());
		ConfigNat.getCurrentConfig().setStyleNoteTr(jtfNotesTr.getText());*/
		ConfigNat.getCurrentConfig().setIndexTable(jchbInsertIndex.isSelected());
		ConfigNat.getCurrentConfig().setIndexTableName(jtfIndexTableName.getText());
		
		ConfigNat.getCurrentConfig().setPreserveHyphen(jchbPreserveHyphen.isSelected());
		ConfigNat.getCurrentConfig().setPreservePageBreakConv(jchbPreservePageBreak.isSelected());
		
		ConfigNat.getCurrentConfig().sauvegarder();
		return retour;
	}

	/**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent ae)
    {
    	if(ae.getSource()==btEditStyle){ConfStyle.showDialog(parent);}
    	/* C'est fait. Je laisse pour l'instant
    	 * en attendant que ce soit interfacé on ouvre le fichier
    	 * avec l'éditeur par défaut 
    	 * et plus tard : {ConfStyle.showDialog(parent, texts, ConfigNat.getCurrentConfig().getStylist());}
    	 */
    	
    		/* Ouverture avec l'éditeur texte par défaut du système*/
    		/*Desktop desktop = null;
	        if (Desktop.isDesktopSupported())
	        {
	        	desktop = Desktop.getDesktop();
	        	if (desktop.isSupported(Desktop.Action.OPEN))
	        	{
	        		String stylistFile = ConfigNat.getCurrentConfig().getStylist();
	        		try{desktop.open(new File(stylistFile));}
					catch (IOException e) {parent.getGestErreur().afficheMessage(texts.getText("buginout")+ stylistFile, Nat.LOG_SILENCIEUX);}
	        	}
	        	else{parent.getGestErreur().afficheMessage(texts.getText("editornotfound"), Nat.LOG_SILENCIEUX);}
	
	        }
	        else{parent.getGestErreur().afficheMessage(texts.getText("desktopnotsupported"), Nat.LOG_SILENCIEUX);}*/
	        
	        
    	
    }
}
