/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.config;

import gestionnaires.GestionnaireMajTabBraille;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ContainerListener;
import java.awt.event.ContainerEvent;

import nat.ConfigNat;
import nat.Nat;
import nat.Scenario;
/**
 * Cette classe de l'interface graphique code la fenêtre permettant l'édition de la table braille
 * @author bruno
 */
public class PropagateFrame extends JFrame implements ActionListener 
{
	/** Textual contents */
	private static Language texts = new Language("ConfTableBraille");
	
	/** Pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;
	/** La table d'affichage */
	private JTable table;
	/** Nom de la table en cours */
	private JLabel nomTable=new JLabel("");
	/** JScrollPane pour la table  */
	private JScrollPane scrollTable;
	/** JButton launch the propagation process */
	private JButton btApply = new JButton(texts.getText("save"),new ImageIcon("ui/icon/document-save.png"));
	/** JButton pour fermer la fenêtre */
	private JButton btCancel = new JButton(texts.getText("cancel"),new ImageIcon("ui/icon/exit.png"));
	/** Nom des colonnes de la table */
	private String[] lesColonnes = new String[]{texts.getText("colProperty"),
									texts.getText("colPropagate")};
	/** Tableau pour les données de la table */
	private String[][] donnees;
	
	/**
	 * Constructeur
	 * @param scen the Scenario instance
	 */
	public PropagateFrame(Scenario scen)
	{
		super(texts.getText("title"));
		
		loadProperties();
		table = new JTable(donnees,lesColonnes);
		// permettre le tri sur les colonnes
		//table.setAutoCreateRowSorter(true);
		//bug de sun pour la maj de la table
		table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		
		scrollTable = new JScrollPane (table);
		scrollTable.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollTable.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollTable.setPreferredSize(new Dimension(300,350));
		
		JPanel pTable = new JPanel();
		pTable.add(scrollTable);
		pTable.setPreferredSize(new Dimension(350,400));
		
		JPanel lesBoutons = new JPanel();
		btApply.addActionListener(this);
		
		Context cbtApply = new Context("a","Button","apply",texts);
		new ContextualHelp(btApply,cbtApply);
		btApply.setMnemonic('a');
		
		btCancel.addActionListener(this);
		Context cbtCancel = new Context("c","Button","cancel",texts);
		new ContextualHelp(btCancel,cbtCancel);
		btCancel.setMnemonic('c');		

		ContextualHelp tableCH = new ContextualHelp(table);
		tableCH.addContext(cbtApply, false);
		String[] tableLabels = {texts.getText("columnbraille"),texts.getText("columnchar")};
		String[] tableKeys = {"columnbraille","columnchar"};
		tableCH.addLabels(tableLabels, tableKeys);
		
		lesBoutons.add(btApply);
		lesBoutons.add(btCancel);
		JPanel entete = new JPanel();
		entete.add(nomTable);
		
		//setLayout(new BorderLayout(5,5));
		add(BorderLayout.NORTH, entete);
		add(BorderLayout.CENTER,pTable);
		add(BorderLayout.SOUTH,lesBoutons);
		setSize(500,500);
	}
	/**
	 * Méthode implémentée d'ActionListener
	 * Gère les évènements des boutons
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
    public void actionPerformed(ActionEvent evt)
	{
		if (evt.getSource()==btCancel){this.dispose();}
		else if (evt.getSource()==btApply){apply();}
	}
	/**
	 * 
	 */
    private void apply()
    {
	    // TODO Auto-generated method stub
	    
    }

	
	
	/**
	 * Charge les données du fichier fichierTable dans le tableau des données pour la table.
	 * @return true si fichier valide, false sinon
	 */
	private boolean loadProperties()
	{
	    boolean retour = true;
		
		return retour;
	}
}
