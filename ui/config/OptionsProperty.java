/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui.config;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import nat.OptDef;
import ui.accessibility.Language;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * This class provides an OptionProperty definition
 * @author Vivien, comments added by Bruno
 *
 */
public class OptionsProperty
{

	/** Language instance providing texts */
	protected static Language texts = OptionsPropertyLanguage.getLanguage();
	/** the OptDef instance */
	protected OptDef optDef = null;
	/** the main JPanel */
	protected JPanel panel = new JPanel();
	/** The JTextPane containing the options */
	protected JTextPane labelName = new JTextPane();

	
	/**
	 * Constructor
	 * @param od option definition
	 */
	public OptionsProperty(OptDef od)
	{
		optDef = od;
		try {
		buildPanel();
		// TODO : catch
		} catch (Exception e)
		{		
			e.printStackTrace();
		}
		
		//	addCompo();
	}
	
	/**
	 * build the JPanel {@link #panel}
	 */
	public void buildPanel()
	{
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		//panel.setBorder(BorderFactory.createLineBorder(Color.black));
		//panel.setBackground(Color.orange);    	
		panel.add(new JSeparator(SwingConstants.HORIZONTAL));
		StyledDocument sDoc = (StyledDocument)labelName.getDocument();
		labelName.setEditable(false);


		Style defaut = labelName.getStyle("default");

		try {
			Style style1 = labelName.addStyle("style1", defaut);			
			StyleConstants.setFontSize(style1, 20);			
			sDoc.insertString(0,texts.getText(optDef.getId()) ,style1);
		}
		catch (BadLocationException e){
			System.err.println("Error during panel construction");
			e.printStackTrace();
		}
/*
		labelName.setText("<p>"+texts.getText(optDef.getId())
				+"</p> : "
				+texts.getText(optDef.getDescription()));
				*/
		panel.add(labelName); 	
	}
	//    public void addCompo(){}
	

	/**
	 * Getter for {@link #panel}
	 * @return {@link #panel}
	 */
	public JPanel getPanel(){return panel;}
	/**
	 * Getter for {@link #optDef}
	 * @return {@link #optDef}
	 */
	public OptDef getOptDef(){return optDef;}

	/**
	 * setter for value, do nothing
	 * @param value the value
	 * TODO not used
	 */
	public void setValue(String value){/*todo*/}
	/**
	 * getter for the value 
	 * @return textfield text
	 */
	public String getValue(){return "textfieldValue.getText()";}

}