/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ui.config;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ListIterator;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import nat.OptDef;
import nat.OptDefs;
import ui.accessibility.Language;

/**
 * Fenêtre d'options paramétrée
 * @author bruno
 * @since 2.1
 */
public class OptionsFrame extends JFrame implements ActionListener
{

	/** For serialisation, not used*/
	private static final long serialVersionUID = 1L;
	/** Textual contents */
	private Language texts = new Language("Configuration");
	/** the properties panel */
	private JPanel panProperties = new JPanel();
	/** the scrollpane*/
	private JScrollPane scrollPanProperties = new JScrollPane(panProperties,
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	/** Options properties array list*/
	private ArrayList<OptionsProperty> optionsProperties = new ArrayList<OptionsProperty>();
	/** list of filters names*/
	private JComboBox<String> comboOptionsFilter = new JComboBox<String>(new String[]{ 
			"detrans_basic", "detrans_advanced", 
			"mep_basic", "mep_advanced",
			"io_basic", "io_advanced",
			"ui_basic", "ui_advanced",
			"system_basic", "system_advanced",
			"no_class","other_basic"
	});

	/** save button */
	private JButton btSave = new JButton(texts.getText("save"),
			new ImageIcon("ui/icon/document-save.png"));
	/** save as button */
	private JButton btSaveAs = new JButton(texts.getText("saveas"),
			new ImageIcon("ui/icon/document-save-as.png"));
	/** cancel button */
	private JButton btCancel= new JButton(texts.getText("cancel"),
			new ImageIcon("ui/icon/edit-undo.png"));

	/**
	 * Load and show an option frame
	 * @param od the property definition
	 * @return the property type
	 */

	private OptionsProperty optionPropertyFactory(OptDef od){
		String rendererName = od.getGuiRendererName();
		OptionsProperty op = null;
		if (rendererName.equals("stringRenderer")){
			op = new OptionsPropertyString(od);
		} else if (rendererName.equals("booleanRenderer")){
			op = new OptionsPropertyBoolean(od);
		} else if (rendererName.equals("staticComboRenderer")){
			op = new OptionsPropertyStaticCombo(od);
		}  else {
			op = new OptionsProperty(od);				
		}
		return op;
	}
	/**
	 * populates the property panel
	 */
	private void populatePanProperties(){
		panProperties.setLayout(new BoxLayout(panProperties, BoxLayout.Y_AXIS));
		//panProperties.setPreferredSize(new Dimension(400, 600));	
		Enumeration<String> allIds = OptDefs.getOptDefsIds();
		while (allIds.hasMoreElements()) {
			String id = allIds.nextElement();
			OptionsProperty oneProperty = optionPropertyFactory(OptDefs.getOptDefById(id));
			JPanel onePropertyPanel = oneProperty.getPanel();
			optionsProperties.add(oneProperty);
			panProperties.add(onePropertyPanel);
		}
		
	Dimension minSize = new Dimension(0, 0);
	Dimension prefSize = new Dimension(5, 5);
	Dimension maxSize = new Dimension(Short.MAX_VALUE, Short.MAX_VALUE);
	panProperties.add(new Box.Filler(minSize, prefSize, maxSize));
	
	}
	/**
	 * constructor
	 * @param show true if options are displayed
	 */
	public OptionsFrame(String show)
	{
		populatePanProperties();
		setDisplayedOptions(show);
		buildFrame();
		
	}
	/**
	 * Build the frame and show it
	 */
	private void buildFrame()	
	{      	
		setTitle("400,600");
		refreshFrame();
		pack();
		setVisible(true);
	}
	/*
	 * Save the panel options, not implemented
	 *
	private void savePanel(){
		

	}*/
	/**
	 * Refresh the frame components
	 */
	private void refreshFrame()
	{
		JPanel p = new JPanel();         
		//	p.setLayout(new BoxLayout());          
//		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		GridBagConstraints cp = new GridBagConstraints();
		p.setLayout(new GridBagLayout());
		cp.insets=new Insets(3,3,3,3);
		cp.gridx=0;
		cp.gridy=0;			
		cp.anchor = GridBagConstraints.PAGE_END;
		p.add(comboOptionsFilter,cp);
		
		comboOptionsFilter.setRenderer(new OptionsFilterComboBoxRenderer());
		comboOptionsFilter.addActionListener(this);

		cp.gridy=1;
		cp.weighty = 1.0;
		p.add(scrollPanProperties,cp);


		GridBagConstraints cPanSave = new GridBagConstraints();
		JPanel panSave = new JPanel(new GridBagLayout());
		cPanSave.insets=new Insets(3,3,3,3);
		cPanSave.gridx=0;
		cPanSave.gridy=0;
		cPanSave.fill=GridBagConstraints.HORIZONTAL;
		panSave.add(btSave,cPanSave);
		cPanSave.gridx++;
		panSave.add(btSaveAs,cPanSave);
		cPanSave.gridx++;
		panSave.add(btCancel,cPanSave);  
		btSave.addActionListener(this);
		btSaveAs.addActionListener(this);
//		panSave.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		
		cp.gridy=2;
		cp.weighty = 0.0;
		cp.anchor = GridBagConstraints.PAGE_END;
		p.add(panSave,cp);    	



		setContentPane(p);
		//      	p.repaint();
	}

	/**
	 * for test purposes only
	 * @param a params, not used
	 */
	public static void main(String [] a)
	{
		JFrame f = new OptionsFrame("detrans_basic");

		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	/**
	 * display the options of the category
	 * @param category the category
	 */
	private void setDisplayedOptions(String category){
		
			ListIterator<OptionsProperty> it = optionsProperties.listIterator();

		while (it.hasNext()){
			OptionsProperty op =  it.next();
			if (op.getOptDef().getCclass().equals(category)){
				op.getPanel().setVisible(true);
			} else {
				op.getPanel().setVisible(false);
			}
			System.out.println(op.getOptDef().getId()+":"+op.getValue());
		}
		
	}
	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
    public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btSave){	       
			ListIterator<OptionsProperty> it = optionsProperties.listIterator();
			while (it.hasNext()){
				OptionsProperty op = it.next();
				System.out.println(op.getOptDef().getId()+":"+op.getValue());
			}
		} else if (e.getSource() == comboOptionsFilter){
/*			System.out.println(e);
			ListIterator<OptionsProperty> it = optionsProperties.listIterator();
	*/
			Object sel = comboOptionsFilter.getSelectedItem();	
			setDisplayedOptions(sel.toString());		
			/*
			while (it.hasNext()){
				OptionsProperty op =  it.next();
				if (op.getOptDef().getCclass().equals(sel.toString())){
					op.getPanel().setVisible(true);
				} else {
					op.getPanel().setVisible(false);
				}
				System.out.println(op.getOptDef().getId()+":"+op.getValue());
			}
*/
		} else {
			System.out.println("àààà");
			pack();
		}
	}
}
