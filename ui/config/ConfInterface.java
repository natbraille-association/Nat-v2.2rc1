/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui.config;

import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import nat.ConfigNat;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import ui.accessibility.AccessibleListRenderer;
import ui.accessibility.AccessibleListStringRenderer;
import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;
/**
* Onglet de configuration de l'interface graphique
* @see OngletConf
* @author bruno
*
*/
public class ConfInterface extends OngletConf implements ItemListener, ActionListener
{
	/** Textual contents */
	private Language texts = new Language("ConfInterface");
	
	/** pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;
	/** Constante d'accès représentant l'indice de "éditeur intégré" dans {@link #jcbEditeur}*/
	private final int EDITEUR_NAT = 0;
	/** Constante d'accès représentant l'indice de "éditeur par défaut" dans {@link #jcbEditeur}*/
	private final int EDITEUR_DEFAUT = 1;
	/** Constante d'accès représentant l'indice de "autre application" dans {@link #jcbEditeur}*/
	private final int EDITEUR_AUTRE = 2;
	/** JComboBox conteannt la liste des types d'éditeurs possibles*/
	private JComboBox<Integer> jcbEditeur;
	/** Label pour jtfEditeur
	 * @see ConfInterface#jtfEditeur
	 */
	private JLabel ljtfEditeur = new JLabel(texts.getText("editorfieldlabel"));
	/** JtextField contenant l'adresse de l'application d'édition*/
	private JTextField jtfEditeur = new JTextField(10);
	/** Bouton ouvrant le JFileChooser permettant de choisir l'application */
	JButton jbtChoixEdit = new JButton(texts.getText("browse"));
	/* JFileChooser pour le choix de l'application /
	private JFileChooser selectionneAppli = new JFileChooser();*/
	/** JCheckBox ouvrir automatiquement l'éditeur après la transcription */
	private JCheckBox editeurBraille = new JCheckBox(texts.getText("openeditor"));
	//private JCheckBox editeurEnBraille = new JCheckBox("Editeur en Braille");
	/** Label pour cbPolice
	 * @see ConfInterface#cbPolice
	 */
	private JLabel lCbPolice = new JLabel(texts.getText("policelistlabel"));
	/** Liste des polices pour la police principale de l'éditeur */
	private JComboBox<String> cbPolice;



	/** Label pour jsTaillePolice
	 * @see ConfInterface#jsTaillePolice
	 */
	private JLabel lTaillePolice = new JLabel(texts.getText("policesizelabel"));
	/** JSpinner pour la taille de la police principale de l'éditeur */
	private JSpinner jsTaillePolice;// = new JTextField(3);
	/** Case à cocher pour l'affichage de la ligne secondaire dans l'éditeur */
	private JCheckBox afficheLigne = new JCheckBox(texts.getText("displayline"));
	/** Label pour cbPolice2
	 * @see ConfInterface#cbPolice2
	 */
	private JLabel lCbPolice2 = new JLabel(texts.getText("policelist2label"));
	/** Liste des polices pour la police secondaire de l'éditeur */
	private JComboBox<String> cbPolice2;
	/** Label pour lTaillePolice2
	 * @see ConfInterface#lTaillePolice2
	 */
	private JLabel lTaillePolice2 = new JLabel(texts.getText("policesize2label"));
	/** JSpinner pour la taille de la police secondaire de l'éditeur */
	private JSpinner jsTaillePolice2;// = new JTextField(3);
	
	/** Case à cocher pour l'affichage du text en plus des icônes dans l'éditeur */
	private JCheckBox jchTextAndIcon = new JCheckBox(texts.getText("iconAndText"));
	//private Configuration jfParent;

	
	/** Constructeur */
	public ConfInterface()
	{
		super();
		// Works with NVDA. Doesn't work with JAWS
		getAccessibleContext().setAccessibleDescription(texts.getText("tabdesc"));
		getAccessibleContext().setAccessibleName(texts.getText("tabname"));
		//this.jfParent = jfp;
		
		/*
		 * Choix de l'éditeur
		 */
		//Construction de jcbEditeur
	    Context cJcbEditeur = new Context("e","ComboBox","editor",texts);
		String[] listeEdit = {texts.getText("implementededitor"),texts.getText("defaulteditor"), texts.getText("externalapplication")};
		Integer[] editorIntArray = new Integer[listeEdit.length];
		for (int i = 0; i < listeEdit.length; i++) {editorIntArray[i] = new Integer(i);}
        AccessibleListRenderer editeurRenderer= new AccessibleListRenderer
        								("gtk-edit.png",
        								cJcbEditeur.getName(),
        								cJcbEditeur.getDesc(),
        								cJcbEditeur.getTTT(),
        								listeEdit);
        jcbEditeur = new JComboBox<Integer>(editorIntArray);
        jcbEditeur.setRenderer(editeurRenderer);
        new ContextualHelp(jcbEditeur,"confinterfaceopt",cJcbEditeur);
		jcbEditeur.addItemListener(this);
		
		//initiailisation
		activeEditeurExterne(false);
		if(ConfigNat.getCurrentConfig().getUseNatEditor()){jcbEditeur.setSelectedIndex(EDITEUR_NAT);}
		else if(ConfigNat.getCurrentConfig().getUseDefaultEditor()){jcbEditeur.setSelectedIndex(EDITEUR_DEFAUT);}
		else
		{
			jcbEditeur.setSelectedIndex(EDITEUR_AUTRE);
			activeEditeurExterne(true);
		}
		
		JLabel ljcbEditeur = new JLabel(texts.getText("editorlabel"));
		ljcbEditeur.setLabelFor(jcbEditeur);
		ljcbEditeur.setDisplayedMnemonic('e');
		
		ljtfEditeur.setLabelFor(jtfEditeur);
		ljtfEditeur.setDisplayedMnemonic('x');
		
		jtfEditeur.setText(ConfigNat.getCurrentConfig().getEditeur());
		Context cJtfEditeur = new Context("x","TextField","editorfield",texts);
		new ContextualHelp(jtfEditeur,"confinterfaceopt",cJtfEditeur);
		
		jbtChoixEdit.addActionListener(this);
		Context cJbtChoixEdit = new Context("r","Button","browse",texts);
		new ContextualHelp(jbtChoixEdit,"confinterfaceopt",cJbtChoixEdit);
		jbtChoixEdit.setMnemonic('r');
		
		editeurBraille.setSelected(ConfigNat.getCurrentConfig().getOuvrirEditeur());
		Context cEditeurBraille = new Context("i","CheckBox","openeditor",texts);
		new ContextualHelp(editeurBraille,"confinterfaceautodisplay",cEditeurBraille);
		editeurBraille.setMnemonic('i');		
		
		/*
		 * Option de l'éditeur intégré
		 */
		GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		/** Liste de toutes les polices : */
		//Font[ ] polices = environment.getAllFonts();
		/** Liste des noms de toutes les polices : */
		String [] nomPolices = environment.getAvailableFontFamilyNames();
		Context cCbPolice = new Context("u","ComboBox","policelist",texts);
		
        AccessibleListStringRenderer policesRenderer= new AccessibleListStringRenderer
        								("police.png",
        								cCbPolice.getName(),
        								cCbPolice.getDesc(),
        								cCbPolice.getTTT());
        cbPolice = new JComboBox<String>(nomPolices);
        cbPolice.setRenderer(policesRenderer);
        new ContextualHelp(cbPolice,"confinterfacefont",cCbPolice);
		
		lCbPolice.setLabelFor(cbPolice);
		lCbPolice.setDisplayedMnemonic('u');
		
		cbPolice.setSelectedItem(ConfigNat.getCurrentConfig().getPoliceEditeur());
		
		jsTaillePolice = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getTaillePolice(), 1, 300, 1));
		Context cJsTaillePolice = new Context("t", "Spinner","policesize",texts);
		new ContextualHelp(jsTaillePolice,"confinterfacefont",cJsTaillePolice);
		
		lTaillePolice.setLabelFor(jsTaillePolice);
		lTaillePolice.setDisplayedMnemonic('t');
		//jtfTaillePolice.setText(ConfigNat.getCurrentConfig().getTaillePolice() + "");
		
		afficheLigne.setSelected(ConfigNat.getCurrentConfig().getAfficheLigneSecondaire());
		Context cAfficheLigne = new Context("d", "CheckBox","displayline",texts);
		new ContextualHelp(afficheLigne,"confinterface2ndline",cAfficheLigne);
		afficheLigne.setMnemonic('d');
		afficheLigne.addItemListener(this);
		
		cbPolice2 = new JComboBox<String>(nomPolices);
		Context cCbPolice2 = new Context("o", "ComboBox","policelist2",texts);
		new ContextualHelp(cbPolice2,"confinterface2ndfont",cCbPolice2);
		
		lCbPolice2.setLabelFor(cbPolice2);
		lCbPolice2.setDisplayedMnemonic('o');
		cbPolice2.setSelectedItem(ConfigNat.getCurrentConfig().getPolice2Editeur());
		
		AccessibleListStringRenderer polices2Renderer= new AccessibleListStringRenderer
        								("police.png",
        								cCbPolice2.getName(),
        								cCbPolice2.getDesc(),
        								cCbPolice2.getTTT());
		cbPolice2.setRenderer(polices2Renderer);

		jsTaillePolice2 = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getTaillePolice2(), 1, 300, 1));
		Context cJsTaillePolice2 = new Context("a", "Spinner","policesize2",texts);
		new ContextualHelp(jsTaillePolice2,"confinterface2ndfont",cJsTaillePolice2);
		
		lTaillePolice2.setLabelFor(jsTaillePolice2);
		lTaillePolice2.setDisplayedMnemonic('a');
		//jtfTaillePolice2.setText(ConfigNat.getCurrentConfig().getTaillePolice2() + "");
		
		if (!afficheLigne.isSelected())
		{
			lCbPolice2.setEnabled(false);
			cbPolice2.setEnabled(false);
			jsTaillePolice2.setEnabled(false);
			lTaillePolice2.setEnabled(false);
		}
		
		jchTextAndIcon.setSelected(ConfigNat.getCurrentConfig().getShowIconText());
		Context cJchTextAndIcons = new Context("b", "CheckBox","iconAndText",texts);
		new ContextualHelp(jchTextAndIcon,"iconAndText",cJchTextAndIcons);
		jchTextAndIcon.setMnemonic('b');
		jchTextAndIcon.addItemListener(this);
		
		
		/*
		 * Mise en page
		 */
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		//setLayout(gbl);
		
		JPanel p = new JPanel();
		p.setLayout(gbl);
		
		gbc.anchor = GridBagConstraints.WEST;
		
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		JLabel titre = new JLabel(texts.getText("title1"));
		titre.getAccessibleContext().setAccessibleName(texts.getText("title1name"));
		gbl.setConstraints(titre, gbc);
		p.add(titre);
		
		gbc.insets = new Insets(3,30,3,3);
		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy++;
		gbl.setConstraints(ljcbEditeur, gbc);
		p.add(ljcbEditeur);
		
		gbc.gridx++;
		gbl.setConstraints(jcbEditeur, gbc);
		p.add(jcbEditeur);
		
		gbc.gridx=1;
		gbc.gridwidth = 1;
		gbc.gridy++;
		gbl.setConstraints(ljtfEditeur, gbc);
		p.add(ljtfEditeur);
		
		gbc.gridx++;
		gbl.setConstraints(jtfEditeur, gbc);
		p.add(jtfEditeur);
		
		gbc.gridx++;
		gbl.setConstraints(jbtChoixEdit, gbc);
		p.add(jbtChoixEdit);
		
		gbc.gridx=1;
		gbc.gridwidth = 3;
		gbc.gridy++;
		gbl.setConstraints(editeurBraille, gbc);
		p.add(editeurBraille);
		
		gbc.gridwidth = 3;
		gbc.insets = new Insets(3,3,3,3);
		gbc.gridx=0;
		gbc.gridy++;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		JLabel titreEditeur = new JLabel(texts.getText("title2"));
		titreEditeur.getAccessibleContext().setAccessibleName(texts.getText("title2name"));
		gbl.setConstraints(titreEditeur, gbc);
		p.add(titreEditeur);
		
		gbc.insets = new Insets(3,30,3,3);
		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy++;
		gbl.setConstraints(lCbPolice, gbc);
		p.add(lCbPolice);
		gbc.gridx++;
		gbl.setConstraints(cbPolice, gbc);
		p.add(cbPolice);

		gbc.gridx = 1;
		gbc.gridy++;
		gbl.setConstraints(lTaillePolice, gbc);
		p.add(lTaillePolice);
		gbc.gridx++;
		gbl.setConstraints(jsTaillePolice, gbc);
		p.add(jsTaillePolice);
		
		gbc.gridwidth = 2;
		gbc.gridy++;
		gbc.gridx = 1;
		gbl.setConstraints(afficheLigne, gbc);
		p.add(afficheLigne);
		
		gbc.gridwidth = 1;
		gbc.gridy++;
		gbl.setConstraints(lCbPolice2, gbc);
		p.add(lCbPolice2);
		gbc.gridx=2;
		gbl.setConstraints(cbPolice2, gbc);
		p.add(cbPolice2);
		
		gbc.gridx = 1;
		gbc.gridy++;
		gbl.setConstraints(lTaillePolice2, gbc);
		p.add(lTaillePolice2);
		gbc.gridx++;
		gbl.setConstraints(jsTaillePolice2, gbc);
		p.add(jsTaillePolice2);
		
		gbc.gridx=1;
		gbc.gridwidth=2;
		gbc.gridy++;
		p.add(jchTextAndIcon,gbc);
		
		add(p);
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	@Override
    public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer()
	 */
	@Override
    public boolean enregistrer()
	{
		boolean retour = true;
		try
		{
			ConfigNat.getCurrentConfig().setPoliceEditeur((String)cbPolice.getSelectedItem());
			ConfigNat.getCurrentConfig().setOuvreEditeurApresTranscription(editeurBraille.isSelected());
			ConfigNat.getCurrentConfig().setTaillePolice(((Integer)jsTaillePolice.getValue()).intValue());
			ConfigNat.getCurrentConfig().setAfficheLigneSecondaire(afficheLigne.isSelected());
			ConfigNat.getCurrentConfig().setTaillePolice2(((Integer)jsTaillePolice2.getValue()).intValue());
			ConfigNat.getCurrentConfig().setPolice2Editeur((String)cbPolice2.getSelectedItem());
			ConfigNat.getCurrentConfig().setShowIconText(jchTextAndIcon.isSelected());

			switch(jcbEditeur.getSelectedIndex())
			{
				case(EDITEUR_NAT):
					ConfigNat.getCurrentConfig().setUseNatEditor(true);
					ConfigNat.getCurrentConfig().setUseDefaultEditor(false);
					break;
				case(EDITEUR_DEFAUT):
					ConfigNat.getCurrentConfig().setUseNatEditor(false);
					ConfigNat.getCurrentConfig().setUseDefaultEditor(true);
					break;
				case(EDITEUR_AUTRE):
					ConfigNat.getCurrentConfig().setUseNatEditor(false);
					ConfigNat.getCurrentConfig().setUseDefaultEditor(false);
					break;	
			}
			ConfigNat.getCurrentConfig().setEditeur(jtfEditeur.getText());
		}
		/*
		catch (NumberFormatException nbe)
		{
			/*JOptionPane jopErreur = new JOptionPane();
			jopErreur.showMessageDialog( jfParent,"Vous devez entrer un entier pour la longueur de la ligne et la taille de police","Erreur de saisie", jopErreur.ERROR_MESSAGE);*/
			/*JOptionPane.showMessageDialog(jfParent,"Vous devez entrer un entier pour la longueur de la ligne et la taille de police","Erreur de saisie", JOptionPane.ERROR_MESSAGE);
			retour = false;
		}*/
		catch (Exception e){e.printStackTrace();retour=false;}
			
		return retour;
	}
	/**
	 * Implémentation de ItemListener
	 * Active ou désactive les options concernant la ligne secondaire suivant la valeur de afficheLigne
	 * @param ie l'ItemEvent généré
	 * @see ConfInterface#afficheLigne
	 * @see  ConfInterface#lCbPolice2
	 * @see  ConfInterface#cbPolice2
	 * @see  ConfInterface#lTaillePolice2
	 */
	@Override
    public void itemStateChanged(ItemEvent ie) 
	{
		if(ie.getSource()==afficheLigne)
	    {
			if(!afficheLigne.isSelected())
		    {
		    	lCbPolice2.setEnabled(false);
				cbPolice2.setEnabled(false);
				jsTaillePolice2.setEnabled(false);
				lTaillePolice2.setEnabled(false);
		    }
		    else
		    {
		    	lCbPolice2.setEnabled(true);
				cbPolice2.setEnabled(true);
				jsTaillePolice2.setEnabled(true);
				lTaillePolice2.setEnabled(true);
		    }
	    }
		else if(ie.getSource()==jcbEditeur)
		{
			if(jcbEditeur.getSelectedIndex()==EDITEUR_AUTRE){activeEditeurExterne(true);}
			else{activeEditeurExterne(false);}
		}
		
	}
	
	/**
	 * Modifie les composant de configuration en fonction de b
	 * @param b true si utilisation d'un éditeur externe
	 */
	private void activeEditeurExterne(boolean b)
	{
		jtfEditeur.setEnabled(b);
		ljtfEditeur.setEnabled(b);
		jbtChoixEdit.setEnabled(b);
	}
	/**
	 * Implémentation de ActionListener
	 * Ouvre un JFileChooser pour le choix de l'application externe de l'édition
	 * Remplit le JTextField associé
	 * @param ae ActionEvent généré
	 * @see ConfInterface#jtfEditeur
	 * @see ConfInterface#jbtChoixEdit
	 */
	@Override
    public void actionPerformed(ActionEvent ae)
	{
		JFileChooser selectionneAppli = new JFileChooser();
		selectionneAppli.setDialogTitle(texts.getText("externalapplicationmessage"));
 		if (selectionneAppli.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
         {    
 			//si un fichier est selectionné, récupérer le fichier puis son path
 			jtfEditeur.setText(selectionneAppli.getSelectedFile().getAbsolutePath()); 
         }
	}
}