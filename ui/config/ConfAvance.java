/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui.config;

import gestionnaires.GestionnaireOuvrir;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;
import nat.ConfigNat;
/**
 * Onglet de configuration des options avancées
 * @author bruno
 *
 */
public class ConfAvance extends OngletConf implements ActionListener
{
	/** Textual contents */
	private Language texts = new Language("ConfAvance");
	
	/** pour la sérialisation (non utilisé)*/
	private static final long serialVersionUID = 1L;
	/** case à cocher activant ou non Saxon plutôt que Xalan
	 * <p>Saxon est normallement l'implémentation xsl à utiliser, car xalan ne fait pas de xsl2</p>
	 */
	private JCheckBox jcbXsltProc = new JCheckBox(texts.getText("xsltproc"));
	/** JSpinner nombre de fichiers de log */
	JSpinner jsNbFichLog;
	/** JSpinner taille max en Ko du fichier de log */
	JSpinner jsTailleLog;
	/** case à cocher activant ou non les optimisations; pour l'instant, lance une transcription vide à l'ouverture*/
	private JCheckBox jcbOpti = new JCheckBox(texts.getText("opti"));
	
	/** JTextField pour l'adresse du fichier source */
    private JTextField  workingDir = new JTextField("",30);
    /** Label associé à workingDir
     * @see #workingDir
     * */
    private JLabel lWorkingDir = new JLabel(texts.getText("natworkspacelabel"));
    /** Bouton ouvrant le JFileChooser pour l'adresse du fichier source 
     * @see GestionnaireOuvrir
     * */
    private JButton jbWorkingDir = new JButton(texts.getText("browse"),new ImageIcon("ui/icon/document-open.png"));
    
    /** Bouton pour effacer les fichiers temporaires */
    private JButton jbEraseTemp = new JButton (texts.getText("erasebt"));
    
    /** jcheckbox vérifier l'existence d'une MAJ"*/
    private JCheckBox jcbVerifMAJ = new JCheckBox(texts.getText("check"));
    
    /** jcheckbox to translate files */
    private JCheckBox jcbTranslationMode = new JCheckBox(texts.getText("translationmode"));
    
    /** jcheckbox to change system's texual files instead of custom ones */
    private JCheckBox jcbChangeSysFiles = new JCheckBox(texts.getText("changesysfiles"));
    
    /** jspinner indiquant le temps d'attente pour la dernière tenative de connexion à OpenOffice */
	private JSpinner jspOOWait;
	
	/** Constructeur*/
	public ConfAvance()
	{
		super();	
		// Works with NVDA. Doesn't work with JAWS
		getAccessibleContext().setAccessibleDescription(texts.getText("tabdesc"));
		getAccessibleContext().setAccessibleName(texts.getText("tabname"));
		
		jcbXsltProc.setSelected(ConfigNat.getCurrentConfig().getSaxonAsXsltProcessor());
		Context cjcbXsltProc = new Context("x","CheckBox","xsltproc",texts);
		new ContextualHelp(jcbXsltProc,"confavancesaxon",cjcbXsltProc);
		jcbXsltProc.setMnemonic('x');
		
		jsNbFichLog = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getNbLogFiles(), 1, 5, 1));
		Context cjsNbFichLog = new Context("b","Spinner","lognumb",texts);
		new ContextualHelp(jsNbFichLog,"confavancelogsnumber",cjsNbFichLog);
		
		JLabel lJsNbFichLog = new JLabel(texts.getText("lognumblabel"));
		lJsNbFichLog.setLabelFor(jsNbFichLog);
		lJsNbFichLog.setDisplayedMnemonic('b');
		
		jsTailleLog = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getLogFileSize(), 1, 3000, 100));
		Context cjsTailleLog = new Context("m","Spinner","logsize",texts);
		new ContextualHelp(jsTailleLog,"confavancelogsize",cjsTailleLog);
		
		JLabel lJsTailleLog = new JLabel(texts.getText("logsizelabel"));
		lJsTailleLog.setLabelFor(jsTailleLog);
		lJsTailleLog.setDisplayedMnemonic('m');
		
		
		jcbOpti.setSelected(ConfigNat.getCurrentConfig().getOptimize());
		Context cjcbOpti = new Context("p","CheckBox","opti",texts);
		new ContextualHelp(jcbOpti,"confavanceprocesstime",cjcbOpti);
		jcbOpti.setMnemonic('p');
		
		lWorkingDir.setLabelFor(workingDir);
		jbWorkingDir.addActionListener(this);
		Context cjbWorkingDir = new Context("r","Button","browse",texts);
		new ContextualHelp(jbWorkingDir,"confavanceworkspace",cjbWorkingDir);
		jbWorkingDir.setMnemonic('r');
		
		ConfigNat.getCurrentConfig();
		workingDir.setText(ConfigNat.getWorkingDir());
		workingDir.setEditable(false);
		Context cworkingDir = new Context("","TextField","natworkspace",texts);
		new ContextualHelp(workingDir,"confavanceworkspace",cworkingDir);
		
		jcbVerifMAJ.setSelected(ConfigNat.getCurrentConfig().getUpdateCheck());
		Context cjcbVerifMAJ = new Context("j","CheckBox","check",texts);
		new ContextualHelp(jcbVerifMAJ,"confavanceupdates",cjcbVerifMAJ);
		jcbVerifMAJ.setMnemonic('j');
		
		jcbTranslationMode.setSelected(ConfigNat.getCurrentConfig().getTranslationMode());
		Context cjcbTranslationMode = new Context("t","CheckBox","translationmode",texts);
		new ContextualHelp(jcbTranslationMode,cjcbTranslationMode);
		jcbTranslationMode.setMnemonic('t');
		jcbTranslationMode.addActionListener(this);
		
		jcbChangeSysFiles.setSelected(ConfigNat.getCurrentConfig().getChangeSysFiles());
		Context cjcbChangeSysFiles = new Context("c","CheckBox","changesysfiles",texts);
		new ContextualHelp(jcbChangeSysFiles,cjcbChangeSysFiles);
		jcbChangeSysFiles.setMnemonic('c');
		jcbChangeSysFiles.setEnabled(jcbTranslationMode.isSelected());
		
		jbEraseTemp.addActionListener(this);
		Context cjbEraseTemp = new Context("e","Button","erasebt",texts);
		new ContextualHelp(jbEraseTemp,"confavanceerase",cjbEraseTemp);
		jbEraseTemp.setMnemonic('e');
		
		jspOOWait = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getTempsAttenteOO(), 1, 30, 1));
		JLabel lJspOOWait = new JLabel(texts.getText("oowaitlabel"));
		lJspOOWait.setLabelFor(jspOOWait);
		Context cjspOOWait = new Context("o","Spinner","oowait",texts);
		new ContextualHelp(jspOOWait,"guiOptionsAvancees",cjspOOWait);
		lJspOOWait.setDisplayedMnemonic('o');
		
		/*********
		 * Mise en page
		 */
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(15,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		
		//setLayout(gbl);
		
		JPanel p = new JPanel();
		p.setLayout(gbl);
		
		gbc.gridx=1;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		p.add(jbEraseTemp,gbc);
		
		gbc.gridx=0;
		gbc.gridy++;
		//gbc.gridheight = 3;
		p.add(jcbXsltProc,gbc);
		
		
		gbc.gridy++;
		gbc.gridwidth = 2;
		p.add(lJsNbFichLog,gbc);
		
		
		gbc.gridwidth = 1;
		gbc.gridx+=2;
		p.add(jsNbFichLog,gbc);
		
		gbc.insets = new Insets(3,3,3,3);
		gbc.gridy++;
		gbc.gridx=0;
		gbc.gridwidth = 2;
		p.add(lJsTailleLog,gbc);
		
		gbc.gridx+=2;
		gbc.gridwidth = 1;
		p.add(jsTailleLog,gbc);
		
		gbc.insets = new Insets(15,3,3,3);
		gbc.gridx=0;
		gbc.gridy++;
		gbc.gridwidth = 4;
		p.add(jcbOpti,gbc);
		
		gbc.gridx=0;
		gbc.gridy++;
		gbc.gridwidth=1;
		p.add(lWorkingDir,gbc);
		gbc.gridx++;
		gbc.gridwidth=2;
		p.add(workingDir,gbc);
		
		gbc.gridx+=2;
		p.add(jbWorkingDir,gbc);
		
		gbc.gridx=0;
		gbc.gridy++;
		gbc.gridwidth = 4;
		p.add(jcbVerifMAJ,gbc);
		
		gbc.gridx=0;
		gbc.gridy++;
		gbc.gridwidth = 4;
		p.add(jcbTranslationMode,gbc);
		
		gbc.gridx=0;
		gbc.gridy++;
		gbc.gridwidth = 4;
		p.add(jcbChangeSysFiles,gbc);
		
		gbc.gridwidth = 3;
		gbc.gridy++;
		p.add(lJspOOWait,gbc);
		
		gbc.gridx+= 3;
		p.add(jspOOWait,gbc);
		add(p);
		
		
	}

	/**
	 * enregistre les options
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	@Override
    public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * enregistre les options
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer()
	 */
	@Override
    public boolean enregistrer()
	{
		boolean retour = true;
		ConfigNat.getCurrentConfig().setSaxonAsXsltProcessor(jcbXsltProc.isSelected());
		ConfigNat.getCurrentConfig().setLogFileSize(((Integer)jsTailleLog.getValue()).intValue());
		ConfigNat.getCurrentConfig().setNbLogFiles(((Integer)jsNbFichLog.getValue()).intValue());
		ConfigNat.getCurrentConfig().setOptimize(jcbOpti.isSelected());
		ConfigNat.getCurrentConfig().setUpdateCheck(jcbVerifMAJ.isSelected());
		ConfigNat.getCurrentConfig().setChangeSysFiles(jcbChangeSysFiles.isSelected());
		ConfigNat.getCurrentConfig().setTranslationMode(jcbTranslationMode.isSelected());
		ConfigNat.getCurrentConfig().setTempsAttenteOO(((Integer)jspOOWait.getValue()).intValue());
		return retour;
	}

	/**
	 * choisit le répertoire de travail avec un JFileChooser
	 */
	private void chooseWorkingDir()
	{
		/* paramétrage du file chooser*/
		JFileChooser jfc = new JFileChooser();
		/*FiltreFichier ff = new FiltreFichier(new String [] {""},"Dossier");
		jfc.addChoosableFileFilter(ff);
		jfc.setAcceptAllFileFilterUsed(true);
		jfc.setFileFilter(ff);*/
		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		File f = new File(".");
		jfc.setCurrentDirectory(f);
		jfc.setApproveButtonText(texts.getText("chosefile")); //intitulé du bouton
		
		/* selection du dico */
		jfc.setDialogTitle(texts.getText("dirselection"));
		if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
		{
			workingDir.setText(jfc.getSelectedFile().getAbsolutePath());
			ConfigNat.getCurrentConfig().setWorkingDir(workingDir.getText(),true);
		}
	}

	/**
	 * efface les fichier d'un répertoire et récursivement les fichiers des sous-répertoires.
	 * SAUF les fichiers de log (nat_log.*)
	 * pompé (et amélioré) sur : 
	 * http://www.javafr.com/forum/sujet-SUPPRIMER-REPERTOIRE-JAVA_531086.aspx
	 * @param path dossier à effacer
	 * @return true si tout s'est bien passé
	 */
	private boolean deleteDirectory (File path)
	{
		Boolean resultat = path.exists();
		
		if( resultat )
		{
            File[] files = path.listFiles();
            for(int i=0; i<files.length; i++)
            {
                    if(files[i].isDirectory()) 
                    { resultat &= deleteDirectory(files[i]); } 

                    if (!(files[i].getName().startsWith("nat_log")))
                    	{ resultat &= files[i].delete(); }

            }
        
		}
		return resultat;
	}
	
	/**
	 * gère les évènements sur {@link #jbWorkingDir}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		if (evt.getSource()==jcbTranslationMode) { jcbChangeSysFiles.setEnabled(jcbTranslationMode.isSelected());}
		if (evt.getSource()==jbWorkingDir) { chooseWorkingDir(); }
		if (evt.getSource()==jbEraseTemp)
		{
			
			File path = new File (ConfigNat.getUserTempFolder());
			
			Boolean resultat = deleteDirectory(path);
			
			if (resultat)
			{
				JOptionPane.showMessageDialog(null,texts.getText("tempdeleted") +
						texts.getText("rebootnat"),texts.getText("info"),JOptionPane.INFORMATION_MESSAGE);
			}
			else
			{
				JOptionPane.showMessageDialog(null,texts.getText("cantdeletetemp") +
						texts.getText("doitmanually"),texts.getText("bug"),JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
