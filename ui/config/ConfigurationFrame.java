/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ui.config;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ui.ConfigurationsListItem;
import ui.accessibility.Language;


/**
 * Fenêtre d'options paramétrée
 * @author bruno
 * @since 2.1
 */
public class ConfigurationFrame extends JFrame implements TreeSelectionListener
{

    /** For serialisation, not used*/
    private static final long serialVersionUID = 1L;
    /** Textual contents */
    private Language texts = new Language("Configuration");
    /** the configuration menu tree */
    private JTree menu;
    /** the configuration panels */
    private ArrayList<JPanel> panels = new ArrayList<JPanel>();
    /** the current panel */
    private JPanel panOp = new JPanel();
    /** menu hierarchy */
    private ArrayList<ArrayList<String>> hierarchy = new ArrayList<ArrayList<String>>();
    /** save button */
    private JButton btSave = new JButton(texts.getText("save"),new ImageIcon("ui/icon/document-save.png"));
    /** save as button */
    private JButton btSaveAs = new JButton(texts.getText("saveas"),new ImageIcon("ui/icon/document-save-as.png"));
    /** cancel button */
    private JButton btCancel= new JButton(texts.getText("cancel"),new ImageIcon("ui/icon/edit-undo.png"));
    
	
    /**
     * Load and show an option frame
     * @param f the file containing options to be shown
     * @param jcb the combo box containing the configuration
     */
    public ConfigurationFrame(File f, JComboBox<ConfigurationsListItem> jcb)
    {
    	buildFrame(f);
    }
    
    /**
     * Build the frame
     * @param f the file containing options to be shown
     */
     private void buildFrame(File f)
     {
    	//root node for menu is given after parsing conf file
     	DefaultMutableTreeNode root = loadFile(f);
     	menu = new JTree(root);
     	
     	//no icons, no lines for menu
     	DefaultTreeCellRenderer renderer2 = new DefaultTreeCellRenderer();
        renderer2.setOpenIcon(null);
        renderer2.setClosedIcon(null);
        renderer2.setLeafIcon(null);
        renderer2.setBackground(Color.LIGHT_GRAY);
        renderer2.setBackgroundNonSelectionColor(Color.LIGHT_GRAY);
        renderer2.setBackgroundSelectionColor(Color.white);
        menu.putClientProperty("JTree.lineStyle", "None");
        menu.setCellRenderer(renderer2);
        menu.setBackground(Color.LIGHT_GRAY);
        
        menu.addTreeSelectionListener(this);
        //expand all
        for(int i=0;i<menu.getRowCount();i++){menu.expandRow(i);}  
        
        refreshFrame();
     	pack();
     	setVisible(true);
     }
     
     /**
     * Refresh the configuration frame components
     */
    private void refreshFrame()
     {
    	 GridBagConstraints c = new GridBagConstraints();
         
         //tmp panel
         panOp.setBackground(Color.white);
         JPanel panMain = new JPanel();
         panMain.setBackground(Color.blue);
          
         JPanel panSave = new JPanel(new GridBagLayout());
         c.insets=new Insets(3,3,3,3);
         c.gridx=0;
         c.gridy=0;
         c.fill=GridBagConstraints.HORIZONTAL;
         panSave.add(btSave,c);
         c.gridx++;
         panSave.add(btSaveAs,c);
         c.gridx++;
         panSave.add(btCancel,c);
         
         //frame layout
         JPanel p = new JPanel();
         p.setLayout(new GridBagLayout());
          
         c.gridx=0;
         c.gridy=0;
         c.gridwidth=4;
         c.fill=GridBagConstraints.HORIZONTAL;
          
         p.add(panMain,c);
          
         c.gridy++;
         c.gridwidth=1;
      	p.add(menu,c);
      	
      	c.gridx++;
      	c.gridwidth=3;
      	p.add(panOp,c);
      	
      	c.gridx=1;
      	c.gridy++;
      	p.add(panSave,c);
      	
      	setContentPane(p);
      	p.repaint();
     }
    
    /**
     * Load and parse the file <code>f</code> and set up the frame
     * Create {@link #panels} items
     * @param f the xml file to be loaded
     * @return the root node of the JTree menu {@link #menu}
     */
    private DefaultMutableTreeNode loadFile(File f)
    {
    	DefaultMutableTreeNode root = new DefaultMutableTreeNode();
    	//parsing file to obtain menu
    	try 
		{
			//Parsing
			DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
			DocumentBuilder constructeur= fabrique.newDocumentBuilder();
		    Document doc = constructeur.parse(f);
		    
		    /*
		     * getting infos
		     */
		    
		    //menuItem
		    //root
		    NodeList optionFrame = doc.getElementsByTagName("optionFrame").item(0).getChildNodes();
		    
		    for(int i= 0; i<optionFrame.getLength();i++)
		    {
		    	//menuItems
		    	Node n = optionFrame.item(i);
		    	if(n.getNodeName().equals("menuItem"))
		    	{
		    		
		    		ArrayList<String> lNames = new ArrayList<String>();//contains the names of the menu followed by submenus names if exist
		    		parseItem(n,lNames);
		    		if(lNames.size()>0){hierarchy.add(lNames);}
		    	}
		    }
		}
    	//TODO manage exceptions
		catch (NullPointerException e){e.printStackTrace();}
		catch (ParserConfigurationException e) {e.printStackTrace();} 
		catch (IOException ioe) {ioe.printStackTrace();}
		catch (SAXException e){e.printStackTrace();}
		
		//creating object menu
		DefaultMutableTreeNode node = new DefaultMutableTreeNode();
		for(ArrayList<String> al : hierarchy)
		{
			node = new DefaultMutableTreeNode("<html><strong>"+al.get(0)+"</strong></html>");
			if(al.size()>1)
			{
				for(int i=1; i<al.size();i++){node.add(new DefaultMutableTreeNode("<html><em>"+al.get(i)+"</em></html>"));}
			}
			root.add(node);
		}
        return(root);
      }

    /**
     * Parse an item node
     * @param n the node to parse
     * @param al the current list of menu names 
     */
    private void parseItem(Node n, ArrayList<String>al)
    {
    	NodeList childs = n.getChildNodes();//childs of the menuItem tag
    	JPanel panOptions = new JPanel();
    	panels.add(panOptions);
    	for(int i=0; i<childs.getLength();i++)
    	{	    	
	    	Node c  = childs.item(i);
    		if(c.getNodeName().equals("name"))
    		{
    			al.add(c.getTextContent());
    			panOptions.add(new JLabel("Panneau:"+c.getTextContent()));
    		}
    		else if(c.getNodeName().equals("chb"))
    		{
    			System.out.println("chb:"+c.getTextContent());
    			//boolean indent = c.getAttributes().getNamedItem("indent")!=null;
    		}
    		else if(c.getNodeName().equals("menuItem"))//subMenu
    		{
    			parseItem(c,al);
    		}
    	}
    }
    
    /**
     * for test purposes only
     * @param a params, not used
     */
    public static void main(String [] a)
    {
    	JFrame f = new ConfigurationFrame(new File("ui/config/ressources/modeConf.xml"),null);
    	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


	/**
     * @see javax.swing.event.TreeSelectionListener#valueChanged(javax.swing.event.TreeSelectionEvent)
     */
    @Override
    public void valueChanged(TreeSelectionEvent tse)
    {
    	ArrayList<TreePath> paths = new ArrayList<TreePath>();
    	for(int i=0; i<menu.getRowCount();i++){paths.add(menu.getPathForRow(i));}
    	TreePath p = tse.getNewLeadSelectionPath();
    	System.out.println(" - path:"+p+"\n - index:"+paths.indexOf(p));
    	
	    int selected = hierarchy.indexOf(hierarchy.get(paths.indexOf(tse.getNewLeadSelectionPath())));
	    panOp = panels.get(selected);
	    refreshFrame();
	    System.out.println("sel="+selected+tse.getNewLeadSelectionPath());
	    
    }
}
