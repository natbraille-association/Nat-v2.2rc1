/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret, Frédérick Schwebel, Vivien Guillet
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.config;


import java.io.File;

/**
 * Item de liste pour le JComboBox de la fenêtre de configuration contenant 
 * les tables brailles possibles
 * @see ConfGeneral
 * @author Bruno
 *
 */
public class BrailleTableListItem implements Comparable<BrailleTableListItem>
{
    /** adresse de la table Braille */
    private String  filename = "";
    /** Nom de la table */
    private String  name = "";
    /** Vrai si la configuration est une configuration système */
    private boolean isSystem;
    /**
     * Constructeur
     * @param in_filename adresse du fichier de configuration
     * @param sys true si configuration système
     */
    public BrailleTableListItem(String in_filename, boolean sys)
    {
		isSystem = sys;
		if (!(in_filename.endsWith(".ent"))) {in_filename=in_filename + ".ent"; }
		name = new File(in_filename).getName();
		name = name.substring(0, name.length() - 4);
		filename = in_filename;
	}
    /**
     * Compare deux configurations, une (<code>cli</code>) représentée par une instance de ConfigurationsListItem
     * et l'autre par le nom de la configuration représentée par <code>this</code>
     * @param cli l'instance de {@link BrailleTableListItem}
     * @return true si {@link #filename} est le même que <code>cli</code> 
     */
    public boolean equals(BrailleTableListItem cli)
    	{return (filename.equals(cli.filename));}
    /**
     * Redéfinition de la méthode equals
     * @param cli l'instance de {@link BrailleTableListItem}
     * @return true si {@link #filename} est le même que <code>cli</code> 
     */
    @Override
	public boolean equals(Object cli)
    {
    	boolean retour = false;
    	if(cli instanceof BrailleTableListItem)
    	{
    		retour = ((BrailleTableListItem) cli).equals(this);
    	}
    	else
    	{
    		retour = super.equals(cli);
    	}
    	return retour;
    }
    /**
     * Méthode d'accès en lecture à {@link #filename}
     * @return {@link #filename}
     */
    public String getFilename(){return filename;}
    /**
     * Méthode d'accès en lecture à {@link #name}
     * @return {@link #name}
     */
    public String getName(){return name;}
    /**
     * Méthode d'accès en lecture à {@link #isSystem}
     * @return {@link #isSystem}
     */
    public boolean getIsSystem(){return isSystem;}
    
	/**
	 * Méthode permettant la comparaison de deux instances de BrailleTableListItem
	 * La comparaison se fait sur l'attribut name 
	 * @param btli l'instance de BrailleTableListItem à comparer
	 * @return la comparaison entre les deux attributs {@link #name}
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
    public int compareTo(BrailleTableListItem btli)
	{
		return name.compareTo(btli.getName());
	}
	
	/**
	 * Renvoie {@link #filename}
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){return name+".ent";}
    
}
