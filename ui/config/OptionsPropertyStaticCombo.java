/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui.config;

import javax.swing.JComboBox;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import nat.OptDef;
/**
 * This class provides an OptionPropertyStaticCombo definition
 * @author Vivien, comments added by Bruno
 *
 */
public class OptionsPropertyStaticCombo  extends OptionsProperty {

	/** The JComboBox instance
	 *  TODO parameters as Object, could be changed to a more precise class
	 **/
    private JComboBox<Object> comboBox = new JComboBox<Object>();

    /**
     * Constructor
     * @param od the OptDef instance
     */
    public OptionsPropertyStaticCombo(OptDef od)
    {
		super(od);
		addCompo();
    }
    
    /**
     * add {@link #comboBox} to {@link OptionsProperty#panel}
     */
    public void addCompo()
    {
		Node rendererOptions = getOptDef().getGuiRendererNode();
		NodeList childs = rendererOptions.getChildNodes();
		for(int i= 0; i< childs.getLength();i++)  {
		    Node ch = childs.item(i);
		    if (ch.getNodeName().equals("option")){
			String val = OptDef.nodeAttributeValue(ch,"id");
			comboBox.addItem(texts.getText(val));
		    }
		}
		panel.add(comboBox);
    }
    
    /**
     * @see ui.config.OptionsProperty#setValue(java.lang.String)
     */
    @Override
    public void setValue(String value)
    {
    	//	comboBoxValue.setSelected();
    }

    /**
     * @see ui.config.OptionsProperty#getValue()
     */
    @Override
    public String getValue()
    {
		//return (checkBoxValue.isSelected())?("true"):("false");
		return "aa";
    }
}