/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.config;

import java.awt.AWTKeyStroke;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ui.accessibility.AccessibleListRenderer;
import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;

import nat.ConfigNat;

/**
 * Onglet d'accessibilité de l'interface
 * @see OngletConf
 * @author gregoire
 *
 */
public class ConfAccessibilite extends OngletConf implements ActionListener, ItemListener, KeyListener
{	
	/** Textual contents */
	private Language texts = new Language("ConfAccessibilite");
	/** Languages' names */
	Language langNames = new Language("Languages");
	
	/** Pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;
	
	/** case à cocher pour la mémorisation des dimensions des fenêtres */
	private JCheckBox jcbTailleFenetre = new JCheckBox(texts.getText("windowsize"));
	/** case à cocher pour le centrage des fenêtres à l'écran */
	private JCheckBox jcbCentrerFenetre = new JCheckBox(texts.getText("windowcenter"));
	/** case à cocher pour jouer un son périodiquement pendant la transcription */
	private JCheckBox jcbSonPendantTrans = new JCheckBox(texts.getText("soundduring"));
	/** case à cocher pour jouer un son à la fin de la transcription */
	private JCheckBox jcbSonFin = new JCheckBox(texts.getText("soundafter"));
	/** case à cocher pour nommer automatiquement le fichier de sortie */
	private JCheckBox jcbSortieAuto = new JCheckBox(texts.getText("autooutput"));
	/** Available screen readers */ 
	private JComboBox<Integer> jcbScrReader;
	/** Label for the screen readers list */
	private JLabel ljcbScrReader = new JLabel(texts.getText("screenreader"));
	/** liste des niveaux de verbosité possibles */ 
	private JComboBox<Integer> cbLog;
	/** label pour la liste des niveaux de verbosité possibles */
	private JLabel lLog = new JLabel(texts.getText("verbosity"));
	
	/** Available languages */ 
	private JComboBox<Integer> jcbLanguage;
	/** Label for the languages list */
	private JLabel ljcbLanguage = new JLabel(texts.getText("language"));
	/** langs list */
	private String[] langNamesList;
	
	/** check box to always read the description */
	private JCheckBox jcbReadDescr = new JCheckBox(texts.getText("readdescr"));
	/** checkbox to use the tool tip text as a description */
	private JCheckBox jcbUseTTT = new JCheckBox(texts.getText("usettt"));
	/** checkbox to remove every context and replace it with the tool tip text, when possible */
	private JCheckBox jcbReadTTT = new JCheckBox(texts.getText("readttt"));
	/** checkbox to never read the description */
	private JCheckBox jcbNeverReadDescr = new JCheckBox(texts.getText("neverreaddescr"));
	/** checkbox to launch help with the default browser */
	private JCheckBox jcbUseBrowser = new JCheckBox(texts.getText("usebrowser"));
	/** checkbox to launch help with the default browser */
	private JCheckBox jcbUseInternet = new JCheckBox(texts.getText("useinternet"));
	/** button to change the help key*/
	private JButton jbtChangeHelpKey;
	/** boolean to allow user to change the help key */
	private Boolean isNewHelpKeyPossible=false;
	/** new help key */
	private AWTKeyStroke newHelpKey;
	/** help key modifiers*/
	private int helpModifiers;
	/** button to change the traduction key*/
	private JButton jbtChangeTradKey;
	/** boolean to allow user to change the traduction key */
	private Boolean isNewTradKeyPossible=false;
	/** new traduction key */
	private AWTKeyStroke newTradKey;
	/** help key modifiers*/
	private int tradModifiers;
	
	
	/** Constructor*/
	public ConfAccessibilite()
	{
		super();
		// Works with NVDA. Doesn't work with JAWS
		getAccessibleContext().setAccessibleDescription(texts.getText("tabdesc"));
		getAccessibleContext().setAccessibleName(texts.getText("tabname"));
		
		/* options valables pour toute l'interface quelle que soit la conf */
		jcbTailleFenetre.setSelected(ConfigNat.getCurrentConfig().getMemoriserFenetre());
		Context cJcbTailleFenetre = new Context("m","CheckBox","windowsize",texts);
		jcbTailleFenetre.setMnemonic('m');
		new ContextualHelp(jcbTailleFenetre,"confaccessoptions", cJcbTailleFenetre);
		
		jcbCentrerFenetre.setSelected(ConfigNat.getCurrentConfig().getCentrerFenetre());
		Context cJcbCentrerFenetre = new Context("c","CheckBox","windowcenter",texts);
		jcbCentrerFenetre.setMnemonic('c');
		new ContextualHelp(jcbCentrerFenetre,"confaccessoptions", cJcbCentrerFenetre);
		
		jcbSonPendantTrans.setSelected(ConfigNat.getCurrentConfig().getSonPendantTranscription());
		Context cJcbSonPendantTrans = new Context("p","CheckBox","soundduring",texts);
		jcbSonPendantTrans.setMnemonic('p');
		new ContextualHelp(jcbSonPendantTrans,"confaccessoptions", cJcbSonPendantTrans);
		
		jcbSonFin.setSelected(ConfigNat.getCurrentConfig().getSonFinTranscription());
		Context cJcbSonFin = new Context("f","CheckBox","soundafter",texts);
		jcbSonFin.setMnemonic('f');
		new ContextualHelp(jcbSonFin,"confaccessoptions", cJcbSonFin);
		
		jcbSortieAuto.setSelected(ConfigNat.getCurrentConfig().getSortieAuto());
		Context cjcbSortieAuto = new Context("q","CheckBox","autooutput",texts);
		jcbSortieAuto.setMnemonic('q');
		new ContextualHelp(jcbSortieAuto,"confaccessoptions", cjcbSortieAuto);
		
		// screen reader
		Context cJcbScrReader = new Context("l","ComboBox","screenreader",texts);
		String [][] scrReadTable = Language.getSR();
		String [] scrReadList = new String[scrReadTable.length];
		for(int i=1 ; i<scrReadTable.length ; i++)
		{
			scrReadList[i]=scrReadTable[i][1];
		}
		scrReadList[0]=texts.getText("defaultsr");                               
		Integer[] intArray = new Integer[scrReadList.length];
		for (int i = 0; i < scrReadList.length; i++) {intArray[i] = new Integer(i);}
        AccessibleListRenderer scrReaderRenderer= new AccessibleListRenderer
        								("microphone.png",
        								cJcbScrReader.getName(),
        								cJcbScrReader.getDesc(),
        								cJcbScrReader.getTTT(),
        								scrReadList);
        jcbScrReader = new JComboBox<Integer>(intArray);
		jcbScrReader.setRenderer(scrReaderRenderer);
		new ContextualHelp(jcbScrReader,"confaccessscreenreader", cJcbScrReader);
		jcbScrReader.setSelectedIndex(ConfigNat.getCurrentConfig().getScrReader()-1);
		
		ljcbScrReader.setLabelFor(jcbScrReader);
		ljcbScrReader.setDisplayedMnemonic('l');
		
		// verbosité des logs:
		Context cCbLog = new Context("v","ComboBox","verbosity",texts);
		String [] listeModeVerbeux = {texts.getText("nullverbosity"),texts.getText("weakverbosity"),texts.getText("averageverbosity"),texts.getText("highverbosity"),texts.getText("debugverbosity")};
		String [] verbosityIcons = {"volume-mute.png",
					    "volume-low.png",
					    "volume-mid.png",
					    "volume-up.png",
					    "script-error.png"};
		Integer[] verbosityIntArray = new Integer[listeModeVerbeux.length];
		for (int i = 0; i < listeModeVerbeux.length; i++) {verbosityIntArray[i] = new Integer(i);}
		AccessibleListRenderer verbosityRenderer= new AccessibleListRenderer
        								(verbosityIcons,
        								cCbLog.getName(),
        								cCbLog.getDesc(),
        								cCbLog.getTTT(),
        								listeModeVerbeux);
		cbLog = new JComboBox<Integer>(verbosityIntArray);
		cbLog.setRenderer(verbosityRenderer);
		cbLog.setSelectedIndex(ConfigNat.getCurrentConfig().getNiveauLog());
		new ContextualHelp(cbLog,"confaccessverbosity", cCbLog);

		lLog.setLabelFor(cbLog);
		lLog.setDisplayedMnemonic('v');
		
		// language
		Context cJcbLanguage = new Context("w","ComboBox","language",texts);
		
		int i=0,j=0,langIndex=0;
		String[][] langTable = Language.getLangs();
		langNamesList = new String[langTable.length]; // labels of the languages' list
		String[] flagNamesList = new String[langTable.length]; // icons
		
		for(i=0; i<langTable.length; i++)
		{
			langNamesList[i]=langNames.getText(langTable[i][0]);
			if(langNamesList[i].contains("not found"))langNamesList[i]=langTable[i][0];
		}
		// languages list sorting out
		String[] langNamesListSvg = new String[langNamesList.length];
		langNamesListSvg = langNamesList.clone();
		java.util.Arrays.sort(langNamesList);
		
		i=0;
		for(String s1 : langNamesList)
		{
			j=0;
			for(String t : langNamesListSvg)
			{
				if(s1.equals(t))
    			{flagNamesList[i]="flag-"+langTable[j][1]+".png";}
    			j++;
    		}
			if(s1.equals(langNames.getText(langTable[ConfigNat.getCurrentConfig().getLanguage()][0])))langIndex=i;
			i++;
		}
		
		Integer[] langIntArray = new Integer[langNamesList.length];
		for (int i1 = 0; i1 < langNamesList.length; i1++) 
		{
			langIntArray[i1] = new Integer(i1);
		}
        AccessibleListRenderer languageRenderer= new AccessibleListRenderer
        								(flagNamesList,
        								cJcbLanguage.getName(),
        								cJcbLanguage.getDesc(),
        								cJcbLanguage.getTTT(),
        								langNamesList);
        languageRenderer.setDefaultIcon("white-flag.png");
        
        jcbLanguage = new JComboBox<Integer>(langIntArray);
        jcbLanguage.setSelectedItem(langIndex);
        jcbLanguage.setRenderer(languageRenderer);
		ljcbLanguage.setLabelFor(jcbLanguage);
		ljcbLanguage.setDisplayedMnemonic('w');
		new ContextualHelp(jcbLanguage,"confaccesslanguage", cJcbLanguage);
		
		jcbReadDescr.setSelected(ConfigNat.getCurrentConfig().getReadDescr());
		Context cjcbReadDescr = new Context("d","CheckBox","readdescr",texts);
		jcbReadDescr.setMnemonic('d');
		new ContextualHelp(jcbReadDescr,"confaccessalwaysreaddesc", cjcbReadDescr);
		jcbReadDescr.addItemListener(this);
		if (jcbReadDescr.isSelected())
		{
			jcbNeverReadDescr.setEnabled(false);
		}
		
		jcbNeverReadDescr.setSelected(ConfigNat.getCurrentConfig().getNeverReadDescr());
		Context cjcbNeverReadDescr = new Context("e","CheckBox","neverreaddescr",texts);
		jcbNeverReadDescr.setMnemonic('e');
		new ContextualHelp(jcbNeverReadDescr,"confaccessneverreaddesc", cjcbNeverReadDescr);
		jcbNeverReadDescr.addItemListener(this);
		if (jcbNeverReadDescr.isSelected())
		{
			jcbReadDescr.setEnabled(false);
		}
		
		jcbUseTTT.setSelected(ConfigNat.getCurrentConfig().getUseTTT());
		Context cjcbUseTTT = new Context("t","CheckBox","usettt",texts);
		jcbUseTTT.setMnemonic('t');
		new ContextualHelp(jcbUseTTT,"confaccessusetooltip", cjcbUseTTT);
		
		jcbReadTTT.setSelected(ConfigNat.getCurrentConfig().getReadTTT());
		Context cjcbReadTTT = new Context("o","CheckBox","readttt",texts);
		jcbReadTTT.setMnemonic('o');
		new ContextualHelp(jcbReadTTT,"confaccessreadtooltip", cjcbReadTTT);
		jcbReadTTT.addItemListener(this);
		
		if (jcbReadTTT.isSelected())
		{
			jcbReadDescr.setEnabled(false);
			jcbUseTTT.setEnabled(false);
		}
		
		jcbUseBrowser.setSelected(ConfigNat.getCurrentConfig().getOpenWithBrowser());
		Context cjcbUseBrowser = new Context("b","CheckBox","usebrowser",texts);
		jcbUseBrowser.setMnemonic('b');
		new ContextualHelp(jcbUseBrowser,"confaccessopenwithbrowser", cjcbUseBrowser);
		jcbUseBrowser.addItemListener(this);
		
		if (!jcbUseBrowser.isSelected())
		{
			jcbUseInternet.setEnabled(false);
		}
		
		jcbUseInternet.setSelected(ConfigNat.getCurrentConfig().getUseInternet());
		Context cjcbUseInternet = new Context("i","CheckBox","useinternet",texts);
		jcbUseInternet.setMnemonic('i');
		new ContextualHelp(jcbUseInternet,"confaccessuseinternet", cjcbUseInternet);
		
		newHelpKey=ConfigNat.getCurrentConfig().getHelpKey();
		helpModifiers=newHelpKey.getModifiers();
		Context cjbtChangeHelpKey = new Context("k", "Button", "changehelpkey",texts);
		String changeKeyText =texts.getText("changehelpkey")+" ("
							+ InputEvent.getModifiersExText(helpModifiers);
		changeKeyText+=(helpModifiers!=0)?"+":"";
		changeKeyText+=KeyEvent.getKeyText(newHelpKey.getKeyCode())
							+ ")";
		jbtChangeHelpKey = new JButton(changeKeyText);
		jbtChangeHelpKey.getAccessibleContext().setAccessibleName(
				cjbtChangeHelpKey.getName()
				+jbtChangeHelpKey.getText().replaceFirst(texts.getText("changehelpkey"), ""));
		jbtChangeHelpKey.getAccessibleContext().setAccessibleDescription(cjbtChangeHelpKey.getDesc());
		jbtChangeHelpKey.setToolTipText(cjbtChangeHelpKey.getTTT());
		jbtChangeHelpKey.setMnemonic('k');
		jbtChangeHelpKey.addActionListener(this);
		jbtChangeHelpKey.addKeyListener(this);
		
		newTradKey=ConfigNat.getCurrentConfig().getTradKey();
		tradModifiers=newTradKey.getModifiers();
		Context cjbtChangeTradKey = new Context("k", "Button", "changetradkey",texts);
		String changeKeyText1 =texts.getText("changetradkey")+" ("
							+ InputEvent.getModifiersExText(tradModifiers);
		changeKeyText1+=(tradModifiers!=0)?"+":"";
		changeKeyText1+=KeyEvent.getKeyText(newTradKey.getKeyCode())
							+ ")";
		jbtChangeTradKey = new JButton(changeKeyText1);
		jbtChangeTradKey.getAccessibleContext().setAccessibleName(
				cjbtChangeTradKey.getName()
				+jbtChangeTradKey.getText().replaceFirst(texts.getText("changetradkey"), ""));
		jbtChangeTradKey.getAccessibleContext().setAccessibleDescription(cjbtChangeTradKey.getDesc());
		jbtChangeTradKey.setToolTipText(cjbtChangeTradKey.getTTT());
		jbtChangeTradKey.addActionListener(this);
		jbtChangeTradKey.addKeyListener(this);
		
		
		/*
		 * Mise en page
		 */
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		//setLayout(gbl);
		
		JPanel p = new JPanel();
		p.setLayout(gbl);
		
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		gbc.gridx = 0;
		gbc.insets = new Insets(3,3,3,3);
		gbc.gridwidth = 3;
		gbc.gridy++;
		JLabel titreLog = new JLabel(texts.getText("title3"));
		titreLog.getAccessibleContext().setAccessibleName(texts.getText("title3name"));
		gbl.setConstraints(titreLog, gbc);
		p.add(titreLog);
		
		/*gbc.gridx=0;
		gbc.gridy++;
		gbc.gridwidth = 4;
		gbc.insets = new Insets(3,3,3,3);
		JLabel readerChoice = new JLabel(texts.getText("title1"));
		readerChoice.getAccessibleContext().setAccessibleName(texts.getText("title1name"));
		readerChoice.setLabelFor(ljcbScrReader);
		gbl.setConstraints(readerChoice, gbc);
		p.add(readerChoice);*/
		
		JLabel lInfo1 = new JLabel(texts.getText("info1"));
		lInfo1.getAccessibleContext().setAccessibleName(texts.getText("info1name"));
		gbc.gridwidth = 4;
		gbc.gridy++;
		gbl.setConstraints(lInfo1, gbc);
		p.add(lInfo1);
		
		gbc.insets = new Insets(3,30,3,3);
		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy++;
		gbl.setConstraints(ljcbScrReader, gbc);
		p.add(ljcbScrReader);
		gbc.gridx++;
		gbl.setConstraints(jcbScrReader, gbc);
		p.add(jcbScrReader);
		gbc.gridy++;
		

		gbc.insets = new Insets(3,30,3,3);
		gbc.gridwidth = 1;
		gbc.gridx=1;
		gbc.gridy++;
		gbl.setConstraints(lLog, gbc);
		p.add(lLog);
		gbc.gridx++;
		gbl.setConstraints(cbLog, gbc);
		p.add(cbLog);
		
		gbc.gridwidth = 1;
		gbc.gridx=1;
		gbc.gridy++;
		gbl.setConstraints(ljcbLanguage, gbc);
		p.add(ljcbLanguage);
		gbc.gridx++;
		gbl.setConstraints(jcbLanguage, gbc);
		p.add(jcbLanguage);
		
		JLabel lTitreGeneral = new JLabel(texts.getText("title2"));
		lTitreGeneral.getAccessibleContext().setAccessibleName(texts.getText("title2name"));
		gbc.gridx = 0;
		gbc.insets = new Insets(3,3,3,3);
		gbc.gridwidth = 3;
		gbc.gridy++;
		gbl.setConstraints(lTitreGeneral, gbc);
		p.add(lTitreGeneral);
		
		JLabel lInfo2 = new JLabel(texts.getText("info2"));
		lInfo2.getAccessibleContext().setAccessibleName(texts.getText("info2name"));
		gbc.gridwidth = 4;
		gbc.gridy++;
		gbl.setConstraints(lInfo2, gbc);
		p.add(lInfo2);
		
		gbc.insets = new Insets(3,30,3,3);
		gbc.gridwidth = 2;
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(jcbTailleFenetre, gbc);
		p.add(jcbTailleFenetre);
		
		gbc.gridy++;
		gbl.setConstraints(jcbCentrerFenetre, gbc);
		p.add(jcbCentrerFenetre);
		
		gbc.gridy++;
		gbl.setConstraints(jcbSonFin, gbc);
		p.add(jcbSonFin);
		
		gbc.gridy++;
		gbl.setConstraints(jcbSonPendantTrans, gbc);
		p.add(jcbSonPendantTrans);
		
		gbc.gridy++;
		gbl.setConstraints(jcbSortieAuto, gbc);
		p.add(jcbSortieAuto);
		
		JLabel lTitreAdvancedAccess = new JLabel(texts.getText("title4"));
		lTitreAdvancedAccess.getAccessibleContext().setAccessibleName(texts.getText("title4name"));
		gbc.gridx = 0;
		gbc.insets = new Insets(3,3,3,3);
		gbc.gridwidth = 3;
		gbc.gridy++;
		gbl.setConstraints(lTitreAdvancedAccess, gbc);
		p.add(lTitreAdvancedAccess);
		
		JLabel lInfo3 = new JLabel(texts.getText("info3"));
		lInfo3.getAccessibleContext().setAccessibleName(texts.getText("info3name"));
		gbc.gridwidth = 4;
		gbc.gridy++;
		gbl.setConstraints(lInfo3, gbc);
		p.add(lInfo3);
		
		gbc.insets = new Insets(3,30,3,3);
		gbc.gridwidth = 1;
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(jcbReadTTT, gbc);
		p.add(jcbReadTTT);
		
		//gbc.gridy++;
		gbc.gridx++;
		gbl.setConstraints(jcbUseTTT, gbc);
		p.add(jcbUseTTT);
		
		gbc.gridx=1;
		gbc.gridy++;
		gbl.setConstraints(jcbReadDescr, gbc);
		p.add(jcbReadDescr);
		
		gbc.gridx++;
		gbl.setConstraints(jcbNeverReadDescr, gbc);
		p.add(jcbNeverReadDescr);
		
		gbc.gridx=1;
		gbc.gridy++;
		gbl.setConstraints(jcbUseBrowser, gbc);
		p.add(jcbUseBrowser);
		
		gbc.gridx++;
		gbl.setConstraints(jcbUseInternet, gbc);
		p.add(jcbUseInternet);
		
		gbc.gridwidth = 2;
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(jbtChangeTradKey, gbc);
		p.add(jbtChangeTradKey);
		
		gbc.gridy++;
		gbl.setConstraints(jbtChangeHelpKey, gbc);
		p.add(jbtChangeHelpKey);
		
		add(p);
	}
	
	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
    public void actionPerformed(ActionEvent ae) 
	{
		if (ae.getSource()==jbtChangeHelpKey)
			{
				isNewHelpKeyPossible=true;
				helpModifiers=0;
				jbtChangeHelpKey.setText(texts.getText("changehelpkey"));
			}
		if (ae.getSource()==jbtChangeTradKey)
		{
			isNewTradKeyPossible=true;
			tradModifiers=0;
			jbtChangeTradKey.setText(texts.getText("changetradkey"));
		}
	}
	
	/**
	 * ItemListener implementation
	 * @param ie generated ItemEvent
	 * @see #jcbUseTTT
	 * @see #jcbReadDescr
	 */
	@Override
    public void itemStateChanged(ItemEvent ie) 
	{
		if(ie.getSource()==jcbReadTTT)
	    {
			if(jcbReadTTT.isSelected())
		    {
		    	jcbUseTTT.setEnabled(false);
				jcbReadDescr.setEnabled(false);
		    }
		    else
		    {
		    	jcbUseTTT.setEnabled(true);
		    	jcbReadDescr.setEnabled(true);
		    }
	    }
		if(ie.getSource()==jcbReadDescr)
	    {
			if(jcbReadDescr.isSelected())
		    {
				jcbNeverReadDescr.setEnabled(false);
		    }
		    else
		    {
		    	jcbNeverReadDescr.setEnabled(true);
		    }
	    }
		if(ie.getSource()==jcbNeverReadDescr)
	    {
			if(jcbNeverReadDescr.isSelected())
		    {
				jcbReadDescr.setEnabled(false);
		    }
		    else
		    {
		    	jcbReadDescr.setEnabled(true);
		    }
	    }
		if(ie.getSource()==jcbUseBrowser)
		{
			if(jcbUseBrowser.isSelected())jcbUseInternet.setEnabled(true);
			else jcbUseInternet.setEnabled(false);
		}
	}

	/**
	 * Saves the accessibility tab settings
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer()
	 */
	
	@Override
    public boolean enregistrer()
	{
		boolean retour = true;
		try
		{
			ConfigNat.getCurrentConfig().setLanguage(langNames.findLangFileIndex(langNamesList[jcbLanguage.getSelectedIndex()]));
			ConfigNat.getCurrentConfig().setNiveauLog(cbLog.getSelectedIndex());
			ConfigNat.getCurrentConfig().setCentrerFenetre(jcbCentrerFenetre.isSelected());
			ConfigNat.getCurrentConfig().setMemoriserFenetre(jcbTailleFenetre.isSelected());
			ConfigNat.getCurrentConfig().setSonFinTranscription(jcbSonFin.isSelected());
			ConfigNat.getCurrentConfig().setSonPendantTranscription(jcbSonPendantTrans.isSelected());
			ConfigNat.getCurrentConfig().setSortieAuto(jcbSortieAuto.isSelected());
			ConfigNat.getCurrentConfig().setScrReader(jcbScrReader.getSelectedIndex()+1);
			ConfigNat.getCurrentConfig().setReadDescr(jcbReadDescr.isSelected());
			ConfigNat.getCurrentConfig().setUseTTT(jcbUseTTT.isSelected());
			ConfigNat.getCurrentConfig().setReadTTT(jcbReadTTT.isSelected());
			ConfigNat.getCurrentConfig().setNeverReadDescr(jcbNeverReadDescr.isSelected());
			ConfigNat.getCurrentConfig().setOpenWithBrowser(jcbUseBrowser.isSelected());
			ConfigNat.getCurrentConfig().setHelpKey(newHelpKey);
			ConfigNat.getCurrentConfig().setUseInternet(jcbUseInternet.isSelected());
			ConfigNat.getCurrentConfig().setTradKey(newTradKey);
		}
		catch (Exception e){e.printStackTrace();retour=false;}
		return retour;
	}

	/**
	 * Enregistre les options de l'onglet
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	@Override
    public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}

	/**
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent ke) 
	{
		if(ke.getSource()==jbtChangeHelpKey && isNewHelpKeyPossible)
		{
			if(ke.getKeyCode()==KeyEvent.VK_CONTROL && !hasModifier(InputEvent.CTRL_DOWN_MASK,true)
					|| ke.getKeyCode()==KeyEvent.VK_SHIFT && !hasModifier(InputEvent.SHIFT_DOWN_MASK,true))
						{helpModifiers+=(ke.getKeyCode()==KeyEvent.VK_CONTROL)?
								InputEvent.CTRL_DOWN_MASK:InputEvent.SHIFT_DOWN_MASK;
						jbtChangeHelpKey.setText(texts.getText("changehelpkey")+" ("+
								InputEvent.getModifiersExText(helpModifiers)+"+");}
			else if(isValid(ke.getKeyCode(),true))
			{
				String s = (helpModifiers==0)?" (":"";
				newHelpKey=AWTKeyStroke.getAWTKeyStroke(ke.getKeyCode(),helpModifiers,true);
				jbtChangeHelpKey.setText(jbtChangeHelpKey.getText()+s+KeyEvent.getKeyText(ke.getKeyCode())+")");
				jbtChangeHelpKey.getAccessibleContext().setAccessibleName(
						texts.getText("changehelpkeyname")
						+jbtChangeHelpKey.getText().replaceFirst(texts.getText("changehelpkey"), ""));
				isNewHelpKeyPossible=false;
			}
		}
		if(ke.getSource()==jbtChangeTradKey && isNewTradKeyPossible)
		{
			
			if(ke.getKeyCode()==KeyEvent.VK_CONTROL && !hasModifier(InputEvent.CTRL_DOWN_MASK,false)
					|| ke.getKeyCode()==KeyEvent.VK_SHIFT && !hasModifier(InputEvent.SHIFT_DOWN_MASK,false))
						{tradModifiers+=(ke.getKeyCode()==KeyEvent.VK_CONTROL)?
								InputEvent.CTRL_DOWN_MASK:InputEvent.SHIFT_DOWN_MASK;
						jbtChangeTradKey.setText(texts.getText("changetradkey")+" ("+
								InputEvent.getModifiersExText(tradModifiers)+"+");}
			else if(isValid(ke.getKeyCode(),false))
			{
				String s = (tradModifiers==0)?" (":"";
				newTradKey=AWTKeyStroke.getAWTKeyStroke(ke.getKeyCode(),tradModifiers,true);
				jbtChangeTradKey.setText(jbtChangeTradKey.getText()+s+KeyEvent.getKeyText(ke.getKeyCode())+")");
				jbtChangeTradKey.getAccessibleContext().setAccessibleName(
						texts.getText("changetradkeyname")
						+jbtChangeTradKey.getText().replaceFirst(texts.getText("changetradkey"), ""));
				isNewTradKeyPossible=false;
			}
		}
	}

	/**
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	/** 
	 * checks if the modifier belongs to modifiers' list
	 * @param key : key of the modifier
	 * @param b : true : help / false : translation
	 * @return TODO
	 * */
	private boolean hasModifier(int key, Boolean b)
	{
		int i=13;
		int j=b?helpModifiers:tradModifiers;
		while(i>0)
		{
			if (Math.pow(2, i)==key && Math.pow(2, i)<=j) return true;
			if (Math.pow(2, i) <=j) j-=Math.pow(2,i);
			if (Math.pow(2,i)<key)return false;
			i--;
		}
		return false;
	}
	/**
	 * checks is the new key is valid
	 * @param key : key to check
	 * @param b : true : help / false : translation
	 * @return TODO
	 */
	private boolean isValid(int key, Boolean b)
	{
		if(key==KeyEvent.VK_CONTROL
		|| key==KeyEvent.VK_SHIFT
		|| key==KeyEvent.VK_ALT
		|| (key==KeyEvent.VK_SPACE && helpModifiers==0)
		|| KeyEvent.getKeyText(key).contains("Unknown")
		|| hasModifier(InputEvent.CTRL_DOWN_MASK, b)
			&& (key==KeyEvent.VK_1)
				|| (key==KeyEvent.VK_2)
				|| (key==KeyEvent.VK_3)
				|| (key==KeyEvent.VK_4)
				|| (key==KeyEvent.VK_5)
				|| (key==KeyEvent.VK_6)
				|| (key==KeyEvent.VK_7)
				|| (key==KeyEvent.VK_8)
			
		)return false;
		
		return true;
	}
}
