/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.config;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;
import ui.dialog.DialogueListe;

import nat.ConfigNat;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Onglet de configuration de la transcription
 * @author bruno
 *
 */
public class ConfTranscription extends OngletConf implements ItemListener, ActionListener
{
	/** Textual contents */
	private Language texts = new Language("ConfTranscription");
	
	/** Pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;
	/** Bouton activant toute les règles complémentaires de transcription */
	private JButton jbtTout = new JButton(texts.getText("all"));
	/** Bouton désactivant toutes les règles complémentaires de transcription */
	private JButton jbtRien = new JButton(texts.getText("none"));
	/** case à cocher utiliser le double préfixe majuscule*/
	private JCheckBox jchbMajDouble = new JCheckBox(texts.getText("doublecaps"));
	/** case à cocher utiliser les règles de passage en majuscule*/
	private JCheckBox jchbMajPassage = new JCheckBox(texts.getText("capsmode"));
	/** case à cocher utiliser les règles de mélange de minuscules et de majuscules*/
	private JCheckBox jchbMajMots = new JCheckBox(texts.getText("casesensitive"));
	/** case à cocher signaler la mise en évidence d'un mot*/
	private JCheckBox jchbEviMot = new JCheckBox(texts.getText("hlword"));
	/** case à cocher utiliser les règles des passages en évidence*/
	private JCheckBox jchbEviPassage = new JCheckBox(texts.getText("hlwords"));
	/** case à cocher utiliser les règles des mises en évidence à l'intérieur des mots*/
	private JCheckBox jchbEviDansMot = new JCheckBox(texts.getText("hlinsideword"));
	/** case à cocher utiliser la notation trigonométrique spécifique*/
	private JCheckBox jchbMathsTrigoSpec = new JCheckBox(texts.getText("trigo"));
	/** case à cocher utiliser la notation trigonométrique spécifique*/
	private JCheckBox jchbMathsPref = new JCheckBox(texts.getText("mathprefix"));
	/** case à cocher transcrire les images en braille*/
	private JCheckBox jchbImages = new JCheckBox(texts.getText("image"));
	/** JtextField contenant l'adresse du répertoire d'installation d'image magick*/
	private JTextField jtfIM = new JTextField(20);
	/** Bouton ouvrant le JFileChooser permettant de choisir le répertoire d'installation d'image magick */
	JButton jbtChoixIM = new JButton(texts.getText("browse"));
	/** jspinner indiquant le niveau de titre à partir duquel on abrège les titres */
	private JSpinner jspAbrTitres = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
	/** case à cocher demander les exeptions*/
	private JCheckBox jchbDemandeExc = new JCheckBox(texts.getText("exceptions"));
	/** Bouton ouvrant la fenetre d'édition des exception de l'abrégé */
	private JButton jbtOuvreListeException = new JButton(texts.getText("exclist"));
	/** case à cocher demander les ivb*/
	private JCheckBox jchbDemandeIVB = new JCheckBox(texts.getText("ivb"));
	/** Bouton ouvrant la fenêtre d'édition des ivb */
	private JButton jbtOuvreListeIVB = new JButton(texts.getText("ivblist"));
	
	/** Configuration contenant l'onglet */
	private Configuration parent;
	/** Constructeur 
	 * @param p la JFrame Configuration contenant l'onglet, utile pour l'ouverture des dialogues*/
	public ConfTranscription(Configuration p)
	{
		super();
		parent = p;
		// Works with NVDA. Doesn't work with JAWS
		getAccessibleContext().setAccessibleDescription(texts.getText("tabdesc"));
		getAccessibleContext().setAccessibleName(texts.getText("tabname"));
		setToolTipText(texts.getText("tabttt"));
		/**********
		 * Préparation des composants texts.getText("")
		 */
	    
		Context cJcbTout = new Context("t","Button","all",texts);
		jbtTout.setMnemonic('t');
		jbtTout.addActionListener(this);
		new ContextualHelp(jbtTout,"conftransoptions",cJcbTout);
		
		Context cJcbRien = new Context("r","Button","none",texts);
		jbtRien.setMnemonic('r');
		jbtRien.addActionListener(this);
		new ContextualHelp(jbtRien,"conftransoptions",cJcbRien);
		
		Context cJchbMajDouble = new Context("d","CheckBox","doublecaps",texts);
		jchbMajDouble.setMnemonic('d');
		jchbMajDouble.setSelected(ConfigNat.getCurrentConfig().getLitMajDouble());  
		jchbMajDouble.addItemListener(this);
		new ContextualHelp(jchbMajDouble,"conftransoptions",cJchbMajDouble);
	  
		Context cJchbMajPassage = new Context("p","CheckBox","capsmode",texts);
		jchbMajPassage.setMnemonic('p');
		jchbMajPassage.setSelected(ConfigNat.getCurrentConfig().getLitMajPassage());    
		jchbMajPassage.addItemListener(this);
		new ContextualHelp(jchbMajPassage,"conftransoptions",cJchbMajPassage);
		
		Context cJchbMajMots = new Context("m","CheckBox","casesensitive",texts);
		jchbMajMots.setMnemonic('m');
		jchbMajMots.setSelected(ConfigNat.getCurrentConfig().getLitMajMelange());	    
		jchbMajMots.addItemListener(this);
		new ContextualHelp(jchbMajMots,"conftransoptions",cJchbMajMots);
		
		Context cJchbEviMot = new Context("v","CheckBox","hlword",texts);
		jchbEviMot.setMnemonic('v');
		jchbEviMot.setSelected(ConfigNat.getCurrentConfig().getLitEvidenceMot());	    
		jchbEviMot.addItemListener(this);
		new ContextualHelp(jchbEviMot,"conftransoptions",cJchbEviMot);
		
		Context cJchbEviPassage = new Context("e","CheckBox","hlwords",texts);
		jchbEviPassage.setMnemonic('e');
		jchbEviPassage.setSelected(ConfigNat.getCurrentConfig().getLitEvidencePassage());	    
		jchbEviPassage.addItemListener(this);
		new ContextualHelp(jchbEviPassage,"conftransoptions",cJchbEviPassage);
		
		Context cJchbEviDansMot = new Context("i","CheckBox","hlinsideword",texts);
		jchbEviDansMot.setMnemonic('i');
		jchbEviDansMot.setSelected(ConfigNat.getCurrentConfig().getLitEvidenceDansMot());	    
		jchbEviDansMot.addItemListener(this);
		new ContextualHelp(jchbEviDansMot,"conftransoptions",cJchbEviDansMot);
		
		Context cJchbMathsTrigoSpec = new Context("q","CheckBox","trigo",texts);
		jchbMathsTrigoSpec.setMnemonic('q');
		jchbMathsTrigoSpec.setSelected(ConfigNat.getCurrentConfig().getMathTrigoSpec());
		jchbMathsTrigoSpec.addItemListener(this);
		new ContextualHelp(jchbMathsTrigoSpec,"conftransoptions",cJchbMathsTrigoSpec);
		
		Context cJchbMathsPref = new Context("x","CheckBox","mathprefix",texts);
		jchbMathsPref.setMnemonic('x');
		jchbMathsPref.setSelected(ConfigNat.getCurrentConfig().getMathPrefixAlways());
		jchbMathsPref.addItemListener(this);
		new ContextualHelp(jchbMathsPref,"conftransmaths",cJchbMathsPref);
		
		Context cJchbImages = new Context("a","CheckBox","image",texts);
		jchbImages.setMnemonic('a');
		jchbImages.setSelected(ConfigNat.getCurrentConfig().getTranscrireImages());
		new ContextualHelp(jchbImages,"conftransimages",cJchbImages);

		Context cTfIM = new Context("k","TextField","imloc",texts);
		jtfIM.setText(ConfigNat.getCurrentConfig().getImageMagickDir());
		new ContextualHelp(jtfIM,"conftransimages",cTfIM);
		
		Context cJbtChoixIM = new Context("c","Button","browse",texts);
		jbtChoixIM.addActionListener(this);
		jbtChoixIM.setMnemonic('c');
		new ContextualHelp(jbtChoixIM,"conftransimages",cJbtChoixIM);
		
		Context cJspAbrTitres = new Context("b","Spinner","shortsp",texts);
		JLabel lJspAbrTitres = new JLabel(texts.getText("shortsplabel"));
		lJspAbrTitres.setLabelFor(jspAbrTitres);
		jspAbrTitres.setValue(ConfigNat.getCurrentConfig().getNiveauTitreAbrege());
		lJspAbrTitres.setDisplayedMnemonic('b');
		new ContextualHelp(jspAbrTitres,"conftranstitles",cJspAbrTitres);
		
		Context cJchbDemandeExc = new Context("j","CheckBox","exceptions",texts);
		jchbDemandeExc.setSelected(ConfigNat.getCurrentConfig().getDemandeException());
		jchbDemandeExc.setMnemonic('j');
		new ContextualHelp(jchbDemandeExc,"conftransintegral",cJchbDemandeExc);
		
		Context cJbtOuvreListeException = new Context("l","Button","exclist",texts);
		jbtOuvreListeException.setMnemonic('l');
		jbtOuvreListeException.addActionListener(this);
		new ContextualHelp(jbtOuvreListeException,"conftransintegral",cJbtOuvreListeException);
		
		Context cJchbDemandeIVB = new Context("u","CheckBox","ivb",texts);
		jchbDemandeIVB.setSelected(ConfigNat.getCurrentConfig().getDemandeIVB());
		jchbDemandeIVB.setMnemonic('u');
		new ContextualHelp(jchbDemandeIVB,"conftransIVB",cJchbDemandeIVB);
		
		Context cJbtOuvreListeIVB = new Context("o","Button","ivblist",texts);
		jbtOuvreListeIVB.setMnemonic('o');
		jbtOuvreListeIVB.addActionListener(this);
		new ContextualHelp(jbtOuvreListeIVB,"conftransIVB",cJbtOuvreListeIVB);
		
		/*********
		 * Mise en page
		 */
		
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		
		
		JLabel titre = new JLabel(texts.getText("transcribetitle"));
		titre.getAccessibleContext().setAccessibleName(texts.getText("transcribetitlename"));
		JLabel titre2 = new JLabel(texts.getText("integraltitle"));
		titre2.getAccessibleContext().setAccessibleName(texts.getText("integraltitlename"));
		JLabel titre2_1 = new JLabel(texts.getText("shortenedtitle"));
		titre2_1.getAccessibleContext().setAccessibleName(texts.getText("shortenedtitlename"));
		JLabel titre3 = new JLabel(texts.getText("mathstitle"));
		titre3.getAccessibleContext().setAccessibleName(texts.getText("mathstitlename"));
		JLabel titre4 = new JLabel(texts.getText("imagestitle"));
		titre4.getAccessibleContext().setAccessibleName(texts.getText("imagestitlename"));
		JLabel titre5 = new JLabel(texts.getText("intabrtitle"));
		titre5.getAccessibleContext().setAccessibleName(texts.getText("intabrtitlename"));
		
		JLabel lIM = new JLabel(texts.getText("imloclabel"));
		lIM.setLabelFor(jtfIM);
		lIM.setDisplayedMnemonic('k');
		
		JPanel panTr = new JPanel();
		panTr.setLayout(gbl);
		gbc.anchor = GridBagConstraints.WEST;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth=3;
		gbl.setConstraints(titre, gbc);
		panTr.add(titre);
		
		gbc.gridy++;
		gbc.gridx = 1;
		gbc.gridwidth=1;
		gbl.setConstraints(jbtTout, gbc);
		panTr.add(jbtTout);
		
		gbc.gridx=3;
		gbc.gridwidth=1;
		gbl.setConstraints(jbtRien, gbc);
		panTr.add(jbtRien);
		
		gbc.gridx=0;
		gbc.gridy++;
		gbc.gridwidth=1;
		gbl.setConstraints(titre2, gbc);
		panTr.add(titre2);
		
		gbc.gridy++;
		gbc.gridx++;
		gbc.gridwidth=2;
		gbl.setConstraints(jchbMajDouble, gbc);
		panTr.add(jchbMajDouble);
		
		gbc.gridy++;
		gbl.setConstraints(jchbMajPassage, gbc);
		panTr.add(jchbMajPassage);
		
		gbc.gridy++;
		gbl.setConstraints(jchbMajMots, gbc);
		panTr.add(jchbMajMots);
		
		gbc.gridy++;
		gbl.setConstraints(jchbEviMot, gbc);
		panTr.add(jchbEviMot);
		
		gbc.gridy++;
		gbl.setConstraints(jchbEviDansMot, gbc);
		panTr.add(jchbEviDansMot);
		
		gbc.gridy++;
		gbl.setConstraints(jchbEviPassage, gbc);
		panTr.add(jchbEviPassage);
		
		gbc.gridy++;
		gbc.gridx = 0;
		gbc.gridwidth = 3;
		gbl.setConstraints(titre2_1, gbc);
		panTr.add(titre2_1);
		
		gbc.gridy++;
		gbc.gridx++;
		gbc.gridwidth = 2;
		gbl.setConstraints(lJspAbrTitres, gbc);
		panTr.add(lJspAbrTitres);
		
		gbc.gridx++;
		gbc.gridwidth = 1;
		gbl.setConstraints(jspAbrTitres, gbc);
		panTr.add(jspAbrTitres);
		
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth = 2;
		gbl.setConstraints(jchbDemandeIVB, gbc);
		panTr.add(jchbDemandeIVB);
		
		gbc.gridx+=2;
		gbc.gridwidth = 1;
		gbl.setConstraints(jbtOuvreListeIVB, gbc);
		panTr.add(jbtOuvreListeIVB);
		
		gbc.gridy++;
		gbc.gridx = 0;
		gbc.gridwidth = 3;
		gbl.setConstraints(titre5, gbc);
		panTr.add(titre5);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth = 2;
		gbl.setConstraints(jchbDemandeExc, gbc);
		panTr.add(jchbDemandeExc);
		
		gbc.gridx+=2;
		gbc.gridwidth = 1;
		gbl.setConstraints(jbtOuvreListeException, gbc);
		panTr.add(jbtOuvreListeException);
		
		gbc.gridy++;
		gbc.gridx = 0;
		gbc.gridwidth = 3;
		gbl.setConstraints(titre3, gbc);
		panTr.add(titre3);
		
		gbc.gridy++;
		gbc.gridx++;
		gbc.gridwidth = 2;
		gbl.setConstraints(jchbMathsTrigoSpec, gbc);
		panTr.add(jchbMathsTrigoSpec);
		
		gbc.gridy++;
		panTr.add(jchbMathsPref,gbc);
		
		gbc.gridy++;
		gbc.gridx = 0;
		gbl.setConstraints(titre4, gbc);
		panTr.add(titre4);
		
		gbc.gridy++;
		gbc.gridx++;
		gbl.setConstraints(jchbImages, gbc);
		panTr.add(jchbImages);
		
		gbc.gridy++;
		gbc.gridwidth = 1;
		gbl.setConstraints(lIM, gbc);
		panTr.add(lIM);

		gbc.gridx++;
		gbl.setConstraints(jtfIM, gbc);
		panTr.add(jtfIM);
		
		gbc.gridx++;
		gbl.setConstraints(jbtChoixIM, gbc);
		panTr.add(jbtChoixIM);

		add(panTr);
	}	
	/**
	 * Implémentation de java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 * <p>Active (resp. désactive) les boutons appliquant toutes (resp. aucune) règles</p>
	 * @see #jbtRien
	 * @see #jbtTout
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	@Override
    public void itemStateChanged(ItemEvent ie) 
	{
		jbtTout.setEnabled(true);
		jbtRien.setEnabled(true);
		if(((JCheckBox)(ie.getSource())).isSelected())
		{
			if(jchbMajDouble.isSelected()&&jchbMajPassage.isSelected()&&jchbMajMots.isSelected()&&
					jchbEviMot.isSelected()&&jchbEviPassage.isSelected()&&jchbEviDansMot.isSelected()
					&&jchbMathsTrigoSpec.isSelected())
			{
				jbtTout.setEnabled(false);
			}
		}
		else if(!(jchbMajDouble.isSelected()||jchbMajPassage.isSelected()||jchbMajMots.isSelected()||
					jchbEviMot.isSelected()||jchbEviPassage.isSelected()||jchbEviDansMot.isSelected()
					||jchbMathsTrigoSpec.isSelected()))
		{
			jbtRien.setEnabled(false);
		}		
	}
	/**
	 * Implémentation de java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 * Active (resp. désactive) toutes (resp.aucune) règle(s)
	 * @see #jbtRien
	 * @see #jbtTout
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
    public void actionPerformed(ActionEvent ae) 
	{
		if(ae.getSource()==jbtTout)
		{
			jchbMajDouble.setSelected(true);
			jchbMajPassage.setSelected(true);
			jchbMajMots.setSelected(true);
			jchbEviMot.setSelected(true);
			jchbEviPassage.setSelected(true);
			jchbEviDansMot.setSelected(true);
			jchbMathsTrigoSpec.setSelected(true);
			jbtTout.setEnabled(false);
			jbtRien.setEnabled(true);
		}
		else if(ae.getSource()==jbtRien)
		{
			jchbMajDouble.setSelected(false);
			jchbMajPassage.setSelected(false);
			jchbMajMots.setSelected(false);
			jchbEviMot.setSelected(false);
			jchbEviPassage.setSelected(false);
			jchbEviDansMot.setSelected(false);
			jchbMathsTrigoSpec.setSelected(false);
			jbtRien.setEnabled(false);
			jbtTout.setEnabled(true);
		}
		else if (ae.getSource()==jbtChoixIM)
		{
			JFileChooser selectionneAppli = new JFileChooser();
			selectionneAppli.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			selectionneAppli.setDialogTitle(texts.getText("magickfolderselect"));
	 		if (selectionneAppli.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
	         {    
	 			//si un fichier est selectionné, récupérer le fichier puis son path
	 			jtfIM.setText(selectionneAppli.getSelectedFile().getAbsolutePath()); 
	         }
		}
		else if (ae.getSource()==jbtOuvreListeException)
		{
			DialogueListe.showDialog(parent,false,parent.getGestErreur(),new Language("DialogueIntegral"),ConfigNat.getUserTempFolder()+"listeIntegral.txt",DialogueListe.INTEGRAL_LIST);
		}
		else if (ae.getSource()==jbtOuvreListeIVB)
		{
			DialogueListe.showDialog(parent,false,parent.getGestErreur(),new Language("DialogueIVB"),ConfigNat.getFichierIVB(),DialogueListe.IVB_LIST);
		}
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	@Override
    public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer()
	 */
	@Override
    public boolean enregistrer()
	{
		boolean retour = true;
		ConfigNat.getCurrentConfig().setLitEvidenceMot(jchbEviMot.isSelected());
		ConfigNat.getCurrentConfig().setLitEvidencePassage(jchbEviPassage.isSelected());
		ConfigNat.getCurrentConfig().setLitEvidenceDansMot(jchbEviDansMot.isSelected());
		ConfigNat.getCurrentConfig().setLitMajDouble(jchbMajDouble.isSelected());
		ConfigNat.getCurrentConfig().setLitMajMelange(jchbMajMots.isSelected());
		ConfigNat.getCurrentConfig().setLitMajPassage(jchbMajPassage.isSelected());
		ConfigNat.getCurrentConfig().setMathTrigoSpec(jchbMathsTrigoSpec.isSelected());
		ConfigNat.getCurrentConfig().setMathPrefixAlways(jchbMathsPref.isSelected());
		ConfigNat.getCurrentConfig().setTranscrireImages(jchbImages.isSelected());
		ConfigNat.getCurrentConfig().setImageMagickDir(jtfIM.getText());
		ConfigNat.getCurrentConfig().setNiveauTitreAbrege(((Integer)(jspAbrTitres.getValue())).intValue());
		ConfigNat.getCurrentConfig().setDemandeException(jchbDemandeExc.isSelected());
		ConfigNat.getCurrentConfig().setDemandeIVB(jchbDemandeIVB.isSelected());
		
		ConfigNat.getCurrentConfig().sauvegarder();
		return retour;
	}
}