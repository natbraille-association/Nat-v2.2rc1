/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui.config;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.SortedMap;
import java.util.Vector;
import nat.ConfigNat;

import ui.accessibility.AccessibleListStringRenderer;
import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import outils.FileToolKit;

/**
 * Onglet de configuration générale (principales options)
 * @see OngletConf
 * @author bruno
 *
 */
public class ConfGeneral extends OngletConf implements ActionListener, ItemListener
{
	/** Textual contents */
	private Language texts = new Language("ConfGeneral");
	
	/** Pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;

	/** Champ d'information sur la configuration */
	private JTextField tfInfos= new JTextField(20);
	/** JCheckBox activer braille abrégé */
	private JCheckBox abreger = new JCheckBox(texts.getText("shorten"));
	/** JCheckBox activer braille littéraire */
	private JCheckBox bLit = new JCheckBox(texts.getText("litbraille"));
	/** JCheckBox activer braille mathématique */
	private JCheckBox bMaths = new JCheckBox(texts.getText("mathbraille"));
	/** JCheckBox activer braille musical */
	private JCheckBox bMusique = new JCheckBox(texts.getText("musicbraille"));
	/** Liste des tables brailles */
	private JComboBox<BrailleTableListItem> comboTables;
	/** label pour la liste des tables braille */
	private JLabel lComboTables = new JLabel(texts.getText("brailletable"));
	/** bouton édition de la table braille */
	private JButton btEditTable =new JButton(texts.getText("tableedit"),new ImageIcon("ui/icon/gtk-edit.png"));
	/** liste des encodages possibles de la source */
	private JComboBox<String> jcbCharsetSource = new JComboBox<String>();
	/** label pour la liste des encodages possibles de la source */
	private JLabel ljcbCharsetSource = new JLabel(texts.getText("sourceencoding"));
	/** liste des encodages possibles de la sortie */
	private JComboBox<String> jcbCharsetSortie = new JComboBox<String>();
	/** label pour la liste des encodages possibles de la sortie */
	private JLabel ljcbCharsetSortie = new JLabel(texts.getText("outputencoding"));
	/** bouton pour l'édition des règles d'abrégé */
	private JButton btEditAbr = new JButton(texts.getText("shortenrules"),new ImageIcon("ui/icon/gtk-edit.png"));
	
	
	/**
	 * Active la mise en page (dont la coupure mathématique)
	 * Remplace jcbCoupure
	 * @since 2.0
	 */
	private JCheckBox jcbMEP = new JCheckBox(texts.getText("layout"));
	/**
	 * Active ou non la coupure
	 * @deprecated 2.0
	 * @see #jcbMEP
	 */
	@Deprecated
	private JCheckBox jcbCoupure = new JCheckBox(texts.getText("hyphenationactivation"));
	/** JCheckbox activer la coupure littéraire */
	private JCheckBox jcbCoupureLit = new JCheckBox(texts.getText("lithyphenation"));
	/** Bouton éditer les règles de coupure littéraire */
	private JButton btEditCoup =new JButton(texts.getText("hyphenationrule"),new ImageIcon("ui/icon/gtk-edit.png"));
	/** JCheckBox activer le mode sagouin pour la coupure */
	private JCheckBox jcbSagouin = new JCheckBox(texts.getText("crudemode"));

	
	/** L'instance de la fenêtre Configuration contenant l'onglet */ 
	private Configuration jfParent;
	
	/**
	 * Constructeur
	 * @param jfp la fenêtre parente Configuration 
	 */
	public ConfGeneral(Configuration jfp)
	{
		super();
		// Works with NVDA. Doesn't work with JAWS
		getAccessibleContext().setAccessibleDescription(texts.getText("tabdesc"));
		getAccessibleContext().setAccessibleName(texts.getText("tabname"));
		this.jfParent = jfp;
		
		/**********
		 * Préparation des composants
		 */
		
		JLabel titreInfos = new JLabel(texts.getText("description"));
		titreInfos.setLabelFor(tfInfos);
		titreInfos.setDisplayedMnemonic('d');
		Context cTitreInfos = new Context("d","TextField","configdesc",texts);
		tfInfos.setText(ConfigNat.getCurrentConfig().getInfos());
		new ContextualHelp(tfInfos,"confgendesc", cTitreInfos);
		
		
		bLit.setSelected(ConfigNat.getCurrentConfig().getTraiterLiteraire());
		Context cBLit = new Context("l","CheckBox","litbraille",texts);
		bLit.setMnemonic('l');
		new ContextualHelp(bLit,"confgencontents", cBLit);
		
		bMaths.setSelected(ConfigNat.getCurrentConfig().getTraiterMaths());
		Context cBMaths = new Context("m","CheckBox","mathbraille",texts);
		bMaths.setMnemonic('m');
		new ContextualHelp(bMaths,"confgencontents",cBMaths);
		
		//bMusique.setSelected(ConfigNat.getCurrentConfig().getTraiterMusique());
		bMusique.setSelected(false);
		Context cBMusique = new Context("u","CheckBox","musicbraille",texts);
		bMusique.setMnemonic('u');
		new ContextualHelp(bMusique,"confgencontents",cBMusique);
		
		abreger.setSelected(ConfigNat.getCurrentConfig().getAbreger());
		Context cAbreger = new Context("a","CheckBox","shorten",texts);
		abreger.setMnemonic('a');
		new ContextualHelp(abreger,"confgencontents",cAbreger);
		
		btEditAbr.addActionListener(this);
		btEditAbr.addItemListener(this);
		Context cBtEditAbr = new Context("h","Button","shortenrules",texts);
		btEditAbr.setMnemonic('h');
		new ContextualHelp(btEditAbr,"confgencontents",cBtEditAbr);

		/* Liste Tables braille */ 
		File repertoire =new File("xsl/tablesBraille");
		File[] listTablesSys = repertoire.listFiles();
		File rep2 = new File(ConfigNat.getUserBrailleTableFolder());
		File[] listTablesUser = rep2.listFiles();
	    
		int tailleSys = listTablesSys.length;
		int tailleUser = listTablesUser.length;
		
		Vector<BrailleTableListItem> namesList = new Vector<BrailleTableListItem>(tailleSys + tailleUser);
		
		for(int i=0;i<tailleSys;i++)
	    {
			String nomTable = FileToolKit.getSysDepPath(listTablesSys[i].getAbsolutePath()) ;
	    	if (nomTable.endsWith(".ent") && !nomTable.endsWith("Brltab.ent"))
	    	{
	    		//namesList.add(nomTable.substring(0,nomTable.length()-4));
	    		namesList.add(new BrailleTableListItem(nomTable,true));
	    	}
	    }
		for(int i=0;i<tailleUser;i++)
	    {
			String nomTable = FileToolKit.getSysDepPath(listTablesUser[i].getAbsolutePath()) ;
	    	if (nomTable.endsWith(".ent") && !nomTable.endsWith("Brltab.ent"))
	    	{
	    		//namesList.add(nomTable.substring(0,nomTable.length()-4));
	    		namesList.add(new BrailleTableListItem(nomTable,false));
	    	}
	    }
		
		Collections.sort(namesList);
		
		comboTables  = new JComboBox<BrailleTableListItem>(namesList);
		String selectedTablePath = ConfigNat.getUserBrailleTableFolder();
		if(ConfigNat.getCurrentConfig().getIsSysTable()){selectedTablePath = ConfigNat.getInstallFolder()+"xsl/tablesBraille/";}
		comboTables.setSelectedItem(new BrailleTableListItem(selectedTablePath+ConfigNat.getCurrentConfig().getTableBraille(),
				ConfigNat.getCurrentConfig().getIsSysTable()));
		comboTables.setRenderer(new BrailleTableComboBoxRenderer());
	    comboTables.setEditable(false);
		Context cComboTables = new Context("b","ComboBox","brailletables",texts);
		new ContextualHelp(comboTables,"confgenencoding",cComboTables);
		
		lComboTables.setLabelFor(comboTables);
		lComboTables.setDisplayedMnemonic('b');
	    
	    /* Encodage */
	    //ajout de la détection automatique
		jcbCharsetSource.addItem("automatique");
		jcbCharsetSortie.addItem("automatique");
		//récupération des charsets disponibles
		SortedMap<String,Charset> charsets = Charset.availableCharsets();
		for(String nom : charsets.keySet())
		{
			jcbCharsetSource.addItem(nom);
			jcbCharsetSortie.addItem(nom);

    		if(nom.equals(ConfigNat.getCurrentConfig().getNoirEncoding()))
    		{
    			jcbCharsetSource.setSelectedIndex(jcbCharsetSource.getItemCount()-1);
    		}
    		if(nom.equals(ConfigNat.getCurrentConfig().getBrailleEncoding()))
    		{
    			jcbCharsetSortie.setSelectedIndex(jcbCharsetSortie.getItemCount()-1);
    		}
		}
		Context cJcbCharsetSource = new Context("e","ComboBox","sourceencoding",texts);
		new ContextualHelp(jcbCharsetSource,"confgenencoding",cJcbCharsetSource);
		ljcbCharsetSource.setLabelFor(jcbCharsetSource);
		ljcbCharsetSource.setDisplayedMnemonic('e');
		AccessibleListStringRenderer sourceRenderer = new AccessibleListStringRenderer
												("accessories-dictionary-2.png",
												cJcbCharsetSource.getName(),
												cJcbCharsetSource.getTTT(),
												cJcbCharsetSource.getDesc());
		jcbCharsetSource.setRenderer(sourceRenderer);
		
		Context cJcbCharsetSortie = new Context("o","ComboBox","outputencoding",texts);
		new ContextualHelp(jcbCharsetSortie,"confgenencoding",cJcbCharsetSortie);

		ljcbCharsetSortie.setLabelFor(jcbCharsetSortie);
		ljcbCharsetSortie.setDisplayedMnemonic('o');
		AccessibleListStringRenderer outputRenderer = new AccessibleListStringRenderer
												("accessories-dictionary-2.png",
												cJcbCharsetSortie.getName(),
												cJcbCharsetSortie.getTTT(),
												cJcbCharsetSortie.getDesc());
		jcbCharsetSortie.setRenderer(outputRenderer);
		
		btEditTable.addActionListener(this);
		Context cBtEditTable = new Context("t","Button","tableedit",texts);
		btEditTable.setMnemonic('t');
		new ContextualHelp(btEditTable,"confgenencoding",cBtEditTable);
		
		jcbMEP.setSelected(ConfigNat.getCurrentConfig().getMep());
		Context cJcbMEP = new Context("i","CheckBox","layout",texts);
		jcbMEP.setMnemonic('i');	
		jcbMEP.addItemListener(this);
		jcbMEP.addActionListener(this.jfParent);
		new ContextualHelp(jcbMEP,"confgenlayout",cJcbMEP);
		
		/*jcbCoupure.setSelected(ConfigNat.getCurrentConfig().getCoupure());
		Context cJcbCoupure = new Context("","CheckBox","Case à cocher coupure","Cocher cette case pour activer la coupure");
		jcbCoupure.getAccessibleContext().setAccessibleName(cJcbCoupure.getName());
		jcbCoupure.getAccessibleContext().setAccessibleDescription(cJcbCoupure.getDesc());
		jcbCoupure.setToolTipText("Activer la coupure");
		jcbCoupure.setMnemonic('');	
		jcbCoupure.addItemListener(this);*/
		
		jcbCoupureLit.setSelected(ConfigNat.getCurrentConfig().getCoupureLit());
		Context cJcbCoupureLit = new Context("p","CheckBox","lithyphenation",texts);
		jcbCoupureLit.setMnemonic('p');
		new ContextualHelp(jcbCoupureLit,"confgenhyph",cJcbCoupureLit);
		
		btEditCoup.addActionListener(this);
		Context cBtEditCoup = new Context("r","Button","hyphenationrule",texts);
		btEditCoup.setMnemonic('r');
		new ContextualHelp(btEditCoup,"confgenedithyph",cBtEditCoup);

		
		jcbSagouin.setSelected(ConfigNat.getCurrentConfig().getModeCoupureSagouin());
		Context cJcbSagouin = new Context("c","CheckBox","crudemode",texts);
		jcbSagouin.setMnemonic('c');
		new ContextualHelp(jcbSagouin,"confgencrude",cJcbSagouin);

		
		//if (!jcbCoupure.isSelected()){jcbSagouin.setEnabled(false);}
		if (!jcbMEP.isSelected())
		{
			jcbSagouin.setEnabled(false);
			jcbCoupureLit.setEnabled(false);
		}
		
			
		/* ********
		 * Mise en page
		 */
		
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		JPanel panGen = new JPanel(gbl);
		
		gbc.anchor = GridBagConstraints.WEST;
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		
		gbl.setConstraints(titreInfos, gbc);
		panGen.add(titreInfos);
		
		gbc.gridx=1;
		gbc.gridy++;
		tfInfos.setCaretPosition(0);
		gbl.setConstraints(tfInfos, gbc);
		panGen.add(tfInfos);
		
		gbc.gridx=0;
		gbc.gridy++;
		JLabel titreEcritures = new JLabel(texts.getText("brailletitle"));
		titreEcritures.getAccessibleContext().setAccessibleName(texts.getText("brailletitlename"));
		titreEcritures.getAccessibleContext().setAccessibleDescription(texts.getText("brailletitledesc"));
		gbl.setConstraints(titreEcritures, gbc);
		panGen.add(titreEcritures);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(bLit, gbc);
		panGen.add(bLit);
		
		gbc.gridy++;
		gbl.setConstraints(bMaths, gbc);
		panGen.add(bMaths);
		
		/*gbc.gridy++;
		gbl.setConstraints(bMusique, gbc);
		panGen.add(bMusique);*/
		
		gbc.gridy++;
		gbc.gridwidth=  1;
		gbl.setConstraints(abreger, gbc);
		panGen.add(abreger);
		
		gbc.gridx++;
		gbl.setConstraints(btEditAbr, gbc);
		panGen.add(btEditAbr);
		
		JLabel titreEncodage = new JLabel(texts.getText("encodingtitle"));
		gbc.gridy++;
		gbc.gridx=0;
		gbc.gridwidth = 3;
		gbl.setConstraints(titreEncodage, gbc);
		panGen.add(titreEncodage);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth = 1;
		gbl.setConstraints(lComboTables, gbc);
		panGen.add(lComboTables);
		gbc.gridx++;
		gbl.setConstraints(comboTables, gbc);
		panGen.add(comboTables);
		gbc.gridx++;
		gbl.setConstraints(btEditTable, gbc);
		panGen.add(btEditTable);

		gbc.gridy++;
		gbc.gridx = 1;
		gbl.setConstraints(ljcbCharsetSource, gbc);
		panGen.add(ljcbCharsetSource);
		gbc.gridx++;
		gbl.setConstraints(jcbCharsetSource, gbc);
		panGen.add(jcbCharsetSource);
		
		gbc.gridy++;
		gbc.gridx = 1;
		gbl.setConstraints(ljcbCharsetSortie, gbc);
		panGen.add(ljcbCharsetSortie);
		gbc.gridx++;
		gbl.setConstraints(jcbCharsetSortie, gbc);
		panGen.add(jcbCharsetSortie);
	
		gbc.gridx = 0;
		gbc.gridy++;
		gbc.gridwidth = 1;
		JLabel titreCoupure = new JLabel(texts.getText("layouttitle"));
		titreCoupure.getAccessibleContext().setAccessibleName(texts.getText("layouttitlename"));
		titreCoupure.getAccessibleContext().setAccessibleDescription(texts.getText("layouttitledesc"));
		gbl.setConstraints(titreCoupure, gbc);
		panGen.add(titreCoupure);
		
		gbc.gridwidth = 1;
		gbc.gridx=1;
		gbc.gridy++;
		gbl.setConstraints(jcbMEP, gbc);
		panGen.add(jcbMEP);
		
		/*gbc.gridwidth = 1;
		gbc.gridx=1;
		gbc.gridy++;
		gbl.setConstraints(jcbCoupure, gbc);
		add(jcbCoupure);*/
		
		gbc.gridy++;
		gbl.setConstraints(jcbSagouin, gbc);
		panGen.add(jcbSagouin);
		
		gbc.gridy++;
		gbl.setConstraints(jcbCoupureLit, gbc);
		panGen.add(jcbCoupureLit);
		
		gbc.gridx++;
		gbl.setConstraints(btEditCoup, gbc);
		panGen.add(btEditCoup);
		
		setLayout(new BorderLayout());
		add(panGen,BorderLayout.NORTH);
	}
	
	/**
	 * Redéfinie de ActionListener
	 * Ouvre les éditions de la table braille ou des règles de coupure
	 * @param evt L'instance d'ActionEvent
	 */
	@Override
    public void actionPerformed(ActionEvent evt)
	{
		if (evt.getSource()==btEditTable)
		{
			ConfTableBraille ctb = new ConfTableBraille((BrailleTableListItem)comboTables.getSelectedItem(), jfParent.getGmtb());
			ctb.setVisible(true);
		}
		else if (evt.getSource()==btEditCoup)
		{
			ConfDictCoup cdc = new ConfDictCoup();
			cdc.setVisible(true);
		}
		else if (evt.getSource()==btEditAbr)
		{
			new ConfAbrege(jfParent.getGestErreur());
		}
	}
	/**
	 * Redéfinie de ItemListener
	 * Active ou désactive les options de mise en page suivant la valeur de {@link #jcbMEP}
	 * @param e l'ItemEvent
	 */
	@Override
    public void itemStateChanged(ItemEvent e)
	{
		if(e.getSource()==btEditAbr)
		{
			System.err.println("caca");//new ContextualHelp(btEditAbr, "");
		}
		if(e.getSource()==jcbMEP)
		{
			if(!jcbMEP.isSelected())
			{
				jcbSagouin.setEnabled(false);
				jcbCoupureLit.setEnabled(false);
			}
			else
			{
				jcbSagouin.setEnabled(true);
				jcbCoupureLit.setEnabled(true);
			}
		}
	}
	/**
	 * Enregistre les options de l'onglet(non-Javadoc)
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	@Override
    public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.config.SavableTabbedConfigurationPane#enregistrer()
	 */
	@Override
    public boolean enregistrer()
	{
		boolean retour = true;
		try
		{
			ConfigNat.getCurrentConfig().setInfos(tfInfos.getText());
			ConfigNat.getCurrentConfig().setTableBraille(comboTables.getSelectedItem().toString(),
					((BrailleTableListItem)comboTables.getSelectedItem()).getIsSystem());
			ConfigNat.getCurrentConfig().setIsSysTable(((BrailleTableListItem)comboTables.getSelectedItem()).getIsSystem());
			ConfigNat.getCurrentConfig().setAbreger(abreger.isSelected());
			ConfigNat.getCurrentConfig().setTraiterLiteraire(bLit.isSelected());
			ConfigNat.getCurrentConfig().setTraiterMaths(bMaths.isSelected());
			ConfigNat.getCurrentConfig().setTraiterMusique(bMusique.isSelected());
			ConfigNat.getCurrentConfig().setNoirEncoding((String)jcbCharsetSource.getSelectedItem());
			ConfigNat.getCurrentConfig().setBrailleEncoding((String)jcbCharsetSortie.getSelectedItem());
			ConfigNat.getCurrentConfig().setMep(jcbMEP.isSelected());
			ConfigNat.getCurrentConfig().setCoupure(jcbCoupure.isSelected());
			ConfigNat.getCurrentConfig().setCoupureLit(jcbCoupureLit.isSelected());
			ConfigNat.getCurrentConfig().setModeCoupureSagouin(jcbSagouin.isSelected());
		}
		catch(Exception e){e.printStackTrace();retour=false;}
		
		return retour;
	}
	/**
	 * Méthode d'accès
	 * @return le JComboBox {@link #comboTables}
	 */
	public JComboBox<BrailleTableListItem> getComboTables(){return comboTables;}
	/**
	 * Méthode d'accès
	 * @return le JCheckBox {@link #jcbMEP}
	 */
	public JCheckBox getMepBox() {return jcbMEP;}
}
