/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui.config;

import javax.swing.JTextField;
import nat.OptDef;

/**
 * This class provides an OptionPropertyString definition
 * @author Vivien, comments added by Bruno
 *
 */
public class OptionsPropertyString  extends OptionsProperty {

	/** The JTextField instance */
    private JTextField textField = new JTextField();

    
    /**
     * Constructor
     * @param od the OptDef instance
     */
    public OptionsPropertyString(OptDef od)
    {
		super(od);
		addCompo();
    }
    
    /**
     * adds {@link #textField} to {@link OptionsProperty#panel}
     */
    public void addCompo(){panel.add(textField);}
    
    
    /**
     * @see ui.config.OptionsProperty#setValue(java.lang.String)
     */
    @Override
    public void setValue(String value){textField.setText(value);}
    /**
     * @see ui.config.OptionsProperty#getValue()
     */
    @Override
    public String getValue(){return textField.getText();}
    
}
