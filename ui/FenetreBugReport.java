/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui;

import gestionnaires.GestionnaireErreur;

import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;

import nat.ConfigNat;
import nat.Nat;

import java.io.IOException;
/**
 * Fenêtre générant les rapports de bug
 * @author bruno
 *
 */
public class FenetreBugReport extends JDialog implements ActionListener
{
	/** Textual contents */
	private static Language texts = new Language("FenetreBugReport");

	/** Pour la sérialisation (non utilisé)*/
	private static final long serialVersionUID = 1L;
	/** message du rapport */
	private String message="";
	/** champs de texte de titre du bug */
	private JTextField jtfDesc = new JTextField(20);
	/** zone de texte description du bug */
	private JTextArea jtaDescription = new JTextArea(5,20);
	/** JCheckBox activant l'insertion des fichiers dans le rapport */
	private JCheckBox jchbWithTmpFiles = new JCheckBox(texts.getText("addlogfile"),true);
	/** bouton envoyer le rapport */
	private JButton btEnvoyer = new JButton(texts.getText("submit"));
	/** bouton annuler */
	private JButton btAnnuler = new JButton(texts.getText("cancel"));
	/** Gestionnaire d'erreur récupéré de fenêtre princ	 */
	private GestionnaireErreur gest = null;
	
	/**
	 * Constructeur
	 * @param f instance de la fenêtre principale appellante
	 * @param m valeur pour {@link #message}s
	 */
	public FenetreBugReport(FenetrePrinc f,String m)
	{
		super(f, texts.getText("bugreport"));
		message = m;
		setModal(true);
		fabriqueFenetre();
		gest = f.getGestErreur();
	}

	/**
	 * Fabrique la fenêtre de dialogue et ses composants
	 */
	private void fabriqueFenetre()
	{
		// préparation des composants
		Context cjtfDesc = new Context("t","TextField","title",texts);
		new ContextualHelp(jtfDesc,"FAQ",cjtfDesc);
		JLabel lDesc = new JLabel(texts.getText("titlelabel"));
		lDesc.setLabelFor(jtfDesc);
		lDesc.setDisplayedMnemonic('t');

		Context cjtaDescription = new Context("d","TextArea","message",texts);
		new ContextualHelp(jtaDescription,"FAQ",cjtaDescription);
		JLabel lDescLong = new JLabel(texts.getText("messagelabel"));
		lDescLong.setLabelFor(jtaDescription);
		lDescLong.setDisplayedMnemonic('d');
		
		jchbWithTmpFiles.setMnemonic('i');
		Context cjchbWithTmpFiles = new Context("i","CheckBox","addlogfile",texts);
		new ContextualHelp(jchbWithTmpFiles,"FAQ",cjchbWithTmpFiles);
		
		JLabel lAttach = new JLabel("<html><font color=\"red\">"+texts.getText("addsourcefile")+"</font>" +
				" "+texts.getText("toyourmsg"));
		
		btAnnuler.setMnemonic('a');
		btAnnuler.addActionListener(this);
		Context cbtAnnuler = new Context("a","Button","cancel",texts);
		new ContextualHelp(btAnnuler,"FAQ",cbtAnnuler);
		
		btEnvoyer.setMnemonic('e');
		btEnvoyer.addActionListener(this);
		Context cbtEnvoyer = new Context("e","Button","submit",texts);
		new ContextualHelp(btEnvoyer,"FAQ",cbtEnvoyer);
		
		// mise en forme
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.anchor = GridBagConstraints.BASELINE_LEADING;
		c.insets = new Insets(3,3,3,3);
		c.gridwidth = 1;
		c.gridx=0;
		c.gridy=0;
		add(lDesc,c);
		
		c.gridx++;
		add(jtfDesc,c);
		
		c.gridx=0;
		c.gridy++;
		add(lDescLong,c);
		
		c.gridx++;
		add(jtaDescription,c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=2;
		add(jchbWithTmpFiles,c);
		
		c.gridy++;
		add(lAttach,c);
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth=1;
		c.gridy++;
		add(btEnvoyer,c);
		
		c.gridx++;
		add(btAnnuler,c);
		
		pack();
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	}

	/** 
	 * Redéfinition; gère les actions sur les boutons {@link #btAnnuler} et {@link #btEnvoyer}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent ae) 
	{
		if(ae.getSource()==btEnvoyer){envoyer();}
		else if(ae.getSource()==btAnnuler){dispose();}
	}

	/**
	 * Gère la préparation du message à envoyer.
	 * <p>Si Desktop permet d'utiliser mail(), préparation de l'URL du mail.</p>
	 * <p>Sinon, préparation d'un message à copier-coller</p>
	 */
	private void envoyer()
	{
		// utilisation du client mail par défaut
		Desktop bureau = Desktop.getDesktop();
		boolean ok = true;
		if(bureau.isSupported(Desktop.Action.MAIL))
		{
			URI uriMailTo;
			
			String body = jtaDescription.getText();
			if(jchbWithTmpFiles.isSelected()){body = body + "\n" + message;}
			String mail = "nat-dev@listes.univ-lyon1.fr?" +
				"SUBJECT=" + jtfDesc.getText().replaceAll("&", "amp;") +
				"&BODY=" + body.replaceAll("&", "amp;");
			try
			{
				uriMailTo = new URI("mailto", mail, null);
				bureau.mail(uriMailTo);
				dispose();
			}
			catch (URISyntaxException e)
			{
				gest.afficheMessage(texts.getText("syntaxerror"), Nat.LOG_SILENCIEUX);
				if(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG){e.printStackTrace();}
				ok = false;
			}
			catch (IOException e)
			{
				gest.afficheMessage(texts.getText("iobug"), Nat.LOG_SILENCIEUX);
				if(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG){e.printStackTrace();}
				ok = false;
			}
        }
		else{ok = false;}
		if(!ok)
		{
			JOptionPane.showMessageDialog(this,"<html>"+texts.getText("error1")+"<br>" +
					texts.getText("error2")+
					texts.getText("error3")+"</html>",
					texts.getText("error4"),JOptionPane.ERROR_MESSAGE);
		}
	}
}