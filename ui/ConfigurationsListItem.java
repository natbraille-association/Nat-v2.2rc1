/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret, Frédérick Schwebel, Vivien Guillet
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui;

import gestionnaires.GestionnaireErreur;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
//import java.io.InputStreamReader;
import java.util.Properties;

import nat.OptNames;
import nat.Scenario;
/**
 * Item de liste pour le JComboBox de la fenêtre principale de NAT contenant 
 * les configurations possibles
 * @see FenetrePrinc
 * @author Vivien, Bruno
 *
 */
public class ConfigurationsListItem implements Comparable<ConfigurationsListItem>
{
    /** adresse du fichier de configuration */
    private String  filename = null;
    /** Nom de la configuration */
    private String  name = null;
    /** Informations sur la configuration */
    private String  infos = null;
    /** Vrai si la configuration est une configuration système */
    private boolean isSystem;
    /** vrai si la configuration est valide */
    private boolean isValid;
    /** Scenario de la conf, =null si conf normale */
    private Scenario scen = null;
    /** numéro de l'étape (0 si pas scnéario)*/
    private int step = 0;
    /** dernière étape du scénario?)*/
    private boolean lastStep = false;
    /**
     * Constructeur
     * @param in_filename adresse du fichier de configuration
     * @param gest GestionnaireErreur instance
     */
    public ConfigurationsListItem(String in_filename, GestionnaireErreur gest)
    {
    	//scenario ou conf simple?
    	boolean valid = true;
    	File f = new File(in_filename);
    	if (f.isDirectory())
    	{
    		//a true scenario ? must content a CONTENT_FILE file
    		if(new File(in_filename+"/"+Scenario.CONTENT_FILE).exists())
    		{
    			scen = new Scenario(in_filename,gest);
				if (scen.getStep() == 0) {
					scen.setStep(1);
				
				}
    			//steps availables?
    			if(scen.getSize()>0)
    			{
    				in_filename = scen.getCurrentStepPath();
    				step = scen.getCurrentStep();
    				lastStep = scen.getSize()==step;
    			}
    			else{valid = false;}
    		}
    		else
    		{
    			valid = false;
    			//System.err.println("Not a true scenario dir: "+in_filename);
    		}
    	}
    	if(valid)
    	{
			//System.out.println ("nik conf check : "+in_filename+" existe "+f.isDirectory());
    		Properties conf = new Properties();
			try
			{
			    conf.load(new FileInputStream(in_filename));
			    isSystem = new Boolean(conf.getProperty(OptNames.fi_is_sys_config,"false")).booleanValue();
			    infos    = conf.getProperty(OptNames.fi_infos);
			    name     = conf.getProperty(OptNames.fi_name);
			    filename = in_filename;
			    if ((filename != null)&&(name != null)){isValid = true;}
			    else {isValid = false;}
			}
			catch (IOException ioe) {System.err.println("Exception while trying to parse config file "+in_filename);}
    	}
    }
    /**
     * Compare deux configurations, une (<code>cli</code>) représentée par une instance de ConfigurationsListItem
     * et l'autre par le nom de la configuration représentée par <code>this</code>
     * @param cli l'instance de {@link ConfigurationsListItem}
     * @return true si {@link #filename} est le même que <code>cli</code> 
     */
    public boolean equals(ConfigurationsListItem cli){return (filename.equals(cli));}
    /**
     * Méthode d'accès en lecture à {@link #isValid}
     * @return {@link #isValid}
     */
    public boolean getIsValid(){return isValid;}
    /**
     * Méthode d'accès en lecture à {@link #filename}
     * @return {@link #filename}
     */
    public String getFilename(){return filename;}
    /**
     * Méthode d'accès en lecture à {@link #name}
     * Si scénario, préfixage par nom scénario = étape
     * @return {@link #name}
     */
    public String getName()
    {
    	String ret = name;
    	if(scen!=null){ret = "Scénario "+scen.getPrettyName()+"("+scen.getCurrentStep()+"/"+scen.getSize()+"):"+ret;} 
    	return ret;
    }
    /**
     * Méthode d'accès en lecture à {@link #infos}
     * @return {@link #infos}
     */
    public String getInfos()
    {
    	String ret = infos;
    	if(scen!=null){ret = "Scénario "+scen.getPrettyName()+"("+scen.getCurrentStep()+"/"+scen.getSize()+"):"+ret;}
    	return ret;
    }
    /**
     * Méthode d'accès en lecture à {@link #isSystem}
     * @return {@link #isSystem}
     */
    public boolean getIsSystem(){return isSystem;}
    /** @return {@link #isStep}*/
    public boolean isStep(){return scen!=null;}
    /** @return the step */
    public int getStep(){return step;}
    /** @param s the step to set*/
    public void setStep(int s){scen.setStep(s);}
    /** @return {@link #lastStep} */
    public boolean isLastStep(){return lastStep;}
    /** @return size of {@link #scen} in step*/
    public int getScenSize(){return scen.getSize();}

	/**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(ConfigurationsListItem cli)
    {
    	int retour = 0;
	    if(cli.isSystem)
	    {
	    	if(isSystem){retour = getName().toLowerCase().compareTo(cli.getName().toLowerCase());}
	    	else{retour = -1;}
	    }
	    else if(isSystem){retour = 1;}
	    else{retour = getName().toLowerCase().compareTo(cli.getName().toLowerCase());}
	    return retour;
    }
    
}
