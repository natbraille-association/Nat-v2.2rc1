/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui.listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.text.DefaultEditorKit;

import nat.ConfigNat;

/**
 * This class is a Perkins input observer
 * It notifies registered objects and send them the converted char
 * @author bruno
 *
 */
public class PerkinsNotifier implements KeyListener
{

	/** Braille table file not valid code*/
	public static final int FILE_NOT_VALID = 1;
	
	/** tableau destiné à recevoir les codes perkins */
	protected boolean [] tabPoint = new boolean[6];
	/** nombre de touches pressées lors d'une saisie perkins */
	protected int nbTouches = 0;
	/** table braille à utiliser pour la saisie perkins */
	protected String tableBraille = ConfigNat.getUserBrailleTableFolder()+"Brltab.ent";
	/** HashTable pour la correspondance entre les caractères braille saisis en mode perkins et leur
	 * représentation dans la table braille */
	protected HashMap<String,String> ptPerkins = new HashMap<String,String>();
	/**
	 * Code du caractère produit par la saisie Perkins 
	 * Perkins char code result
	 **/
	protected int res = 0;

	/**
	 * Abonnés à la saisie Perkins
	 * MUST extends JComponent
	 * Perkins Observers
	 */
	private ArrayList<PerkinsObserver> jcPerkinsObserver = new ArrayList<PerkinsObserver>();

	/**
	 * Default constructor
	 * The braille table is the
	 */
	public PerkinsNotifier()
	{
		try
        {
	        setTableBraille(tableBraille);
        }
        catch (NumberFormatException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (IOException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
	}
	
	/**
	 * Constructor that sets up {@link #tableBraille} by calling {@link #setTableBraille(String)}
	 * @param bt the braille table to be used
	 * @throws IOException problème d'entrée sortie avec le fichier table braille
	 * @throws NumberFormatException problème de conversion des données du fichier table braille
	 */
	public PerkinsNotifier(String bt) throws NumberFormatException, IOException{setTableBraille(bt);}
	
	/**
	 * Adds a PerkinsObserver to {@link #jcPerkinsObserver}
	 * po MUST implements PerkinsObserver and MUST be a child of JComponent
	 * @param po the PerkinsObserver to be added
	 * @return true if success
	 */
	public boolean addPerkinsObserver(PerkinsObserver po)
	{
		boolean retour = false;
		if(po instanceof JComponent)
		{
			JComponent jc = (JComponent) po;
			retour = jcPerkinsObserver.add(po);
			setPerkinsMap(jc);
	        jc.addKeyListener(this);
	        //System.out.println("oui: " + jc.toString());
		}
		return retour;
	}
	
	/**
	 * Adds all PerkinsObserver in {@link #jcPerkinsObserver}
	 * and restore the input map of each component
	 * @param l the PerkinsObserver list
	 */
	public void addAll(ArrayList<PerkinsObserver> l)
	{
		for(PerkinsObserver po:l){addPerkinsObserver(po);}
	}
	
	/**
	 * Removes a PerkinsObserver to {@link #jcPerkinsObserver}
	 * @param po the PerkinsObserver to be removed
	 * @return true if success
	 */
	public boolean removePerkinsObserver(PerkinsObserver po)
	{
		boolean retour = false;
		if(po instanceof JComponent)
		{
			JComponent jc = (JComponent) po;
			restoreMap(jc);
	        retour = jcPerkinsObserver.remove(po);
		}
		return retour;
	}
	
	/**
	 * Removes all PerkinsObserver in {@link #jcPerkinsObserver}
	 * and restore the input map of each component
	 */
	public void removeAllPerkinsObserver()
	{
		for(PerkinsObserver po:jcPerkinsObserver)
		{
			restoreMap((JComponent)po);
		}
		jcPerkinsObserver.clear();
	}
	
	/**
	 * Sets the Perkins Input Map for the component jc
	 * @param jc the component to be set
	 */
	private void setPerkinsMap(JComponent jc)
	{
		/* Changement de l'action par défaut pour les touches de la saisie
		 * perkins (s, d, f j, k, l). Amélioration possible: plutôt que d'utiliser
		 * writableAction, fabriquer une Action qui ne fait rien
		 */
		InputMap inputMap = jc.getInputMap();
        KeyStroke key = KeyStroke.getKeyStroke('s');
        inputMap.put(key, DefaultEditorKit.writableAction);
        key = KeyStroke.getKeyStroke('d');
        inputMap.put(key, DefaultEditorKit.writableAction);
        key = KeyStroke.getKeyStroke('f');
        inputMap.put(key, DefaultEditorKit.writableAction);
        key = KeyStroke.getKeyStroke('j');
        inputMap.put(key, DefaultEditorKit.writableAction);
        key = KeyStroke.getKeyStroke('k');
        inputMap.put(key, DefaultEditorKit.writableAction);
        key = KeyStroke.getKeyStroke('l');
        inputMap.put(key, DefaultEditorKit.writableAction);
        key = KeyStroke.getKeyStroke(' ');
        inputMap.put(key, DefaultEditorKit.writableAction);
	}
	
	/**
	 * rétablissement de l'action par défaut associée aux touches de la saisie perkins
	 * @param jc the JComponent to be restored
	 */
	private void restoreMap(JComponent jc)
	{
		/* rétablissement de l'action par défaut associée aux touches de la saisie perkins*/
		InputMap inputMap = jc.getInputMap();
        KeyStroke key = KeyStroke.getKeyStroke('s');
        inputMap.put(key, DefaultEditorKit.defaultKeyTypedAction);
        key = KeyStroke.getKeyStroke('d');
        inputMap.put(key, DefaultEditorKit.defaultKeyTypedAction);
        key = KeyStroke.getKeyStroke('f');
        inputMap.put(key, DefaultEditorKit.defaultKeyTypedAction);
        key = KeyStroke.getKeyStroke('j');
        inputMap.put(key, DefaultEditorKit.defaultKeyTypedAction);
        key = KeyStroke.getKeyStroke('k');
        inputMap.put(key, DefaultEditorKit.defaultKeyTypedAction);
        key = KeyStroke.getKeyStroke('l');
        inputMap.put(key, DefaultEditorKit.defaultKeyTypedAction);
        key = KeyStroke.getKeyStroke(' ');
        inputMap.put(key, DefaultEditorKit.defaultKeyTypedAction);
	}
	
	/** Méthode d'accès, indique le nom de la table braille à utiliser 
	 * Modifie ensuite la map {@link #ptPerkins}
	 * @param tb valeur pour {@link #tableBraille}
	 * @throws IOException problème d'entrée sortie avec le fichier table braille
	 * @throws NumberFormatException problème de conversion des données du fichier table braille*/
	public void setTableBraille(String tb) throws NumberFormatException, IOException
	{
		tableBraille = tb;
		initialiseMap();
	}
	
	/**
	 * Initialise la HashMap d'équivalence entre les entités de la forme &pt123456; et leur
	 * valeur en binaire.
	 * Fait appel à {@link #initialiseMap(String, HashMap)} avec comme paramètre {@link #tableBraille} pour 
	 * la table braille et {@link #ptPerkins} pour la Hashtable
	 * @return true si succès, false si erreut lors du chargement
	 * @throws IOException problème d'entrée sortie avec le fichier table braille
	 * @throws NumberFormatException problème de conversion des données du fichier table braille 
	 */
	protected boolean initialiseMap() throws NumberFormatException, IOException
	{
		return initialiseMap(tableBraille, ptPerkins);
	}
	
	/**
	 * Initialise une HashMap d'équivalence entre les entités de la forme &pt123456; et leur
	 * valeur en binaire
	 * @param fichierTable adresse de la table braille à utiliser
	 * @param table la Hashtable cible
	 * @return true si succès, false si erreur lors du chargement
	 * @throws IOException problème d'entrée sortie avec le fichier table braille
	 * @throws NumberFormatException problème de conversion des données du fichier table braille
	 * @exception NumberFormatException problème lors de la conversion des entités, la table ne doit pas être valide
	 */
	public boolean initialiseMap(String fichierTable,HashMap<String,String> table) throws IOException, NumberFormatException
	{
		boolean retour = true;
		//création de la hashtable pt -> caractère 
		//String fichierTable = tableBraille;
	
		RandomAccessFile raf = new RandomAccessFile(fichierTable, "r");
		String ligne;
		String[] enregistrement;
		int i=0;
  
		ligne = raf.readLine();
		//on cherche le début des entitées
		while(ligne!=null && !ligne.startsWith("<!ENTITY"))
		{
			ligne = raf.readLine();
		}
		if (ligne==null)
		{
			sendError(FILE_NOT_VALID, fichierTable);
			
			retour =false;
		}
		else
		{
			do
			{
				String pt = "0";
				String code ="";
			   enregistrement = ligne.split(" ");
			   if(!enregistrement[2].startsWith("\"&#"))
			   {
				  if (enregistrement[2].startsWith("\"&apos;")){code = "'";}
				  else if (enregistrement[2].startsWith("\"&quot;")){code = "\"";}
				  else if (enregistrement[2].startsWith("\"&lt;")){code = "<";}
				  else if (enregistrement[2].startsWith("\"&gt;")){code = ">";}
				  else {code = "&";}
				}
			   else{ code = Character.toString((char)(Integer.parseInt(enregistrement[2].substring(3, enregistrement[2].length()-3))));}
			   pt = convertitPoint2Int(enregistrement[1].substring(2,enregistrement[1].length()));
			   table.put(pt, code);
			   i++;
			}
		   while ((ligne = raf.readLine()) != null && i<64);
		}
		raf.close();
		/*
	   catch (IOException e)
	   {
		   System.err.println(texts.getText("bugin")+": " + e);
		   message.setText(texts.getText("ioerror"));
	   }
	   catch (NumberFormatException e)
	   {
		   System.err.println(texts.getText("brailletablenotvalid")+": " + e);
		   message.setText("<html><br> <p color=\"red\">"+texts.getText("brailletablenotvalid")+"</p></html>");
	   }*/
	   return retour;
	}
	
	/**
	 * Notifies listener that an error occured 
     * @param errorCode the error code (should be a constant of this class) 
     * @param message the error message
     */
    private void sendError(int errorCode, String message)
    {
	    for(PerkinsObserver po:jcPerkinsObserver){po.manageError(errorCode, message);}
	}

	/**
	 * Outil de conversion des entités 123456 par la notation "binaire"
	 * @param s La chaine d'origine sous forme 123456
	 * @return Une chaine représentant l'entier en base 10 obtenu par conversion binaire
	 */
	private String convertitPoint2Int(String s)
	{
		int retour = 0;
		if (s.indexOf("1")>= 0){retour = retour + 1;}
		if (s.indexOf("2")>= 0){retour = retour + 2;}
		if (s.indexOf("3")>= 0){retour = retour + 4;}
		if (s.indexOf("4")>= 0){retour = retour + 8;}
		if (s.indexOf("5")>= 0){retour = retour + 16;}
		if (s.indexOf("6")>= 0){retour = retour + 32;}
		return Integer.toString(retour);
	}
	
	/** Méthode redéfinie de KeyListener
	 * Gère la navigation
	 * L'affichage est réalisé dans la méthode keyReleased
	 * @param e L'objet KeyEvent intercepté
	 */
	@Override
    public void keyPressed(KeyEvent e)
	{
		char ch = ' ';
		ch = e.getKeyChar();

		
		switch (ch)
		{
			case 's':
				tabPoint[3]=true;
				nbTouches++;
			break;
			case 'd':
				tabPoint[4]=true;
				nbTouches++;
			break;
			case 'f':
				tabPoint[5]=true;
				nbTouches++;
			break;
			case 'l':
				tabPoint[0]=true;
				nbTouches++;
			break;
			case 'k':
				tabPoint[1]=true;
			break;
			case 'j':
				tabPoint[2]=true;
				nbTouches++;
			break;	
			case ' ':
				nbTouches++;
			break;	
		}
	}
	
	/** Méthode redéfinie de KeyListener
	 * Gère la saisie en mode Perkins
	 * Réalise l'affichage du caractère braille dans le JTextPane resultat
	 * @param e L'objet KeyEvent intercepté
	 */
	@Override
    public void keyReleased(KeyEvent e) 
	{
		char ch = e.getKeyChar();		
		if(ch == 's' || ch == 'd' || ch=='f' || ch=='j' || ch=='k' || ch == 'l')
		{
			nbTouches--;
			if(nbTouches <= 0)
			{
				res =0;
				for (int i=0;i<tabPoint.length;i++)
				{
					if (tabPoint[i])
					{
						res = res + (int)Math.pow(2, tabPoint.length - i -1);
						tabPoint[i] = false;
					}
				}
				if(res>0)
				{
					for(PerkinsObserver po:jcPerkinsObserver){po.perkinsInput(ptPerkins.get(Integer.toString(res)));}
				}
			}
		}
		if(ch == ' ')
		{
				for(PerkinsObserver po:jcPerkinsObserver){po.perkinsInput(ptPerkins.get("0"));}
				nbTouches = 0; //it is a security here
		}
	}
	

	/** Méthode redéfinie de KeyListener
	 * ne fait rien
	 * @param e Le KeyEvent
	 */
	@Override
    public void keyTyped(KeyEvent e){/*do nothing*/}
	
	/**
	 * @return the hasmap {@link #ptPerkins}
	 */
	public HashMap<String,String> getPtPerkins(){return ptPerkins;}
}
