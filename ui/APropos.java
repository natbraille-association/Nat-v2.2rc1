/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;

import nat.ConfigNat;
import nat.Nat;

import java.awt.Dimension;
/**
 * Fenêtre d'information sur NAT
 * @author bruno
 *
 */
public class APropos extends JFrame implements ActionListener
{
	/** Textual contents */
	static Language texts = new Language("APropos");
	
	/** Pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;
	/** Bouton fermant la fenêtre */
	private JButton fermer = new JButton(texts.getText("close"));
	/** JLabel de titre */
	private JLabel jlHead = new JLabel();
	/** JLabel contenant le texte à afficher (en pseudo-html pour java)*/
	private JLabel texte = new JLabel();
	/** Accessible header : */
	private JTextField aHead = new JTextField();
	/** Accessible text : */
	private JTextField aText = new JTextField();
	/** largeur de la fenêtre */
	private int largeur = 580;
	/** hauteur de la fenêtre */
	private int hauteur = 600;
	/** JLabel pour le logo de nat */
	private JLabel logo = new JLabel();
	/** Constructeur */
	public APropos()
	{
		super("A propos de NAT");
		setLayout(new BorderLayout(5, 5));
		setSize(new Dimension(largeur,hauteur));
		
		int sr=ConfigNat.getCurrentConfig().getScrReader(); //screen reader
		
		jlHead.setIcon(new ImageIcon("ui/logoNat.png"));
		//jlHead.setText("<html><img src=\"file://" + Toolkit.getDefaultToolkit().natbraille.free.fr/images/logoNat.png\"></html>");
		setResizable(false);
		String headText = texts.getText("head").
			replaceFirst("__VERSION__",ConfigNat.getVersionLong()).
			replaceFirst("__SVN_VERSION__",(new Integer(ConfigNat.getSvnVersion())).toString());
		
		switch(sr)
		{
		case Nat.SR_DEFAULT : 
		{
			jlHead.setText(headText);
			break;
		}
		default : 
		{
			jlHead.setText("<html><center><h2>"+texts.getText("warningmsg1")+"<br>"+texts.getText("warningmsg2")+"</h2></center></html>");
		}
		}
		//texte.insert("NAT est un logiciel libre créé dans le cadre du Master Handi de l'université Paris 8.",texte.getLineCount());
		texte.setText(texts.getText("text"));

		
		/** Accessible contents : */
		aHead.setText(removeHTML(headText));
		aHead.setCaretPosition(0);
		aHead.setEditable(false);
		
		String accesstxt=texts.getText("text");
		aText.setText(removeHTML(accesstxt));
		aText.setCaretPosition(0);
		aText.setEditable(false);
		//aText.setRows(15);
		//aText.setColumns(40);
		
		fermer.addActionListener(this);
		fermer.setPreferredSize(new Dimension(100,30));
		fermer.setSize(new Dimension(100,30));
		Context cfermer = new Context("f","Button","close",texts);
		new ContextualHelp(fermer,"credits",cfermer);
		fermer.setMnemonic('f');
		
		JScrollPane scrollRes = new JScrollPane (texte);
		scrollRes.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollRes.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollRes.setPreferredSize(new Dimension(largeur-40, hauteur - 200));
		texte.setBackground(Color.getHSBColor((float)0.0, (float)0.0, (float)10.0));
		texte.setOpaque(true);
		
		JScrollPane accessibleTextScroll = new JScrollPane (aText);
		accessibleTextScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		accessibleTextScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		accessibleTextScroll.setPreferredSize(new Dimension(largeur-40, hauteur - 200));
		
		jlHead.setPreferredSize(new Dimension(largeur-40,190));
		
		JPanel pHead=new JPanel();
		pHead.setLayout(new BorderLayout());
		pHead.add("West",logo);

		
		switch(sr)
		{
		case 1 :
		{
			pHead.add("Center",jlHead);
			add("North", pHead);
			add("Center",scrollRes);
			break;

		}
		default :
		{	
			pHead.add("Center",jlHead);
			pHead.add("South",aHead);
			add("North", pHead);
			add("Center",accessibleTextScroll);
			break;
		}
		}
		
		JPanel pFermer = new JPanel();
		pFermer.add(fermer);
		add("South",pFermer);
		if(ConfigNat.getCurrentConfig().getCentrerFenetre())
		{
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension size = this.getPreferredSize();
			if(ConfigNat.getCurrentConfig().getScrReader()!=Nat.SR_DEFAULT)
			{
				size=new Dimension(largeur,hauteur);
			}
			screenSize.height = screenSize.height/2;
			screenSize.width = screenSize.width/2;
			size.height = size.height/2;
			size.width = size.width/2;
			int y = screenSize.height - size.height;
			int x = screenSize.width - size.width;
			setLocation(x, y);
		}
	}
	
	/**
	 * @param s TODO
	 * @return TODO
	 */
	private String removeHTML(String s)
	{
		if(s.contains("<") && s.contains(">"))
		{
			String[] a=s.split("<",2);
			String[] b=a[1].split(">",2);
			return removeHTML(a[0]+b[1]);
		}
		return s;
	}
	
	/**
	 * Implémentation de java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 * <p>Gére la fermeture de la fenêtre</p>
	 * @see #fermer
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
    public void actionPerformed(ActionEvent evt){if (evt.getSource()==fermer){this.dispose();}}
}
