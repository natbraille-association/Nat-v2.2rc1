/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.accessibility;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.ListCellRenderer;
import javax.swing.JList;
import javax.swing.ImageIcon;

import nat.ConfigNat;
import nat.Nat;

/**
 * Class describing JComboBox's accessible renderer
 * @author Gregoire
 *
 */
public class AccessibleListRenderer extends JLabel implements ListCellRenderer<Object>
{
    /** Pour la sérialisation, non utilisé */
	private static final long serialVersionUID = 1L;
	
	/** default icon*/
	private ImageIcon defaultIcon = new ImageIcon("ui/icon/default.png");
    /** icons lists */
    private ImageIcon[] images;
    /** names */
    private String[] labels;
    /** Accessible name */
    private String iconName;
    /** Accessible description */
    private String iconDesc;
    /** Tool tip text */
    private String iconTTT;
    
    /** Constructor for a list with a unique icon
     * @param img : icon's name (in the folder ui\icon\ ). For instance : "exit.png"
     * @param lbls : labels
     * @param accessName : accessible name
     * @param accessDesc : accessible description
     * @param ttt : tool tip text
     */
    public AccessibleListRenderer(String img, String accessName, String accessDesc, String ttt, String[] lbls) 
    {
    	setOpaque(true);
		setHorizontalAlignment(LEFT);
		setVerticalAlignment(CENTER);
        labels = lbls;
        images=new ImageIcon[lbls.length]; 
        for(int i=0;i<lbls.length;i++)
        {
        	images[i] = new ImageIcon("ui/icon/" + img);
            if(images[i]==null)images[i] = defaultIcon;
        }
        iconName = accessName;
        iconDesc = accessDesc;
        iconTTT = ttt;
    }
    
    /** Constructor for a list with different icons
     * @param imgs : icons' names (in the folder ui\icon\)
     * @param lbls : labels
     * @param accessName : accessible name
     * @param accessDesc : accessible description
     * @param ttt : tool tip text
     */
    public AccessibleListRenderer(String[] imgs, String accessName, String accessDesc, String ttt, String[] lbls) 
    {
    	images = new ImageIcon[lbls.length];
    	int i=0;
    	for(i=0;i<imgs.length;i++)
        {
    		if(i<lbls.length)
    		{
       		images[i] = new ImageIcon("ui/icon/" + imgs[i]);
       		if(images[i].getIconHeight()==-1)images[i] = defaultIcon;
    		}
       	}
    	if(i<lbls.length)
    	{
    		for(int j=i;j<lbls.length;j++)
    		{
    			images[j] = defaultIcon;
    		}
    	}
    	setOpaque(true);
		setHorizontalAlignment(LEFT);
		setVerticalAlignment(CENTER);
        labels = lbls;
        iconName = accessName;
        iconDesc = accessDesc;
        iconTTT = ttt;
    }
    
    /**
     * returns the configured component
     * @param list TODO
     * @param value TODO
     * @param index TODO
     * @param isSelected TODO
     * @param cellHasFocus TODO
     * @return TODO
     */
    @Override
    public Component getListCellRendererComponent(
			  JList<?> list,
			  Object value,
			  int index,
			  boolean isSelected,
			  boolean cellHasFocus)
    {
	try {	
	    int i = ((Integer)value).intValue();
	    ImageIcon icon; String text;

	    if(i<images.length) 
    		{icon = images[i];
		    text= labels[i];}
	    else 
    		{icon = defaultIcon;
		    text = "";}
	    /*
	    System.out.print("--[");
	    System.out.print(index);
	    System.out.print(text);
	    System.out.println("]--");
	    */
	    
	    if (isSelected)
		{
		    setBackground(list.getSelectionBackground());
		    setForeground(list.getSelectionForeground());
		}
	    else
		{
		    setBackground(list.getBackground());
		    setForeground(list.getForeground());
		}
	    setIcon(icon);
	    int sr=ConfigNat.getCurrentConfig().getScrReader(); // current screen reader
	    if(sr==Nat.SR_NVDA)
		{
		    getAccessibleContext().setAccessibleDescription(
								    (ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_SILENCIEUX)?" ":iconDesc);
		    getAccessibleContext().setAccessibleName(iconName+". "+text);
		}
	    setToolTipText(iconTTT);
	    if (icon != null) {setText(text);}
	} catch (Exception e){
	    System.out.println("!");
	    System.out.println(e);
	}
    	return this;
    }
    
    /** 
     * Sets the default icon
     * @param defaultName : the name of the default icon in ui/icon/ folder
     */
    public void setDefaultIcon(String defaultName)
    {
    	for(int i=0 ; i<images.length ; i++)
    	{
    		if(images[i].equals(defaultIcon))images[i]=new ImageIcon("ui/icon/"+defaultName);
    	}
    	defaultIcon = new ImageIcon("ui/icon/" + defaultName);
    }
}