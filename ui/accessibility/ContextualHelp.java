/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.accessibility;

import java.awt.AWTKeyStroke;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;

import outils.FileToolKit;


import nat.ConfigNat;

/** 
 * Contextual Help
 * @author gregoire
 */
public class ContextualHelp implements FocusListener, KeyListener
{
	/** Textual contents */
	private Language texts = new Language("ContextualHelp");
	
	/** 1 if button has focus */
	private int hasFocus=0;
	/** false if with context */
	Boolean hasContext=false;
	/** true : should add texts */
	private Boolean addTexts=false;
	/** otherLabels */
	private String[] otherLabels;
	/** otherKeys */
	private String[] otherKeys;
	/** context */
	Context context;
	/** component name */
	private String helpName;
	/** 1 if helpkey is pressed */
	private int isPressed=0;
	/** component */
	JComponent comp;
	/** id of the help page*/
	private String id;
	/** help key*/
	private AWTKeyStroke helpKey=ConfigNat.getCurrentConfig().getHelpKey();
	/** @param ks : new helpkey */
	public void setHelpKey(AWTKeyStroke ks)
	{helpKey=ks;}
	/** help key*/
	private AWTKeyStroke tradKey=ConfigNat.getCurrentConfig().getTradKey();
	
	/** Contructor */
	public ContextualHelp() 
	{
		// rien
	}
	
	/**
	 * sets parameters
	 * @param jc component
	 * @param helpid id in the help sheet (=anchor)
	 * @param ctext context
	 */
	public void setParameters(JComponent jc, String helpid, Context ctext)
	{
		id=helpid;
		context=ctext;
		comp=jc;
		hasContext=true;
		if(jc!=null)
		{
			setShortCut();
			if(jc.getClass()==JSpinner.class)
			{
				JSpinner.DefaultEditor editor = ((JSpinner.DefaultEditor)((JSpinner) jc).getEditor());
				comp = editor.getTextField();
			}
			comp.addFocusListener(this);
			comp.addKeyListener(this);
			comp.getAccessibleContext().setAccessibleName(ctext.getName());
			comp.getAccessibleContext().setAccessibleDescription(ctext.getDesc());
			comp.setToolTipText(ctext.getTTT());
		}
		helpName=ctext.getTTT();
	}
	
	/**
	 * @param jc component's name
	 * @param helpid id of the help page ("target" in the mapfile : nat.jhm ) 
	 */
	public void setParameters(JComponent jc, String helpid)
	{
		id=helpid;
		if(jc!=null)
		{
			comp=jc;
			if(jc.getClass()==JSpinner.class)
			{
				JSpinner.DefaultEditor editor = ((JSpinner.DefaultEditor)((JSpinner) jc).getEditor());
				comp = editor.getTextField();
			}
			comp.addFocusListener(this);
			comp.addKeyListener(this);
			helpName=jc.getToolTipText();
		}
	}
	
	/**Constructor with context (implements tooltip,name,description)
	 * @param jc component's name
	 * @param helpid id of the help page ("target" in the mapfile : nat.jhm ) 
	 * @param ctext contexts
	 */
	public ContextualHelp (JComponent jc, String helpid, Context ctext)
	{
		setParameters(jc,helpid,ctext);
	}
	/**
	 * Constructor with context (implements tooltip,name,description)
	 * @param jc component's name
	 * @param ctext contexts
	 */
	public ContextualHelp (JComponent jc, Context ctext)
	{
		setParameters(jc,"top",ctext);
	}
	/**Constructor
	 * @param jc component's name
	 * @param helpid id of the help page ("target" in the mapfile : nat.jhm ) 
	 */
	public ContextualHelp (JComponent jc, String helpid)
	{
		setParameters(jc,helpid);
	}
	/**Constructor
	 * @param jc component's name
	 */
	public ContextualHelp (JComponent jc)
	{
		setParameters(jc,"top");
	}
	/**
	 * Contextual Help for the settings tabbed pane
	 * @param jtp tabbed pane */
	public ContextualHelp (JTabbedPane jtp)
	{
		jtp.addFocusListener(this);
		jtp.addKeyListener(this);
		id="guiOptionsPrinc";
		hasFocus=1;
		helpName="";
		addTexts=true;
		otherKeys=new String[2];otherLabels=new String[2];
		otherKeys[0]="general";otherLabels[0]="Nom de l'onglet";
		otherKeys[1]="generalalt";otherLabels[1]="Infobulle";
		context = new Context("","","title",new Language("Configuration"));
	}
	/** change help tab
	 * @param newID new id*/
	public void changeID(String newID)
	{
		id=newID;
	}

	/** set help name 
	 * @param newName : name in the help page*/
	public void setHelpName(String newName)
	{
		helpName=newName;
	}

	/** @param e : focus event*/
	@Override
	public void focusGained(FocusEvent e) 
	{
		hasFocus=1;
	}

	/** @param e : focus event*/
	@Override
	public void focusLost(FocusEvent e) 
	{
		hasFocus=0;
		isPressed=0;
	}

	/** @param k : key event*/
	@Override
	public void keyPressed(KeyEvent k) 
	{
		if(AWTKeyStroke.getAWTKeyStroke(k.getKeyCode(),k.getModifiers(),true).equals(helpKey))
		{
			isPressed=1;
			if(hasFocus*isPressed!=0) 
			{
				new HelpAction(id,helpName,true);
				isPressed=0;
				hasFocus=0;
			}
		}
		if((hasContext || addTexts) && AWTKeyStroke.getAWTKeyStroke(k.getKeyCode(),k.getModifiers(),true).equals(tradKey))
		{
			isPressed=1;
			if(hasFocus*isPressed!=0) 
			{
				isPressed=0;
				hasFocus=0;
				int num=0;
				String[] labels = new String[20];
				String[] keys = new String[20];
				String l="",v="",sr="",file="",key="";
			
				Language text = context.getLanguage();					
				key = context.getKey();
				file=context.getBundle();
				
				l=text.getLoc()[0];
				v=text.getLoc()[1];
				sr=text.getLoc()[2];
				
				File f;
				ConfigNat.getCurrentConfig();
				String path = ConfigNat.getConfTextFolder();
				path=path+l+"/"+file+"_"+l+"_"+v+"_"+sr+".properties";
				f = new File(path);
				if(!f.exists())
				{
					path = ConfigNat.getInstallFolder()+"text/";
					path=path+l+"/"+file+"_"+l+"_"+v+"_"+sr+".properties";
					f = new File(path);
				}
				if(!f.exists())
				{
					v="";sr="";
					path = ConfigNat.getConfTextFolder();
					path=path+l+"/"+file+"_"+l+".properties";
					f = new File(path);
					if(!f.exists())
					{
						path=ConfigNat.getInstallFolder()+"text/";
						path=path+l+"/"+file+"_"+l+".properties";
						f = new File(path);
					}
					if(!f.exists())l="";
				}
				if(hasContext)
				{
					num=5;
					
					labels[0]=texts.getText("name");
					labels[1]=texts.getText("label");
					labels[2]=texts.getText("accessname");
					labels[3]=texts.getText("accessdesc");
					labels[4]=texts.getText("tooltip");
					labels[5]="<html><p class=\"shortcut\">"+texts.getText("shortcut")+"</p></html>";
					
					keys[0]=key;
					keys[1]=key+"label";
					keys[2]=key+"name";
					keys[3]=key+"desc";
					keys[4]=key+"ttt";
					
					path = ConfigNat.getConfTextFolder()+file+".properties";
					f = new File(path);
					
					if (!f.exists())
					{
						path=ConfigNat.getInstallFolder()+"text/"+file+".properties";
						f = new File(path);
					}
					// Loads shortcuts
					if(f.exists())
					{
						int i=0;
						String[] lines = FileToolKit.loadFileToStr(path,"ISO-8859-1").split("\n");
						for (String s : lines)
						{
							if(s.contains(" = "))
							if(s.split(" = ",2)[0].startsWith(key+"KEY"))
							{
								i++;num++;String s2=s.split(" = ",2)[0].split(key+"KEY",2)[1];
								keys[4+i]=s.split(" = ",2)[0];
								labels[4+i]="<html><p class=\"shortcut\">"+texts.getText("shortcut")+" "+s2+"</p></html>";
							}
						}
					}
					
				}
				
				if(addTexts)
				{
					int m=num;
					for(int i=0; i<otherLabels.length; i++)
					{
						labels[m+i]=otherLabels[i];
						keys[m+i]=otherKeys[i];
						num++;
					}
				}
				String[] labels2 = new String[num];
				String[] keys2 = new String[num];
				for(int i=0; i<num; i++)
				{
					labels2[i]=labels[i];
					keys2[i]=keys[i];
				}
				new DialogueAccess(labels2,keys2,l,v,sr,file,false);
			}
		}
	}
	
	/** */

	/** @param k : key event*/
	@Override
	public void keyReleased(KeyEvent k) {
		// TODO Auto-generated method stub
		
	}

	/** @param k : key event*/
	@Override
	public void keyTyped(KeyEvent k) {
		// TODO Auto-generated method stub
		
	}
	/** add labels 
	 * @param labels labels
	 * @param keys keys */
	public void addLabels(String[] labels, String[] keys)
	{
		if(labels.length==keys.length)
		{
			otherLabels=labels;
			otherKeys=keys;
			addTexts=true;
		}
	}
	/**
	 * Sert à lier le fichier .properties au bouton.
	 * Pour ne pas ajouter les clés de base, mettre b=false
	 * @param c context
	 * @param b true to add basic keys (name, description, tooltip, label) to the dialog
	 * 
	 * */
	public void addContext(Context c, Boolean b)
	{
		context=c;
		hasContext=b;
	}
	/** 
	 * Adds label|key
	 * @param label label
	 * @param key key */
	public void addLabel(String label, String key)
	{
		if(addTexts)
		{
			String[] labels2 = new String[otherLabels.length+1];
			String[] keys2 = new String[otherLabels.length+1];
			Boolean keyAlreadyIncluded=false;
			for(int i=0; i<otherKeys.length; i++)
			{
				if(otherKeys[i]==key)keyAlreadyIncluded=true;
				labels2[i+1]=otherLabels[i];
				keys2[i+1]=otherKeys[i];
			}
			labels2[0]=label;
			keys2[0]=key;
			if(!keyAlreadyIncluded)
			{
				otherLabels = new String[labels2.length];
				otherKeys = new String[labels2.length];
				otherLabels = labels2.clone();
				otherKeys = keys2.clone();
			}
		}
		else
		{
			otherLabels= new String[1]; otherLabels[0]=label;
			otherKeys= new String[1]; otherKeys[0]=key;
			addTexts=true;
		}
	}
	
	/** set shortcut*/
	private void setShortCut() 
	{
		for(int i=0;i<5;i++)
		if(context.shortcut[i][0]!=0)
		{
			Action selectAction = new AbstractAction() 
			{
				private static final long serialVersionUID = 1L;
				@Override
                public void actionPerformed(ActionEvent e) 
				{
					comp.requestFocusInWindow();
					if(comp.getClass()==JButton.class)((JButton) comp).doClick();
					if(comp.getClass()==JCheckBox.class)((JCheckBox) comp).doClick();
					if(comp.getClass()==JRadioButton.class)((JRadioButton) comp).doClick();
				}
			};
			
			Boolean isMnemonic=false;
			if(context.shortcut[i][1]==InputEvent.ALT_DOWN_MASK)
			{
				isMnemonic=true;
				if(comp.getClass()==JCheckBox.class)
					{
					((JCheckBox)comp).setMnemonic(context.shortcut[i][0]);
					((JCheckBox)comp).setDisplayedMnemonicIndex(((JCheckBox)comp).getText().toUpperCase().indexOf(
							KeyEvent.getKeyText(context.shortcut[i][0])));
					}
				else if(comp.getClass()==JButton.class)((JButton)comp).setMnemonic(context.shortcut[i][0]);
				else if(comp.getClass()==JRadioButton.class)
				{
					((JRadioButton)comp).setMnemonic(context.shortcut[i][0]);
					((JRadioButton)comp).setDisplayedMnemonicIndex(((JRadioButton)comp).getText().toUpperCase().indexOf(
							KeyEvent.getKeyText(context.shortcut[i][0])));
					
				}
				else isMnemonic=false;
			}
			if(!isMnemonic)
			{
				InputMap im = comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
				ActionMap am = comp.getActionMap();
				am.put("selectAction", selectAction);
				KeyStroke k = KeyStroke.getKeyStroke(context.shortcut[i][0],context.shortcut[i][1]);
				im.put(k,"selectAction");
			}
		}
	}
}