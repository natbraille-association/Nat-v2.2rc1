/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.accessibility;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.ListCellRenderer;
import javax.swing.JList;
import javax.swing.ImageIcon;

import nat.ConfigNat;
import nat.Nat;


/**
 * Class describing JComboBox's accessible renderer
 * @author Gregoire
 *
 */
public class AccessibleListStringRenderer extends JLabel implements ListCellRenderer<Object>
{
    /** Pour la sérialisation, non utilisé */
	private static final long serialVersionUID = 1L;
	
	/** icon */
    private ImageIcon image;
    /** Accessible name */
    private String iconName;
    /** Accessible description */
    private String iconDesc;
    /** Tool tip text */
    private String iconTTT;
    
    /** Constructor for a renderer of a string
     * @param img : icon's name (in the folder ui\icon\ ). For instance : "exit.png"
     * @param accessName : accessible name
     * @param accessDesc : accessible description
     * @param ttt : tool tip text
     */
    public AccessibleListStringRenderer(String img, String accessName, String accessDesc, String ttt) 
    {
    	setOpaque(true);
		setHorizontalAlignment(LEFT);
		setVerticalAlignment(CENTER);
        image = new ImageIcon("ui/icon/" + img);
        iconName = accessName;
        iconDesc = accessDesc;
        iconTTT = ttt;
        if(image==null)image = new ImageIcon("ui/icon/default.png");
    }
    
    /**
     * returns the configured component
     * @param list TODO
     * @param value TODO
     * @param index TODO
     * @param isSelected TODO
     * @param cellHasFocus TODO
     * @return TODO
     */
    @Override
    public Component getListCellRendererComponent(
			  JList<?> list,
			  Object value,
			  int index,
			  boolean isSelected,
			  boolean cellHasFocus)
    {
    	String s = (String)value;
    	ImageIcon icon = image;
    	if (isSelected)
    	{
    		setBackground(list.getSelectionBackground());
    		setForeground(list.getSelectionForeground());
    	}
    	else
    	{
    		setBackground(list.getBackground());
    		setForeground(list.getForeground());
    	}
    	setIcon(icon);
		int sr=ConfigNat.getCurrentConfig().getScrReader(); // current screen reader
		if(sr==Nat.SR_NVDA)
		{
			getAccessibleContext().setAccessibleDescription(
					(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_SILENCIEUX)?" ":iconDesc);
			getAccessibleContext().setAccessibleName(iconName+s);
		}
    	setToolTipText(iconTTT);
    	if (icon != null) {setText(s);}
    	return this;
    }
}