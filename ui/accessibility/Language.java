/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.accessibility;

import java.io.File;
//import java.io.IOException;
import java.util.*;

import outils.FileToolKit;
import nat.ConfigNat;
import nat.Nat;

/** Internationalization
 * @author Gregoire 
 */
public class Language 
{
	/** language of the client */
	private Locale current;
	/** locale */
	private String[] loc;
	/** custom keys*/
	private ArrayList<String> customKeys = new ArrayList<String>();
	/** custom texts*/
	private ArrayList<String> customResources = new ArrayList<String>();
	/** system keys*/
	private ArrayList<String> systemKeys = new ArrayList<String>();
	/** system texts*/
	private ArrayList<String> systemResources = new ArrayList<String>();
	/** backup keys */
	private ArrayList<String> backupKeys = new ArrayList<String>();
	/** backup texts */
	private ArrayList<String> backupResources = new ArrayList<String>();
	/** screen readers list */
	private static String[][] scrReaders;
	/** languages list */
	private static String[][] langs;
	/** Name of the java file*/
	private String filename;
	/** custom file path*/
	private String customPath;
	/** system file path */
	private String systemPath;
	/** */
	private Boolean customResourcesAvailable;
	/** loads custom bundle */
	private void loadCustomBundle()
	{
		customPath = ConfigNat.getConfTextFolder()+loc[0]+"/"+filename+"_"+loc[0]+"_"+loc[1]+"_"+loc[2]+".properties";
		File cf = new File(customPath);
		customResourcesAvailable=true;
		if(!cf.exists())
		{
			//System.out.println("skip missing custom bundle : "+cf);
			customPath = ConfigNat.getConfTextFolder()+loc[0]+"/"+filename+"_"+loc[0]+"_"+loc[1]+".properties";
			cf = new File(customPath);
			if(!cf.exists())
			{
				//System.out.println("skip missing custom bundle : "+cf);
				customPath = ConfigNat.getConfTextFolder()+loc[0]+"/"+filename+"_"+loc[0]+".properties";
				cf = new File(customPath);
			}
		}
		if(!cf.exists())customResourcesAvailable=false;
		if(customResourcesAvailable)
		{
			System.out.println("reading custom bundle : "+cf);
			String[] s = FileToolKit.loadFileToStr(customPath, "UTF-8").split("\n");
			for(String g : s)
			{
				if (g.contains(" = ") && !g.startsWith("#") && !g.startsWith(" =") && !g.endsWith("= ")) 
				{
					customKeys.add(g.split(" = ",2)[0]);
					customResources.add(g.split(" = ",2)[1].replace("\\n", "\n"));
				}
			}
		}
	}
	/** loads system bundle */
	private void loadSystemBundle()
	{
		systemPath = ConfigNat.getInstallFolder()+"text/"+loc[0]+"/"+filename+"_"+loc[0]+"_"+loc[1]+"_"+loc[2]+".properties";
		File cf = new File(systemPath);
		if(!cf.exists())
		{
			//System.out.println("skip missing system bundle : "+cf);
			systemPath = ConfigNat.getInstallFolder()+"text/"+loc[0]+"/"+filename+"_"+loc[0]+"_"+loc[1]+".properties";
			cf = new File(systemPath);
			if(!cf.exists())
			{
				//System.out.println("skip missing system bundle : "+cf);
				systemPath = ConfigNat.getInstallFolder()+"text/"+loc[0]+"/"+filename+"_"+loc[0]+".properties";
				cf = new File(systemPath);
			}
		}
		if(cf.exists())
		{
			System.out.println("reading system bundle : "+cf);
			String[] s = FileToolKit.loadFileToStr(systemPath, "UTF-8").split("\n");
			for(String g : s)
			{
				if (g.contains(" = ") && !g.startsWith("#") && !g.startsWith(" =") && !g.endsWith("= ")) 
				{
					systemKeys.add(g.split(" = ",2)[0]);
					systemResources.add(g.split(" = ",2)[1].replace("\\n", "\n"));
				}
			}
		}
	}
	
	/** Constructor. Just enter the name of the file. 
	 * @param file : name of the java file + ".properties" */
	public Language(String file)
	{
		ConfigNat.getCurrentConfig();
		
		ResourceBundle.clearCache();
		filename=file;
		
		String[] scrReadersList=FileToolKit.loadFileToStr("outils/ScrReaders.txt","UTF8").split("\n");
		scrReaders = new String[scrReadersList.length][2];	
		int i=0;
		for(String it : scrReadersList)
		{
			scrReaders[i]=it.split("=");
			i++;
		}
		int pos = 0;
		int posLang = 0;
		int verbosity = Nat.LOG_NORMAL;
		if(ConfigNat.getCurrentConfig()!=null)
		{
			pos = ConfigNat.getCurrentConfig().getScrReader()-1;
			posLang = ConfigNat.getCurrentConfig().getLanguage();
			verbosity= ConfigNat.getCurrentConfig().getNiveauLog();
		}
		String sr=scrReaders[pos][0];
		
		String[] langList=FileToolKit.loadFileToStr("outils/Languages.txt","UTF8").split("\n");
		langs = new String[langList.length][2];
		i=0;
		for(String it : langList)
		{
			langs[i]=it.split("=");
			i++;
		}
		String l=langs[posLang][0];
		String v;
		switch (verbosity)
		{
		// Average verbosity
		case Nat.LOG_DEBUG :{v="debug";break;}
		case Nat.LOG_VERBEUX :{v="high";break;}
		case Nat.LOG_SILENCIEUX :{v="low";break;}
		default :{v="average";}
		}
		current = new Locale(l,v,sr);
		
		loc = new String[3];
		loc[0]=l;loc[1]=v;loc[2]=sr;
		
		loadCustomBundle();
		loadSystemBundle();
		ArrayList<String> languages = new ArrayList<String>();
		languages.add(loc[0]);
		languages.add("");
		
		ArrayList<String> screenreaders = new ArrayList<String>();
		screenreaders.add(loc[2]);
		screenreaders.add("");
		
		ArrayList<String> verbosities = new ArrayList<String>();
		verbosities.add(loc[1]);
		verbosities.add("");
		
		/** 
		 * Pour chercher dans tous les fichiers potentiels.
		 * Ralentit énormément le chargement de chaque page
		for(String[] a : langs)if(!a[0].equals(loc[0]))languages.add(a[0]);
		for(String[] a : scrReaders)if(!a[0].equals(loc[2]))screenreaders.add(a[0]);
		if(!loc[1].equals("low"))verbosities.add("low");
		if(!loc[1].equals("high"))verbosities.add("high");
		if(!loc[1].equals("debug"))verbosities.add("debug");
		if(!loc[1].equals("average"))verbosities.add("average");
		*/
		
		for(String lang : languages)
		{
			for(String verb : verbosities)
			{
				for (String s : screenreaders)
				{
					String newCustPath = ConfigNat.getConfTextFolder();
					String newSysPath = ConfigNat.getInstallFolder()+"text/";
					String extension = "";
					if(!lang.equals("")) extension =lang+"/";
					extension = extension + filename;
					if(!lang.equals("")) 
						{
							extension = extension+"_"+lang;
							if(!verb.equals("")) extension = extension+"_"+verb;
							if(!s.equals("")) extension = extension+"_"+s;
						}
					extension = extension+".properties";
					newCustPath = newCustPath + extension;
					newSysPath = newSysPath + extension;
					File f = new File(newCustPath);
					if(f.exists())
					{
						String[] lines = FileToolKit.loadFileToStr(newCustPath, "UTF-8").split("\n");
						for (String a : lines)
						{
							if (a.contains(" = ") && !a.startsWith("#"))
							{
								backupKeys.add(a.split(" = ",2)[0]);
								backupResources.add(a.split(" = ",2)[1].replace("\\n", "\n"));		
							}
						}
					}
					f = new File(newSysPath);
					if(f.exists())
					{
						String[] lines = FileToolKit.loadFileToStr(newSysPath, "UTF-8").split("\n");
						for (String a : lines)
						{
							if (a.contains(" = ") && !a.startsWith("#"))
							{
								backupKeys.add(a.split(" = ",2)[0]);
								backupResources.add(a.split(" = ",2)[1].replace("\\n", "\n"));		
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * @return current locale
	 */
	public Locale getLocale()
	{
		return current;
	}
	
	/**
	 * @return screen readers
	 */
	public static String[][] getSR()
	{
		return scrReaders;
	}
	
	/**
	 * @return langs
	 */
	public static String[][] getLangs()
	{
		return langs;
	}
	
	/** @return true if key exists in custom or system bundle
	 * @param k : key to test
	 */
	public Boolean hasText(String k)
	{
		if(customResourcesAvailable)
		{
			//int i=0;
			for (String s : customKeys)
			{
				if(s.equals(k))return true;
				//i++;
			}
		}
		//int j=0;
		for (String s : systemKeys)
		{
			if(s.equals(k))return true;
			//j++;
		}
		if(backupKeys.size()!=0)
		{
			//int i=0;
			for (String s : backupKeys)
			{
				if(s.equals(k))return true;
				//i++;
			}
		}
		return false;
	}
	
	/** @return text indexed by parameter k
	 * @param k : key to access text
	 */
	public String getText(String k)
	{
		if(customResourcesAvailable)
		{
			int i= customKeys.lastIndexOf(k);
			if(i!=-1)return customResources.get(i);
		}
		
		int w = systemKeys.lastIndexOf(k);
		if(w!=-1)return systemResources.get(w);
		
		if(backupKeys.size()!=0)
		{
			for(int i=0; i<backupKeys.size(); i++)
			{
				if (backupKeys.get(i).equals(k))return backupResources.get(i);
			}
		}
		
		if(!k.contains("KEY") && hasText(k+"1"))
		{
			int i=1;
			String s="";
			while(hasText(k+i))
			{
				s=s+getText(k+i);
				if(!s.endsWith(" "))s=s+" ";
				i++;
			}
			return s;
		}
		return "--- Key : \""+k+"\" not found, check text\\"+filename+".properties ---";
	}
	
	/** finds the index of the selected language in the file Languages.txt
	 * @param langName : selected name
	 * @return index*/
	public int findLangFileIndex(String langName)
	{
		for(int i=0;i<langs.length;i++)
		{
			if(langName.equals(getText(langs[i][0])))return i;
		}
		return 0;
	}
	/** @return locale */
	public String[] getLoc()
	{
		return loc;
	}
	/** @return unmodifiedFilename*/
	public String getFilename()
	{
		return filename;
	}
}