/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package ui.accessibility;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import outils.FileToolKit;

import nat.ConfigNat;
import nat.Nat;

/**
 * @author gregoire
 */
public class DialogueAccess extends JFrame implements WindowListener, ActionListener, CaretListener, KeyListener
{
	/** Textual contents */
	static Language texts = new Language("DialogueAccess");
	
	/** pour la sérialisation (non utilisé)*/
    private static final long serialVersionUID = 1L;
    
    /** full translation*/
    private Boolean fullTranslation;
    /** save translation button */
    private JButton jbSave = new JButton(texts.getText("continue"),new ImageIcon("ui/icon/go-next.png"));
    /** save translation button */
    private JButton jbAddShortcut = new JButton(texts.getText("add"),new ImageIcon("ui/icon/list-add.png"));
    /** save translation button */
    private JButton jbDelShortcut = new JButton(texts.getText("delete"),new ImageIcon("ui/icon/list-remove.png"));
    /** cancel button */
    private JButton jbCancel = new JButton(texts.getText("cancel"),new ImageIcon("ui/icon/exit.png"));
    /** load file button */
    private JButton jbLoad = new JButton(texts.getText("load"),new ImageIcon("ui/icon/document-open.png"));
    /** new translation button */
    private JButton jbTranslation = new JButton(texts.getText("translation"),new ImageIcon("ui/icon/document-new.png"));
    /** jlabels */
    private JLabel[] labels;
    /** textfields */
    private JTextField[] textFields;
    /** length */
    private int num=-1;
    /** initial texts */
    private String[] unmodifiedTexts;
    /** keys */
    private String[] keys;
    /** language text field */
    private JTextField jtfLanguage = new JTextField(8);
    /** language label */
    private JLabel jlLanguage = new JLabel(texts.getText("languagelabel"),new ImageIcon("ui/icon/emblem-default.png","ok"),JLabel.RIGHT);
    /** verbosity text field */
    private JTextField jtfVerbosity = new JTextField(8);
    /** verbosity label */
    private JLabel jlVerbosity = new JLabel(texts.getText("verbositylabel"),new ImageIcon("ui/icon/emblem-default.png","ok"),JLabel.RIGHT);
    /** screen reader text field */
    private JTextField jtfScreenReader = new JTextField(8);
    /** screen reader label */
    private JLabel jlScreenReader = new JLabel(texts.getText("screenreaderlabel"),new ImageIcon("ui/icon/emblem-default.png","ok"),JLabel.RIGHT);
    /** file name */
    private String fileName;
    /** locale (language, verbosity, screen reader) */
    private String[] locale = new String[3];
    /** shortcuts */
    private JButton[] jbShortcuts;
    /** true when button's pressed */
    private Boolean[] shortcutButtonPressed;
    /** key codes */
    private int[] keyCodes;
    /** key modifiers */
    private int[] keyModifiers;
    /**
     * Constructor
     * @param labelList labels
     * @param keyList keys
     * @param language lang
     * @param verbosity verbosity
     * @param screenreader screen reader
     * @param file file name
     * @param ft : true if full translation.
     */
    public DialogueAccess(String[] labelList, String[] keyList,
    			String language, String verbosity, String screenreader, String file, Boolean ft)
    {
    	super(texts.getText("title"));
    	fullTranslation = ft;
    	keys=keyList;
    	num=labelList.length;
    	jtfLanguage.setText(language);locale[0]=language;
    	jtfVerbosity.setText(verbosity);locale[1]=verbosity;
    	jtfScreenReader.setText(screenreader);locale[2]=screenreader;
    	fileName=file;
    	
    	labels = new JLabel[num];
    	for(int i=0; i<num; i++)
    	{
    		labels[i] = new JLabel(labelList[i]);
    	}
    	textFields = new JTextField[num];
    	unmodifiedTexts = new String[num];
    	for(int i=0; i<num; i++)
    	{
    		textFields[i] = new JTextField("");
    	}
    	int keyNumbers=0;
		for(int i=0;i<num;i++)
			if(labels[i].getText().contains("class=\"shortcut\""))keyNumbers++;
		jbShortcuts = new JButton[keyNumbers];
		keyCodes = new int[keyNumbers];
		keyModifiers = new int[keyNumbers];
		shortcutButtonPressed = new Boolean[keyNumbers];
    	for(int i=0;i<keyNumbers;i++)
    	{
			shortcutButtonPressed[i]=false;
			jbShortcuts[i]= new JButton();
			jbShortcuts[i].setToolTipText(texts.getText("shortcutbuttonttt"));
			jbShortcuts[i].addActionListener(this);
			jbShortcuts[i].addKeyListener(this);
    	}
		
    	ConfigNat.getCurrentConfig();
		String path="";
		
		if(jtfLanguage.getText().length()!=0)
		{
			path=jtfLanguage.getText()+"/";
			new File(ConfigNat.getConfTextFolder()+path).mkdir();
		}
		path=path+fileName;
		if(jtfLanguage.getText().length()!=0)
		{
			path=path+"_"+jtfLanguage.getText();
			if(jtfVerbosity.getText().length()!=0)
			{
				path=path+"_"+jtfVerbosity.getText();
				if(jtfScreenReader.getText().length()!=0)path=path+"_"+jtfScreenReader.getText();
			}
		}
		path=path+".properties";
		File f = new File(ConfigNat.getConfTextFolder()+path);
		if (!f.exists())FileToolKit.copyFile(ConfigNat.getInstallFolder()+"text/"+path, 
				ConfigNat.getConfTextFolder()+path, "ISO-8859-1", "ISO-8859-1");
		path=fileName+".properties";
		f = new File(ConfigNat.getConfTextFolder()+path);
		if (!f.exists())FileToolKit.copyFile(ConfigNat.getInstallFolder()+"text/"+path, 
				ConfigNat.getConfTextFolder()+path, "ISO-8859-1", "ISO-8859-1");
		
    	load();
    	createDialog();
    }
    
    /**
     * creates dialog
     */
    private void createDialog()
    {	
    	// closes unused default textfields
    	if(textFields.length>=5 && !fullTranslation)
    		if(keys[1].endsWith("label") && keys[2].endsWith("name") 
    			&& keys[3].endsWith("desc") && keys[4].endsWith("ttt"))
    			for(int l=0; l<5; l++)
    			{
    				if(textFields[l].getText().length()==0)
    	    			textFields[l].setEnabled(false);
    			}
    	
		new ContextualHelp(jbSave,
				new Context("","Button","continue",texts));
		jbSave.addActionListener(this);
		
		new ContextualHelp(jbCancel,
				new Context("","Button","cancel",texts));
		jbCancel.addActionListener(this);

		new ContextualHelp(jbLoad,
				new Context("","Button","load",texts));
		jbLoad.addActionListener(this);
		
		new ContextualHelp(jbTranslation,
				new Context("","Button","translation",texts));
		jbTranslation.addActionListener(this);
		
		new ContextualHelp(jbAddShortcut,
				new Context("","Button","add",texts));
		jbAddShortcut.addActionListener(this);
		
		new ContextualHelp(jbDelShortcut,
				new Context("","Button","delete",texts));
		jbDelShortcut.addActionListener(this);
		
		jtfLanguage.addCaretListener(this);
		String langChoices = texts.getText("choices");
		String[] a = FileToolKit.loadFileToStr("outils/Languages.txt","ISO-8859-1").split("\n");
		for (String l : a)langChoices+=", "+l.split("=",2)[0];
		jtfLanguage.setToolTipText(langChoices.replaceFirst(", "," : "));
		
		jtfVerbosity.addCaretListener(this);
		jtfVerbosity.setToolTipText(texts.getText("choices")+" : high, low, average, debug");
		
		jtfScreenReader.addCaretListener(this);
		String scrReadersChoices = texts.getText("choices");
		String[] b = FileToolKit.loadFileToStr("outils/ScrReaders.txt","ISO-8859-1").split("\n");
		for (String l : b)scrReadersChoices+=", "+l.split("=",2)[0];
		jtfScreenReader.setToolTipText(scrReadersChoices.replaceFirst(", "," : "));
		
		if(jtfLanguage.getText().length()==0)
		{
			jtfVerbosity.setEnabled(false);
			jtfScreenReader.setEnabled(false);
		}
		
		if(jtfVerbosity.getText().length()==0)
		{
			jtfScreenReader.setEnabled(false);
		}
		
		/*
		 * Layout
		 */
		
		GridBagConstraints gbc = new GridBagConstraints();
		GridBagLayout gbl = new GridBagLayout();
		JPanel pan = new JPanel(gbl);
		Insets insets = new Insets(1,1,1,1);
		
		JPanel jpLocale = new JPanel(gbl);
		jlLanguage.setLabelFor(jtfLanguage);
		jlLanguage.setHorizontalTextPosition(JLabel.LEFT);
		jlLanguage.setDisplayedMnemonic('l');
		gbc = new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
		jpLocale.add(jlLanguage,gbc);
    	
    	gbc = new GridBagConstraints(1,0,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
    	jpLocale.add(jtfLanguage,gbc);
    	
		jlVerbosity.setLabelFor(jtfVerbosity);

		jlVerbosity.setHorizontalTextPosition(JLabel.LEFT);
		jlVerbosity.setDisplayedMnemonic('v');
		gbc = new GridBagConstraints(0,1,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
		jpLocale.add(jlVerbosity,gbc);
    	
    	gbc = new GridBagConstraints(1,1,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
    	jpLocale.add(jtfVerbosity,gbc);
    	
		jlScreenReader.setLabelFor(jtfScreenReader);
		jlScreenReader.setHorizontalTextPosition(JLabel.LEFT);
		jlScreenReader.setDisplayedMnemonic('c');
		gbc = new GridBagConstraints(0,2,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
		jpLocale.add(jlScreenReader,gbc);
    	
    	gbc = new GridBagConstraints(1,2,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
    	jpLocale.add(jtfScreenReader,gbc);
    	
    	gbc = new GridBagConstraints(0,0,2,3,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
    	pan.add(jpLocale,gbc);
    	
    	int keyNumber=0;
    	for(int i=0; i<num; i++)
    	{
    		if(textFields[i].isEnabled() && ConfigNat.getCurrentConfig().getTranslationMode())
    		{
    			labels[i].setLabelFor(textFields[i]);
    			gbc = new GridBagConstraints(0,3+i,2,1,1.0,1.0,GridBagConstraints.WEST,
    	    			GridBagConstraints.BOTH, insets,0,0);
    	    	pan.add(labels[i],gbc);
    	    	
    			gbc = new GridBagConstraints(2,3+i,2,1,1.0,1.0,GridBagConstraints.WEST,
    	    			GridBagConstraints.BOTH, insets,0,0);
    	    	pan.add(textFields[i],gbc);
    	    	
    		}
    		else if(labels[i].getText().contains("class=\"comment\""))
    		{
    			labels[i].setHorizontalAlignment(JLabel.CENTER);
    			gbc = new GridBagConstraints(0,3+i,2,1,1.0,1.0,GridBagConstraints.WEST,
    	    			GridBagConstraints.BOTH, insets,0,0);
    	    	pan.add(labels[i],gbc);
    		}
    		else if(labels[i].getText().contains("class=\"shortcut\"")){
    				if( textFields[i].getText().length()!=0)
    		{
    			gbc = new GridBagConstraints(0,3+i,2,1,1.0,1.0,GridBagConstraints.WEST,
    	    			GridBagConstraints.BOTH, insets,0,0);
    	    	pan.add(labels[i],gbc);
    	    	
    	    	labels[i].setLabelFor(jbShortcuts[keyNumber]);
    	    	
    			gbc = new GridBagConstraints(2,3+i,2,1,1.0,1.0,GridBagConstraints.WEST,
    	    			GridBagConstraints.BOTH, insets,0,0);
    	    	pan.add(jbShortcuts[keyNumber],gbc);
    	    	
    		}
    		keyNumber++;}
    	}
    	JPanel jpButtons = new JPanel(gbl);
		
    	gbc = new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
    	jpButtons.add(jbSave,gbc);
		
    	gbc = new GridBagConstraints(0,1,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
    	jpButtons.add(jbCancel,gbc);
		
    	gbc = new GridBagConstraints(1,0,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
    	jpButtons.add(jbLoad,gbc);
		
    	gbc = new GridBagConstraints(1,1,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
    	jpButtons.add(jbTranslation,gbc);
    	
    	if(!fullTranslation)
    	{
    		gbc = new GridBagConstraints(0,2,1,1,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
    		jpButtons.add(jbAddShortcut,gbc);
    		
    		gbc = new GridBagConstraints(1,2,1,1,1.0,1.0,GridBagConstraints.WEST,
        			GridBagConstraints.BOTH, insets,0,0);
    		jpButtons.add(jbDelShortcut,gbc);
    	}
    	
    	gbc = new GridBagConstraints(2,0,2,3,1.0,1.0,GridBagConstraints.WEST,
    			GridBagConstraints.BOTH, insets,0,0);
    	pan.add(jpButtons,gbc);

		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.CENTER;
		JPanel jp = new JPanel();
		jp.add(pan,gbc);
    	
		JScrollPane scrollRes = new JScrollPane (jp);
		scrollRes.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollRes.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		getContentPane().add(scrollRes);
    	pack();
    	if(!fullTranslation && ConfigNat.getCurrentConfig().getScrReader()==Nat.SR_DEFAULT)
		{
    		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension size = this.getPreferredSize();
			screenSize.height = screenSize.height/2;
			screenSize.width = screenSize.width/2;
			size.height = size.height/2;
			size.width = size.width/2;
			int y = screenSize.height - size.height;if(y<0)y=0;
			int x = screenSize.width - size.width;if(x<0)x=0;
			setLocation(x, y);
		}
		else setExtendedState(MAXIMIZED_BOTH);
    	setVisible(true);
    }
    
	/**
     * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
     */
    @Override
    public void windowActivated(WindowEvent arg0){/*do nothing*/}
	/**
     * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
     */
    @Override
    public void windowClosed(WindowEvent arg0){/*do nothing*/}

	/**
	 * 
     * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
     */
    @Override
    public void windowClosing(WindowEvent arg0){/*do nothing*/}

	/**
     * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
     */
    @Override
    public void windowDeactivated(WindowEvent arg0){/*do nothing*/}

	/**
     * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
     */
    @Override
    public void windowDeiconified(WindowEvent arg0){/*do nothing*/}

	/**
     * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
     */
    @Override
    public void windowIconified(WindowEvent arg0){/*do nothing*/}

	/**
     * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
     */
    @Override
    public void windowOpened(WindowEvent arg0){/*do nothing*/}

	/**
	 * buttons action
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent ae)
    {
    	if(ae.getSource()==jbCancel)
	    {
    		printShortcuts();
	    	Boolean b=isModified();
    		if(b)
    			if(confirm("unsavedmodif"))b=false;
    		if(!b)dispose();
	    }
    	else if(ae.getSource()==jbSave)
	    {
	    	printShortcuts();
	    	if(!isModified())dispose();
	    	else
	    	{
	    		save(getPath(), textFields, labels, keys);
		    	dispose();
	    	}
	    }
    	else if(ae.getSource()==jbLoad)
	    {
	    	printShortcuts();
	    	Boolean b=isModified();
    		if(b)
    			if(confirm("unsavedmodif"))b=false;
    		if(!b)
    		{
    			b=fullTranslation;
    			if(b)
    				if(confirm("betterusetranslation"))
    					b=false;
    			if(!b)
    				if(checkLocale())load();
    		}
	    }
    	else if(ae.getSource()==jbTranslation)
	    {
	    	printShortcuts();
	    	Boolean b=isModified();
    		if(b)
    			if(confirm("unsavedmodif"))b=false;
    		if(!b)
    			if(checkLocale())loadTranslation();
    			
	    }
    	else if(ae.getSource()==jbAddShortcut)
	    {
    		printShortcuts();
	    	Boolean b=isModified();
    		if(b)
    			if(confirm("unsavedmodif"))b=false;
    		if(!b)
    			addShortcut();
	    }
    	else if(ae.getSource()==jbDelShortcut)
	    {
    		printShortcuts();
	    	Boolean b=isModified();
    		if(b)
    			if(confirm("unsavedmodif"))b=false;
    		if(!b)
    			delShortcut();
	    }
    	else for(int j=0;j<jbShortcuts.length;j++)
	    {
	    	if(ae.getSource()==jbShortcuts[j])
	    	{
	    		keyModifiers[j]=0;
	    		keyCodes[j]=0;
	    		shortcutButtonPressed[j]=true;
	    		setShortcutDisplay(j);
	    	}
	    	else shortcutButtonPressed[j]=false;
	    }
    }
    
    /** @return true if modified */
	private boolean isModified() 
	{
		for(int i=0; i<num; i++)
		{
			if(textFields[i].getText().length()!=0 && unmodifiedTexts[i].length()==0)
				return true;
			if((textFields[i].isEnabled() || labels[i].getText().contains("class=\"shortcut\"")) && 
					!unmodifiedTexts[i].equals(textFields[i].getText()))
				return true;
		}
		return false;
	}
	
	/** @return true if user */
	private Boolean checkLocale()
	{
		Boolean answer = true;
		// checking if the locale is valid
		if(!knownLanguage(jtfLanguage.getText())
				|| (jtfVerbosity.isEnabled() && !knownVerbosity(jtfVerbosity.getText()))
				|| (jtfScreenReader.isEnabled() && !knownScreenReader(jtfScreenReader.getText())))
		{
			answer = confirm("unknownlocale");
		}
		return answer;
	}
	
	/** @return shortcut path (folder/name.properties) */
	private String getShortcutPath()
	{
		ConfigNat.getCurrentConfig();
		if(!ConfigNat.getCurrentConfig().getChangeSysFiles())
			return ConfigNat.getConfTextFolder()+fileName+".properties";
		return ConfigNat.getInstallFolder()+"text/"+fileName+".properties";
	}
	
	/** @return basic path (folder/lang/name_lang.properties) */
	private String getBasicPath()
	{
		ConfigNat.getCurrentConfig();
		if(!ConfigNat.getCurrentConfig().getChangeSysFiles())
			return ConfigNat.getConfTextFolder()+texts.getLoc()[0]+"/"+fileName+"_"+texts.getLoc()[0]+".properties";
		return ConfigNat.getInstallFolder()+"text/"+texts.getLoc()[0]+"/"+fileName+"_"+texts.getLoc()[0]+".properties";
	}
	
	/** @return path to the file */
	private String getPath()
	{
		ConfigNat.getCurrentConfig();
		String path="";
		if(jtfLanguage.getText().length()!=0)path=path+jtfLanguage.getText()+"/";
		path=path+fileName;
		if(jtfLanguage.getText().length()!=0)
		{
			path=path+"_"+jtfLanguage.getText();
			if(jtfVerbosity.getText().length()!=0)
			{
				path=path+"_"+jtfVerbosity.getText();
				if(jtfScreenReader.getText().length()!=0)path=path+"_"+jtfScreenReader.getText();
			}
		}
		path=path+".properties";
		if(!ConfigNat.getCurrentConfig().getChangeSysFiles())path=ConfigNat.getConfTextFolder()+path;
		else path=ConfigNat.getInstallFolder()+"text/"+path;
		return path;
	}
	
	/** @return true if file exists */
	private Boolean checkFile()
	{
		File f = new File(getPath());
		if(f.exists())return true;
		return false;
	}
    
	/** create whole file, with layout */
    private void loadTranslation() 
    {
    	String[] labelList = new String[500];
    	String[] keyList = new String[500];
    	int i=0;
    	
    	Boolean addShortcuts = jtfLanguage.getText().length()==0?true:false;
    	
    	// base file : text/lang/file_lang.properties or text/file.properties
		String path=getBasicPath();
		File f = new File(path);
		if(!f.exists())
		{
			path=getShortcutPath();
			f = new File(path);
		}
		if(f.exists())
		{
			int k=0;
			Boolean blankLine=false; //if previous line is blank
			Boolean answer = true;
			String[] lines = FileToolKit.loadFileToStr(path,"ISO-8859-1").split("\n");
			for(String s : lines)
			{
				if(s.length()==0)blankLine=true; // might add indentation to the next line
				else
				{
					if(!s.startsWith("#") && s.contains(" = ")) // line with a key
					{
						// case shortcut
						if(s.split("=",2)[0].contains("KEY"))
						{
							if(addShortcuts)
							{
								keyList[i] = s.split(" = ",2)[0];
								labelList[i]="<html><p class=\"shortcut\">"+texts.getText("shortcut")+"</p></html>";
								i++;
							}
						}
						else
						{
							labelList[i] = s.split(" = ",2)[1];
							
			    			// shows html code
							labelList[i]=labelList[i].replaceAll("<","&lt;").replaceAll(">", "&gt;").replaceAll(" ","&nbsp;");
			        		
							if(blankLine)
							{
								// labelList[i]="       "+labelList[i];
							}
						keyList[i] = s.split(" = ",2)[0];
						i++;
						}
					}
					else if (s.startsWith("#") && !s.contains("----")) // commentary line
					{
						String title = "<h4 color=\"blue\" ";
						if(k!=0 && k<lines.length-1)
						{
							if(lines[k-1].startsWith("#") && lines[k-1].contains("----")
							&& lines[k+1].startsWith("#") && lines[k+1].contains("----"))
								// <=> title
								title="<h4 style=\"text-decoration:underline\" color=\"red\" ";
						}
						// comment lines in the dialog : 
						labelList[i] = "<html>"+title+"class=\"comment\">"
									+ s.replaceFirst("#", "")
									+ "</h4></html>";
						keyList[i] = labelList[i]; // random unknown key
						i++;
					}
					blankLine=false;
				}
				k++;if(k==499)break;
			}
			// Copying labels and keys
			String[] labelList2 = new String[i];
			String[] keyList2 = new String[i];
			for(int j=0; j<i; j++)
			{
				labelList2[j]=labelList[j];
				keyList2[j]=keyList[j];
			}
			if(!checkFile()) // file doesn't exist
			{
				answer = confirm("notfound"); // asks to create the file
				if(answer)
				{
					String newBundle="";
					for(String s : lines)
					{
						Boolean b=true;
						if(s.contains(" = "))
								if(s.split(" = ")[0].contains("KEY"))
									b=false;
						if(b)newBundle+="\n"+s; // copying base file
					}
					if(knownLanguage(jtfLanguage.getText()))
					{
						FileToolKit.saveStrToFile(newBundle, getPath(), "ISO-8859-1");
					}
					else 
					{
						answer=false;
						System.err.println("Cannot create file. Reason : language does not exist");
					}
				}
			}
			if(answer) // file successfully created or already existing
			{
				String verb = !jtfVerbosity.isEnabled()?"":jtfVerbosity.getText();
				String sr = !jtfScreenReader.isEnabled()?"":jtfScreenReader.getText();
				new DialogueAccess(labelList2, keyList2, jtfLanguage.getText(), 
						verb, sr, fileName,true);
				dispose();
			}
		}
	}

	/** 
	 * saves 
	 * @param path : path to the bundle 
	 * @param tfs textfields to save
	 * @param labels2 labels
	 * @param keys2 keys
	 */
	private void save(String path, JTextField[] tfs, JLabel[] labels2, String[] keys2) 
	{
		File f = new File(path);
		String top = "";
		if(!f.exists())
		{
			top = "# -----------------------------------------------------\n"
				+ "# Textual Contents for the "+fileName+" window\n"
				+ "# -----------------------------------------------------\n";
		}
		String[] lines = FileToolKit.loadFileToStr(path,"ISO-8859-1").split("\n");
		String bottom = "";
		Boolean hasKey;
		String path2=getShortcutPath();
		for(int i=0; i<tfs.length; i++)
		{
			if(tfs[i].isEnabled()
					|| labels2[i].getText().contains("class=\"shortcut\""))
			{
				if(!tfs[i].getText().equals(unmodifiedTexts[i])
					|| tfs.length==1
					|| labels2[i].getText().contains("class=\"shortcut\""))
				{
					int j=0;
					hasKey = false;
					for(String s : lines)
					{
						if(s.startsWith(keys2[i]+" = ") 
								&& (tfs.length==1
								|| !labels2[i].getText().contains("class=\"shortcut\"") 
								|| path2.equals(path))) // == right path
						{
							lines[j]=keys2[i]+" = "+tfs[i].getText();
							hasKey=true;
							break;
						}
						j++;
					}
					if(!hasKey)
					{
						Boolean b=false; // true : must add at the bottom
						File f2 = new File(path2);
						if(!path2.equals(path) && labels2[i].getText().contains("class=\"shortcut\""))
						{
							b=true; // doesn't try to save it twice
							if(f2.exists())
							{
								JTextField[] tf = {tfs[i]};
								JLabel[] jl = {labels2[i]};
								String[] k2 = {keys2[i]};
								save(path2,tf, jl, k2);
								
							}
						}
						else
						{
							// tries to add the key with it's context
							String z=" "; // basic key name
							if(keys2[i].contains("KEY"))z=keys2[i].split("KEY",2)[0];
							else if(keys2[i].endsWith("label"))z=keys2[i].replaceAll("label","");
							else if(keys2[i].endsWith("desc"))z=keys2[i].replaceAll("desc","");
							else if(keys2[i].endsWith("name"))z=keys2[i].replaceAll("name","");
							else if(keys2[i].endsWith("ttt"))z=keys2[i].replaceAll("ttt","");
							if(!z.equals(" "))
							{
								for(int x=0; x<lines.length; x++)
								{
									String y=lines[lines.length-x-1];
									if(y.contains(" = "))
										if(y.split(" = ",2)[0].startsWith(z) 
										&& y.split(" = ",2)[0].length()-z.length()<6) // ~same size
										{
											b=true;
											lines[lines.length-x-1]+="\n"+keys2[i]+" = "+tfs[i].getText();
											break;
										}
								}
							}
						}
						if(!b) // if context cannot be found
							bottom=bottom+"\n"+keys2[i]+" = "+tfs[i].getText();
					}
				}
			}
		}
		String newBundle=top; Boolean isBlank=true; // removes first blank lines
		for (String s : lines)
		{
			if(s.length()>1)isBlank=false;
			if(!isBlank)newBundle+=s+"\n";
		}
		newBundle = newBundle+bottom.replaceFirst("\n","");
		FileToolKit.saveStrToFile(newBundle,path,"ISO-8859-1");
	}
	
	/** load */
	private void load()
	{
		if(!checkFile() && !ConfigNat.getCurrentConfig().getChangeSysFiles())
		{
			ConfigNat.getCurrentConfig();
			String path="";
			
			if(jtfLanguage.getText().length()!=0)
			{
				path=jtfLanguage.getText()+"/";
				new File(ConfigNat.getConfTextFolder()+path).mkdir();
			}
			path=path+fileName;
			if(jtfLanguage.getText().length()!=0)
			{
				path=path+"_"+jtfLanguage.getText();
				if(jtfVerbosity.getText().length()!=0)
				{
					path=path+"_"+jtfVerbosity.getText();
					if(jtfScreenReader.getText().length()!=0)path=path+"_"+jtfScreenReader.getText();
				}
			}
			path=path+".properties";
			File f = new File(ConfigNat.getConfTextFolder()+path);
			if (!f.exists())
				{
					f = new File(ConfigNat.getInstallFolder()+"text/"+path);
					if(f.exists())
					FileToolKit.copyFile(ConfigNat.getInstallFolder()+"text/"+path,
						ConfigNat.getConfTextFolder()+path, "ISO-8859-1", "ISO-8859-1");
				}
						
		}
		if(checkFile())
		{
			locale[0]=jtfLanguage.getText();
			locale[1]=jtfVerbosity.getText();
			locale[2]=jtfScreenReader.getText();
			Boolean hasKey;
			String[] lines = FileToolKit.loadFileToStr(getPath(),"ISO-8859-1").split("\n");
			String[] lines2 = FileToolKit.loadFileToStr(getShortcutPath(),"ISO-8859-1").split("\n");
			for(int i=0; i<num; i++)
			{
				hasKey = false;
				if(labels[i].getText().contains("class=\"comment\""))
				{
					textFields[i].setText("");
					textFields[i].setEnabled(false);
				}
				else if(textFields[i].isEnabled())
				{
					for(String s : lines)
					{
						if(s.startsWith(keys[i]+" ="))
						{
							textFields[i].setText(s.split(" = ",2)[1]);
							hasKey = true;
						}
					}
				}
				if(!hasKey && labels[i].getText().contains("class=\"shortcut\""))
				{
					for(String s : lines2)
					{
						if(s.startsWith(keys[i]+" ="))
						{
							textFields[i].setText(s.split(" = ",2)[1]);
							hasKey = true;
						}
					}
				}
				else if(!hasKey)textFields[i].setText("");
				String s=" ";
				if(keys[i].endsWith("ttt"))s="tooltip";
				if(keys[i].endsWith("name"))s="accessname";
				if(keys[i].endsWith("desc"))s="description";
				if(keys[i].endsWith("label"))s="label";
				if(s!=" ")textFields[i].setToolTipText(texts.getText(s+"ttt"));
				if(fullTranslation)
				{
					String b=(s!=" ")?"<html><p><b>["+texts.getText(s)+"]</b> ":"<html><p>";
					labels[i].setText(b+"<span style=\"font-weight:lighter\">"+labels[i].getText()+"</span></p></html>");
				}
				if(labels[i].getText().contains("class=\"shortcut\""))textFields[i].setEnabled(false);
				unmodifiedTexts[i]=textFields[i].getText();
			}
			loadShortcuts();
		}
		else
		{
			if(confirm("notfound"))
				for(int i=0; i<num; i++)
				{
					locale[0]=jtfLanguage.getText();
					locale[1]=jtfVerbosity.getText();
					locale[2]=jtfScreenReader.getText();
					// TODO : proposer le choix de copier un fichier texte existant
					if(!labels[i].getText().contains("class=\"shortcut\""))textFields[i].setText("");
					unmodifiedTexts[i]=textFields[i].getText();
					loadShortcuts();
				}
		}
	}

	/** load shortcuts */
	private void loadShortcuts() 
	{
		int j=0;
		for(int i=0;i<num;i++)
			if(labels[i].getText().contains("class=\"shortcut\""))
			{
				String[] sk=(textFields[i].getText().contains("-")?
								textFields[i].getText():"0-0").split("-",2);
				try
				{
					keyCodes[j] = new Integer(sk[0]);
					keyModifiers[j] = new Integer(sk[1]);
				}
				catch(NumberFormatException nfe)
				{
					keyCodes[j] = 0;
					keyModifiers[j] = 0;
				}
				textFields[i].setText(keyCodes[j]+"-"+keyModifiers[j]);
				
				// ca peut ajouter des modifs sans que l'utilisateur ait touché à rien, du coup
				checkShortcuts(j);
				setShortcutDisplay(j);
				j++;
			}
		for(int i=0;i<num;i++){
		unmodifiedTexts[i]=textFields[i].getText();}
	}
	
	/** prints the shortcuts in textfields */
	private void printShortcuts() 
	{
		int j=0;
		for(int i=0;i<num;i++)
			if(labels[i].getText().contains("class=\"shortcut\""))
			{
				textFields[i].setText(keyCodes[j]+"-"+keyModifiers[j]);
				j++;
			}
	}
	
	/** 
	 * displays the shortcut
	 * @param j : number
	 */
	private void setShortcutDisplay(int j)
	{
		String modifier = keyModifiers[j]==0?" ":" "+InputEvent.getModifiersExText(keyModifiers[j])+" +";
		String code = keyCodes[j]==0?" ":" "+KeyEvent.getKeyText(keyCodes[j]);
		if(keyModifiers[j]==0 && keyCodes[j]==0)code=texts.getText("none");
		jbShortcuts[j].setText(texts.getText("currentshortcut")+modifier+code);
	}

	
	
	/**
	 * @see javax.swing.event.CaretListener#caretUpdate(javax.swing.event.CaretEvent)
	 */
	@Override
	/** changes in locale's textfields */
	public void caretUpdate(CaretEvent ae) 
	{
		if(ae.getSource()==jtfLanguage)
    	{
    		if(jtfLanguage.getText().length()==0)
    		{
    			jtfVerbosity.setEnabled(false);
    			jtfScreenReader.setEnabled(false);
    		}
    		else
    		{
    			jtfVerbosity.setEnabled(true);
    			if(jtfVerbosity.getText().length()!=0)jtfScreenReader.setEnabled(true);
    		}
    		if(((ImageIcon) jlLanguage.getIcon()).getDescription().equals("ok"))
    		{
    			if(!knownLanguage(jtfLanguage.getText()))
    				jlLanguage.setIcon(new ImageIcon("ui/icon/dialog-error.png","error"));
    		}
    		else if(((ImageIcon) jlLanguage.getIcon()).getDescription().equals("error"))
    		{
    			if(knownLanguage(jtfLanguage.getText()))
    				jlLanguage.setIcon(new ImageIcon("ui/icon/emblem-default.png","ok"));
    		}
    	}
		if(ae.getSource()==jtfVerbosity)
    	{
    		if(jtfVerbosity.getText().length()==0 || !jtfVerbosity.isEnabled())
    			jtfScreenReader.setEnabled(false);
    		else
    			jtfScreenReader.setEnabled(true);
    		if(((ImageIcon) jlVerbosity.getIcon()).getDescription().equals("ok"))
    		{
    			if(!knownVerbosity(jtfVerbosity.getText()))
    				jlVerbosity.setIcon(new ImageIcon("ui/icon/dialog-error.png","error"));
    		}
    		else if(((ImageIcon) jlVerbosity.getIcon()).getDescription().equals("error"))
    		{
    			if(knownVerbosity(jtfVerbosity.getText()))
    				jlVerbosity.setIcon(new ImageIcon("ui/icon/emblem-default.png","ok"));
    		}
    	}
		if(ae.getSource()==jtfScreenReader)
		{
			if(((ImageIcon) jlScreenReader.getIcon()).getDescription().equals("ok"))
    		{
    			if(!knownScreenReader(jtfScreenReader.getText()))
    				jlScreenReader.setIcon(new ImageIcon("ui/icon/dialog-error.png","error"));
    		}
    		else if(((ImageIcon) jlScreenReader.getIcon()).getDescription().equals("error"))
    		{
    			if(knownScreenReader(jtfScreenReader.getText()))
    				jlScreenReader.setIcon(new ImageIcon("ui/icon/emblem-default.png","ok"));
    		}
		}
	}
	/** @param s TODO
	 * @return true if verbosity is known */
	private Boolean knownVerbosity(String s)
	{
		if(s.equals("high")
    			|| s.equals("low")
        		|| s.equals("debug")
        		|| s.equals("average")
        		|| s.equals(""))
			return true;
		return false;
	}
	
	/** @param s TODO
	 * @return true if language is known */
	private Boolean knownLanguage(String s)
	{
		String[] a = FileToolKit.loadFileToStr("outils/Languages.txt","ISO-8859-1").split("\n");
		for (String l : a)
		{
			if(l.split("=",2)[0].equals(s))return true;
		}
		if(s.equals(""))return true;
		return false;
	}
	
	/** @param s TODO
	 * @return true if screen reader is known */
	private Boolean knownScreenReader(String s)
	{
		String[] a = FileToolKit.loadFileToStr("outils/ScrReaders.txt","ISO-8859-1").split("\n");
		for (String l : a)
		{
			if(l.split("=",2)[0].equals(s))return true;
		}
		if(s.equals(""))return true;
		return false;
	}

	/**
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent ke) 
	{
		enableComponents(true);
		for(int j=0;j<jbShortcuts.length;j++)
	    {
	    	if(ke.getSource()==jbShortcuts[j])
	    	{
	    		if(keyCodes[j]==0)
	    		{
	    			keyCodes[j]=0;keyModifiers[j]=0;
		    		setShortcutDisplay(j);
	    		}
	    	}
	    }
	}

	/**
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent ke) 
	{
		for(int j=0;j<jbShortcuts.length;j++)
	    {
	    	if(ke.getSource()==jbShortcuts[j] && shortcutButtonPressed[j])
	    	{
	    		enableComponents(false);
	    		Boolean b=false;
	    		int modif=0;
	    		if(ke.getKeyCode()==KeyEvent.VK_CONTROL){modif = InputEvent.CTRL_DOWN_MASK;b=true;}
	    		if(ke.getKeyCode()==KeyEvent.VK_ALT) {modif = InputEvent.ALT_DOWN_MASK;b=true;}
	    		if(ke.getKeyCode()==KeyEvent.VK_SHIFT) {modif = InputEvent.SHIFT_DOWN_MASK;b=true;}
	    		
	    		if(b)
	    		{
	    			Boolean b2=false; // true if modifier is already included
	    			for(int n=0;n<2;n++)
	    			{
	    				for(int m=0;m<2;m++)
		    			{
	    					// vérifie les combinaisons linéaires de modifiers
	    					if(modif==InputEvent.ALT_DOWN_MASK && keyModifiers[j]==InputEvent.ALT_DOWN_MASK+
	    																	m*InputEvent.CTRL_DOWN_MASK+
	    																	n*InputEvent.SHIFT_DOWN_MASK)
	    						b2=true;
	    					if(modif==InputEvent.CTRL_DOWN_MASK && keyModifiers[j]==InputEvent.CTRL_DOWN_MASK+
									m*InputEvent.ALT_DOWN_MASK+
									n*InputEvent.SHIFT_DOWN_MASK)
	    						b2=true;
	    					if(modif==InputEvent.SHIFT_DOWN_MASK && keyModifiers[j]==InputEvent.SHIFT_DOWN_MASK+
									m*InputEvent.CTRL_DOWN_MASK+
									n*InputEvent.ALT_DOWN_MASK)
	    						b2=true;
		    			}
	    			}
	    			if(!b2)keyModifiers[j]+=modif;
	    		}
	    		if(!b)
	    		{
	    			keyCodes[j]=ke.getKeyCode();
	    			shortcutButtonPressed[j]=false;
	    		}
	    		setShortcutDisplay(j);
	    		checkShortcuts(j);
	    	}
	    }
	}

	/**
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	/** 
	 * opens a confirmation dialog
	 * @param s : key of the message
	 * @return boolean : answer (true=yes)
	 */
	private Boolean confirm(String s)
	{
		int answer = JOptionPane.YES_OPTION ;
		answer = JOptionPane.showConfirmDialog(this, 
			texts.getText(s),
			texts.getText("confirm"),	
			JOptionPane.YES_NO_OPTION);
		if(answer==JOptionPane.YES_OPTION)return true;
		return false;
	}
	
	/** check shortcuts to prevent 2 buttons to have the same shortcut 
	 * @param j TODO*/
	private void checkShortcuts(int j)
	{
		if(keyCodes[j]!=0 || keyModifiers[j]!=0)
		for(int i=0;i<keyCodes.length;i++)
		{
			if(i!=j &&keyCodes[i]==keyCodes[j] && keyModifiers[i]==keyModifiers[j])
			{
				keyCodes[j]=0; keyModifiers[j]=0;
				jbShortcuts[j].setText(texts.getText("shortcutexists"));
			}
		}
	}
	
	/** add shortcut */
	private void addShortcut()
	{
		if(!fullTranslation)
		{
			int j=1;String name=" ";
			for(String s : keys)
			{
				if(s.contains("KEY"))
				{
					if(s.endsWith("KEY"))name=s;
					j++;
				}
			}
			if(name==" ")
			{
				for(String s : keys)
				{
					if(s.endsWith("label")){name=s.replaceFirst("label", "");break;}
					if(s.endsWith("name")){name=s.replaceFirst("name", "");break;}
					if(s.endsWith("desc")){name=s.replaceFirst("desc", "");break;}
					if(s.endsWith("ttt")){name=s.replaceFirst("ttt", "");break;}
				}
			}
			if(name!=" " && j<6)
			{
				String[] labelList = new String[num+1];
				String[] keyList = new String[num+1];
				for(int i=0;i<num;i++)
				{
					labelList[i]=labels[i].getText();
					keyList[i]=keys[i];
				}
				labelList[num]="<html><p class=\"shortcut\">"+texts.getText("shortcut")+" "+j+"</p></html>";
				keyList[num]=(j>1)?name+j:name+"KEY";
				new DialogueAccess(labelList, keyList, locale[0], 
						locale[1], locale[2], fileName,false);
				dispose();
			}
		}
	}
	
	/** removes last shortcut */
	private void delShortcut()
	{
		if(!fullTranslation)
		{
			for(int i=num-1;i>=0;i--)
			{
				if(keys[i].contains("KEY"))
					{
						String[] labelList = new String[num-1];
						String[] keyList = new String[num-1];
						for(int j=0;j<i;j++)
						{
							labelList[j]=labels[j].getText();
							keyList[j]=keys[j];
						}
						for(int j=0;j<num-1-i;j++)
						{
							labelList[i+j]=labels[i+j+1].getText();
							keyList[i+j]=keys[i+j+1];
						}
						
						String path=getShortcutPath();
						File f = new File(path);
						if(f.exists())
						{
							String newBundle="";
							String[] lines = FileToolKit.loadFileToStr(path,"ISO-8859-1").split("\n");
							for (String s : lines)
							{
								Boolean b=true;
								if(s.contains(" = "))
									if(s.split(" = ",2)[0].equals(keys[i]))
										b=false;
								if(b)newBundle+="\n"+s;
							}
							FileToolKit.saveStrToFile(newBundle, path, "ISO-8859-1");
						}
						new DialogueAccess(labelList, keyList, locale[0], 
								locale[1], locale[2], fileName,false);

						dispose();
						break;
					}
			}
		}
	}
	
	/** 
	 * pas vraiment propre
	 * @param b : true to enable, false to disable*/
	private void enableComponents(Boolean b)
	{
		jbLoad.setEnabled(b);
		jbSave.setEnabled(b);
		jbCancel.setEnabled(b);
		jbTranslation.setEnabled(b);
		jbAddShortcut.setEnabled(b);
		jbDelShortcut.setEnabled(b);
		jtfLanguage.setEnabled(b);
		jtfVerbosity.setEnabled(b);
		jtfScreenReader.setEnabled(b);
	}
}