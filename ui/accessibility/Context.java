/*
 * NAT - An universal Translator * Copyright (C) 2005 Bruno Mascret, Frédérick Schwebel, Vivien Guillet
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.accessibility;

import javax.swing.JComponent;
import nat.ConfigNat;
import nat.Nat;

/**
 * Builds an adapted context in the general case, depending on the screen reader and the language
 * @author Grégoire
 */
public class Context
{
	/** Textual contents */
	private Language texts;
	/** Type */
	private char type='d'; // default
	/** Mnemonic */
	private String mnemonic="";
	/** Key */
	private String key="";
	/** Bundle */
	private String bundle="";
	/** Accessible name */
	private String accName;
	/** Accessible description */
	private String accDescription;
	/** Tool tip text */
	private String ttt;
	/** 
	 * Shortcut key : (key,modifier)<br>
	 * use : KeyStroke.getKeyStroke(context.shortcut[i][0],context.shortcut[i][1])
	 */
	public int[][] shortcut;
	/** ContextualHelp */
	private ContextualHelp ch = new ContextualHelp();
	
	/** @return the accessible name */
	public String getName()
	{
		String name=accName;
		return name;
	}
	/** @return the accessible description */
	public String getDesc()
	{
		String desc=accDescription;
		return desc;
	}
	/** @return the tool tip text */
	public String getTTT()
	{
		return ttt;
	}
	/** sets contextualhelp 
	 * @param jc component
	 * @param helpid id */
	public void setContextualHelp(JComponent jc, String helpid)
	{
		ch.setParameters(jc,helpid,this);
	}
	
	/** Constructor :
	 *  @param m the shortcut, 
	 *  @param ctype the content type (ComboBox, Button, TextField, RadioBox, CheckBox, Spinner ...) (optional),
	 *  @param k the key to access the name, the description, and the tool tip text
	 *  @param l language
	 *  
	 *  The file must content 3 keys : [key]name, [key]desc, [key]ttt.
	 */
	public Context (String m, String ctype, String k, Language l)
	{
		if(ctype=="ComboBox"){type='c';}
		if(ctype=="Button"){type='b';}
		if(ctype=="TextField"){type='t';}
		if(ctype=="RadioBox"){type='r';}
		if(ctype=="CheckBox"){type='x';}
		if(ctype=="Spinner"){type='s';}
		key=k;
		mnemonic=m;
		texts=l;
		this.treat();
	}
	
	/** Constructor :
	 *  @param m the shortcut, 
	 *  @param k the key to access the name, the description, and the tool tip text
	 *  @param l language
	 *  
	 *  The file must content 3 keys : [key]name, [key]desc, [key]ttt
	 */
	public Context (String m, String k, Language l)
	{
		key=k;
		mnemonic=m;
		texts=l;
		this.treat();
	}
	/** Constructor :
	 *  @param k the key to access the name, the description, and the tool tip text
	 *  @param l language
	 *  
	 *  The file must content 3 keys : [key]name, [key]desc, [key]ttt
	 */
	public Context (String k, Language l)
	{
		key=k;
		mnemonic="";
		texts=l;
		this.treat();
	}
	
	/** Treats the context 
	 * */
	public void treat()
	{
		bundle = texts.getFilename();
		
		int sr=ConfigNat.getCurrentConfig().getScrReader(); // current screen reader
		int v=ConfigNat.getCurrentConfig().getNiveauLog(); // current verbosity
		
		ttt=texts.getText(key+"ttt");
		String accessDesc=texts.getText(key+"desc");
		String accessName=texts.getText(key+"name");
		
		shortcut = new int[5][2];
		for(int i=0;i<5;i++)
		{
			String e=(i==0)?"":""+(i+1);
			String s=texts.getText(key+"KEY"+e);
			String[] sk = new String[2];
			try
			{
				if(s.contains("-"))sk=s.split("-",2);
				else sk= new String[] {"0","0"};
			
				shortcut[i][0] = new Integer(sk[0]);
				shortcut[i][1] = new Integer(sk[1]);
			}
			catch(NumberFormatException nfe)
			{
				shortcut[i][0] = 0;
				shortcut[i][1] = 0;
			}
		}
		switch (sr)
		{
		case Nat.SR_JAWS :
		{
			switch (type)
			{
			case 'b' :
			{
				if(!mnemonic.isEmpty())accessName+=". (Alt + "+mnemonic+").";
				break;
			}case 'c' :
			{
				if(!mnemonic.isEmpty())accessName+=". (Alt + "+mnemonic+").";
				break;
			}
			case 't' :
			{
				if(!mnemonic.isEmpty())accessName+=". (Alt + "+mnemonic+").";
				break;
			}
			case 'x' :
			{
				if(v>=Nat.LOG_VERBEUX)accessName+=". Cette case est : ";
				break;
			}
			}
		if(v>=Nat.LOG_VERBEUX)accessDesc+=". Pour plus d'informations, consultez l'aide de NAT en appuyant sur la touche d'aide (Par défaut : F1).";
		break;
		}
		
		case Nat.SR_NVDA : // NVDA
		{
		if(v==Nat.LOG_SILENCIEUX) {accessDesc="   ";}
		if(!mnemonic.isEmpty())accessDesc=". (Alt + "+mnemonic+")."+accessDesc;
		break;
		}
		
		case Nat.SR_WEYES : // Window-Eyes
		{
			// TODO : trouver comment window eyes traite le context sans casser java
		}
		
		default : // unknown or default screen reader
		{
			accessDesc=(v==Nat.LOG_SILENCIEUX)?" ":accessDesc;
		}
		}
	//	
	
	this.accName=accessName;
	this.accDescription=(ConfigNat.getCurrentConfig().getNeverReadDescr())?" ":accessDesc;
	
	// If the "Read tool tip text" option is selected : 
	if(ConfigNat.getCurrentConfig().getReadTTT() && texts.hasText(key+"ttt"))
	{
		this.accName=texts.getText(key+"ttt");
		this.accDescription=" ";
	}
	
	// If the "Use tool tip text" option is selected : 
	if(ConfigNat.getCurrentConfig().getUseTTT() && texts.hasText(key+"ttt"))
	{
		this.accDescription=texts.getText(key+"ttt");
	}
	
	// If the "Read description" option is selected : 
	if(ConfigNat.getCurrentConfig().getReadDescr())
	{
		this.accName+=this.accDescription;
		this.accDescription=" ";
	}
	}
	
	/**
	 * reloads texts
	 */
	public void reload()
	{
		treat();
	}
	
	/** @return language */
	public Language getLanguage()
	{
		return texts;
	}
	
	/** @return key */
	public String getKey() 
	{
		return key;
	}
	/**
	 * @param labels labels
	 * @param keys keys */
	public void addLabels(String[] labels, String[] keys)
	{
		ch.addLabels(labels, keys);
	}
	/**
	 * @param label label
	 * @param newkey key */
	public void addLabel(String label, String newkey)
	{
		ch.addLabel(label, newkey);
	}
	/** @return bundle */
	public String getBundle()
	{
		return bundle;
	}
	/**
	 * @param s string to replace
	 * @param r replacement string */
	public void replace(String s, String r)
	{
		if(accName.contains(s))accName=accName.replaceAll(s, r);
		if(accDescription.contains(s))accDescription=accDescription.replaceAll(s, r);
		if(ttt.contains(s))ttt=ttt.replaceAll(s, r);
	}
}