/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret, Frédérick Schwebel, Vivien Guillet
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.accessibility;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

import nat.ConfigNat;
import nat.Nat;

/**
 * Accessible content for the 4th option tab's table 
 * @author Gregoire
 */
public class AccessibleTableCellRenderer extends JLabel implements TableCellRenderer
{
	/** Textual contents */
	Language texts = new Language("ConfMiseEnPageAvancee");
	
    /** Pour la sérialisation, non utilisé */
	private static final long serialVersionUID = 1L;
	/** table reference (unused) */
	private int ref;
    /** 4th tab's table */
    public final static int TABLE_ADDSTRINGS = 1;

	/** Constructor
	 *  @param reference : name of the table
	 * */
	public AccessibleTableCellRenderer(int reference)
	{
    	setOpaque(true);
		setHorizontalAlignment(LEFT);
		setVerticalAlignment(CENTER);
		ref=reference;
	}
	
	/** Renderer
	 * @param table : TODO
	 * @param value : TODO
	 * @param isSelected : TODO
	 * @param hasFocus : TODO
	 * @param row : TODO 
	 * @param column : TODO
	 * @return TODO
	 * */
	@Override
    public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) 
	{
		JTextField cell = new JTextField();
	    if (value != null)
	    cell.setText(value.toString());
	    String accessName="";
	    String accessDesc="";
	    
	    switch (ref)
	    {
	    case TABLE_ADDSTRINGS :
	    {
		    cell.setBackground((row % 2 == 0) ? Color.white : Color.lightGray);
		    cell.setBorder(hasFocus?BorderFactory.createMatteBorder(1,2,1,2,Color.black):null);
	       	if(column==0)
	    	{
	    		accessName=texts.getText("beforename")+table.getValueAt(row, 1).toString();
	    		accessDesc=(ConfigNat.getCurrentConfig().getNiveauLog()!=Nat.LOG_SILENCIEUX)?texts.getText("beforedesc"):" ";
	    	}
	    	if(column==2)
	    	{
	    		accessName=texts.getText("aftername")+table.getValueAt(row, 1).toString();
	    		accessDesc=(ConfigNat.getCurrentConfig().getNiveauLog()!=Nat.LOG_SILENCIEUX)?texts.getText("afterdesc"):" ";
	    	}
	    	if(column==1)
	    	{
	    		accessName=texts.getText("elementname")+table.getValueAt(row,1).toString();
	    		accessDesc=(ConfigNat.getCurrentConfig().getNiveauLog()!=Nat.LOG_SILENCIEUX)?texts.getText("elementdesc"):" ";
	    		//cell.setEnabled(false);
	    	}
	    	break;
	    }

	    
	    }
	    cell.getAccessibleContext().setAccessibleName(accessName);
	    cell.getAccessibleContext().setAccessibleDescription(accessDesc);
	    return cell;
	   
	}

}
