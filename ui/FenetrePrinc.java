/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package ui;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.swing.ImageIcon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import outils.FileToolKit;
import outils.emboss.Embosseur;
import outils.emboss.EmbosseurInstalle;
import outils.emboss.EmbosseurLDC;
import ui.ConfigurationsComboBoxRenderer;
import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;
import ui.config.Configuration;
import ui.editor.Editeur2;
import ui.editor.EditeurTan;

//*** Gestionnaires ***
import gestionnaires.GestionnaireOuvrir;
import gestionnaires.GestionnaireErreur;
import gestionnaires.GestionnaireOuvrirFenetre;
import nat.ConfigNat;
import nat.Nat;
import nat.NatThread;
import nat.OptNames;
import nat.Transcription;
import ui.FenetreSystemConfRestorer;

/**
 * Fenêtre principale de l'interface graphique
 * @author bruno et vivien et fred
 *
 */
public class FenetrePrinc extends JFrame implements ActionListener, FocusListener, WindowListener, ComponentListener, ChangeListener, PopupMenuListener
{
	/*----------------------------------------------------------------------------------------
	 *                                   CONSTANTS
	 *----------------------------------------------------------------------------------------
	 */
	/** numéro de version pour la sérialisation (inutilisé par NAT) */
    private static final long serialVersionUID = 1L;
    /** Look and feel constants */
    private static final Color PAN_COLOR = Color.getHSBColor(250f/360, 0, 0.96f);
    /** Look and feel constants */
    private static final Color PAN_CONF_COLOR = Color.getHSBColor(250f/360, 0, 0.98f);
    
    /*----------------------------------------------------------------------------------------
	 *                                   NO COMPONENT ATTRIBUTES
	 *----------------------------------------------------------------------------------------
	 */
    /** Textual contents */
	private Language texts = new Language("FenetrePrinc");
	/** Indique si le Thread du scénario de transcription est lancé */
    private boolean running = false;
    /** Le gestionnaire d'erreur de NAT utilisé */
    private GestionnaireErreur gestErreur;
    /** L'instance de Nat */
    private Nat nat;
    /**
     * Vrai si la fenêtre option est ouverte
     * @see GestionnaireOuvrirFenetre
     * @see Configuration
     */
    private boolean optionsOuvertes = false;
    /** Indique si une erreur s'est produite lors de la transcription */
    private boolean error = false;
    /** Thread où s'exécute la transcription */
    private Thread thrTrans = null;
    /** sense button help */
    private ContextualHelp chSens = new ContextualHelp();
    
	
    /*----------------------------------------------------------------------------------------
	 *                                   COMPONENTS
	 *----------------------------------------------------------------------------------------
	 */
	
	//-------------- Buttons -----------
	
	/** Bouton ouvrant le JFileChooser pour l'adresse du fichier noir 
     * @see FenetrePrinc#jtfNoir
     * @see GestionnaireOuvrir
     * */
    private JButton btSelectNoir = new JButton(texts.getText("handwrittenbutton"),new ImageIcon("ui/icon/document-open.png"));
    /** Bouton ouvrant le JFileChooser pour l'adresse du fichier braille 
     * @see FenetrePrinc#jtfBraille
     * @see GestionnaireOuvrir
     * */
    private JButton btSelectBraille = new JButton(texts.getText("braillefilebutton"),new ImageIcon("ui/icon/document-open.png"));
    /** Bouton lançant la transcription 
     * @see Nat#fabriqueTranscriptions
     * @see Nat#lanceScenario()
     * */
    private JButton btTranscrire = new JButton(texts.getText("transcribe"),new ImageIcon("ui/icon/system-run.png"));
    /** Bouton ouvrant l'éditeur de fichier transcrit
     * @see FenetrePrinc#afficheFichier(String adresse)
     */
    private JButton btEditeur = new JButton(texts.getText("editor"),new ImageIcon("ui/icon/gtk-edit.png"));
    /**
     * Bouton annulant la transcription
     * Button for stopping the current transcription
     */
    private JButton btCancel = new JButton(texts.getText("cancel"),new ImageIcon("ui/icon/process-stop.png"));
    /** Bouton ouvrant l'éditeur avec un fichier déjà transcrit
     * @see FenetrePrinc#afficheFichier(String adresse)
     * @since 2.0
     */
    private JButton btEditeurTrans = new JButton("editortrans",new ImageIcon("ui/icon/gtk-edit.png"));
    /**
     * Bouton ouvrant la fenetre d'options
     * @see GestionnaireOuvrirFenetre
     */
    private JButton btOption = new JButton(texts.getText("option"),new ImageIcon("ui/icon/document-properties.png"));
    /**
     * Bouton ouvrant la fenetre d'aide
     * @see GestionnaireOuvrirFenetre
     */
    private JButton btAide = new JButton(texts.getText("help"),new ImageIcon("ui/icon/help-browser.png"));
    /**
     * Bouton ouvrant la fenetre de renseignements sur Nat
     * @see GestionnaireOuvrirFenetre
     */
    private JButton btAPropos = new JButton(texts.getText("about"),new ImageIcon("ui/icon/help-about.png"));
    /** Bouton permettant de quitter le programme */
    private JButton btQuitter = new JButton(texts.getText("quit"),new ImageIcon("ui/icon/exit.png"));
    /* Bouton activant l'interface de rapport d'erreur 
    private JButton btSignalBug = new JButton(texts.getText("bug"),new ImageIcon("ui/icon/script-error.png"));*/
    /** Bouton inversant le sens de transcription*/
    private JButton btSens = new JButton(texts.getText("reverse"), new ImageIcon("ui/icon/object-flip-vertical.png"));
    /** Bouton montrant les logs/ Button showing/hiding logs*/
    private JButton btShowLog = new JButton(texts.getText("showLog"),new ImageIcon("ui/icon/logviewer.png"));
    /** Bouton montrant le détail de la configuration/ Button showing/hiding configuration details*/
    private JButton btShowConf = new JButton(texts.getText("showConf"),new ImageIcon("ui/icon/preferences-other.png"));
    /** Bouton enregistrer la configuration/ button save configuration */
    private JButton btSaveConf = new JButton(texts.getText("save"),new ImageIcon("ui/icon/document-save.png"));
    /** Bouton enregistrer la configuration sous /button save configuration as...  */
    private JButton btSaveConfAs = new JButton(texts.getText("saveAs"),new ImageIcon("ui/icon/document-save-as.png"));
    
    //-------------- ComboBoxes -----------
    
    /** JComboBox listant les configurations possibles  */
    private JComboBox<ConfigurationsListItem> jcbConfig = new JComboBox<ConfigurationsListItem>();
    /** Combobox for mode configurations*/
    private JComboBox<ConfigurationsListItem> jcbConfMode = null;
    /** Combobox for layout configurations*/
    private JComboBox<ConfigurationsListItem> jcbConfLayout= null;
    /** Combobox for format configurations*/
    private JComboBox<ConfigurationsListItem> jcbConfFormats = null;
   
    //-------------- Labels -----------
	
    /** Label associé à jtfNoir
     * @see FenetrePrinc#jtfNoir
     * */
    private JLabel lJtfNoir = new JLabel(texts.getText("handwrittenlabel"));
	/** Label associé à {@link #jtfBraille}
	 * @see FenetrePrinc#jtfBraille
	 */
	private JLabel lJtfBraille = new JLabel(texts.getText("braillelabel"));
	/** Label contenant l'icône NAT/TAN correspondant la transcription ou à la détranscription*/
	private JLabel lIcone = new JLabel(new ImageIcon("ui/nat.png"));
	/** Label sens de transcription/ reverse or normal transcription label*/
	private JLabel lSens = new JLabel(texts.getText("dir1"));
	/** Label contenant l'icône correspondant au sens transcription ou détranscription*/
	private JLabel lSensIcon = new JLabel(new ImageIcon("ui/icon/go-down.png"));
	/**
     * Label associé à jcbConfig
     * @see FenetrePrinc#jcbConfig
     */
    private JLabel lConfig = new JLabel(texts.getText("configlabel"));
    
    //-------------- Menus ------------
    
    /** JMenu contenant les actions possibles sur la fenêtre principale  */
    private JMenu jmAction = new JMenu(texts.getText("menu"));
    /** JMenu contenant les raccourcis vers les actions d'information de la fenêtre principale  */
    private JMenu jmAPropos = new JMenu(texts.getText("helpmenu"));
    /** JMenuBar de la fenêtre principale */
    private JMenuBar jmb = new JMenuBar();
    /**
     * Elément de menu Choisir fichier noir
     * @see FenetrePrinc#jmAction
     * @see GestionnaireOuvrir
     */
    private JMenuItem jmiSelectNoir;
    /**
     * Elément de menu Choisir fichier braille
     * @see FenetrePrinc#jmAction
     * @see GestionnaireOuvrir
     */
    private JMenuItem jmiSelectBraille;
    /**
     * Elément de menu Ouvrir options
     * @see FenetrePrinc#jmAction
     * @see GestionnaireOuvrirFenetre
     */
    private JMenuItem jmiOptions;
    /**
     * Elément de menu Importer une configuration
     * @see FenetrePrinc#jmAction
     */
    private JMenuItem jmiImporter;
    /**
     * Elément de menu Exporter une configuration
     * @see FenetrePrinc#jmAction
     */
    private JMenuItem jmiExporter;
    /**
     * Elément de menu Importer une transcription
     * @see FenetrePrinc#jmAction
     */
    private JMenuItem jmiImporterTrans;
    /**
     * Elément de menu sens de Transcription
     * @see FenetrePrinc#jmAction
     */
    private JMenuItem jmiSensTr;
    /**
     * Elément de menu Transcrire
     * @see Nat#fabriqueTranscriptions
     * @see Nat#lanceScenario()
     */
    private JMenuItem jmiTranscrire;
    /**
     * Elément de menu Ouvrir l'éditeur
     * @see FenetrePrinc#jmAction
     * @see GestionnaireOuvrirFenetre
     */
    private JMenuItem jmiOuvrirTrans;
    /**
     * Elément de menu Quitter
     * @see FenetrePrinc#jmAction
     */
    private JMenuItem jmiQuitter;
    /**
     * Elément de menu Aide
     * @see FenetrePrinc#jmAPropos
     */
    private JMenuItem jmiAide;
    /**
     * Elément de menu A propos de NAT
     * @see FenetrePrinc#jmAPropos
     */
    private JMenuItem jmiAPropos;
    /**
     * Elément de menu Restaurer confs système
     * @see FenetrePrinc#jmAction
     */
    private JMenuItem jmiRestoreSystemConfs;
    /**
     * Elément de menu Ouvrir fichier déjà transcrit
     * @see FenetrePrinc#jmAction
     * @see GestionnaireOuvrir
     */
	private JMenuItem jmiOuvrirDejaTrans;
	/**
	 * Elément de menu Signaler un Bug
	 * @see FenetrePrinc#jmAction
	 */
	private JMenuItem jmiSignalBug;
	/**
	 * Elément de menu détranscription
	 * @see FenetrePrinc#jmiTan
	 */
	private JMenuItem jmiTan;

	/** 
	 * nouveaux éléments du menu 
	 */
	
	// document en noir
	//Document en noir, 	Zone d'édition, 	Alt U, 	Ctrl U
	//Document en braille, 	Zone d'édition, 	Alt R, 	Ctrl R
	//Configuration active,	Liste déroulante,	Alt C,	Ctrl C
	/**
	 * Elément de menu saisie adresse du document Noir
	 */
	private JMenuItem jmiDocumentEnNoir;
	/**
	 * Elément de menu saisie adresse du document Braille
	 */
	private JMenuItem jmiDocumentEnBraille;
	/**
	 * Elément de menu choix de la configuration active
	 */
	private JMenuItem jmiConfigurationActive;
	/**
	 * Elément du menu pour maximiser la fenêtre
	 */
	private JMenuItem jmiMaximizeWindow;

	//-------------- Panels -----------

	/** panneau détail de la configuration/configuration details pannel */
    private JPanel panConfigDetails = new JPanel();
	
    //-------------- TextField and TextArea-----------

    /** JTextField pour l'adresse du fichier noir */
    private JTextField  jtfNoir = new JTextField("."+System.getProperty("file.separator")+"licence.txt",30);
    /** JTextField pour l'adresse du fichier braille */
    private JTextField  jtfBraille = new JTextField(System.getProperty("user.home")+System.getProperty("file.separator")+"braille.txt",30);
    /**
     * Afficheur graphique de logs
     * @since 2.0
     */
    private AfficheurJTASwing panneauLog;// = new JTextArea (15,40);
    /** ScrollPane for logs */
    private JScrollPane scrollLog = null;
    
    //---------------- Spinner --------------------------
	/** Spinner indiquant le numéro de l'étape */
    private JSpinner jsStep;

    /*----------------------------------------------------------------------------------------
	 *                                   CONSTRUCTOR
	 *----------------------------------------------------------------------------------------
	 */
    /**
     * Constructeur de FenetrePrinc
     * @param n une instance de Nat
     */
    public FenetrePrinc(Nat n)
    {
    	/* initialisation de propriétés générales aux interfaces graphiques*/
		UIManager.put("FileChooser.lookInLabelMnemonic", new Integer(KeyEvent.VK_R));
	    setIconImage(new ImageIcon("ui/nat.png").getImage());
	    //the component listener must be added AFTER the window is done
	    //otherwise it can't be maximized on start
	    //addComponentListener(this);
	    nat = n;
	    panneauLog = new AfficheurJTASwing(15, 40);
	    panneauLog.setEditable(false);
	    panneauLog.setLineWrap(true);
	    panneauLog.setCaretPosition(0);
	    gestErreur = n.getGestionnaireErreur();
	    gestErreur.addAfficheur(panneauLog);
	    /*gestErreur.addAfficheur(new AfficheurConsole());
	    gestErreur.addAfficheur(new AfficheurLog());
	    gestErreur.setModeDebugage(debug.isSelected());*/
	    fabriqueFenetre();
	    addComponentListener(this);
    	//pour optimisation
	    if(ConfigNat.getCurrentConfig().getOptimize())
	    {
	    	gestErreur.afficheMessage(texts.getText("optimisationmsg"), Nat.LOG_SILENCIEUX);
	    	new NatThread(nat).start();
	    }
	    //vérif si update disponible
	    if(ConfigNat.getCurrentConfig().getUpdateCheck())
	    {
	    	if(nat.checkUpdate())
	    	{
				 if (nat.isUpdateAvailable()==1)
				 {
					 panneauLog.setText("**************************************************\n" +
							 			texts.getText("pannel1") +
							 			texts.getText("pannel2") +
					 					"**************************************************\n");
				 }
				 else if (nat.isUpdateAvailable()==-1)
				 {
					 panneauLog.setText("****************************************************************\n" +
							 			texts.getText("pannel3") +
							 			texts.getText("pannel4") +
					 					"****************************************************************\n");
				 }
				 else
				 {
					 gestErreur.afficheMessage(texts.getText("lastversion"), Nat.LOG_VERBEUX);
				 }
            }
	    }
    }

    /*----------------------------------------------------------------------------------------
	 *                                   GETTERS AND SETTERS
	 *----------------------------------------------------------------------------------------
	 */
    /** @return @link FenetrePrinc#entreeXML}*/
    public JTextField getEntree(){return jtfNoir;}
    
    /** @return @link FenetrePrinc#sortie}*/
    public JTextField getSortie(){return jtfBraille;}
    /** 
     * Change la valeur du texte de {@link FenetrePrinc#jtfNoir}
     * @param entree la nouvelle entrée
     */
    public void setEntree(String entree)
    {
    	jtfNoir.setText(entree);
    	if(ConfigNat.getCurrentConfig().getSortieAuto() && !ConfigNat.getCurrentConfig().isReverseTrans())
    	{
    		setSortieAuto(false);
    	}
    }
 
    /** 
     * Change la valeur du texte de {@link FenetrePrinc#jtfBraille}
     * et active le bouton {@link FenetrePrinc#btEditeur} si l'adresse donnée est valide
     * @param tgt la nouvelle sortie
     */
    public void setSortie(String tgt)
    {
    	jtfBraille.setText(tgt);
    	verifieBtEditeur();
    	if(ConfigNat.getCurrentConfig().getSortieAuto() && ConfigNat.getCurrentConfig().isReverseTrans())
    	{
    		setSortieAuto(true);
    	}
    }
    /** @param e setter for {@link #error} */
    public void setError(boolean e){error = e;}
    /** @return  {@link #error} */
    public boolean getError(){return error;}
    /** @return {@link FenetrePrinc#optionsOuvertes}*/
    public boolean getOptionsOuvertes(){return optionsOuvertes;}
    /** @param oo la valeur de {@link FenetrePrinc#optionsOuvertes}*/
    public void setOptionsOuvertes(boolean oo){optionsOuvertes=oo;}
    /**
     * génère un nom de fichier de sortie automatiquement
     * @param reverse vrai si détranscription (donc renommage fichier noir)
     * et faux si transcription (donc renommage fichier braille)
     */
    public void setSortieAuto(boolean reverse)
    {
    	if(!reverse)
    	{
    		jtfBraille.setText(FileToolKit.nomSortieAuto(jtfNoir.getText(),"txt"));
    	}
    	else
    	{
    		jtfNoir.setText(FileToolKit.nomSortieAuto(jtfBraille.getText(),"xhtml"));
    	}
    }
    /**
     * Renvoie le gestionnaire d'erreur utilisé dnas cette fenêtre 
     * @return le gestionnaire d'erreur utilisé
     */
	public GestionnaireErreur getGestErreur() {return gestErreur;}

	/**
	 * Méthode d'accès à {@link #running}
	 * @param r valeur pour {@link #running}
	 */
	public void setRunning(boolean r) {running = r;}

	/**
	 * Méthode d'accès à {@link #running}
	 * @return true si {@link #running} est vrai
	 */
	public boolean getRunning() {return running;}

	/**
	 * Méthode d'accès à {@link #nat}
	 * @return l'instance de nat utilisée
	 */
	public Nat getNat() {return nat;}

	/**
	 * Méthode d'accès à {@link #btEditeur}
	 * @return le bouton {@link #btEditeur}
	 */
	public JButton getBtEditeur() {return btEditeur;}
	    
    /*----------------------------------------------------------------------------------------
	 *                                   LAYOUT AND GUI
	 *----------------------------------------------------------------------------------------
	 */
	
    /** loads texts */
    public void loadTexts()
    {
    	texts = new Language("FenetrePrinc");
    	
    	lJtfNoir.setText(texts.getText("handwrittenlabel"));
        btSelectNoir.setText(texts.getText("handwrittenbutton"));
        lJtfBraille.setText(texts.getText("braillelabel"));
        btSelectBraille.setText(texts.getText("braillefilebutton"));
        btTranscrire.setText(texts.getText("transcribe"));
        btEditeur.setText(texts.getText("editor"));
        btEditeurTrans.setText(texts.getText("editortrans"));
        lConfig.setText(texts.getText("configlabel"));
        btOption.setText(texts.getText("option"));
        btAide.setText(texts.getText("help"));
        btAPropos.setText(texts.getText("about"));
        btQuitter.setText(texts.getText("quit"));
        btCancel.setText(texts.getText("cancel"));
        //btSignalBug.setText(texts.getText("bug"));
        jmAction.setText(texts.getText("menu"));
        jmAPropos.setText(texts.getText("helpmenu"));
        
        jmiSelectNoir.setText(texts.getText("handwrittenbutton"));
        jmiSelectBraille.setText(texts.getText("braillefilebutton"));
        jmiOptions.setText(texts.getText("option"));
        jmiImporter.setText(texts.getText("importconf"));
        jmiExporter.setText(texts.getText("exportconf"));
        jmiImporterTrans.setText(texts.getText("importtrans"));
        jmiSensTr.setText(texts.getText("reverse"));
        jmiTranscrire.setText(texts.getText("transcribe"));
	    jmiOuvrirTrans.setText(texts.getText("opentranscriptionmenu"));
	    jmiTan.setText(texts.getText("reverseeditor"));
	    jmiOuvrirDejaTrans.setText(texts.getText("editortrans"));
		jmiSignalBug.setText(texts.getText("bug"));
		jmiQuitter.setText(texts.getText("quit"));
	    jmiAide.setText(texts.getText("help"));
	    jmiAPropos.setText(texts.getText("about"));
	    jmiRestoreSystemConfs.setText(texts.getText("restoresystemconfs"));
	    
	    
	    jmiDocumentEnNoir.setText(texts.getText("handwrittenfile"));
	    jmiDocumentEnBraille.setText(texts.getText("braillefile"));
	    jmiConfigurationActive.setText(texts.getText("configlabel"));
	    
	    
	    
	    setName(texts.getText("maintitle"));
	    /* TODO : utilisation de l'accessible name => enlève tous les autres contexts*/
	    //getAccessibleContext().setAccessibleName(texts.getText("maintitlename"));
	    getAccessibleContext().setAccessibleDescription(texts.getText("maintitledesc"));

		jtfNoir.setName(texts.getText("handwrittenfile"));
		
	    Context cJtfNoir = new Context("u","TextField","handwritten",texts);
	    Context cBtSelectNoir = new Context("u","Button","handwrittenbutton",texts);
	    Context cJtfBraille = new Context("r","TextField","braille",texts);
	    Context cBtSelectBraille = new Context("r","TextField","braillefilebutton",texts);
	    //verif button?
	    Context cBtTranscrire = new Context("t","TextField","transcribe",texts);
	    Context cBtEditeur = new Context("l","Button","editor",texts);
	    Context cBtEditeurTrans = new Context("d","Button","editortrans",texts);
	    Context cscrollLog = new Context("","ScrollPane","scrolllog",texts);
	    Context cBtOption = new Context("o","Button","option",texts);
	    Context cBtAide = new Context("F6","Button","help",texts);
	    Context cBtAPropos = new Context("F11","Button","about",texts);
	    //Context cBtSignalBug = new Context("p","Button","bug",texts);
	    Context cBtQuitter = new Context("q","Button","quit",texts);
	    Context cBtSens = new Context("i","Button","reverse",texts);
	    Context cBtShowLog = new Context("","Button","showLog",texts);
	    Context cBtCancel = new Context("F4","Button","cancel",texts);
	    Context cJcbConfig = new Context("c","ComboBox","config",texts);
	    /*Context cJbtPrecStep = new Context("<","Button","precStep",texts);
	    Context cJbtNextStep = new Context(">","Button","nextStep",texts);*/
	    Context cJbtSelectStep = new Context("é","Spinner","selectStep",texts);
	    Context cBtShowConf = new Context("","Button", "showConf", texts);
	    
	    cBtShowConf.treat();
	    cBtShowConf.setContextualHelp(btShowConf,"mainbuttons");
	    cBtSens.treat();
	    cBtSens.setContextualHelp(btSens,"mainbuttons");
	    cBtShowLog.treat();
	    cBtShowLog.setContextualHelp(btShowLog,"mainbuttons");
	    cJtfNoir.treat();
	    cJtfNoir.addLabel(texts.getText("defaultname"), "handwrittenfile");
	    cJtfNoir.setContextualHelp(jtfNoir,"mainfiles");
	    cBtSelectNoir.treat();
	    cBtSelectNoir.setContextualHelp(btSelectNoir,"mainfiles");
	    cJtfBraille.treat();
	    cJtfBraille.setContextualHelp(jtfBraille,"mainfiles");
	    jtfBraille.setName(texts.getText("braillefile"));
	    cJtfBraille.addLabel(texts.getText("defaultname"), "braillefile");
	    cBtSelectBraille.treat();
	    cBtSelectBraille.setContextualHelp(btSelectBraille,"mainfiles");
	    btSens.getAccessibleContext().setAccessibleName(texts.getText("reversename"));
	    chSens.setParameters(btSens,"mainfiles");
	    chSens.setHelpName(btSens.getToolTipText());
	    String[] chSensKeys = {"dir1ttt","dir1name","dir1iconettt",
	    		"dir2ttt","dir2name","dir2iconettt"};
	    String[] chSensLabels = { texts.getText("natortantooltip")+" (NAT)",
	    		texts.getText("natortanaccessname")+" (NAT)",
	    		texts.getText("natortanicon")+" NAT",
	    		texts.getText("natortantooltip")+" (TAN)",
	    		texts.getText("natortanaccessname")+" (TAN)",
	    		texts.getText("natortanicon")+" TAN"};
	    chSens.addLabels(chSensLabels, chSensKeys);
	    chSens.addContext(cBtSelectBraille, false);
	    setReverseTrans(ConfigNat.getCurrentConfig().isReverseTrans());
	    cBtTranscrire.treat();
	    String[] cBtTranscrireKeys = {"dir1buttonname","dir2buttonname"};
	    String[] cBtTranscrireLabels = {"Nom du bouton : sens NAT",
	    								"Nom du bouton : sens TAN"};
	    cBtTranscrire.addLabels(cBtTranscrireLabels, cBtTranscrireKeys);
	    cBtTranscrire.setContextualHelp(btTranscrire,"mainstartbutton");
		cBtEditeur.treat();
		cBtEditeur.setContextualHelp(btEditeur,"mainopen");
	    cBtEditeurTrans.treat();
	    cBtEditeurTrans.setContextualHelp(btEditeurTrans,"mainopenother");
	    cscrollLog.treat();
	    cscrollLog.setContextualHelp(panneauLog,"mainpane");
	    cBtOption.treat();
	    cBtOption.setContextualHelp(btOption,"guiOptions");
	    cBtAide.treat();
	    cBtAide.setContextualHelp(btAide,"FAQ");
	    cBtAPropos.treat();
	    cBtAPropos.setContextualHelp(btAPropos,"credits");
	    /*cBtSignalBug.treat();
	    cBtSignalBug.setContextualHelp(btSignalBug,"limites");*/
	    cBtQuitter.treat();
	    cBtQuitter.setContextualHelp(btQuitter,"mainbuttons");
	    cBtCancel.treat();
	    cBtCancel.setContextualHelp(btCancel,"mainbuttons");
		cJcbConfig.treat();
		cJcbConfig.setContextualHelp(jcbConfig,"mainconfigs");
		/*cJbtPrecStep.treat();
		cJbtPrecStep.setContextualHelp(jbtPrecStep, "mainconfigs");
		cJbtNextStep.treat();
		cJbtNextStep.setContextualHelp(jbtNextStep, "mainconfigs");*/
		cJbtSelectStep.treat();
		cJbtSelectStep.setContextualHelp(jsStep, "mainconfigs");
		
		
	    jcbConfig.setRenderer(new ConfigurationsComboBoxRenderer());
        
    }
    
    /** Fabrique la fenêtre {@link FenetrePrinc} */
    private void fabriqueFenetre()
    {
	    // titre de la fenêtre 
	    setTitle("NatBraille " + nat.getVersionLong());
	    /* -----------------------------------------------------------------------
	     * panneaux principaux/ main panels
	     */
	    JPanel panFiles = new JPanel();
	    JPanel panConfig = new JPanel();
	    panFiles.setLayout(new GridBagLayout());
	    panConfig.setLayout(new GridBagLayout());
	    panConfigDetails.setLayout(new GridBagLayout());
	    //look and feel
	    panFiles.setBackground(PAN_COLOR);
	    panConfig.setBackground(PAN_COLOR);
	    panConfigDetails.setBackground(PAN_COLOR);
	    panFiles.setBorder(BorderFactory.createLineBorder(Color.white,5));
	    panConfig.setBorder(BorderFactory.createLineBorder(Color.white,5));
	    panConfigDetails.setBorder(BorderFactory.createLineBorder(Color.white,5));
	    panConfigDetails.setVisible(false);
	    
	    /* -------------------------------------------
	     * Conf detail panels
	     */
	    JPanel panConfMode = new JPanel();
	    JPanel panConfLayout = new JPanel();
	    JPanel panConfFormat = new JPanel();
	    //look and feel
	    panConfMode.setBackground(PAN_CONF_COLOR);
	    panConfMode.setBorder(BorderFactory.createLineBorder(Color.black));	    
	    panConfLayout.setBackground(PAN_CONF_COLOR);
	    panConfLayout.setBorder(BorderFactory.createLineBorder(Color.black));
	    panConfFormat.setBackground(PAN_CONF_COLOR);
	    panConfFormat.setBorder(BorderFactory.createLineBorder(Color.black));
	    
	    
	    /* ---------------------------------------------------------------------
	     * Menus
	     */
	    
	    

	    jmiSelectNoir= new JMenuItem(texts.getText("handwrittenbutton"));
	    jmiSelectNoir.setMnemonic('e');
	    jmiSelectNoir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiSelectNoir.addActionListener(new GestionnaireOuvrir(this,GestionnaireOuvrir.OUVRIR_SOURCE));
	    jmAction.add(jmiSelectNoir);
		
	    jmiSelectBraille= new JMenuItem(texts.getText("braillefilebutton"));
	    jmiSelectBraille.setMnemonic('s');
	    jmiSelectBraille.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiSelectBraille.addActionListener(new GestionnaireOuvrir(this,GestionnaireOuvrir.OUVRIR_SORTIE));
	    jmAction.add(jmiSelectBraille);
		
	    jmAction.addSeparator();
		
	    jmiOptions= new JMenuItem(texts.getText("option"));
	    jmiOptions.setMnemonic('o');
	    jmiOptions.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiOptions.addActionListener(new GestionnaireOuvrirFenetre(GestionnaireOuvrirFenetre.OUVRIR_OPTIONS,this));
	    jmAction.add(jmiOptions);
	    
	    jmiImporter= new JMenuItem("Importer...");
	    jmiImporter.setMnemonic('m');
	    jmiImporter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiImporter.addActionListener(this);
	    jmAction.add(jmiImporter);
	    
	    jmiExporter= new JMenuItem("Exporter...");
	    jmiExporter.setMnemonic('x');
	    jmiExporter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiExporter.addActionListener(this);
	    jmAction.add(jmiExporter);
	    
	    jmiImporterTrans= new JMenuItem(texts.getText("importtrans"));
	    //jmiImporterTrans.setMnemonic('x');
	    //jmiImporterTrans.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiImporterTrans.addActionListener(this);
	    jmAction.add(jmiImporterTrans);
	    
	    jmAction.addSeparator();
		
	    jmiSensTr = new JMenuItem(texts.getText("reverse"));
	    jmiSensTr.setMnemonic('i');
	    jmiSensTr.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiSensTr.addActionListener(this);
	    jmAction.add(jmiSensTr);
	    
	    jmiTranscrire = new JMenuItem(texts.getText("transcribe"));
	    jmiTranscrire.setMnemonic('t');
	    jmiTranscrire.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiTranscrire.addActionListener(this);
	    jmAction.add(jmiTranscrire);
		
	    jmAction.addSeparator();
		
	    jmiOuvrirTrans = new JMenuItem(texts.getText("opentranscriptionmenu"));
	    jmiOuvrirTrans.setMnemonic('l');
	    jmiOuvrirTrans.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiOuvrirTrans.addActionListener(this);
	    jmAction.add(jmiOuvrirTrans);
	    
	    jmiTan = new JMenuItem(texts.getText("reverseeditor"));
	    jmiTan.setMnemonic('n');
	    jmiTan.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiTan.addActionListener(this);
	    jmAction.add(jmiTan);
		
		//jmiOuvrirMEP = new JMenuItem("Ouvrir un fichier à mettre en page");
		//jmiOuvrirMEP.setMnemonic('m');
		//jmiOuvrirMEP.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		//jmiOuvrirMEP.addActionListener(new GestionnaireOuvrir(this,GestionnaireOuvrir.OUVRIR_MEP));
		//jmAction.add(jmiOuvrirMEP);
		
		jmiOuvrirDejaTrans = new JMenuItem(texts.getText("editortrans"));
		jmiOuvrirDejaTrans.setMnemonic('d');
		jmiOuvrirDejaTrans.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiOuvrirDejaTrans.addActionListener(new GestionnaireOuvrir(this,GestionnaireOuvrir.OUVRIR_TRANS));
		jmAction.add(jmiOuvrirDejaTrans);
		
		jmiSignalBug = new JMenuItem(texts.getText("bug"));
		jmiSignalBug.setMnemonic('p');
		jmiSignalBug.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiSignalBug.addActionListener(this);
		jmAction.add(jmiSignalBug);
		
		jmAction.addSeparator();
			

	    jmiDocumentEnNoir = new JMenuItem(texts.getText("handwrittenfile"));
	    jmiDocumentEnNoir.setMnemonic('u');
		jmiDocumentEnNoir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiDocumentEnNoir.addActionListener(this);
	    jmAction.add(jmiDocumentEnNoir);

	    jmiDocumentEnBraille = new JMenuItem(texts.getText("braillefile"));
	    jmiDocumentEnBraille.setMnemonic('r');
		jmiDocumentEnBraille.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiDocumentEnBraille.addActionListener(this);
		jmAction.add(jmiDocumentEnBraille);

	    jmiConfigurationActive = new JMenuItem(texts.getText("activeconfig"));
	    jmiConfigurationActive.setMnemonic('g');
		jmiConfigurationActive.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiConfigurationActive.addActionListener(this);
	    jmAction.add(jmiConfigurationActive);
		
	    
		jmAction.addSeparator();
		
	    jmiQuitter = new JMenuItem(texts.getText("quit"));
	    jmiQuitter.setMnemonic('q');
	    jmiQuitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiQuitter.addActionListener(this);
	    jmAction.add(jmiQuitter);
		
	    jmiAide = new JMenuItem(texts.getText("help"));
	    jmiAide.setMnemonic(KeyEvent.VK_F6);
	    jmiAide.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F6, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiAide.addActionListener(new GestionnaireOuvrirFenetre(GestionnaireOuvrirFenetre.OUVRIR_AIDE,this));
	    jmAPropos.add(jmiAide);
		
	    jmAPropos.addSeparator();
		
	    jmiAPropos = new JMenuItem(texts.getText("about"));
	    //jmiAPropos.setMnemonic(java.awt.event.KeyEvent.VK_F11);
	    jmiAPropos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F11, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiAPropos.addActionListener(new GestionnaireOuvrirFenetre(GestionnaireOuvrirFenetre.OUVRIR_APROPOS,this));
	    jmAPropos.add(jmiAPropos);
		
	    jmAction.setMnemonic('m');
	    jmAPropos.setMnemonic('a');
		
	    jmiMaximizeWindow = new JMenuItem(texts.getText("maxwin"));
	    jmiMaximizeWindow.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F11,0,false));
	    jmiMaximizeWindow.addActionListener(this);
	    jmAPropos.add(jmiMaximizeWindow);
		
	    
	    jmiRestoreSystemConfs = new JMenuItem(texts.getText("restoresystemconfslabel"));
	    jmiRestoreSystemConfs.addActionListener(this);
	    jmAction.add(jmiRestoreSystemConfs);

	    jmb.add(jmAction);
	    jmb.add(jmAPropos);
		
	    setName(texts.getText("maintitle"));
	    /* TODO : utilisation de l'accessible name => enlève tous les autres contexts*/
	    //getAccessibleContext().setAccessibleName(texts.getText("maintitlename"));
	    getAccessibleContext().setAccessibleDescription(texts.getText("maintitledesc"));
	    
	    
	    lJtfNoir.setLabelFor(jtfNoir);
	    if(ConfigNat.getCurrentConfig().getFichNoir().length()>0)
	    {
		    jtfNoir.setText(ConfigNat.getCurrentConfig().getFichNoir());
	    }
	    lJtfNoir.setDisplayedMnemonic('u');
	    jtfNoir.setName(texts.getText("handwrittenfile"));
	    //entreeXML.getAccessibleContext().setAccessibleDescription(entreeXML.getToolTipText());
	    jtfNoir.addFocusListener(this);
	    
	    btSelectNoir.addActionListener(new GestionnaireOuvrir(this,GestionnaireOuvrir.OUVRIR_SOURCE));
	    btSelectNoir.setMnemonic('e');

	    // Ajout d'une fonction "refresh" en appuyant sur F5
	    Action refreshAction = new AbstractAction() 
		{private static final long serialVersionUID = 1L;
			@Override
            public void actionPerformed(ActionEvent e){loadTexts();}};
		
		InputMap im = btSelectNoir.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = btSelectNoir.getActionMap();
		am.put("refreshAction", refreshAction);
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5,0,true),"refreshAction");
		
		// Ajout d'une fonction "arrêt" en appuyant sur ctrl+c
		Action stopAction = new AbstractAction() 
			{private static final long serialVersionUID = 1L;
				@Override
                public void actionPerformed(ActionEvent e){stopTrans();}};
		
		am.put("stopAction", stopAction);
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_C,InputEvent.SHIFT_MASK+InputEvent.CTRL_MASK,true),"stopAction");
	    // fichier de sortie : label, champ, bouton
	    lJtfBraille.setLabelFor(jtfBraille);
	    lJtfBraille.setDisplayedMnemonic('r');
	    if(ConfigNat.getCurrentConfig().getFBraille().length()>0)
	    {
		    jtfBraille.setText(ConfigNat.getCurrentConfig().getFBraille());
	    }
	    jtfBraille.setName(texts.getText("braillefile"));
	    jtfBraille.addFocusListener(this);
	    
	    btSelectBraille.addActionListener(new GestionnaireOuvrir(this,GestionnaireOuvrir.OUVRIR_SORTIE));
	    btSelectBraille.setMnemonic('s');
	     
	    btSens.addActionListener(this);
	    btSens.setMnemonic('i');
	    
	    btShowLog.addActionListener(this);
	    btShowConf.addActionListener(this);
		btShowConf.setEnabled(false);
	    
	    // bouton pour lancer la transcription
	    btTranscrire.addActionListener(this);
	    btTranscrire.setMnemonic('t');
	    
	    //les étapes
	    /*jbtNextStep.addActionListener(this);
	    jbtPrecStep.addActionListener(this);*/
	    
	    jsStep = new JSpinner(new SpinnerNumberModel(1, 1, 1, 1));
	    jsStep.addChangeListener(this);
	    /*
	      jpb.getAccessibleContext().setAccessibleName("Barre de progression de la transcription");
	      jpb.getAccessibleContext().setAccessibleDescription("Barre de progression indiquant l'état d'avancement de la transcription");
	    */
	    /* c'est quoi ça? 
	     * Bruno: c'est pour cacher / montrer la fenetre de log. C'est pas implémenté: je commente
	 * ********** 
	    voir_log.addActionListener(this);
	    voir_log.setMnemonic('a');
	    voir_log.getAccessibleContext().setAccessibleName("Bouton transcrire2");
	    voir_log.getAccessibleContext().setAccessibleDescription("Valider pou2r lancer la transcription");
	    voir_log.setToolTipText("Lancer la transcri2ption");
	    /* ***********/
	    btEditeur.addActionListener(this);
	    btEditeur.setMnemonic('l');
	    verifieBtEditeur();
		
	    /*
		btEditeurMep.addActionListener(new GestionnaireOuvrir(this,GestionnaireOuvrir.OUVRIR_MEP));
		//btEditeurMep.setMnemonic('m');
		//btEditeurMep.getAccessibleContext().setAccessibleName("Ouvrir un fichier à mettre en page");
		//btEditeurMep.getAccessibleContext().setAccessibleDescription("Pour ouvrir un fichier à mettre en page");
		btEditeurMep.setToolTipText("Pour ouvrir un fichier à mettre en page (Alt+m) ou (Ctrl+m)");
		btEditeurMep.setEnabled(false);*/
		
		
		btEditeurTrans.addActionListener(new GestionnaireOuvrir(this,GestionnaireOuvrir.OUVRIR_TRANS));
		btEditeurTrans.setMnemonic('d');
	    verifieBtEditeur();
	
		// TODO : ajouter des messages au scrollpane
	    scrollLog = new JScrollPane (panneauLog);
	    scrollLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	    scrollLog.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    
	    // bouton pour la fenêtre d'options
	    btOption.addActionListener(new GestionnaireOuvrirFenetre(GestionnaireOuvrirFenetre.OUVRIR_OPTIONS,this));
	    btOption.setMnemonic('O');
	    
	    // saving configurations
	    btSaveConf.addActionListener(this);
	    btSaveConfAs.addActionListener(this);
	    
	    // bouton pour la fenêtre d'aide
	    btAide.addActionListener(new GestionnaireOuvrirFenetre(GestionnaireOuvrirFenetre.OUVRIR_AIDE,this));
	    btAide.setMnemonic(java.awt.event.KeyEvent.VK_F6);
	    
	    // bouton pour la fenêtre a propos
	    btAPropos.addActionListener(new GestionnaireOuvrirFenetre(GestionnaireOuvrirFenetre.OUVRIR_APROPOS,this));
	    btAPropos.setMnemonic(java.awt.event.KeyEvent.VK_F11);
	    
	    /* bouton pour la fenêtre de rapport de bug
	    btSignalBug.addActionListener(this);
	    btSignalBug.setMnemonic('p');*/
	   
	    // bouton pour quitter
	    btQuitter.addActionListener(this);
	    btQuitter.setMnemonic('q');
	    	
	    // label de config
	    lConfig.setLabelFor(jcbConfig);
	    lConfig.setDisplayedMnemonic('g');
	    chargeConfigurations();//jcbConfig);
	    //		jcbConfig.addActionListener(this);
	
	    // combo de choix de la config
	    jcbConfig.setEditable(false);
	    jcbConfig.addPopupMenuListener(this);
	    //	jcbConfig.addActionListener(this);
	    
	    //désactivation des composants liés au fichier cible si option automatique activée
	    /*if(ConfigNat.getCurrentConfig().getSortieAuto())
	    {
	    	setSortieAuto(ConfigNat.getCurrentConfig().isReverseTrans());
	    }*/

	    this.loadTexts();
	    
	    setLayout(new GridBagLayout());
	    
	    GridBagConstraints c = new GridBagConstraints();
	    //menu
	    setJMenuBar(jmb);
	    
	    /* -------------------------------------------------------------------------------------
	     * Panneau fichiers/ panel files
	     */
	    c.insets = new Insets(10,10,3,3);
	    c.gridx = 0;
	    c.gridy = 1;
	    c.gridwidth = 1;
	    
	    c.anchor = GridBagConstraints.CENTER;
	    panFiles.add(new JLabel(texts.getText("currentDir")),c);
	    
	    c.insets = new Insets(10,3,3,3);
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.gridx++;
	    c.anchor = GridBagConstraints.LINE_START;
	    panFiles.add(lSens,c);
	    c.insets = new Insets(10,3,3,10);
	    c.gridx++;
	    panFiles.add(btSens,c);
	    
	    // l 1
	    c.insets = new Insets(10,10,0,3);
	    c.anchor = GridBagConstraints.CENTER;
	    c.gridx=0;
	    c.gridy++;
	    panFiles.add(lJtfNoir,c);
	    
	    c.insets = new Insets(10,3,0,3);
	    c.gridx = 1;
	    c.weightx = 1.0;
	    panFiles.add(jtfNoir,c);
		
	    c.insets = new Insets(10,3,0,10);
	    c.gridx = 2;
	    c.weightx = 0.0;
	    panFiles.add(btSelectNoir,c);
	
	    c.insets = new Insets(0,0,0,0);
	    // l 1-2
	    c.gridx = 0;
	    c.gridy++;
	    c.insets = new Insets(0,3,0,3);
	    c.fill = GridBagConstraints.NONE;
	    panFiles.add(lSensIcon,c);
	    
	    // l 2
	    c.insets = new Insets(0,10,10,3);
	    c.anchor = GridBagConstraints.LINE_END;
	    c.fill = GridBagConstraints.NONE;
	    c.gridx = 0;
	    c.gridy++;
	    panFiles.add(lJtfBraille,c);
	    
	    c.insets = new Insets(0,3,10,3);
	    c.anchor = GridBagConstraints.CENTER;
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.gridx = 1;
	    panFiles.add(jtfBraille,c);
	    
	    c.insets = new Insets(0,3,10,10);
	    c.gridx = 2;
	    panFiles.add(btSelectBraille,c);
		
		/* -------------------------------------------------------------------------------------
	     * Panneau configuration/ configuration panel
	     */
		c.insets = new Insets(3,10,3,3);
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 2;
		panConfig.add(lIcone,c);
		
		c.anchor = GridBagConstraints.LINE_END;
		c.gridheight = 1;
	    c.fill = GridBagConstraints.NONE;
	    c.gridx++;
	    panConfig.add(lConfig,c);
	    
		c.gridx++;
		panConfig.add(jcbConfig,c);
		c.gridx++;
		panConfig.add(jsStep,c);
		
	    c.gridy++;
	    panConfig.add(btOption,c);	
	    
	    //c.gridx--; on le met pas encore ce bouton
	    //panConfig.add(btShowConf,c);
	    
	    /*---------------------------------------------------
	     * Config detail panel
	     */
	    panConfMode.setLayout(new GridBagLayout());
	    c.gridx=0;
	    c.gridy=0;
	    c.anchor=GridBagConstraints.CENTER;
	    c.fill=GridBagConstraints.HORIZONTAL;
	    panConfMode.add(new JLabel(texts.getText("mode")),c);
	    c.gridy++;
	    jcbConfMode = new JComboBox<ConfigurationsListItem>();
	    panConfMode.add(jcbConfMode,c);
	    c.gridy++;
	    panConfMode.add(new JButton(texts.getText("editMode"),new ImageIcon("ui/icon/document-properties.png")),c);
	    
	    panConfLayout.setLayout(new GridBagLayout());
	    c.gridx=0;
	    c.gridy=0;
	    panConfLayout.add(new JLabel(texts.getText("layout")),c);
	    c.gridy++;
	    jcbConfLayout = new JComboBox<ConfigurationsListItem>();
	    panConfLayout.add(jcbConfLayout,c);
	    c.gridy++;
	    panConfLayout.add(new JButton(texts.getText("editLayout"),new ImageIcon("ui/icon/document-properties.png")),c);
	    
	    panConfFormat.setLayout(new GridBagLayout());
	    c.gridx=0;
	    c.gridy=0;
	    jcbConfFormats = new JComboBox<ConfigurationsListItem>();
	    panConfFormat.add(new JLabel(texts.getText("formats")),c);
	    c.gridy++;
	    panConfFormat.add(jcbConfFormats,c);
	    c.gridy++;
	    panConfFormat.add(new JButton(texts.getText("editFormats"),new ImageIcon("ui/icon/document-properties.png")),c);
	    
	    panConfigDetails.setLayout(new GridBagLayout());
	    c.gridx=0;
	    c.gridy=0;
	    c.gridwidth=3;
	    panConfigDetails.add(new JLabel(texts.getText("configInfo")),c);
	    
	    c.gridy++;
	    c.gridwidth=1;
	    c.weightx=0.33;
	    panConfigDetails.add(panConfMode,c);
	    
	    c.gridx++;
	    panConfigDetails.add(panConfLayout,c);
	    c.gridx++;
	    panConfigDetails.add(panConfFormat,c);
	    c.weightx=0.0;
	    //TODO actions
	    c.gridy++;
	    c.gridx=1;
	    c.gridwidth=2;
		c.insets = new Insets(13,10,3,10);
		JPanel btConf = new JPanel(new GridBagLayout());
		panConfigDetails.add(btConf,c);

		c.gridwidth=1;
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(3,3,3,30);
	    btConf.add(btSaveConf,c);
	    
	    c.anchor = GridBagConstraints.EAST;
	    c.gridx++;
	    btConf.add(btSaveConfAs,c);
	    
	    /*// l 7 (boutons du bas)
	    JPanel pBoutons = new JPanel(new GridBagLayout());
	    
	    c.gridx=0;
	    c.anchor = GridBagConstraints.WEST;
	    c.gridy++;
	    c.weightx = 0.0;
	    c.weighty = 0.0;
	    c.gridwidth = 3;
	    c.insets = new Insets(3,3,3,3);
	    add(pBoutons,c);	    
	    
	    c.anchor = GridBagConstraints.LINE_START;
	    c.gridwidth = 1;
	    c.weightx = 1.0;
	    c.weighty = 1.0;
	    c.gridx=0;
	    c.ipadx=0;
	    c.insets = new Insets(3,20,3,20);
	    pBoutons.add(btAide,c);
	    
	    c.gridx++;
	    pBoutons.add(btAPropos,c);
	    c.gridx++;
	    c.weightx = 2.0;
	    c.weighty = 2.0;
	    pBoutons.add(btQuitter,c);*/
	    
	    /*-----------------------------------------------------------------------
	     * Mise en page générale/general layout
	     */
	    c.gridx=0;
	    c.gridy=0;
	    c.gridwidth=3;
	    c.anchor=GridBagConstraints.CENTER;
	    c.fill=GridBagConstraints.NONE;
	    c.insets = new Insets(10,10,10,10);
	    add(new JLabel(texts.getText("titleTrans")),c);
	    
	    c.fill=GridBagConstraints.HORIZONTAL;
	    c.gridy++;
	    c.gridx=0;
		add(panFiles,c);
		
		c.gridy++;
		add(panConfig,c);
		
		c.insets = new Insets(3,10,3,10);
		c.gridy++;
		add(panConfigDetails,c);
		
		c.fill=GridBagConstraints.NONE;
		c.anchor=GridBagConstraints.CENTER;
		c.insets = new Insets(13,10,3,10);
		c.gridy++;
		c.gridx=1;
		JPanel panBtAction = new JPanel(new GridBagLayout());
		add(panBtAction,c);
		c.gridwidth=1;
		c.gridx=0;
		c.insets=new Insets(0,0,0,30);
		panBtAction.add(btTranscrire,c);
		c.gridx++;
		c.insets=new Insets(0,30,0,0);
		panBtAction.add(btCancel,c);
		
	    JPanel panneauAffichage = new JPanel();
	    panneauAffichage.add(scrollLog);
	    //nom et description devraient appartenir au scrollPane scrollLog ?
	    /* TODO : idem, context non utilisé pour le scrollpane */
	    panneauAffichage.getAccessibleContext().setAccessibleName("Panneau d'affichage");
	    panneauAffichage.getAccessibleContext().setAccessibleDescription("Panneau d'affichage dynamique des traitements (logs)");
	    
	    c.fill = GridBagConstraints.BOTH;
	    c.gridx = 0;
	    c.gridy++;
	    c.gridwidth = 3;
	    c.weighty =1.0;
	    c.insets = new Insets(3,10,3,10);
	    add(scrollLog,c);
	    
	    c.fill=GridBagConstraints.NONE;
		c.insets = new Insets(3,10,3,10);
		c.gridy++;
		c.gridx=1;
		JPanel panBtAction2 = new JPanel(new GridBagLayout());
		add(panBtAction2,c);
		c.gridwidth=1;
		c.gridx=0;
		c.insets=new Insets(0,0,0,30);
		panBtAction2.add(btEditeur,c);
		c.gridx++;
		c.insets=new Insets(0,30,0,0);
		panBtAction2.add(btQuitter,c);
		
		pack();
		//taille esthétique des boutons de commande
		//max
		int max = btTranscrire.getSize().width;
		max = Math.max(max, btCancel.getSize().width);
		max = Math.max(max, btQuitter.getSize().width);
		max = Math.max(max, btEditeur.getSize().width);
		btTranscrire.setPreferredSize(new Dimension(max,btTranscrire.getHeight()));
		btCancel.setPreferredSize(new Dimension(max,btCancel.getHeight()));
		btQuitter.setPreferredSize(new Dimension(max,btQuitter.getHeight()));
		btEditeur.setPreferredSize(new Dimension(max,btEditeur.getHeight()));
		pack();
		
	    //c'est la méthode de windowslistener qui va gérer la fermeture
	    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	    addWindowListener(this);
	    //position et taille de la fenêtre
	    if(ConfigNat.getCurrentConfig().getMemoriserFenetre())
	    {
	    	int x= ConfigNat.getCurrentConfig().getWidthPrincipal();
	    	int y=ConfigNat.getCurrentConfig().getHeightPrincipal();
	    	if(x+y != 0){setPreferredSize(new Dimension(x,y));}
	    }
	    if(ConfigNat.getCurrentConfig().getCentrerFenetre())
        {
		    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		    Dimension size = this.getPreferredSize();
		    screenSize.height = screenSize.height/2;
		    screenSize.width = screenSize.width/2;
		    size.height = size.height/2;
		    size.width = size.width/2;
		    int y = screenSize.height - size.height;
		    int x = screenSize.width - size.width;
		    setLocation(x, y);
        }
	    if (ConfigNat.getCurrentConfig().getMaximizedPrincipal())
	    {
		    setExtendedState(Frame.MAXIMIZED_BOTH);
		}
    }
	
    /** Ajoute les configurations des répertoires "configurations" et $temp/filters dans {@link FenetrePrinc#jcbConfig} 
     * et sélectionne la configuration active.
     */
    public void chargeConfigurations()//JComboBox jcb)
    {
    	//jcbConfig.removePopupMenuListener(this);         
        jcbConfig.removeAllItems();                   

	//        File repertoireSysteme = new File(ConfigNat.getSystemConfigFilterFolder());
        File repertoireUser   = new File(ConfigNat.getUserConfigFilterFolder());   
        ArrayList<File> both = new ArrayList<File>();                        

	//        try {Collections.addAll(both,repertoireSysteme.listFiles());}
	//        catch (NullPointerException npe){npe.printStackTrace();}                                                          

        try {Collections.addAll(both, repertoireUser.listFiles());}
        catch (NullPointerException npe){npe.printStackTrace();}                                                        
	
	File[] listConfs = both.toArray(new File[] {});
	ArrayList<ConfigurationsListItem> lesConfs=  new ArrayList<ConfigurationsListItem>();
	ConfigurationsListItem selectedConfig = null;
	for(int i=0;i<listConfs.length;i++)
	    {
		try
		    {
			ConfigurationsListItem cli = new ConfigurationsListItem(listConfs[i].getCanonicalPath(), gestErreur);

			//System.out.println(listConfs[i].getCanonicalPath());

			if (cli.getIsValid())
			    {
				lesConfs.add(cli);
				if(cli.getFilename().equals(ConfigNat.getCurrentConfig().getFichierConf()))
				    {
					selectedConfig = cli;
				    }					
			    }
		    }
		catch (IOException ieo){ieo.printStackTrace();}
	    }
	Collections.sort(lesConfs);
	for(ConfigurationsListItem c:lesConfs){jcbConfig.addItem(c);}
	//jcbConfig.addActionListener(this); ya pu
	//jcbConfig.addPopupMenuListener(this); fait à la construction
	if (selectedConfig != null){jcbConfig.setSelectedItem(selectedConfig);}
	checkStepSpinner();
    }

    /**
     * Active ou désactive {@link #jsStep}
     * en fonction de la configuration sélectionnée dans {@link #jcbConfig}
     */
    private void checkStepSpinner()
    {
    	ConfigurationsListItem cli = (ConfigurationsListItem)jcbConfig.getSelectedItem();
    	/*jbtPrecStep.setEnabled(false);
    	jbtNextStep.setEnabled(false);*/
    	jsStep.setEnabled(false);
	try {
	    if(cli.isStep())
		{
		    /*jbtPrecStep.setEnabled(!(cli.getStep()==1));
		      jbtNextStep.setEnabled(!cli.isLastStep());*/
		    jsStep.setEnabled(true);
		    //jsStep.setValue(cli.getStep());
		    jsStep.setModel(new SpinnerNumberModel(cli.getStep(), 1, cli.getScenSize(), 1));
		}
	} catch (NullPointerException e) {
	    System.out.println("houston!");
	    
	}
    }

    /**
     * Change les composants graphiques en fonction de <code>reverse</code>
     * <code>reverse</code> est vrai si il faut passer en mode détranscription et 
     * faux q'il faut passer en mode transcription.
     * Appelle également {@link ConfigNat#setReverseTrans(boolean)}
     * @param reverse true si passer en mode détranscription 
     * 
     */
    private void setReverseTrans(boolean reverse)
    {
    	if(!reverse)
    	{
    		btTranscrire.setText(texts.getText("dir1buttonname"));
    		lSensIcon.setIcon(new ImageIcon("ui/icon/go-down.png"));
    		//btSens.setIcon(new ImageIcon("ui/icon/go-down.png"));
    	    btSens.getAccessibleContext().setAccessibleName(texts.getText("dir1ttt"));
    		lSens.setText(texts.getText("dir1"));
		    btEditeur.setText(texts.getText("dir1edit"));
		    btEditeurTrans.setText(texts.getText("dir1edittrans"));
		    btEditeurTrans.setToolTipText(texts.getText("dir1edittransttt"));
		    lIcone.setIcon(new ImageIcon("ui/nat.png"));
		    lIcone.setToolTipText(texts.getText("dir1iconettt"));
		    ConfigNat.getCurrentConfig().setReverseTrans(false);
		    //setSortieAuto(false); 
    	}
    	else
    	{
    		btTranscrire.setText(texts.getText("dir2buttonname"));
    		lSensIcon.setIcon(new ImageIcon("ui/icon/go-up.png"));
    		//btSens.setIcon(new ImageIcon("ui/icon/go-up.png"));
    	    btSens.getAccessibleContext().setAccessibleName(texts.getText("dir2ttt"));
    		lSens.setText(texts.getText("dir2"));
		    btEditeur.setText(texts.getText("dir2edit"));
		    btEditeurTrans.setText(texts.getText("dir2edittrans"));
		    btEditeurTrans.setToolTipText(texts.getText("dir2edittransttt"));
		    //btEditeurMep.setEnabled(false);
		    lIcone.setIcon(new ImageIcon("ui/tan.png"));
		    lIcone.setToolTipText(texts.getText("dir2iconettt"));
		    ConfigNat.getCurrentConfig().setReverseTrans(true);
		    setSortieAuto(true);
    	}
    	chSens.setHelpName(btSens.getToolTipText());
    }

	/** Vérifie si l'adresse contenu dans {@link #jtfBraille} est valide, et si c'est le cas dégrise {@link #btEditeur}*/ 
    private void verifieBtEditeur()
    {
    	String fich = jtfBraille.getText();
	    if(new File(fich).exists())
	    {
	    	btEditeur.setEnabled(true);
	    	ConfigNat.getCurrentConfig().setFBraille(fich);
	    }
	    else{btEditeur.setEnabled(false);}
    }
    
    /**
	 * Active ou désactive les composants liés à la transcription
	 * @param b true si activation, false sinon
	 */
	public void activeTrans(boolean b)
	{
		btTranscrire.setEnabled(b);
		jmiTranscrire.setEnabled(b);
	}
    
	/*----------------------------------------------------------------------------------------
	 *                                   THREADS
	 *----------------------------------------------------------------------------------------
	 */
	
	/**
	 * Classe interne de {@link FenetrePrinc} permettant de jouer un son à intervalle régulier pendant la transcription
	 */
    public class ThreadJPB extends Thread
    {
	    /**
	     * Joue un son toutes les 5 secondes et le son de fin quand running=false
	     * en fonction des options de l'interface graphique
	     */
	    @Override
		public void run() 
	    {
	        setRunning(true);
	        AudioClip ac;
	        int cycles = 0;
	        try 
		    {
		        File curDir = new File("");
		        ac = Applet.newAudioClip(new URL("file:"+ curDir.getAbsolutePath()+"/ui/sounds/tic.au"));
		        while(getRunning())
			    {
			        if(cycles%5==0 && ConfigNat.getCurrentConfig().getSonPendantTranscription()){ac.play();}
			        try 
				    {
				        sleep(1000);
				        cycles++;
				    } catch (InterruptedException ie) {ie.printStackTrace();}
					
			    }
		        if(ConfigNat.getCurrentConfig().getSonFinTranscription())
			    {
			        if(getGestErreur().getException()==null )
				    {
				        ac = Applet.newAudioClip(new URL("file:"+ curDir.getAbsolutePath()+"/ui/sounds/fin.au"));
				    }
			        else
				    {
				        ac = Applet.newAudioClip(new URL("file:"+ curDir.getAbsolutePath()+"/ui/sounds/erreur.au"));
				    }
			        ac.play();
			    }
		        if(!getError() && ConfigNat.getCurrentConfig().getOuvrirEditeur()){ouvrirEditeur();}
		    } catch (MalformedURLException mue) 
		    {
		        mue.printStackTrace();
		    }
			
	    }
    }
    /**
	 * Classe interne de {@link FenetrePrinc} permettant de jouer un son à intervalle régulier pendant la transcription
	 */
    public class ThreadTrans extends Thread
    {
	    /**
	     * Lance la transcription en arrière plan
	     */
	    @Override
		public void run() 
	    {
	    	activeTrans(false);
	    	setRunning(true);
	    	
	    	setError(!getNat().lanceScenario());
	    	
		    if (getGestErreur().getException() == null )
		    {
			    /*//mettre en place un gestionnaire
			    if (ConfigNat.getCurrentConfig().getOuvrirEditeur())
			    {
				    //afficheFichier(sortie.getText());
			    	ouvrirEditeur();
			    }*/
			    getBtEditeur().setEnabled(true);
			    //jpb.setValue(jpb.getMaximum());
		    }
		    
		    activeTrans(true);
			setRunning(false);
			
		    /*try{sleep(2000);}
		    catch (InterruptedException e)
		    {
	         e.printStackTrace();
		    }*/
	    }
    }
    
    /*----------------------------------------------------------------------------------------
	 *                                   ACTION EVENTS (BUTTONS)
	 *----------------------------------------------------------------------------------------
	 */
    /** implémentation de actionPerformed(ActionEvent evt) de l'interface ActionListener
     * gère tous les boutons, tous les items des menus, MAIS PAS LE CHANGEMENT DE CONFIGURATION
     * qui est pris en charge par le popuplistener et la méthode popupMenuWillBecommeInvisible
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent evt)
    {
    	//lancer transcription
	    if (evt.getSource() == btTranscrire || evt.getSource() == jmiTranscrire){launchScenario();}
	    // copy system configurations in user directory
	    else if (evt.getSource() == jmiRestoreSystemConfs)
	    {
		FenetreSystemConfRestorer fscr = new FenetreSystemConfRestorer(this);
		fscr.pack();
	    }
	    //ouvrir editeur
	    else if (evt.getSource() == btEditeur || evt.getSource() == jmiOuvrirTrans){ouvrirEditeur();}
	    //ouvrir editeur tan
	    else if (evt.getSource() == jmiTan){ouvrirEditeur(true);}
	    //ouvrir rapport de bug
	    //else if (evt.getSource() == btSignalBug || evt.getSource() == jmiSignalBug){doBugReport();}
	    else if (evt.getSource() == jmiSignalBug){doBugReport();}
	    //combo box des configurations
	    //maintenant pris en charge par popupMenuWillBecomeInvisible
	    /*
	    else if (evt.getSource() == jcbConfig)
	    {	
		    ConfigurationsListItem cli = (ConfigurationsListItem)jcbConfig.getSelectedItem();
		    ConfigNat.charger(cli.getFilename());
		    checkStepSpinner();
	    }*/
	    else if(evt.getSource() == btSens || (evt.getSource()== jmiSensTr))
	    {
	    	setReverseTrans(!ConfigNat.getCurrentConfig().isReverseTrans());
	    	if(ConfigNat.getCurrentConfig().getSortieAuto()){setSortieAuto(ConfigNat.getCurrentConfig().isReverseTrans());}
	    }
	    //importer et exporter des configurations et transcriptions
	    else if(evt.getSource() == jmiImporter){importerConfig();}
	    else if(evt.getSource() == jmiExporter){exporterConfig();}
	    else if(evt.getSource() == jmiImporterTrans){importTranscription();}
	    //saving configuration
	    else if(evt.getSource() == btSaveConf){ConfigNat.getCurrentConfig().sauvegarder();}
	    else if(evt.getSource() == btSaveConfAs){saveConfAs();}
	    else if(evt.getSource() == btQuitter || evt.getSource() == jmiQuitter){quitter();}
	    else if(evt.getSource() == btShowConf){panConfigDetails.setVisible(!panConfigDetails.isVisible());}
	    else if(evt.getSource() == btShowLog){scrollLog.setVisible(!scrollLog.isVisible());}
	    else if(evt.getSource() == jmiDocumentEnNoir){jtfNoir.requestFocusInWindow();}
	    else if(evt.getSource() == jmiDocumentEnBraille){jtfBraille.requestFocusInWindow();}
	    else if(evt.getSource() == jmiConfigurationActive){jcbConfig.requestFocusInWindow();}
	    else if (evt.getSource() == jmiMaximizeWindow)
	    	{if (this.getExtendedState()==Frame.MAXIMIZED_BOTH)
	    	{this.setExtendedState(Frame.NORMAL);}
	    	else {this.setExtendedState(Frame.MAXIMIZED_BOTH);}
	    	}
	    
    }
    
    
    /**
     * Ouvre l'éditeur de fichier transcrit dans l'éditeur, en passant à l'éditeur une instance d' {@link Embosseur}
     * si nécessaire. Utilise l'encoding représenté par {@link nat.OptNames} dans {@link ConfigNat}
     * Utilise le fichier de mise en page par défaut ({@link Editeur2#fichMep}
     * @param nomFichier Le nom du fichier transcrit à ouvrir
     */
    public void afficheFichier(String nomFichier)
    {
    	afficheFichier(nomFichier,Editeur2.fichMep);
    }
    
    /**
     * Ouvre l'éditeur de fichier transcrit dans l'éditeur, en passant à l'éditeur une instance d' {@link Embosseur}
     * si nécessaire. Utilise l'encoding représenté par {@link nat.OptNames} dans {@link ConfigNat}
     * @param nomFichier Le nom du fichier transcrit à ouvrir
     * @param origineMEP nom du fichier de mise en page à utiliser (null si inconnu)
     */
    public void afficheFichier(String nomFichier, String origineMEP) 
    {
    	//long tps = System.currentTimeMillis(); 
    	/*il faut mémoriser le maximized editeur AVANT de le recréer sinon le resize
    	crée l'évènement qui annule le maximized */
    	boolean maxi = ConfigNat.getCurrentConfig().getMaximizedEditeur();
    	//mise à jour du nom de fichier
    	this.setSortie(nomFichier);
	    Embosseur emb = null;
	    if(ConfigNat.getCurrentConfig().getUtiliserCommandeEmbossage())
	    {
		    emb = new EmbosseurLDC(nomFichier,gestErreur);
	    }
	    else if (ConfigNat.getCurrentConfig().getUtiliserEmbosseuse())
	    {
		    emb = new EmbosseurInstalle(nomFichier, gestErreur);
	    }
	    //copie du fichier de mise en page
	    if(origineMEP != null && !origineMEP.equals(Editeur2.fichMep))
	    {
	    	gestErreur.afficheMessage(texts.getText("copyMEP"), Nat.LOG_VERBEUX);
	    	FileToolKit.copyFile(origineMEP, Editeur2.fichMep);
	    }
	    Editeur2 editeur = new Editeur2(ConfigNat.getCurrentConfig().getLongueurLigne(),emb,gestErreur);
	    String sourceEncoding = ConfigNat.getCurrentConfig().getBrailleEncoding();
			
	    if (sourceEncoding.equals("automatique"))
	    {
		    sourceEncoding = nat.trouveEncodingSource(nomFichier);
		    if(sourceEncoding == null||sourceEncoding.equals(""))
		    {
			    // pas de bol, on n'a pas réussit à trouver le charset correct
			    sourceEncoding = Charset.defaultCharset().name();
			    gestErreur.afficheMessage(texts.getText("charsetnotfound") + sourceEncoding + "\n",Nat.LOG_NORMAL);
		    }
		    else
		    {
			    gestErreur.afficheMessage(texts.getText("charsetok")+sourceEncoding+"\n", Nat.LOG_SILENCIEUX);
		    }
	    }
	    editeur.setEncodage(sourceEncoding);
	    //editeur.setTableBraille(ConfigNat.getCurrentConfig().getTableBraille());
		
	    editeur.setAfficheLigneSecondaire(ConfigNat.getCurrentConfig().getAfficheLigneSecondaire());
	    editeur.afficheFichier(nomFichier, ConfigNat.getCurrentConfig().getPoliceEditeur(), ConfigNat.getCurrentConfig().getTaillePolice(),ConfigNat.getCurrentConfig().getPolice2Editeur(), ConfigNat.getCurrentConfig().getTaillePolice2());
	    editeur.pack();
	    //mode preview ou edition?
	    editeur.setPreview(origineMEP==null);
	    editeur.setVisible(true);
	    if (maxi)
    	{
	    	editeur.setExtendedState(Frame.MAXIMIZED_BOTH);
	    	ConfigNat.getCurrentConfig().setMaximizedEditeur(true);
	    }
	    //System.out.println("temps :" + (System.currentTimeMillis() - tps));
    }
    /**
     * Ask the new name of the configuration, save the new configuration and update {@link #jcbConfig} 
     */
    private void saveConfAs()
    {
	    // 
	    JOptionPane.showMessageDialog(this, "A faire");
    }

	/**
     * Launch the transcription process after verifing file existences and other
     */
    private void launchScenario()
    {
    	boolean go = false;
		boolean wantErase = true;
    	File f = null;
    	String dest = null;
    	
    	if(ConfigNat.getCurrentConfig().isReverseTrans())
    	{
    		f = new File(jtfNoir.getText());
    		dest = "handwritten";
    	}
    	else
    	{
    		f = new File(jtfBraille.getText());
    		dest = "braille";
    		
    	}
    	
    	// f.canWrite() et f.getParent.canWrite
    	// renvoie des résultats différents et des fois faux (trop souvent true)
    	// selon les OS donc on peut pas s'y fier !
    	
    	if(!f.exists())
    	{try {
			go = f.createNewFile();
			} catch (IOException e) {
			//on s'en fiche car on le gère après
			}
		}
    	else // file exists
    	{
    		boolean eraseOk = false;
    		if (f.canWrite() && f.getParentFile().canWrite())
    		{
    			wantErase = JOptionPane.YES_OPTION == 
	    				JOptionPane.showConfirmDialog(this, texts.getText("delete"+dest),
	    				texts.getText("existing"+dest), JOptionPane.YES_NO_OPTION);
	    		if(wantErase)
	    		{
	    			eraseOk = f.delete();
	    			//=true if the file was deleted
	    		}
    		}
    		go = eraseOk;
    	}
    	
    	//impossible d'écrire
		if(!go && wantErase)
		{
			JOptionPane.showMessageDialog(this,texts.getText("errorFileAccess") ,
					texts.getText("errorFileAccessTitle"), JOptionPane.ERROR_MESSAGE);
		}

	    if(go)
	    {
	    	//jpb.setValue(jpb.getMinimum());
		    Thread thr = new ThreadJPB();
		    thr.start();
			
		    btEditeur.setEnabled(false);
		    error = false;
		    this.repaint();
		    if ((jtfNoir.getText().length()<1)||(jtfBraille.getText().length()<1))
	        {
			    gestErreur.afficheMessage(texts.getText("missingfile"), Nat.LOG_SILENCIEUX);
	        }
		    else
		    {
			    panneauLog.setText("");
			    panneauLog.paintImmediately(new Rectangle(0,0,panneauLog.getWidth(),panneauLog.getHeight()));
			    ArrayList<String> sources = new ArrayList<String>();
			    ArrayList<String> sorties = new ArrayList<String>();
			    sources.add(jtfNoir.getText());
			    sorties.add(jtfBraille.getText());
			    if (nat.fabriqueTranscriptions(sources, sorties))
			    {			
			    	thrTrans = new ThreadTrans();
			    	thrTrans.start();
			    }
		    }
	    }
	    else{gestErreur.afficheMessage(texts.getText("abort"), Nat.LOG_SILENCIEUX);}
    }	
    
    /**
     * Arrête la transcription en cours d'exécution
     */
    public void stopTrans()
    {
    	if(thrTrans!=null)
    	{
    		thrTrans.interrupt();
			setRunning(false);
			activeTrans(true);
			nat.transStopped();
			gestErreur.afficheMessage(texts.getText("abort"), Nat.LOG_SILENCIEUX);
			thrTrans = null;
    	}
    }

    /**
     * exporte la configuration active
     */
    private void exporterConfig()
    {
	    JFileChooser jfc = new JFileChooser();
	    jfc.setCurrentDirectory(new File(ConfigNat.getUserConfigFolder()));
		// ajout des filtres au JFileChooser
		jfc.addChoosableFileFilter(new FiltreFichier( new String[]{ConfigNat.natConfArchive}, "archive configuration nat (*."+ConfigNat.natConfArchive+")"));
		jfc.setDialogTitle("Exporter la configuration courante");
 		if (jfc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
        {    
 			try
 			{
	 			FileToolKit.exportConfig(jfc.getSelectedFile().getCanonicalPath());
 			}
 			catch (IOException e)
 			{
 				gestErreur.afficheMessage("Erreur lors de lecture/écriture lors de l'exportation",Nat.LOG_SILENCIEUX);
 				e.printStackTrace();
 			} 
        }
    }

	/**
     * importe une configuration complète et la sélectionne
     * @param inFilename nom du fichier nca à importer
     * @throws IOException en cas de souci
     * @return true si redémarrage de NatBraille nécessaire
     */
    private boolean importerConfig (String inFilename) throws IOException
    {
    	//suppression des fichiers temporaires de la conf précédente
		File tmpConf = new File(ConfigNat.getConfTempFolder());
		File[] files = tmpConf.listFiles();
		for(File f : files)
		{
			if(f.isFile()){f.delete();}
		}
		// Open the ZIP file
		ZipInputStream in = new ZipInputStream(new FileInputStream(inFilename));
		// entrées
		ZipEntry entry;
		while((entry = in.getNextEntry())!=null)
		{
    		// Open the output file
    		String outFilename = ConfigNat.getConfTempFolder()+entry.getName();
    		OutputStream out = new FileOutputStream(outFilename);
    		// Transfer bytes from the ZIP file to the output file
    		byte[] buf = new byte[1024];
    		int len;
    		while ((len = in.read(buf)) > 0) { out.write(buf, 0, len); }
    		// Close the streams
    		out.close();
		}
		in.close();
		//liste des fichiers
		files = tmpConf.listFiles();
		ArrayList<File> al_Files = new ArrayList<File>();
		for(int i=0; i<files.length;i++){al_Files.add(files[i]);}
		Collections.sort(al_Files);
		
			File fVersion =  al_Files.get(0);
			File fConf = al_Files.get(1);
			//File fIntegral = al_Files.get(2);
			File fAbr = new File("");
			File fCoup = new File("");
			
			for(int i = 3; i < al_Files.size();i++)
			{
				if(al_Files.get(i).getName().startsWith("4")){fAbr = al_Files.get(i);}
				else if(al_Files.get(i).getName().startsWith("5")){fCoup = al_Files.get(i);}
			}
			
			//vérification des versions
			boolean restart = false;
			int version = Integer.parseInt(FileToolKit.loadFileToStr(fVersion.getCanonicalPath()).split("\n")[0]);
			if(version > ConfigNat.getSvnVersion())
			{
				System.out.println("update needed");
			}
			else
			{
				if(version < ConfigNat.getSvnVersion()){restart = true;}
				Properties conf = new Properties();
				FileInputStream fis = new FileInputStream(fConf);
			conf.load(fis);
			fis.close();
			//règles d'abrégé
			String nomFichierReglesG2 = conf.getProperty(OptNames.fi_litt_fr_abbreg_rules_filename_perso);
			boolean abregerDefaut = nomFichierReglesG2 == null || nomFichierReglesG2.equals(ConfigNat.getCurrentConfig().getRulesFrG2());
			if(!abregerDefaut)
			{
				conf.setProperty(OptNames.fi_litt_fr_abbreg_rules_filename_perso,
						ConfigNat.getConfImportFolder()+fAbr.getName());
				FileToolKit.copyFile(fAbr.getCanonicalPath(), ConfigNat.getConfImportFolder()+fAbr.getName());
			}
			//règles de coupures
			String nomFichierCoup = conf.getProperty(OptNames.fi_hyphenation_rulefile_name);
			boolean coupDefaut = nomFichierCoup == null || nomFichierCoup.equals(ConfigNat.getDicoCoupDefaut());
			if(!coupDefaut)
			{
				conf.setProperty(OptNames.fi_hyphenation_rulefile_name,
						ConfigNat.getConfImportFolder()+fCoup.getName());
				FileToolKit.copyFile(fCoup.getCanonicalPath(), ConfigNat.getConfImportFolder()+fCoup.getName());
			}
			//copie de la conf
			FileToolKit.copyFile(fConf.getCanonicalPath(), ConfigNat.getUserConfigFilterFolder()+fConf.getName());
			chargeConfigurations();
			if(restart)
			{
				//JOptionPane.showMessageDialog(this, texts.getText("confRestart") );
			}
			else
			{
				//conf selection - must look all combobox
				File importedConf = new File (ConfigNat.getUserConfigFilterFolder()+fConf.getName());
				String icn = importedConf.getCanonicalPath();
				ConfigNat.charger(icn);
				//jcbConfig.requestFocusInWindow();
				for (int i=0; i<jcbConfig.getItemCount(); i++){
					ConfigurationsListItem cli2 = jcbConfig.getItemAt(i);
					if (cli2.getFilename().equals(icn))
					{jcbConfig.setSelectedIndex(i);}
				}
				//JOptionPane.showMessageDialog(this, texts.getText("confImportSuccessfull") );
			}
			}
		return restart;
    }
    
    /**
     * dialogue d'import d'une conf
     */
    private void importerConfig()
    {
    	JFileChooser jfc = new JFileChooser();
	    jfc.setCurrentDirectory(new File(ConfigNat.getUserConfigFolder()));
	    // ajout des filtres au JFileChooser
		FiltreFichier fimport = new FiltreFichier( new String[]{ConfigNat.natConfArchive}, "archive configuration nat (*."+ConfigNat.natConfArchive+")");
	    jfc.addChoosableFileFilter(fimport);
	    jfc.setFileFilter(fimport);
		jfc.setDialogTitle("Importer une configuration");
 		if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {    
	    	try
	    	{
	    		if (importerConfig(jfc.getSelectedFile().getCanonicalPath()))
	    		{
	    			JOptionPane.showMessageDialog(this, texts.getText("confRestart") );
	    		}
	    		else
	    		{
	    			JOptionPane.showMessageDialog(this, texts.getText("confImportSuccessfull") );
	    		}
	    	}
	    	catch (IOException e) {gestErreur.afficheMessage("Erreur lors de l'importation de conf : "+e.getLocalizedMessage(), Nat.LOG_SILENCIEUX);
	    		e.printStackTrace();}
        }
    }
    
    /**
     * import d'un fichier .natb contenant conf et fichiers temporaires
     * pour ouverture dans l'éditeur de Nat
     */
    
    private void importTranscription()
    {
    	JFileChooser jfc = new JFileChooser();
	    jfc.setCurrentDirectory(new File(ConfigNat.getUserConfigFolder()));
	    // ajout des filtres au JFileChooser
		FiltreFichier fimport = new FiltreFichier( new String[]{ConfigNat.natTransArchive}, "Transcription nat (*."+ConfigNat.natTransArchive+")");
	    jfc.addChoosableFileFilter(fimport);
	    jfc.setFileFilter(fimport);
		jfc.setDialogTitle("Importer une configuration");
		if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {    
			//suppression des fichiers temporaires de la conf/trans précédente
			File tmpConf = new File(ConfigNat.getTransTempFolder());
			File[] files = tmpConf.listFiles();
			for(File f : files)
			{
				if(f.isFile()){f.delete();}
			}
			String inFilename;
			try {
				inFilename = jfc.getSelectedFile().getCanonicalPath();
				// Open the ZIP file
				ZipInputStream in = new ZipInputStream(new FileInputStream(inFilename));
				// entrées
				ZipEntry entry;
				while((entry = in.getNextEntry())!=null)
				{
					// Open the output file
		    		String outFilename = ConfigNat.getTransTempFolder()+entry.getName();
		    		OutputStream out = new FileOutputStream(outFilename);
		    		// Transfer bytes from the ZIP file to the output file
		    		byte[] buf = new byte[1024];
		    		int len;
		    		while ((len = in.read(buf)) > 0) { out.write(buf, 0, len); }
		    		// Close the streams
		    		out.close();
		    		
				}
				in.close();
			} catch (IOException e) {
				gestErreur.afficheMessage("erreur import fichier natb : "+e.getLocalizedMessage(), Nat.LOG_SILENCIEUX);
				e.printStackTrace();
			}
			//liste des fichiers
			files = tmpConf.listFiles();
			ArrayList<File> al_Files = new ArrayList<File>();
			for(int i=0; i<files.length;i++){al_Files.add(files[i]);}
			String brailleFile = null;
			for (File f : al_Files)
			{
				try
				{
					String fname = f.getCanonicalPath();
					String fshortname = f.getName();
					if (fname.contains("used_conf_for_this_transcription."))
						{importerConfig(fname);}
					else if (fname.endsWith("tmp.xml"))
						{FileToolKit.copyFile(fname, Transcription.fTempXML);}
					else if (fname.endsWith("tmp_mep.xml"))
						{FileToolKit.copyFile(fname, Transcription.fTempXML2);}
					else if (fshortname.startsWith("bra_"))
						{brailleFile = fname; }
				}
			    catch (IOException e) {
				gestErreur.afficheMessage("erreur import natb transcription : "+e.getLocalizedMessage(), Nat.LOG_SILENCIEUX);
				e.printStackTrace();
			    }
			}//du for f:al_files
			afficheFichier(brailleFile,Transcription.fTempXML2);
        }//du if approve_option
    }
    
    /**
	 * Crée un zip avec les fichiers temporaires, les logs, la conf, la source et la sortie
	 * et prépare la fenêtre de dialogue {@link FenetreBugReport} pour un envoi de bug
	 */
	private void doBugReport()
	{
		String msg = texts.getText("warningBugReport"); 
			/*	"NatBraille va maintenant créer un fichier zip contenant les fichiers\n" +
				"à envoyer pour tenter de résoudre votre problème ; cela comprend :\n"+
				"- Le fichier source et le fichier sortie choisi dans la fenêtre\n"+
				"- Les fichiers de log contenant les messages de cette transcription\n" +
				"  et aussi des quelques dernières\n" +
				"- La configuration utilisée\n" +
				"SI VOUS PENSEZ, POUR DES RAISONS DE DROIT SUR LE FICHIER SOURCE\n" +
				"OU DE PROTECTION DE VOTRE VIE PRIVEE QUE L'EQUIPE DE DEVELOPPEMENT\n" +
				"DE NATBRAILLE NE DEVRAIT PAS AVOIR ACCES A CERTAINS DE CES FICHIERS,\n" +
				"CLIQUEZ SUR \"ANNULER\". Sachez cependant que tous les fichiers ne\n" +
				"serviront qu'à améliorer NatBraille et seront effacés une fois le bug résolu.";*/
		int okForReport = JOptionPane.showConfirmDialog(this, msg, 
				texts.getText("warningBugReportTitle"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
		if (!(okForReport==JOptionPane.OK_OPTION)){return;}
		JFileChooser jfc = new JFileChooser();
		String fn = "natBugReport_"+System.getProperty("user.name").replace(" ", "")+"_"+
				System.getProperty("os.name").replace(" ", "")+"_java"+System.getProperty("java.version").replace(" ", "");
		jfc.setSelectedFile(new File(fn));
	    jfc.setCurrentDirectory(new File(System.getProperty("user.home")));
		// ajout des filtres au JFileChooser
	    FiltreFichier zipFilter = new FiltreFichier( new String[]{"zip"}, "Archive Zip (*.zip)");
		jfc.addChoosableFileFilter(zipFilter);
		jfc.setFileFilter(zipFilter);
		jfc.setDialogTitle(texts.getText("bugReportFileChooserTitle"));
 		if (!(jfc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION))
 			{return;}
 		try
		{
	 	String bugZipFile = jfc.getSelectedFile().getCanonicalPath();
	 	if (!bugZipFile.endsWith(".zip")){bugZipFile+=".zip";}
		//stockage de la conf utilisée dans tmp
	 	//avec correction du bug que des fois y'a pas de separator à la fin
	 	//http://www.rgagnon.com/javadetails/java-0484.html
	 	String tmpDir = System.getProperty("java.io.tmpdir");
	 	System.out.println(tmpDir);
		if ( !(tmpDir.endsWith("/") || tmpDir.endsWith("\\")) )
			tmpDir = tmpDir + System.getProperty("file.separator");
		String confExp = tmpDir+"current_config."+ConfigNat.natConfArchive;
		FileToolKit.exportConfig(confExp);
		//création du zip de sortie
		byte[] buf = new byte[1024];
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(bugZipFile));
		
		//zippage du tmp qu'on fait pas pour des raison de droits
		//FileToolKit.zipDir(ConfigNat.getUserTempFolder(), out);
		
		//sélection des logs dans le tmp avec un joli filenamefilter
		FilenameFilter natlogFilter = new FilenameFilter() {
			@Override
			public boolean accept(File directory, String filename) {
				// TODO Auto-generated method stub
				return filename.startsWith("nat_log");
			}};
		String[] logFiles = new File(ConfigNat.getUserTempFolder()).list(natlogFilter);
		System.out.println("** log files ****");
		for (String s:logFiles){System.out.println("fich "+s);}
		System.out.println("******");
		String[] otherFiles = new String[] {"natbraille_errors.log","natbraille_output.log",
				getEntree().getText(),getSortie().getText(),confExp};
		
		/* Liste des fichiers dans fileArchives :
		 * - les deux premiers sont la sortie standard et erreur créés par le launcher install4j
		 *   et sont mis à zéro à chaque lancement de NatBraille
		 * - ensuite source et sortie
		 * - puis les nat_log.* de tmp/
		 */
		
		ArrayList <String> fileArchives = new ArrayList<String>();
		fileArchives.addAll(Arrays.asList(otherFiles));
		for (String s: logFiles)
			{ fileArchives.add(ConfigNat.getUserTempFolder()+s);}
		
		gestErreur.afficheMessage(texts.getText("bugReportFileMessage"), Nat.LOG_VERBEUX);
		for (String s: fileArchives)
		{ 
			File f = new File(s);
			gestErreur.afficheMessage("File : "+s+"\n", Nat.LOG_VERBEUX);
			if(f.exists())
			{
				FileInputStream in = new FileInputStream(s);
				// Add ZIP entry to output stream. 
				out.putNextEntry(new ZipEntry(f.getName()));
				// Transfer bytes from the file to the ZIP file 
				int len;
				while ((len = in.read(buf)) > 0) { out.write(buf, 0, len); }
				// Complete the entry 
				out.closeEntry();
				in.close();
			}
		}
		out.close();
		
		//copie de l'adresse mail dans le clipboard
		StringSelection ss = new StringSelection("nat-dev@listes.univ-lyon1.fr");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		
		String message = texts.getText("bugReportFinished");
			/*	"Envoyez maintenant un mail à nat-dev@listes.univ-lyon1.fr\n" +
				"(l'adresse est dans le presse-papier, donc vous pouvez utiliser\n" +
				"Ctrl+V ou autre pour la coller dans le champ Destinataire du mail)\n" +
				"en exposant votre problème le plus précisément possible ET EN\n" +
				"N'OUBLIANT PAS DE JOINDRE LE ZIP A VOTRE MAIL. Tout l'équipe vous\n" +
				"remercie de participer à l'amélioration du logiciel."; */
		JOptionPane.showMessageDialog(this, message, texts.getText("bugReportFinishedTitle")+bugZipFile, JOptionPane.INFORMATION_MESSAGE);
		// lancement de la fenêtre de debug
		//FenetreBugReport f= new FenetreBugReport(this,fileArchives.toString());
		//f.setVisible(true);
		}
		catch (IOException e)
		{
			gestErreur.afficheMessage(texts.getText("bugReportError")+e.getMessage(), Nat.LOG_SILENCIEUX);
			e.printStackTrace();
		}
	}
	
	/**
	 * Open the TAN editor with an empty doc (documents/nouveau.tan) if tan param set to true,
	 * else open the classic editor
	 * @param tan if true open the TAN editor with an empty doc, else open the classic editor
	 */
	private void ouvrirEditeur(boolean tan)
	{
		if(tan)
		{
			EditeurTan et = new EditeurTan(null,ConfigNat.fichTmpTan,nat);
			et.setVisible(true);
			et.setExtendedState(Frame.MAXIMIZED_BOTH);
			et.setState(MAXIMIZED_BOTH);
		}
		else{ouvrirEditeur();}
	}
	
	/** Ouvre le bon éditeur pour le fichier de sortie en fonction des options choisies */
	public void ouvrirEditeur()
	{
		//utilisation de l'éditeur de nat
		if(ConfigNat.getCurrentConfig().getUseNatEditor())
		{
			if(ConfigNat.getCurrentConfig().isReverseTrans())
			{
				//copie du fichier noir xhtml dans le fichier temporaire de tan
				gestErreur.afficheMessage(texts.getText("copy"), Nat.LOG_DEBUG);
				FileToolKit.copyFile(jtfNoir.getText(), EditeurTan.tmpXHTML);
				EditeurTan et = new EditeurTan(null,jtfBraille.getText(),nat);
				//et.setState(MAXIMIZED_BOTH);
				et.setVisible(true);
				et.setExtendedState(Frame.MAXIMIZED_BOTH);
			}
			else{afficheFichier(jtfBraille.getText());}
		}
		//utilisation de l'éditeur par défaut de l'environnement
		else if(ConfigNat.getCurrentConfig().getUseDefaultEditor())
		{
	    	Desktop desktop = null;
	        if (Desktop.isDesktopSupported())
	        {
	        	desktop = Desktop.getDesktop();
	        	if (desktop.isSupported(Desktop.Action.OPEN))
	        	{
	        		try{desktop.open(new File(jtfBraille.getText()));}
					catch (IOException e) {gestErreur.afficheMessage(texts.getText("buginout")+ jtfBraille.getText(), Nat.LOG_SILENCIEUX);}
	        	}
	        	else{gestErreur.afficheMessage(texts.getText("editornotfound"), Nat.LOG_SILENCIEUX);}
	
	        }
	        else{gestErreur.afficheMessage(texts.getText("desktopnotsupported"), Nat.LOG_SILENCIEUX);}
		}
		//utilisation d'un éditeur spécifique
		else
		{
			File fsortie = new File(jtfBraille.getText());
			// la tableau cmd contient deux éléments : le chemin de l'éditeur externe et le chemin du fichier à ouvrir.
			String []cmd= new String[2];
			try
			{
	
				cmd[0] = ConfigNat.getCurrentConfig().getEditeur();
				cmd[1] = fsortie.getCanonicalPath();
				Runtime.getRuntime().exec(cmd);
			}
			catch (IOException ioe) {gestErreur.afficheMessage(texts.getText("buginout2")+cmd[0]+texts.getText("with")+ cmd[1],Nat.LOG_SILENCIEUX); }
		}
	}
	
	 /**
     * charge l'étape s
     * @param s etape cible du scenario
     */
    private void gotoStep(int s)
    {
    	ConfigurationsListItem cli = (ConfigurationsListItem)(jcbConfig.getSelectedItem());
    	if(s>0 && s <= cli.getScenSize()){cli.setStep(s);}
	    int i = jcbConfig.getSelectedIndex();
	    chargeConfigurations();
	    jcbConfig.setSelectedIndex(i);
	    jcbConfig.firePopupMenuWillBecomeInvisible();
	    
    }
	
	/** Quitte le programme en enregistrant les options de l'interface graphique et la configuration actuelle*/
	private void quitter()
	{
		gestErreur.afficheMessage(texts.getText("savemessage"), Nat.LOG_NORMAL);
		ConfigNat.getCurrentConfig().setFBraille(jtfBraille.getText());
		ConfigNat.getCurrentConfig().setFNoir(jtfNoir.getText());
		ConfigNat.getCurrentConfig().saveUiConf();
		gestErreur.afficheMessage(texts.getText("okbye") , Nat.LOG_NORMAL);
	    System.exit(0);
	}
	
	
	/*----------------------------------------------------------------------------------------
	 *                                   COMPONENT EVENTS
	 *----------------------------------------------------------------------------------------
	 */
	
	/** Méthode redéfinie de ComponentListener
	 * Ne fait rien
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentHidden(ComponentEvent arg0){/*do nothing*/}
	/** Méthode redéfinie de ComponentListener
	 * Ne fait rien
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentMoved(ComponentEvent arg0){/*do nothing*/}
	/** Méthode redéfinie de ComponentListener
	 * Ne fait rien
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentShown(ComponentEvent arg0){/*do nothing*/}
	/** Méthode redéfinie de ComponentListener
	 * Mise à jour de l'affichage lors du redimensionement
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentResized(ComponentEvent arg0)
	{
		if (getExtendedState()==Frame.MAXIMIZED_BOTH)
		{
			ConfigNat.getCurrentConfig().setMaximizedPrincipal(true);
		}
		else
		{
			ConfigNat.getCurrentConfig().setWidthPrincipal(getWidth());
			ConfigNat.getCurrentConfig().setHeightPrincipal(getHeight());
			ConfigNat.getCurrentConfig().setMaximizedPrincipal(false);
		}
		repaint();
	}
    /** 
     * implémentation de focusGained de FocusListener; ne fait rien 
     * @param foc Le FocusEvent
     * */
	@Override
    public void focusGained(FocusEvent foc){/*do nothing*/} 
	
	/**
	 * implémentation de focusLost de FocusListener;
	 * positionne le curseur sur le dernier caractère des textes contenus dans {@link #jtfNoir} et {@link #jtfBraille}
     * @param foc Le FocusEvent
	 */
    @Override
    public void focusLost(FocusEvent foc)
    {
	    if (foc.getSource()==jtfBraille)
	    {
		    jtfBraille.setCaretPosition(jtfBraille.getText().length());
		    verifieBtEditeur();
		    if(ConfigNat.getCurrentConfig().getSortieAuto() && ConfigNat.getCurrentConfig().isReverseTrans())
		    {
		    	setSortieAuto(true);
	    	}
	    }
	    else if (foc.getSource()==jtfNoir)
	    {
		    jtfNoir.setCaretPosition(jtfNoir.getText().length());
		    if(ConfigNat.getCurrentConfig().getSortieAuto() && !ConfigNat.getCurrentConfig().isReverseTrans())
	    	{
		    	setSortieAuto(false);
	    	}
	    }
    }
	/**
     * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
     */
    @Override
    public void stateChanged(ChangeEvent e)
    {
	    if (e.getSource() == jsStep)
	    {
	    	gotoStep(((Integer)jsStep.getValue()).intValue());
	    	jcbConfig.requestFocusInWindow();
	    }
    }
    
    /** @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)*/
	@Override
    public void windowActivated(WindowEvent arg0) {/*do nothing*/}
	/** implémentation de WindowsListener; quitte le programme
	 * @see #quitter()
	 * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
	 */
	@Override
    public void windowClosed(WindowEvent arg0){quitter();}
	/**@see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)*/
	@Override
    public void windowClosing(WindowEvent arg0) {/*do nothing*/}
	/**@see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)*/
	@Override
    public void windowDeactivated(WindowEvent arg0) {/*do nothing*/}
	/** @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)*/
	@Override
    public void windowDeiconified(WindowEvent arg0) {/*do nothing*/}
	/** @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)*/
	@Override
    public void windowIconified(WindowEvent arg0) {/*do nothing*/}
	/**@see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)*/
	@Override
    public void windowOpened(WindowEvent arg0){/*do nothing*/}

	/**
	 * @see javax.swing.event.PopupMenuListener#popupMenuCanceled(javax.swing.event.PopupMenuEvent)
	 */
	@Override
	public void popupMenuCanceled(PopupMenuEvent arg0) {/*do nothing*/}
	/**
	 * @see javax.swing.event.PopupMenuListener#popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent)
	 */
	@Override
	public void popupMenuWillBecomeVisible(PopupMenuEvent arg0) {/*do nothing*/}
	/** 
	 * @see javax.swing.event.PopupMenuListener
	 * le changement de configuration par {@link FenetrePrinc#jcbConfig}
	 *pour éviter de charger la conf à chaque fois
	 */
	@Override
	public void popupMenuWillBecomeInvisible(PopupMenuEvent pp) 
	{
		//System.out.println("Va se fermer...");
		ConfigurationsListItem cli = (ConfigurationsListItem)jcbConfig.getSelectedItem();
		ConfigNat.charger(cli.getFilename());
		checkStepSpinner();
	}

}
