/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui;

import java.awt.Point;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.help.HelpBroker;
import javax.help.HelpSet;

import ui.accessibility.Language;

/**
 * Fenêtre d'aide pour NAT
 * <p>Utilise l'API JavaHelp</p> 
 * @author bruno
 *
 */
public class Aide
{
	/** Textual contents */
	static Language texts = new Language("Aide");
	
	/** 
	 * Constructeur
	 * <p>Charge le HelpSet file <code>aide/nat.hs</code> pour fabriquer une instance de HelpSet</p>
	 * <p>Affiche le HelpBroker généré</p>
	 * @param id home ID
	 * @throws IOException TODO
	 * @throws URISyntaxException TODO
	 */
	public Aide(String id) throws IOException, URISyntaxException
	{
		String helpHS = "aide/nat.hs";
		ClassLoader cl = Aide.class.getClassLoader();
		HelpSet hs;
		try 
		{
			URL hsURL = HelpSet.findHelpSet(cl, helpHS);
			//URL hsURL = HelpSet.findHelpSet(cl, helpHS);
			hs = new HelpSet(null,hsURL);
			hs.setHomeID(id);
			
		} 
		catch (Exception ee) 
		{
			// Say what the exception really is
			System.err.println( "HelpSet " + ee.getMessage());
			System.err.println("HelpSet "+ helpHS +" "+texts.getText("notfound"));
			return;
		}
		//Create a HelpBroker object:
		HelpBroker hb = hs.createHelpBroker();
		hb.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		hb.setLocation(new Point(0,0));
		hb.setDisplayed(true); //pas trop tôt!!!!!!!!!!!
	}
}