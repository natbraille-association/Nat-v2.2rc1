/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.editor;

import gestionnaires.GestionnaireErreur;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;


import org.apache.commons.lang.StringUtils;

import nat.ConfigNat;
import nat.Nat;
import nat.Transcription;
import nat.presentateur.Presentateur;
import nat.presentateur.PresentateurMEP;
import nat.transcodeur.TranscodeurNormal;

import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import outils.FileToolKit;
import outils.emboss.Embossage;
import outils.emboss.Embosseur;
import ui.FiltreFichier;
import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;
import ui.dialog.DialogueEmbossage;
import ui.dialog.DialogueLegende;
import ui.listener.PerkinsNotifier;

/**
 * Cette classe de l'interface graphique permet d'afficher le fichier transcrit et d'intéragir avec lui
 * (édition, mode perkins, etc.).
 * @author Bruno Mascret
 */
public class Editeur2 extends EditeurBraille implements ChangeListener, CaretListener, MouseListener, KeyListener, PropertyChangeListener
{
	/** Textual contents */
	private static Language texts = new Language("Editeur");
	
	/** adresse du fichier de mise en page*/
	public static final String fichMep = Transcription.fTempXML2;
	/** Pour la sérialisation (non utilisé)*/
	private static final long serialVersionUID = 1L;
	/** nombre de page en braille, pour faciliter la navigation */
	private int nbPagesBr = 0;
	/** la page actuellement affichée */
	private int pageActu = 0;
	/** la zone secondaire d'affichage (affiche la ligne en cours dans la police secondaire)*/
	private JTextField ligneRes = new JTextField();
	/** JSpinner indiquant le numéro de page actuellement affiché*/
    private JSpinner jsPage;
    /** Le modèle pour le JSpinner jsPage **/
    private SpinnerNumberModel spinMod = new SpinnerNumberModel(1, 1, 1, 1);
	/* longueur de la ligne braille */
	//sert plus a rien pour l'instant
    //private int longueurLigne;
	
	/** ligne secondaire pour l'apercu */
	private JTextField jtfLigneApercu = new JTextField();
	/** scroll pane pour l'apercu*/
	private JScrollPane jspApercu;
	/* panneau contenant les commandes pour l'éditeur MEP 
	private JPanel pCmd = new JPanel();*/
	/** bouton provoquant la mise à jour du fichier braille (mise en page)*/
	private JButton btMaj = null;
	/** JButton pour enregistrer une copie */
	private JButton btEnregistrerCopie = null;
	/** JButton pour charger une copie */
	private JButton btChargerCopie = null;
	
	// boutons d'insertion de caractères spéciaux et édition//
	/** bt insérer coupure */
	private JButton btInsCoup = new JButton();
	/** bt insérer coupure esthétique*/
	private JButton btInsCoupEsth = new JButton();
	/** bt insérer espace sécable*/
	private JButton btInsSpace = new JButton();
	/** bt insérer esapce insécable*/
	private JButton btInsSpaceI = new JButton();
	/** bt insérer saut de page*/
	private JButton btPageBreak = new JButton();
	/** bt couper paragraphe*/
	private JButton btSplitPar = new JButton();
	
	/** caractères spéciaux de formattage */
	private String[] carSpec;
	
	/** research label left*/
	private JLabel jlResearch = new JLabel(texts.getText("researchlabel"));
	/** research field left*/
	protected BrailleTextField jtfResearch = new BrailleTextField(10);
	/** research button left*/
	private JButton jbResearch = new JButton(new ImageIcon("ui/icon/edit-find.png"));
	
	/** research label right*/
	private JLabel jlResearch2 = new JLabel(texts.getText("research2label"));
	/** research field right*/
	protected BrailleTextField jtfResearch2 = new BrailleTextField(10);
	/** research button right*/
	private JButton jbResearch2 = new JButton(new ImageIcon("ui/icon/edit-find.png"));
	
	/** Progress bar for the search algorithm */
	private JProgressBar progressBar = new JProgressBar();
	/** Label displayingprogress information */
	private JLabel progressLabel = new JLabel();
	/** the ProgressTask instance for reseach */
	private ProgressTask progressTask = new ProgressTask(0,null);
	
	// Menu Bar
	/** menu contextuel d'édition*/
	private JPopupMenu jpmEdit = new JPopupMenu();
	/** Menu item mettre à jour le fichier braille */
	private JMenuItem jmiMAJ;
	/** Menu item enregister une copie*/
	private JMenuItem jmiEnregistrerCopy;
	/** Menu item charger une copie*/
	private JMenuItem jmiChargerCopy;
	/** Menu item archiver une transcription */
	private JMenuItem jmiEnregistrerTranscription;
	/** Menu item insérer une coupure*/
	private JMenuItem jmiInsCoup;
	/** Menu item insérer une coupure esthétique*/
	private JMenuItem jmiInsCoupE;
	/** Menu item insérer un espace sécable */
	private JMenuItem jmiInsSpace;
	/** Menu item insérer un espace insécable */
	private JMenuItem jmiInsSpaceI;
	/** Menu item insérer un saut de page */
	private JMenuItem jmiInsSaut;
	/** Menu item couper le paragraphe */
	private JMenuItem jmiSplitPar;
	/** Menu item next match*/
	private JMenuItem jmiNext;
	/** Menu item légende utilisée*/
	private JMenuItem jmiLegende;
	/** Menu item ne plus couper*/
	
	/** Menu item next page*/
	private JMenuItem jmiNextPage;
	/** Menu item previous page*/
	private JMenuItem jmiPreviousPage;
	/** Menu item page number*/
	private JMenuItem jmiPageNumber;
	/** Menu item focus on tagged view*/
	private JMenuItem jmiViewTagged;
	/** Menu item focus on plain view*/
	private JMenuItem jmiViewPlain;
	
	/** Menu item no more hyphenation mark*/
	private JMenuItem jmiInsStopCoup;
	
	
	/** les pages du doc braille */
	private ArrayList<String> lesPages = new ArrayList<String>();
	/** HashTable pour la correspondance entre les caractères de la table
	 * choisie pour la sortie et leur code en version binaire*/
	protected HashMap<String,String> ptBraille = new HashMap<String,String>();
	/** HashTable de correspondance entre les caractères braille UTF-8 et ceux de la table
	 * choisie pour la sortie */
	protected HashMap<String,String> mapUTF8_Table = new HashMap<String,String>();
	/** HashTable de correspondance entre les caractères braille de la table de sortie et ceux
	 * de la table UTF_8 */
	protected HashMap<String,String> map_Table = new HashMap<String,String>();

	/** PerkinsNotifier used to manage braille conversion */
	private PerkinsNotifier pn3 = null;
	
	/** PerkinsNotifier used to manage braille research */
	protected PerkinsNotifier pn2 = null;
	protected PerkinsNotifier pn2b = null;
	
	/** Adresse du fichier éditable */
	private String fichierMEP = fichMep; 
	
	/** Indique si l'éditeur est uniquement en visualisation (pas de fichier MEP connu */
	private boolean preview = false;
	
	/** progress status */
	protected int progress = 0;
	
	/* Rapport entre la hauteur du panneau d'affichage celle de la fenêtre */
	//private double proportionPanneauAffichage = 0.8; 
	
	/** 
	 * Construit un objet Editeur
	 * @param taille la longueur de la ligne braille
	 * @param emb l'objet Embosseur à utiliser pour l'embossage
	 * @param g instance de GestionnaireErreur
	 */
	public Editeur2(int taille, Embosseur emb, GestionnaireErreur g)
	{
	    super(texts.getText("editor"),emb,g,"xsl/tablesBraille/brailleUTF8.ent");
	    //setFichier(fichMep);
	    
	    try
        {
	        pn2 = new PerkinsNotifier(FileToolKit.getSysDepPath(new File("xsl/tablesBraille/brailleUTF8.ent").getAbsolutePath()));
	        pn2b = new PerkinsNotifier ();
	        pn3 = new PerkinsNotifier();
        }
        catch (NumberFormatException e1)
        {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
        }
        catch (IOException e1)
        {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
        }
	    /*String pathTable = "";
	    if(ConfigNat.getCurrentConfig().getIsSysTable())
		{
	    	pathTable = ConfigNat.getInstallFolder()+"/xsl/tablesBraille/";
		}
		else
		{
			pathTable = ConfigNat.getUserEmbossTableFolder()+"/";
		}
	    pathTable += ConfigNat.getCurrentConfig().getTableBraille();
	    /*try
        {
	        ptBraille=pn2.getPtPerkins();//.initialiseMap(pathTable,ptBraille);
        }
        catch (NumberFormatException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (IOException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }*/
	    ptBraille=pn2.getPtPerkins();
	    mapTables();
		//resultat.addKeyListener(this);//plus là, mais dans lors de l'activation de jcbPerkins
	    resultat.setEditorKit(new XmlEditorKit());
	    resultat.addFocusListener(this); //pour régler les histoires de focus dans ctrlF6
	
		//longueurLigne = taille;
		jsPage = new JSpinner(spinMod);
		
		JPanel pApercu = new JPanel();
		jspApercu = new JScrollPane (apercu);
		jspApercu.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jspApercu.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		scrollRes.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollRes.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		/*
		 * Page
		 */
		jsPage.addChangeListener(this);
		//jsPage.addKeyListener(this); don't work
		//http://stackoverflow.com/questions/3873870/java-keylistener-not-firing-on-jspinner/3873930
		//(JSpinner.DefaultEditor) jsPage.getEditor().getTextField().addKeyListener(this);
		Context cjsPage = new Context("","Spinner","pagespin",texts);
		jsPage.getAccessibleContext().setAccessibleName(cjsPage.getName());
		jsPage.getAccessibleContext().setAccessibleDescription(cjsPage.getDesc());
		jsPage.setToolTipText(texts.getText("pagespinttt"));
		jsPage.setEnabled(true);
		jsPage.setPreferredSize(new Dimension(50,30));
		
		ligneRes.setEditable(false); //ne font que de l'affichage
		jtfLigneApercu.setEditable(false);
		apercu.addCaretListener(this);
		apercu.addKeyListener(this); //pour gérer pgup et pgdown
		apercu.setEditable(false);
		//obligatoire si on veut un curseur apès editable false
		apercu.getCaret().setVisible(true);
		apercu.addFocusListener(this);
		JLabel lApercu = new JLabel("Fichier braille (non éditable)");
		lApercu.setLabelFor(apercu);
		lApercu.setDisplayedMnemonic('b');
		
		JLabel lMep = new JLabel("Fichier de mise en forme (éditable)");
		lMep.setLabelFor(resultat);
		lMep.setDisplayedMnemonic('m');
		
		/* fabrication commande */
		fabriqueCmd();

		/*
		 * Mise en page
		 */
		
		
		panneauAffichage.setLayout(new BorderLayout());
		panneauAffichage.add("North",lMep);
		panneauAffichage.add("Center",scrollRes);
		panneauAffichage.add("South",ligneRes);
		//panneauAffichage.add("East",pCmd);
		
		pApercu.setLayout(new BorderLayout());
		pApercu.add("North",lApercu);
		pApercu.add("Center",jspApercu);
		pApercu.add("South",jtfLigneApercu);
		
		/*
		 * Mise en page générale
		 */
		JPanel panneau = new JPanel();
		//taille min des composants du splitpane à initialiser à 0
		//pour pouvoir cacher les panneaux
		Dimension minimumSize = new Dimension(0, 0);
		panneauAffichage.setMinimumSize(minimumSize);
		pApercu.setMinimumSize(minimumSize);
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,panneauAffichage,pApercu);
		splitPane.setOneTouchExpandable(true);
		//splitPane.addFocusListener(this);
		panneau.setLayout(new BorderLayout());
		panneau.add("North",lFichier);
		//panneau.add("West",panneauAffichage);
		panneau.add("Center",splitPane);
		setContentPane(panneau);
		setPreferredSize(new Dimension(640,480));
		if(ConfigNat.getCurrentConfig().getMemoriserFenetre())
		{
			int x= ConfigNat.getCurrentConfig().getWidthEditeur();
			int y=ConfigNat.getCurrentConfig().getHeightEditeur();
			if(x+y != 0){setPreferredSize(new Dimension(x,y));}
			//System.out.println(Toolkit.getDefaultToolkit().isFrameStateSupported(Frame.MAXIMIZED_BOTH));
		}
		if(ConfigNat.getCurrentConfig().getCentrerFenetre())
		{
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension size = this.getPreferredSize();
			screenSize.height = screenSize.height/2;
			screenSize.width = screenSize.width/2;
			size.height = size.height/2;
			size.width = size.width/2;
			int y = screenSize.height - size.height;
			int x = screenSize.width - size.width;
			setLocation(x, y);
		}
		// ajout action pour saut de page
		/*InputMap inputMap = resultat.getInputMap();
        KeyStroke key1 = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.CTRL_MASK, false);
        inputMap.put(key1, DefaultEditorKit.defaultKeyTypedAction);*/
		resultat.addCaretListener(this);
		resultat.addMouseListener(this);

	    pack();
	}
	
	/**
	 * Fabrique le panneau conteannt les commandes pour l'édition du format MEP
	 * Initialise avec les bonnes polices les composants
     */
    private void fabriqueCmd()
    {
    	//fonte pour l'affichage des caractère spéciaux
		Font fonte;
        try
        {
            fonte = Font.createFont(Font.PLAIN, new File("ui/fontes/DejaVuSans.ttf"));
            fonte = fonte.deriveFont(Font.PLAIN, ConfigNat.getCurrentConfig().getTaillePolice());
        }
        catch (Exception e){fonte = new Font("DejaVu Sans",Font.PLAIN,ConfigNat.getCurrentConfig().getTaillePolice());}
	
        //police pour jtfresearch
        jtfResearch.setFont(fonte);
        jtfResearch2.setFont(fonte);
    	carSpec = TranscodeurNormal.donneCharNonUtilise(10, gestErreur,false);
    	
    	if(ConfigNat.getCurrentConfig().getShowIconText()){btMaj = new JButton(texts.getText("refresh"), new ImageIcon("ui/icon/view-refresh.png"));}
    	else{btMaj = new JButton(new ImageIcon("ui/icon/view-refresh.png"));}
    	Context cbtMaj = new Context ("","Button","refresh",texts);
    	btMaj.getAccessibleContext().setAccessibleName(cbtMaj.getName());
    	btMaj.getAccessibleContext().setAccessibleDescription(cbtMaj.getDesc());
    	btMaj.setToolTipText(cbtMaj.getTTT());
	    btMaj.addActionListener(this);
	    btMaj.setMnemonic(KeyEvent.VK_T);
	    
	    Context cbtInsCoup = new Context("i","Button","inshyphen",texts);
	    btInsCoup = new JButton(carSpec[TranscodeurNormal.CHAR_COUP]);
	    btInsCoup.getAccessibleContext().setAccessibleName(cbtInsCoup.getName());
	    btInsCoup.getAccessibleContext().setAccessibleDescription(cbtInsCoup.getDesc());
	    btInsCoup.setToolTipText(cbtInsCoup.getTTT());
	    btInsCoup.setFont(fonte);
	    btInsCoup.addActionListener(this);
	    btInsCoup.setMnemonic('i');
	    
	    Context cbtInsCoupEsth = new Context("j","Button","inshyphen2",texts);
	    btInsCoupEsth = new JButton(carSpec[TranscodeurNormal.CHAR_COUP_ESTH]);
	    btInsCoupEsth.setFont(fonte);
	    btInsCoupEsth.getAccessibleContext().setAccessibleName(cbtInsCoupEsth.getName());
	    btInsCoupEsth.getAccessibleContext().setAccessibleDescription(cbtInsCoupEsth.getDesc());
	    btInsCoupEsth.setToolTipText(cbtInsCoupEsth.getTTT());
	    btInsCoupEsth.addActionListener(this);
	    btInsCoupEsth.setMnemonic('j');
	    
	    if(ConfigNat.getCurrentConfig().getShowIconText()){ btEnregistrerCopie = new JButton(texts.getText("savecopy"),new ImageIcon("ui/icon/document-save-as.png"));}
		else{btEnregistrerCopie = new JButton(new ImageIcon("ui/icon/document-save-as.png"));}
	    btEnregistrerCopie.addActionListener(this);
		Context cbtEnregistrerCopy = new Context("n","Button","savecopy",texts);
		new ContextualHelp(btEnregistrerCopie,cbtEnregistrerCopy);
		btEnregistrerCopie.setMnemonic('n');
		
		if(ConfigNat.getCurrentConfig().getShowIconText()){ btChargerCopie = new JButton(texts.getText("loadcopy"),new ImageIcon("ui/icon/document-open.png"));}
		else{btChargerCopie = new JButton(new ImageIcon("ui/icon/document-open.png"));}
		btChargerCopie.addActionListener(this);
		Context cbtChargerCopie = new Context("o","Button","loadcopy",texts);
		new ContextualHelp(btChargerCopie,cbtChargerCopie);
		btChargerCopie.setMnemonic('o');
	    
	    //mise en forme
	    jlResearch.setLabelFor(jtfResearch);
		new ContextualHelp(jtfResearch,new Context("f","TextField","research",texts));
		jlResearch2.setLabelFor(jtfResearch2);
		new ContextualHelp(jtfResearch2,new Context("F","TextField","research2",texts));
		//addPerkinsObserver(jtfResearch);
		//jtfResearch.addKeyListener(pn2);
		jtfResearch.addKeyListener(this);
		jtfResearch2.addKeyListener(this);
		resultat.addKeyListener(this);
		
		
		jbResearch.setMnemonic('r');
		new ContextualHelp(jbResearch,new Context("r","TextField","researchbutton",texts));
		jbResearch.addActionListener(this);
		
		//jbResearch2.setMnemonic('r');
		new ContextualHelp(jbResearch2,new Context("R","TextField","researchbutton2",texts));
		jbResearch2.addActionListener(this);

		//jmFichier.add(jmiEnregistrer);
	    //jmFichier.add(jmiEnregistrerSous);
	    //jmFichier.addSeparator();
		
		jmiEnregistrerCopy= new JMenuItem(texts.getText("savecopy"),new ImageIcon("ui/icon/document-save-as.png"));
		jmiEnregistrerCopy.setMnemonic('n');
		jmiEnregistrerCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiEnregistrerCopy.addActionListener(this);
		
		jmiChargerCopy= new JMenuItem(texts.getText("loadcopy"),new ImageIcon("ui/icon/document-open.png"));
		jmiChargerCopy.setMnemonic('o');
		jmiChargerCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiChargerCopy.addActionListener(this);
	    
		jmiEnregistrerTranscription= new JMenuItem(texts.getText("savetrans"),new ImageIcon("ui/icon/accessories-dictionary.png"));
		//jmiEnregistrerTranscription.setMnemonic('n');
		//jmiEnregistrerTranscription.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiEnregistrerTranscription.addActionListener(this);
		
	    jmiMAJ= new JMenuItem(texts.getText("refresh"), new ImageIcon("ui/icon/view-refresh.png"));
	    //jmiMAJ.setMnemonic('s');
	    //jmiMAJ.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiMAJ.addActionListener(this);
	    jmiMAJ.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmFichier.add(jmiMAJ);
	    
	    jmFichier.add(jmiEmboss);
	    
	    jmFichier.addSeparator();
	    
	    jmFichier.add(jmiEnregistrerCopy);
	    jmFichier.add(jmiChargerCopy);
	    jmFichier.add(jmiEnregistrerTranscription);
	    
	    jmFichier.addSeparator();
	    
	    jmFichier.add(jmiQuitter);
	    
	    jmEdition.add(jmiUndo);
	    jmEdition.add(jmiRedo);
	    
	    jmEdition.addSeparator();
	    
	    jmiInsCoup = new JMenuItem(texts.getText("inshyphen"));
	    jmiInsCoup.setMnemonic('c');
	    jmiInsCoup.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiInsCoup.addActionListener(this);
	    jmEdition.add(jmiInsCoup);
	    jpmEdit.add(jmiInsCoup);
	    
	    jmiInsCoupE = new JMenuItem(texts.getText("inshyphen2"));
	    jmiInsCoupE.setMnemonic('e');
	    jmiInsCoupE.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_J, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiInsCoupE.addActionListener(this);
	    jmEdition.add(jmiInsCoupE);
	    jpmEdit.add(jmiInsCoupE);
	    
	    jmiInsStopCoup = new JMenuItem(texts.getText("insStopCoup"));
	    //jmiInsStopCoup.setMnemonic('e');
	    jmiInsStopCoup.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiInsStopCoup.addActionListener(this);
	    jmEdition.add(jmiInsStopCoup);
	    jpmEdit.add(jmiInsStopCoup);
	    
	    /*jmiInsSpace = new JMenuItem(texts.getText("insSpace"));
	    jmiInsSpace.setMnemonic('s');
	    jmiInsSpace.addActionListener(this);
	    jmEdition.add(jmiInsSpace);
	    jpmEdit.add(jmiInsSpace); A quoi ça sert ???*/
	    
	    jmiInsSpaceI = new JMenuItem(texts.getText("insSpaceIns"));
	    //jmiInsSpaceI.setMnemonic('i');
	    jmiInsSpaceI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiInsSpaceI.addActionListener(this);
	    jmEdition.add(jmiInsSpaceI);
	    jpmEdit.add(jmiInsSpaceI);
	    
	    
	    jmiInsSaut = new JMenuItem(texts.getText("insPageBreak"));
	    //jmiInsSaut.setMnemonic('p');
	    jmiInsSaut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiInsSaut.addActionListener(this);
	    jmEdition.add(jmiInsSaut);
	    jpmEdit.add(jmiInsSaut);
	    
	    jmiSplitPar = new JMenuItem(texts.getText("splitPar"));
	    //jmiSplitPar.setMnemonic('c');
	    jmiSplitPar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false));
	    jmiSplitPar.addActionListener(this);
	    jmEdition.add(jmiSplitPar);
	    jpmEdit.add(jmiSplitPar);
	    
	    jmEdition.addSeparator();
	    
	    jmEdition.add(jmiPerkins);
	    
	    jmEdition.addSeparator();
	    
	    jmiNext = new JMenuItem(texts.getText("search"));
	    jmiNext.setMnemonic('r');
	    jmiNext.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiNext.addActionListener(this);
	    jmEdition.add(jmiNext);
	    
	    jmiLegende = new JMenuItem(texts.getText("showLegend"));
	    jmiLegende.setMnemonic('l');
	    jmiLegende.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiLegende.addActionListener(this);
	    jmView.add(jmiLegende);
	    

	    jmiViewTagged = new JMenuItem(texts.getText("viewTagged"));
	    jmiViewTagged.setMnemonic('m');
	    jmiViewTagged.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiViewTagged.addActionListener(this);
	    jmView.add(jmiViewTagged);


	    jmiViewPlain = new JMenuItem(texts.getText("viewPlain"));
	    jmiViewPlain.setMnemonic('b');
	    jmiViewPlain.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiViewPlain.addActionListener(this);
	    jmView.add(jmiViewPlain);

	    
	    jmiNextPage = new JMenuItem(texts.getText("nextPage"));
	//    jmiNextPage.setMnemonic('+');
	    jmiNextPage.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiNextPage.addActionListener(this);
	    jmView.add(jmiNextPage);
	    
	    jmiPreviousPage = new JMenuItem(texts.getText("previousPage"));
	  //  jmiPreviousPage.setMnemonic('-');
	    jmiPreviousPage.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiPreviousPage.addActionListener(this);
	    jmView.add(jmiPreviousPage);
		
	    jmiPageNumber = new JMenuItem(texts.getText("pageNumber"));
	  //  jmiPageNumber.setMnemonic('-');
	    jmiPageNumber.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiPageNumber.addActionListener(this);
	    jmView.add(jmiPageNumber);
	    
	    //tool bars
	    //fichiers
		JToolBar jtbF = new JToolBar();
		jtbF.setLayout(new GridBagLayout());
		//edition
		JToolBar jtbE = new JToolBar();
		jtbE.setLayout(new GridBagLayout());
		//recherche à gauche
		JToolBar jtbR = new JToolBar();
		jtbR.setLayout(new GridBagLayout());
		//recherche à droite
		JToolBar jtbR2 = new JToolBar();
		jtbR2.setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0;
		c.gridy=0;
		//c.anchor=GridBagConstraints.LINE_START;
		c.fill=GridBagConstraints.VERTICAL;
		

		//fabrication tool bar
		c.gridwidth=1;
		
		c.gridx=0;
		jtbR.add(jlResearch,c);
		c.gridx++;
		jtbR.add(jtfResearch,c);
		jtfResearch.setMinimumSize(new Dimension(80,10));
		c.gridx++;
		jtbR.add(jbResearch,c);
		jmb.add(jtbR);
		
		c.gridx=0;
		jtbF.add(btMaj,c);
		/*c.gridx++;
		jtbF.add(btEnregistrer,c);
		c.gridx++;
		jtbF.add(btEnregistrersous,c);*/
		c.gridx++;
		jtbF.add(btEmbosser,c);
		c.gridx++;
		jtbF.add(btChargerCopie,c);
		c.gridx++;
		jtbF.add(btEnregistrerCopie,c);
		c.gridx++;
		jmb.add(jtbF);
		
		c.gridx=0;
		jtbR2.add(jlResearch2,c);
		c.gridx++;
		jtbR2.add(jtfResearch2,c);
		jtfResearch2.setMinimumSize(new Dimension(80,10));
		c.gridx++;
		jtbR2.add(jbResearch2,c);
		jmb.add(jtbR2);
		
		c.gridx=0;
		jtbE.add(jcbPerkins,c);
		c.gridx++;
		jtbE.add(btInsCoup,c);
		c.gridx++;
		jtbE.add(btInsCoupEsth,c);
		c.gridx++;
		jtbE.add(btUndo,c);
		c.gridx++;
		jtbE.add(btRedo,c);
		c.gridx++;
		jtbE.add(jsPage,c);
		jmb.add(jtbE);
		
    }

	/** Méthode d'accès, rend visible les lignes secondaires de l'éditeur 
	 * @param b affiche {@link #ligneRes} et {@link #jtfLigneApercu} si true*/
	public void setAfficheLigneSecondaire(boolean b)
	{
		ligneRes.setVisible(b);
		jtfLigneApercu.setVisible(b);
	}
	
	/**
	 * Affiche le fichier dans le JTextPane resultat et configure les lignes secondaires
	 * @param nomFichier nom du fichier transcrit
	 * @param police police principale
	 * @param taillePolice taille de la police principale
	 * @param police2 police secondaire
	 * @param taillePolice2 taille de la police secondaire
	 */
	public void afficheFichier(String nomFichier,String police, int taillePolice, String police2, int taillePolice2)
	{
		afficheFichier(nomFichier, police, taillePolice);
		
	    Font f = new Font(police2, Font.PLAIN, taillePolice2);
	    jtfLigneApercu.setFont(f);
		
		Font fonteMep;
    	try
    	{
    		fonteMep = Font.createFont(Font.PLAIN, new File("ui/fontes/DejaVuSans.ttf"));
    		fonteMep = fonteMep.deriveFont(Font.PLAIN, 1 + (int)(ConfigNat.getCurrentConfig().getTaillePolice()*0.8));
		}
    	catch (Exception e)
    	{
    		System.err.println(texts.getText("unknownpolice"));
    		fonteMep = new Font("DejaVu Sans",Font.PLAIN,1 + (int)(ConfigNat.getCurrentConfig().getTaillePolice()*0.8));
	        	
    	}
    	//the font of the secondary line in apercu must always be dejavu because
    	//if user select a braille font for secondary line, all will be braille
    	//since apercu already has brailleUTF8 as braille table which is always Braille
    	ligneRes.setFont(fonteMep);
	}
	
	/**
	 * Affiche le fichier dans le JTextPane
	 * @param nomFichier nom du fichier transcrit
	 * @param police police principale
	 * @param taillePolice taille de la police principale
	 */
	public void afficheFichier(String nomFichier,String police, int taillePolice)
    {
		setTitle("NAT " + nomFichier);
	    //setFichier(nomFichier);
	    //lFichier.setText(lFichier.getText() + getFichier());
		
	    //styles pour le fichier xml
	    StyledDocument docMep = resultat.getStyledDocument();
	    
	    //affichage du fichier braille
	    majFichierBraille(police, taillePolice);
	    
	    MutableAttributeSet attrsMep = resultat.getInputAttributes();
	    Font fonteMep;
	    Font fonteModMep;
    	try
    	{
    		fonteMep = Font.createFont(Font.PLAIN, new File("ui/fontes/DejaVuSans.ttf"));
    		fonteMep = fonteMep.deriveFont(Font.PLAIN, 1 + (int)(ConfigNat.getCurrentConfig().getTaillePolice()*0.8));
    		fonteModMep = Font.createFont(Font.PLAIN, new File("ui/fontes/DejaVuSans.ttf"));
    		fonteModMep = fonteMep.deriveFont(Font.PLAIN, ConfigNat.getCurrentConfig().getTaillePolice());
		}
    	catch (Exception e)
    	{
    		System.err.println(texts.getText("unknownpolice"));
    		fonteMep = new Font("DejaVu Sans",Font.PLAIN,1 + (int)(ConfigNat.getCurrentConfig().getTaillePolice()*0.8));
    		fonteModMep = new Font("DejaVu Sans",Font.PLAIN,ConfigNat.getCurrentConfig().getTaillePolice());
	        	
    	}
	    StyleConstants.setFontFamily(attrsMep, fonteMep.getFamily());
	    StyleConstants.setFontSize(attrsMep,taillePolice);
	    
	    // important for the XmlEditorKit
	    resultat.setFont(fonteModMep);
	    
		this.pack();
		//System.err.println( fonteBraille.getSize() + " " + taille );;
		//resultat.setText("");
	    
	    //fichier mise en page
	    try
	    {
		    BufferedReader rafMep = new BufferedReader(new InputStreamReader(new FileInputStream(fichMep),"UTF-8"));
	    	String ligne;
	    	StringBuffer resu = new StringBuffer();
	    	//pages.add(new DefaultStyledDocument());
	    	while ( (ligne = rafMep.readLine()) != null )
	    	{
	    		resu.append(ligne+"\n");
	    		/*String decoup [] = ligne.split("[<>]");
	    		if(decoup.length==5)
	    		{
	    			//il s'agit bien d'un tag
	    			
	    		}
	    		else
	    		{
	    			StyleConstants.setFontFamily(attrsMep, fonteMep.getFamily());
	    		    StyleConstants.setFontSize(attrsMep,taillePolice);
	    			//docMep.insertString(docMep.getLength(), ligne + "\n", attrsMep);
	    		}*/
	    	}
	    	docMep.insertString(0, resu.toString(), attrsMep);
	    	//resultat.setText(resu.toString());
	    	/*docMep.insertString(0,resu.toString());
	    	resultat.setDocument(docMep);*/
	    	rafMep.close();
	     }
	    catch (BadLocationException ble){gestErreur.afficheMessage(texts.getText("cantdisplaytxt"),Nat.LOG_SILENCIEUX);}
	    catch (IOException e){gestErreur.afficheMessage(texts.getText("bugin")+": " + e,Nat.LOG_SILENCIEUX);}

		//bru initialiseMap();
		/*
		 * test de la map perkins pour la saisie braille
		for(int i =0; i<ptPerkins.size();i++)
		{
			System.err.println(i +" " + ptPerkins.get(Integer.toString(i)));
		}*/
		ajouteListenerDoc(docMep);
		spinMod.setMaximum(nbPagesBr);
		//super.ajouteListenerDoc(pages.get(0));
		splitPane.setDividerLocation(ConfigNat.getCurrentConfig().getSplitPositionEditor());
		apercu.grabFocus();
    }
	
	/** 
	 * met à jour le panneau {@link #apercu} avec le contenu du fichier {@link EditeurBraille#getFichier()}
	 * @param police police braille à utiliser pour l'affichage
	 * @param taillePolice taille de la police
	 */
	private void majFichierBraille(String police, int taillePolice)
	{
		//Préparation des styles pour le JTextPane
	    StyledDocument doc = apercu.getStyledDocument();
	    //pour éviter les problèmes avec les caret listener
	    //1) on mémorise l'ancienne position
	    int oldPos = apercu.getCaretPosition();
	    positionCurseur = 0;
	    try
        {
	        apercu.getDocument().remove(0, apercu.getDocument().getLength());
        }
        catch (BadLocationException e1)
        {
	        e1.printStackTrace();
        }
	    
	    MutableAttributeSet attrs = apercu.getInputAttributes();
	    
	    Font fonteBraille = new Font(police, Font.PLAIN, taillePolice);
	    
	    StyleConstants.setFontFamily(attrs, fonteBraille.getFamily());
	    StyleConstants.setFontSize(attrs,taillePolice);
	    
	    Font fonteInfo;
    	try
    	{
    		fonteInfo = Font.createFont(Font.PLAIN, new File("ui/fontes/DejaVuSans.ttf"));
    		fonteInfo = fonteInfo.deriveFont(Font.PLAIN, ConfigNat.getCurrentConfig().getTaillePolice());
		}
    	catch (Exception e)
    	{
    		System.err.println(texts.getText("unknownpolice"));
    		fonteInfo = new Font("DejaVu Sans",Font.PLAIN,ConfigNat.getCurrentConfig().getTaillePolice());
    	}
	    
	    FontMetrics fm = getFontMetrics(fonteBraille);
	    tailleCaractere =  fm.charWidth('a');
		
	    String contenu = FileToolKit.loadFileToStr(getFichier(), encodage);
	    String [] pages = contenu.split(""+(char)12);
	    //System.out.println(System.getProperty("line.separator").length());
	    try
	    {
	    	//on supprime les éventuelles anciennes pages 
		    lesPages.clear();
		    //Fichier sans char 12? (possible si pas de mise en page)
		    
		    if(pages.length<2)//une seule page, ou pas de saut de page final
		    {
		    	pages = new String[1];
		    	pages[0]=contenu;
		    	lesPages.add(pages[0]);
		    	StyleConstants.setFontFamily(attrs, fonteInfo.getFamily());
				doc.insertString(doc.getLength(), "--- page "+ (nbPagesBr+1) +" ----\n", attrs);
		    	StyleConstants.setFontFamily(attrs, fonteBraille.getFamily());
				doc.insertString(doc.getLength(), pages[0], attrs);	
		    	nbPagesBr=1;
		    }
		    else
		    {
			    for(int i=0;i<pages.length;i++)
			    {
			    	String s = pages[i];
			    	lesPages.add(s);
			    	StyleConstants.setFontFamily(attrs, fonteInfo.getFamily());
					doc.insertString(doc.getLength(), "--- page "+ (nbPagesBr+1) +" ----\n", attrs);
			    	StyleConstants.setFontFamily(attrs, fonteBraille.getFamily());
					doc.insertString(doc.getLength(), s, attrs);	
					nbPagesBr++;
			    }
		    }
	    }
	    catch (BadLocationException ble){gestErreur.afficheMessage(texts.getText("cantdisplaytxt"),Nat.LOG_SILENCIEUX);}
	    
		apercu.setDocument(doc);
	    
		//on remet le curseur à peu près là où il était
	    if(oldPos < apercu.getDocument().getLength()){apercu.setCaretPosition(oldPos);}
	    else {apercu.setCaretPosition(apercu.getDocument().getLength());}
	}
	
	/**
	 * Renvoie le texte contenu dans les pages du document à mettre en forme
	 * @return Le texte contenu dans les pages du document à mettre en forme
	 */
	@Override
	public String getText()
	{
		return resultat.getText();
	}
	
	/**
	 * Renvoie le texte braille contenu dans les pages du tableau p
	 * @param p tableau des pages à traiter
	 * @return le texte contenu dans les pages du tableau p
	 */
	private String getText(boolean[] p)
	{		
		StringBuffer sauv = new StringBuffer();
		for (int i=0;i<p.length;i++)
		{
			if(p[i])
			{
				//si "interligne double" activé
				String s = lesPages.get(i);
				if(ConfigNat.getCurrentConfig().getDoubleSpace()){s=s.replaceAll("\n", "\n\n");}
				sauv.append(s+(char)12);
			}
		}
		//System.out.println(sauv);
		return sauv.toString();
	}
	
	/**
	 * Indique si l'éditeur doit être uniquement un previewer.
	 * Active ou désactive les composants d'édition suivant le cas.
	 * Mets à jour {@link #preview}
	 * @param p true si mode preview uniquement
	 */
	public void setPreview(boolean p)
	{
		preview = p;
		if(preview)
		{
			btEnregistrerCopie.setEnabled(false);
			btInsCoup.setEnabled(false);
			btInsCoupEsth.setEnabled(false);
			btInsSpace.setEnabled(false);
			btInsSpaceI.setEnabled(false);
			btMaj.setEnabled(false);
			btPageBreak.setEnabled(false);
			btSplitPar.setEnabled(false);
			jpmEdit.setEnabled(false);
			resultat.setEnabled(false);
			splitPane.setDividerLocation(0.0);
		}
		else
		{
			//activation des composants
			btEnregistrerCopie.setEnabled(true);
			btInsCoup.setEnabled(true);
			btInsCoupEsth.setEnabled(true);
			btInsSpace.setEnabled(true);
			btInsSpaceI.setEnabled(true);
			btMaj.setEnabled(true);
			btPageBreak.setEnabled(true);
			btSplitPar.setEnabled(true);
			jpmEdit.setEnabled(true);
			resultat.setEnabled(true);
			splitPane.setDividerLocation(ConfigNat.getCurrentConfig().getSplitPositionEditor());
		}
	}
	
	/**
	 * @return the progress
	 */
    public int getTheProgress(){return progress;}

	/**
	 * @return the progressBar
	 */
    //public JProgressBar getProgressBar(){return progressBar;}

	
	/** Affiche la page newPage dans le JTextPane resultat  
	 *  
	 */
	//@param newPage le numéro de la page à afficher (commence à 1)
	//@param debut vrai si il faut placer le curseur au début du texte
	private void changePage()
	{
		String search = "page "+pageActu+" ";
		//souci selon Linux ou windows de différence de newlines
		//qui ont 1 ou deux chars
		//System.out.println(System.getProperty("line.separator").length());
		int pos = apercu.getText().indexOf(search);
		//obligé à cause de ce putain de char de newline qui change
		//et on veut éviter getcurrentconfig
		int newPos = 0;
		if(pos>-1)
		{
			int nblignes = apercu.getText().substring(0, pos).split("\n").length-1;
			int longNewLine = System.getProperty("line.separator").length();
			newPos=pos+search.length()+5-((longNewLine-1)*nblignes);
		}
		else
		{
			pageActu = 1;
			jsPage.getModel().setValue(1);
		}
		apercu.setCaretPosition(newPos);
		
		/*OBSOLETE car on remplace les newline par un seul char pour pas avoir de diff dans les 
		 * positions de chars sous linux ou windaube
		 * if(pos>-1){
			indexOf ne prend pas en compte les saut de ligne
			alors que setCaretPosition si, le con. D'où le calcul savant :
			le 5 c'est pour ---- dans "page x ----"
			newPos=pos+5+search.length()-(ConfigNat.getCurrentConfig().getNbLigne()+1)*(pageActu-1);
			}
		apercu.setCaretPosition(newPos);
		System.out.println("pos "+pos+" newpos "+newPos);*/
	}
	
	/** écrit les maps de correspondance {@link #mapUTF8_Table}*/  
	private void mapTables()
	{
		HashMap<String,String> ptPerkins = pn3.getPtPerkins();
		for(String k:ptPerkins.keySet())
		{
			String c = ptPerkins.get(k);
			String c8 = ptBraille.get(k);
			mapUTF8_Table.put(c8, c);
			map_Table.put(c, c8);
			//System.out.println(c+":"+c8);
		}
	}
	
	/**
	 * Mise à jour des lignes secondaires
	 */
	private void majLigneRes()
	{
		JTextPane jtp = apercu;//le jtp qui a le focus
		// la ligne à modifier
		JTextField ligne = jtfLigneApercu;
		if(resultat.hasFocus())
		{
			jtp=resultat;
			ligne = ligneRes;
		}
		if(ligne.isVisible())
		{
			int posCurseur = jtp.getCaretPosition();//.getDot();
			//recherche du début de ligne
			/*int debut = posCurseur-1;
			if(debut<0){debut = 0;}*/
			int fin = posCurseur;
			boolean trouve = false;
			try 
			{
				/*while (!trouve && debut > 0)
				{
					if (!(resultat.getText(debut, 1).compareTo("\n")==0)){debut--;}
					else{trouve = true;debut++;}
				}
				trouve = false;*/
				while (!trouve && fin < jtp.getDocument().getLength())
				{
					if (!(jtp.getText(fin, 1).compareTo("\n")==0)){fin++;}
					else{trouve = true;fin--;}
				}
				//calcul du début de la ligne à représenter
				//debut = posCurseur - (posCurseur - debut)%longueurLigne;
				//ligne ci-dessous : le *1.5 est empirique car impossible de connaître le
				//nombre de chars du jtextfield (getcolumns marche pas car non initialisé)
				int longueur = (int) StrictMath.ceil(ligne.getWidth()*1.5/ligne.getFont().getSize2D());
				if ((posCurseur+longueur+1)<fin)
				{
					ligne.setText(jtp.getText(posCurseur, longueur+1));
				}
				else
				{
				ligne.setText(jtp.getText(posCurseur, fin-posCurseur+1));//fin - debut + 1));
				}
				//ligne.setHorizontalAlignment(JTextField.TRAILING);
				ligne.setCaretPosition(0);
			} 
			catch (BadLocationException e){e.printStackTrace();}		
		}
		if(ligne == ligneRes)
		{
			//conversion des points braille dans la table utilisateur
			String s = ligneRes.getText();
			for(int i=0; i<s.length();i++)
			{
				String c = ""+s.charAt(i);
				String replace = mapUTF8_Table.get(c);
				if(replace!=null)
				{
					s = s.replaceAll(c, replace);
				}
			}
			ligneRes.setText(s);
		}
	}
	
	/** 
 	 * Build an display a JFrame instance which contains the graphical component for displaying the search progress, and set up these components
	 * @param max the max size for the progress bar
	 * @return The JFrame instance for progress algorithm
 	 * 
	 */
	
	private ProgressTask showSearchProgressFrame(int max)
	{
		//prepare components
		progressBar = new JProgressBar(0,max);
		progressBar.setStringPainted(true);
		
		JFrame cadre = new JFrame(texts.getText("progressFrameName"));
		JPanel panneau = new JPanel();
		panneau.setLayout(new BorderLayout());
		panneau.add(progressLabel, BorderLayout.NORTH);
		panneau.add(progressBar,BorderLayout.CENTER);
		cadre.setContentPane(panneau);
		cadre.setSize(200,100);
		cadre.pack();
		cadre.setLocation((this.getX()+this.getWidth())/2, (this.getY()+this.getHeight())/2);
		cadre.setVisible(true);
		cadre.setResizable(false);
		//cadre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // comportement par defaut
		
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        //Instances of javax.swing.SwingWorker are not reusuable, so
        //we create new instances as needed.
        return new ProgressTask(max, cadre);
	}
	
	/** Méthode redéfinie de ComponentListener (héritée de EditeurBaille)
	 * Mis à jour de l'affichage lors du redimensionement
	 * @param arg0 Le ComponentEvent
	 */
	@Override
	public void componentResized(ComponentEvent arg0)
	{
		//JOptionPane.showMessageDialog(this, "maximized");
		if (getExtendedState()==Frame.MAXIMIZED_BOTH)
		{
			ConfigNat.getCurrentConfig().setMaximizedEditeur(true);
			//JOptionPane.showMessageDialog(this, "maximized");
		}
		else
		{
			ConfigNat.getCurrentConfig().setWidthEditeur(getWidth());
			ConfigNat.getCurrentConfig().setHeightEditeur(getHeight());
			ConfigNat.getCurrentConfig().setMaximizedEditeur(false);
			//JOptionPane.showMessageDialog(this, "minimized");
		}
		repaint();
	}
	
	
	/** Méthode redéfinie de KeyListener
	 * @param e L'objet KeyEvent intercepté
	 */
	@Override
    public void keyReleased(KeyEvent e)
	{
		/*int keyCode = e.getKeyCode();
		int kc2 = e.getExtendedKeyCode();
        String keyText = KeyEvent.getKeyText(keyCode);
        System.out.println(" touche : " + keyText + " / " + keyCode+"/"+kc2);*/
		
		//System.err.println("source "+e.getSource());
		
		if(e.getKeyCode() == KeyEvent.VK_ENTER && jtfResearch.hasFocus())
		{
			//jbResearch.doClick();
			research(resultat,resultat.getCaretPosition());
			resultat.requestFocusInWindow();
		}
		else if(e.getKeyCode() == KeyEvent.VK_ENTER && jtfResearch2.hasFocus())
		{
			//jbResearch.doClick();
			research(apercu,apercu.getCaretPosition());
			apercu.requestFocusInWindow();
		}
		else if(e.getSource()==resultat && e.getKeyCode()==KeyEvent.VK_CONTEXT_MENU)
		{
			//System.out.println("ouioui");
			jpmEdit.setVisible(true);
		}
		else if(e.getKeyCode() == KeyEvent.VK_ENTER && e.getSource()==jsPage)
		{
			//don't work because spinners don't fire key events 
			apercu.requestFocusInWindow();
		}
		else if(e.getKeyCode() == KeyEvent.VK_PAGE_DOWN && e.getSource()==apercu)
		{
			if (e.getModifiersEx()==InputEvent.CTRL_DOWN_MASK)
				{jsPage.getModel().setValue(pageActu+1);}
		}
		else if(e.getKeyCode() == KeyEvent.VK_PAGE_UP && e.getSource()==apercu)
		{
			if (e.getModifiersEx()==InputEvent.CTRL_DOWN_MASK)
				{jsPage.getModel().setValue(pageActu-1);}
		}
		else if(e.getKeyCode() == KeyEvent.VK_HOME && e.getSource()==apercu)
		{
			if (e.getModifiersEx()==InputEvent.CTRL_DOWN_MASK)
				{jsPage.getModel().setValue(1);}
		}
		else if (e.getKeyCode() == KeyEvent.VK_SPACE && e.getSource()==resultat)
		{
			if (e.getModifiersEx()==InputEvent.CTRL_DOWN_MASK)
			{
				insertString(carSpec[TranscodeurNormal.CHAR_SPACE_UNBREAKABLE]);
			}
		}
		else if (e.getKeyCode() == KeyEvent.VK_I && e.getSource()==resultat)
		{
			if (e.getModifiersEx()==InputEvent.CTRL_DOWN_MASK)
			{
				insertString(carSpec[TranscodeurNormal.CHAR_COUP]);
			}
		}
		else if (e.getKeyCode() == KeyEvent.VK_J && e.getSource()==resultat)
		{
			if (e.getModifiersEx()==InputEvent.CTRL_DOWN_MASK)
			{
				insertString(carSpec[TranscodeurNormal.CHAR_COUP_ESTH]);
			}
		}
		else if (e.getKeyCode() == KeyEvent.VK_W && e.getSource()==resultat)
		{
			if (e.getModifiersEx()==InputEvent.CTRL_DOWN_MASK)
			{
				insertString(carSpec[TranscodeurNormal.CHAR_STOP_COUP]);
			}
		}
		else if(e.getKeyCode() == KeyEvent.VK_F6 && e.getSource()==apercu)
		{ //c'est le super raccourci qui permet de chercher un texte et de mettre le focus
			//dessus dans la fenêtre d'édition
			String selTxt = apercu.getSelectedText();
			//System.err.println(selTxt);
			if (e.getModifiersEx()==InputEvent.CTRL_DOWN_MASK)
				{
					boolean perk = jcbPerkins.isSelected(); //mémorisation du perkins
					if (perk)
					{
						jcbPerkins.setSelected(false); //la recherche marche pas si perkins
						pn.removePerkinsObserver(resultat) ;
						pn2.removePerkinsObserver(jtfResearch);
						pn.removeAllPerkinsObserver();
						pn2.removeAllPerkinsObserver();
					}
					jtfResearch.setText(selTxt);
					//on compte combien de fois ce texte apparaît avant
					//en enlevant les -- page -- car ça compte !
					//IMPOSSIBLE A FAIRE pour les numéros de page en braille
					String txt="";
					try {txt = apercu.getDocument().getText(0,apercu.getSelectionEnd()).replaceAll("--- page [0-9]+ ----","");}
					catch (BadLocationException e1) {gestErreur.afficheMessage(texts.getText("badLocationError")+"\n"+e1.getMessage(), Nat.LOG_SILENCIEUX);}
					
					int nbClicks = StringUtils.countMatches(txt, selTxt);
					//System.err.println("niktxt"+selTxt+" "+txt);
					if (splitPane.getDividerLocation()==splitPane.getMinimumDividerLocation())
					{
						jmiShowLeft.doClick();
					}
					resultat.setCaretPosition(1);
					//JLabel texte = new JLabel ("0 / "+nbClicks);
					//JFrame progress = fenetreProgressionRecherche ("Recherche en cours "+nbClicks, texte);
					//selTxt = map2UTF8 (selTxt);
					progress = 0;
					progressTask = showSearchProgressFrame(nbClicks);
					progressTask.initTask(perk);
					progressTask.addPropertyChangeListener(this);
					progressTask.execute();
					//progressTask.done(); fait automatiquement
					//resultat.requestFocusInWindow(); done in progresstask.done()
					//jcbPerkins.setSelected(perk); done in progresstask.done()
				}
		}
		/*
		{
			super.keyReleased(e);
			if(e.getSource()==jtfResearch&&res!=0)
			{
				jtfResearch.setText(jtfResearch.getText()+ptPerkins.get(Integer.toString(res)));
				res=0;
			}
			
		}*/
		
	}
	
	/** Méthode redéfinie de CaretListener
	 * Si ligne secondaire (ligneRes) présente, charge le contenu de la ligne de résultat dans
	 * la ligne secondaire, avec le curseur en position
	 * Vérifie la position du curseur dans résultat
	 * @param ce Le CaretEvent
	 */
	@Override
    public void caretUpdate(CaretEvent ce) 
	{
		JTextPane src = (JTextPane)ce.getSource();
		if(src.getCaretPosition()==0)
		{
			//maj de la page => positionCurseur >0
			if(positionCurseur>0)
			{
				if(positionCurseur<=src.getDocument().getLength()){src.setCaretPosition(positionCurseur);}
				//else{src.setCaretPosition(src.getDocument().getLength());}//sécurité
			}
			positionCurseur=src.getCaretPosition();
		}
		majLigneRes();
		//System.err.println("posCurAP="+resultat.getCaretPosition()+";pos="+positionCurseur);
	}

	/**
	 * Redéfinie stateChanged de ChangeListener (héritée de EditeurBaille)
	 * Change la page courante
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	@Override
    public void stateChanged(ChangeEvent ce) 
	{
		//System.err.println("state changed");
		if(ce.getSource()==jsPage)
		{
			pageActu = ((Integer)jsPage.getValue()).intValue();
			changePage();
		}
		
	}
	/** 
	 * Implémente la méthode actionPerformed d'ActionListener (héritée de EditeurBaille)
	 * Gère les actions des boutons et met à jour l'InputMap du JTextPane resultat en fonction de
	 * l'état du JCheckBox jcbPerkins
	 * @param evt l'objet ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent evt)
	{
		super.actionPerformed(evt);
		if(evt.getSource()==jcbPerkins)
		{
			//JOptionPane.showMessageDialog(this, "blaa");
			if(jcbPerkins.isSelected()){
				pn2.addPerkinsObserver(jtfResearch);
				pn2b.addPerkinsObserver(jtfResearch2);
				//pn.addPerkinsObserver(resultat); //fait dans editeurbraille
				//JOptionPane.showMessageDialog(this, "perk activated");
				}
			else{
				pn2.removePerkinsObserver(jtfResearch);
				pn2b.removePerkinsObserver(jtfResearch2);
				//JOptionPane.showMessageDialog(this, "no perk");
				//pn.removePerkinsObserver(resultat);// fait dans editeurbraille
				}
		}
		if (evt.getSource()==btEmbosser || evt.getSource()==jmiEmboss)
		{
			gestErreur.afficheMessage("Embosseur de type: "+embosseur.getClass().getName(), Nat.LOG_DEBUG);
			String table1 = ConfigNat.getCurrentConfig().getTableBraille();
			String table2 = ConfigNat.getCurrentConfig().getTableEmbossage();
			if(ConfigNat.getCurrentConfig().getIsSysTable())
			{
			 table1 = "xsl/tablesBraille/"+table1;
			}
			else
			{
			 table1 =  ConfigNat.getUserTempFolder()+"tablesBraille/"+table1;
			}
			if(ConfigNat.getCurrentConfig().getIsSysEmbossTable())
			{
			 table2 = "xsl/tablesEmbosseuse/"+table2;
			}
			else
			{
			 table2 =  ConfigNat.getUserTempFolder()+"tablesEmbosseuse/"+table2;
			}
			/* quelles pages imprimer?*/
			boolean[]p = new boolean[nbPagesBr];
			System.out.println("**** nbpagebr "+nbPagesBr+" ****");
			Embossage emb = new Embossage(p,pageActu);
			new DialogueEmbossage(this,emb);
				
			boolean emboss = true;
			for(int i=0;i<p.length;i++)
			{
				emboss = emboss && !p[i];
				//System.out.println(i+ " :"+p[i]);
			}
			if (!emboss)//emboss est faux si il y a au moins une page à embosser
			{
				String fichierTemp = ConfigNat.getUserTempFolder()+"sourceText.txt" ;
				String fichImp = ConfigNat.getUserTempFolder()+"tmpImp.txt";
				FileToolKit.saveStrToFile (getText(p), fichierTemp);
				//System.out.println(table1 + " " +table2);
				FileToolKit.convertBrailleFile(fichierTemp, fichImp,table1,table2,embosseur.getGest());
				for(int i=0; i<emb.getNbExemplaires();i++){embosseur.Embosser(fichImp);}
			}
			
				
			/*}
			else{embosseur.Embosser(fichier);}*/
		}
		else if(evt.getSource()==btMaj || evt.getSource()==jmiMAJ)
		{
			setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			//enregistrement du fichier
			//sélection du fichier à enregistrer
			setFichier(fichierMEP);
			enregistrerFichier("UTF-8");
			setFichier(ConfigNat.getCurrentConfig().getFBraille());
			nbPagesBr = 0;
			Presentateur p = new PresentateurMEP(gestErreur, ConfigNat.getCurrentConfig().getBrailleEncoding(), 
					Transcription.fTempXML2, getFichier(), ConfigNat.getCurrentConfig().getTableBraille());
			if(p.presenter())
			{
				majFichierBraille(ConfigNat.getCurrentConfig().getPoliceEditeur(),ConfigNat.getCurrentConfig().getTaillePolice());
			}
			else
			{
				apercu.setText("Erreur");
			}

		    setCursor(Cursor.getDefaultCursor());
		}
		else if(evt.getSource()==btInsCoup || evt.getSource()==jmiInsCoup){insertString(carSpec[TranscodeurNormal.CHAR_COUP]);}
		else if(evt.getSource()==btInsCoupEsth || evt.getSource()==jmiInsCoupE){insertString(carSpec[TranscodeurNormal.CHAR_COUP_ESTH]);}
		else if(evt.getSource()==btInsSpace || evt.getSource()==jmiInsSpace){insertString(carSpec[TranscodeurNormal.CHAR_SPACE]);}
		else if(evt.getSource()==btInsSpaceI || evt.getSource()==jmiInsSpaceI){insertString(carSpec[TranscodeurNormal.CHAR_SPACE_UNBREAKABLE]);}
		else if(evt.getSource()==jmiInsStopCoup){insertString(carSpec[TranscodeurNormal.CHAR_STOP_COUP]);}
		else if(evt.getSource()==btPageBreak || evt.getSource()==jmiInsSaut){insertpageBreak();}
		else if(evt.getSource()==btSplitPar || evt.getSource()==jmiSplitPar){split();}
		else if(evt.getSource()==jbResearch || evt.getSource()==jmiNext){research(resultat,resultat.getCaretPosition());resultat.requestFocusInWindow();}
		else if(evt.getSource()==jbResearch2){research(apercu,apercu.getCaretPosition());apercu.requestFocusInWindow();}
		else if(evt.getSource()==jmiLegende){new DialogueLegende(this,carSpec);}
		else if(evt.getSource()==jmiChargerCopy || evt.getSource()==btChargerCopie)
		{
			chargerCopie();
		}
		else if(evt.getSource()==jmiEnregistrerCopy || evt.getSource()==btEnregistrerCopie)
		{
			enregistrerCopie();
		}
		else if(evt.getSource()==jmiEnregistrerTranscription)
		{
			//JOptionPane.showMessageDialog(this, ConfigNat.getCurrentConfig().getFichNoir());
			archiveTrans();
		}
		else if(evt.getSource()==jmiNextPage)
		{
			jsPage.getModel().setValue(pageActu+1);
			//changePage();
		}
		else if(evt.getSource()==jmiPreviousPage)
		{
			jsPage.getModel().setValue(pageActu-1);
			//changePage();
		}
		else if(evt.getSource()==jmiPageNumber){
			//jsPage.requestFocus();
			jsPage.requestFocusInWindow();
		}
		else if(evt.getSource()==jmiViewTagged)
		{
			jmiShowLeft.doClick();
			resultat.requestFocusInWindow();
		}
		else if(evt.getSource()==jmiViewPlain){
			jmiShowRight.doClick();
			apercu.requestFocusInWindow();
		}
	}
	
	/**
	 * archivage d'une transcription avec tous les fichiers
	 */
	
	private void archiveTrans()
	{
		JFileChooser jfc = new JFileChooser();
		String fat = ConfigNat.getCurrentConfig().getFichNoir();
		//System.err.println(fat.substring(0,fat.lastIndexOf(".")+1)+ConfigNat.natTransArchive);
		File f_fat = new File (fat.substring(0,fat.lastIndexOf(".")+1)+ConfigNat.natTransArchive);
		jfc.setSelectedFile(f_fat);
	    jfc.setCurrentDirectory(new File(f_fat.getParent()));
		// ajout des filtres au JFileChooser
	    FiltreFichier natbFilter = new FiltreFichier( new String[]{ConfigNat.natTransArchive}, "Archive NatBraille (*."+ConfigNat.natTransArchive+")");
		jfc.addChoosableFileFilter(natbFilter);
		jfc.setFileFilter(natbFilter);
		jfc.setDialogTitle(texts.getText("archiveTransFileChooserTitle"));
 		if (!(jfc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION))
 			{return;}
 		
 		try {
			FileToolKit.archiveTranscription(jfc.getSelectedFile().getCanonicalPath(), ConfigNat.getCurrentConfig().getFichNoir(), ConfigNat.getCurrentConfig().getFBraille());
		} catch (IOException e) {
			gestErreur.afficheMessage(texts.getText("bugarchive")+" : " + e.getLocalizedMessage(),Nat.LOG_SILENCIEUX);
			e.printStackTrace();
		}
	}
	
	/**
	 * Charge un fichier de mise en page et un fichier braille
	 * Si le fichier de mise en page n'existe pas, passe en mode preview
	 */
	public void chargerCopie()
	{
		//sélection du fichier
		JFileChooser selectionneFichier = new JFileChooser();
		selectionneFichier.setDialogTitle(texts.getText("copyloadtext"));
		JFileChooser selectionneFichier2 = new JFileChooser();
		selectionneFichier2.setDialogTitle(texts.getText("copyloadtarget"));
		setPreview(selectionneFichier.showOpenDialog(this) == JFileChooser.APPROVE_OPTION);
 		if(selectionneFichier2.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
         {    
 			StyledDocument docMep = resultat.getStyledDocument();
 			MutableAttributeSet attrsMep = resultat.getInputAttributes();
 			try
            {
 				resultat.setCaretPosition(0);
 				apercu.setCaretPosition(0);
 				resultat.setText("");
                docMep.insertString(0, FileToolKit.loadFileToStr(selectionneFichier.getSelectedFile().getAbsolutePath(),"UTF-8"),attrsMep);
                setFichier(selectionneFichier2.getSelectedFile().getAbsolutePath());
                btMaj.doClick();
            }
            catch (BadLocationException ble){gestErreur.afficheMessage(texts.getText("cantdisplaytxt"),Nat.LOG_SILENCIEUX);}
         }
	}

	/**
     * 
     */
    private void insertpageBreak()
    {
	    int pos = resultat.getCaretPosition();
	    try
        {
	        //int insert = pos + resultat.getText(pos, resultat.getDocument().getLength()-pos).indexOf("\n");
	    	int insert = resultat.getText(0, pos+1).lastIndexOf("\n");
	        resultat.setCaretPosition(insert+1);
	        insertString("   <page-break/>\n");
        }
        catch (BadLocationException e){gestErreur.afficheMessage(texts.getText("badLocationError"), Nat.LOG_SILENCIEUX);}   
    }

	/**
     * 
     */
    private void split()
    {
	    //récupération du paragraphe
    	int pos = resultat.getCaretPosition();
    	try
    	{
	    	int avant = resultat.getText(0,pos).lastIndexOf("\n")+1;
	    	int apres = pos + resultat.getText(pos, resultat.getDocument().getLength()-pos).indexOf("\n");
	    	//System.err.println(avant+"/"+apres);
	    	String sAvant =  resultat.getText(avant, pos - avant);
	    	String sApres = resultat.getText(pos,apres -pos);
	    	String tag = sAvant.split(">")[0]+">";
	    	String tagClose = tag.trim().split(" ")[0].replace("<", "</");
	    	if(!tagClose.endsWith(">")){tagClose+=">";}
	    	//for(String s:tag.split(" ")){System.out.println("----"+s);}
	    	if(sAvant.matches(".*<.*>.*") && sApres.matches(".*</.*>.*"))
	    	{
	    		insertString(tagClose+"\n"+tag);
	    	}
	    	else{/*erreur*/}
    	}
    	catch (BadLocationException e){e.printStackTrace();gestErreur.afficheMessage(texts.getText("badLocationError"), Nat.LOG_SILENCIEUX);}
	    
    }

	/**
	 * <p>Insère la chaine <code>s</code> dans {@link EditeurBraille#resultat}</p> a la position du curseur
     * @param s la chaine à insérer
     */
    private void insertString(String s)
    {
	    int pos = resultat.getCaretPosition();
	    try
        {
	        resultat.getDocument().insertString(pos, s, resultat.getInputAttributes());
        }
        catch (BadLocationException e)
        {
        	gestErreur.afficheMessage(texts.getText("badLocationError"), Nat.LOG_SILENCIEUX);
	        //e.printStackTrace();
        }
    }

	/**
	 * changes a string from the current braille table to utf8 braille
	 * @param txtBraille string to convert
	 * @return string converted
	 */
    private String map2UTF8 (String txtBraille)//Frédéric
    {
    	for(int i=0; i<txtBraille.length();i++)
		{
			String c = ""+txtBraille.charAt(i);
			String replace = map_Table.get(c);
			//System.out.println("c="+c+";rep="+replace);
			if(replace!=null)
			{
				//le replaceall est pour une regexp donc plante avec ? par exemple
				//le replace est fait pour ça
				txtBraille = txtBraille.replace(c, replace);
			}
		}
    	return txtBraille ;
    }
    
    /** 
	 * Search the string of {@link #jtfResearch} after the <code>caret</code> position in {@link EditeurBraille#resultat}
	 * If the end of document is reached, then start from begining
	 * If {@link EditeurBraille#jcbPerkins} is selected, research text is supposed to be in Braille UTF8
	 * If not, research text is converted into Braille UTF8
	 * @param searchZone left or right BrailleTextPane to search (left = resultat, right = apercu)
	 * @param caret the caret position */
	protected void research(JTextPane searchZone, int caret)
	{
		BrailleTextField searchField;
		if (searchZone.equals(resultat)){
			//System.err.println("resultat");
			searchField = jtfResearch;
		}
		else {searchField = jtfResearch2;}
		if(searchField.getText().length()!=0)
		{
			String research = searchField.getText();
			if (searchZone.equals(resultat)){
			if(jcbPerkins.isSelected()){research.toUpperCase();}
			else
			{
				//converting into braille UTF8
				//conversion des points braille dans la table utf8
				research = map2UTF8(research);
				
			}
			}
			//System.out.println("nik "+research);
			try
			{
				String p = searchZone.getDocument().getText(caret+1,searchZone.getDocument().getLength()-caret-1);
				if (searchZone.equals(resultat)) {
					//on crée un pattern pour pouvoir chercher
					//et trouver indépendamment des chars spéciaux
					String insertPattern = "[";
					for (int i=0;i<carSpec.length;i++)
					{
						insertPattern=insertPattern+carSpec[i];
					}
					insertPattern = insertPattern+"]*";
					String pattern = "";
					for (int i=0;i<research.length();i++)
					{
						pattern=pattern+research.charAt(i)+insertPattern;
					}
					//System.out.println("nik "+pattern);
					// Create a Pattern object
				      Pattern r = Pattern.compile(pattern);
				      // Now create matcher object.
				      Matcher m = r.matcher(p);
				      
					if(m.find())
					{
						int index = p.indexOf(m.group());
						//System.out.println("nik "+m.start()+" index "+index+" group "+m.group());
						searchZone.setCaretPosition(index+caret+1+m.group().length());
						searchZone.moveCaretPosition(index+caret+1);
						/*resultat.setSelectionStart(index+caret+1+research.length());
						resultat.setSelectionEnd(index+caret+1);*/
						//resultat.grabFocus(); (viré pour laisser le focus à la progress bar)
					}
					//starts from begining
					else if(caret > 0){research(searchZone,0);}
				}
				else { //searchzone=apercu, searching on right side of editor
					//System.err.println(p.toUpperCase()+"\n***\n"+research);
					//System.err.println("contains ? "+p.toUpperCase().contains(research.toUpperCase()));
					if(p.toUpperCase().contains(research.toUpperCase()))
					{
						int index = p.toUpperCase().indexOf(research.toUpperCase());
						searchZone.setCaretPosition(index+caret+1+research.length());
						searchZone.moveCaretPosition(index+caret+1);
						/*resultat.setSelectionStart(index+caret+1+research.length());
						resultat.setSelectionEnd(index+caret+1);*/
						//searchZone.grabFocus(); fait dans actionperformed
					}
				}
			}
			catch (BadLocationException e) {gestErreur.afficheMessage(texts.getText("badLocationError"), Nat.LOG_SILENCIEUX);}	
		}
	}
	/**
	 * @see ui.editor.EditeurBraille#focusGained(java.awt.event.FocusEvent)
	 * Used to display caret in the non editable JTextPane {@link #apercu} when focus gained
	 * Call {@link EditeurBraille#focusGained(FocusEvent)}
	 */
	@Override
    public void focusGained(FocusEvent fe)
	{
		if(fe.getSource()==apercu)
		{
			apercu.getCaret().setVisible(true);
		}
		else if(fe.getSource()==resultat)
		{
			//System.out.println("nikk resultat");
			//resultat.getCaret().setVisible(true);
			majLigneRes();
		}
		else{super.focusGained(fe);}
	}
	
	/**
	 * Vérifie si il faut demander l'enregistrement
	 * Enregistre la position de la barre de division
	 * Appelle {@link EditeurBraille#windowClosing(WindowEvent)}
	 * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosing(WindowEvent arg0)
	{
		if(!preview){ConfigNat.getCurrentConfig().setSplitPositionEditor(splitPane.getDividerLocation());}
		super.windowClosing(arg0);
	}
	
	/**
	 * Propose l'enregistrement d'une copie du fichier
	 * @see ui.editor.EditeurBraille#enregistrerFichier()
	 */
	@Override
    protected void enregistrerFichier(){enregistrerCopie();}
	
	/**
	 * Affiche un JFileChooser et enregistre si demandé une copie du fichier de mise en page
	 */
	private void enregistrerCopie()
	{
		//sélection du fichier
		JFileChooser selectionneFichier = new JFileChooser();
		//FileFilter filter = new FileFilter();
		//selectionneFichier.setFileFilter(filter)
		selectionneFichier.setDialogTitle(texts.getText("copysavetext"));
 		if (selectionneFichier.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
         {    
 			FileToolKit.saveStrToFile(resultat.getText(), selectionneFichier.getSelectedFile().getAbsolutePath(),"UTF-8");
         }
	}

	/**
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(MouseEvent me)
    {
	    if(me.getSource()==resultat && (me.getButton()==MouseEvent.BUTTON2||me.getButton()==MouseEvent.BUTTON3))
	    {
	    	jpmEdit.show(resultat, me.getX(), me.getY());
	    }
	    
    }

	/**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseEntered(MouseEvent e){/*nothing*/}

	/**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseExited(MouseEvent e){/*nothing*/}

	/**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent e){/*nothing*/}

	/**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent e){/*nothing*/}

	/**
     * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
     */
    @Override
    public void keyPressed(KeyEvent e)
    {
    	//on est obligés de le mettre en keypressed car en keyreleased,
    	//entrée fait un retour à la ligne inévitable
    	//traite entrée et ctrl+entrée dans résultat (page-break)
    	
    	//System.err.println("touche "+e.getKeyCode()+" modif "+e.getModifiersEx());
        //System.err.println("source "+e.getSource().toString());
    	if(e.getSource()==resultat)
    	{
    		if (e.getKeyCode() == KeyEvent.VK_ENTER)
			{
				e.consume(); //pour que entrée fasse rien !
				split();
				if (e.getModifiersEx()==InputEvent.CTRL_DOWN_MASK)
				{
					insertpageBreak();
				}
				//System.out.println("nikk enter");
			}
    	}
	    
    }

	/**
     * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
     */
    @Override
    public void keyTyped(KeyEvent e)
    {
	    // TODO Auto-generated method stub
	    
    }
    
    /**
     * Invoked when task's progress property changes.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
    	//System.err.println(evt.getOldValue()+";"+evt.getPropertyName()+";"+evt.getNewValue());
        if ("progress".equals(evt.getPropertyName())){
            progressBar.setValue(progress);
        } 
    }
    
    /**
     * Manage the progressbar when searching a pattern in the Editor tagged text component
     * @author bruno
     *
     */
    private class ProgressTask extends SwingWorker<Void, Void> {
    	
    	/** The maximum of the progress bar*/
    	private int maxSearch = 0;
    	/** The windows to be closed */
		private JFrame windows = null;
		/** Memorization of the Perkins saisie */
		private boolean perk = false;
    	
    	/**
    	 * Build a ProgressTask instance
    	 * @param max value for {@link #maxSearch}
    	 * @param w the windows to be closed at the end of the task
    	 */
    	public ProgressTask(int max, JFrame w){
    		maxSearch = max;
    		windows  = w;
    	}
        
    	/**
    	 * Init task : memorizes the saisie Perkins.
    	 * @param p state of the perkins checkbox before calling
    	 */
    	
    	public void initTask(boolean p){
    		perk =p;
    	}
    	
    	/**
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
        	setProgress(0);
            //Initialize progress property.
            while (getTheProgress() < maxSearch) {
            		research(resultat,resultat.getCaretPosition());
					progress++;
                //Sleep for up to 0.5 second.
            	this.setProgress(getTheProgress()*100/maxSearch);
            	//windows.toFront();
                /*try {Thread.sleep(20);}
                catch (InterruptedException ignore){gestErreur.afficheMessage("Interruption progress search", Nat.LOG_SILENCIEUX);}*/
                
                
                //System.out.println(getProgressBar().getValue()+":"+getTheProgress()+":"+maxSearch);
            }
            done();
            return null;
        }
        

        /**
         * Executed in event dispatching thread
         */
        @Override
        public void done() {
            Toolkit.getDefaultToolkit().beep();//may be removed
            setCursor(null); //turn off the wait cursor
            //end the prossess here
            //System.err.println("done !");
            jcbPerkins.setSelected(perk); //set the perkins as it was before research
            if (perk) //on remet les listeners qu'on avait enlevés
            {
            	pn2.removePerkinsObserver(jtfResearch); //il faut le réenlever sinon y'en a 2 ???
            	pn2b.removePerkinsObserver(jtfResearch2);
				pn.removePerkinsObserver(resultat);
            	pn2.addPerkinsObserver(jtfResearch);
            	pn2b.addPerkinsObserver(jtfResearch2);
				pn.addPerkinsObserver(resultat);
            }
            windows.dispose();
            resultat.requestFocusInWindow();
        }
    } 
}

