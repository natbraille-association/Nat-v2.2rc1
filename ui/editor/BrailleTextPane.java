/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui.editor;

import javax.swing.JTextPane;

import ui.listener.PerkinsObserver;

/**
 * @author bruno
 *
 */
public class BrailleTextPane extends JTextPane implements PerkinsObserver
{

	/** Pour sérialisation*/
    private static final long serialVersionUID = 1L;

    /** The parent Frame*/
    private EditeurBraille parent = null;
    
	/**
	 * @param p the parent EditeurBraille
     */
    public BrailleTextPane(EditeurBraille p)
    {
    	super();
    	parent = p;
    }

    /**
     * @see ui.listener.PerkinsObserver#manageError(int, java.lang.String)
     */
    @Override
    public void manageError(int code, String m)
    {
    	parent.manageError(code,m);    	
    }
    
    /**
     * @see ui.listener.PerkinsObserver#perkinsInput(java.lang.String)
     */
    @Override
    public void perkinsInput(String s)
    {
		try
		{
			getDocument().insertString(getCaretPosition(), s, getInputAttributes());
		}
		catch (Exception exp){exp.printStackTrace();}
    }


}
