/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.io.IOException;

import javax.swing.ImageIcon;
//import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import nat.ConfigNat;
import nat.Nat;
import nat.convertisseur.Convertisseur2ODT;
import net.sourceforge.jeuclid.swing.JMathComponent;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;

import outils.FileToolKit;
import outils.Path;
import outils.TextConverter;
import outils.emboss.Embosseur;
import ui.AfficheurJTASwing;
import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;
import ui.listener.EntityResolverNull;
import ui.listener.FileChangeListener;
import ui.listener.FileMonitor;

import java.util.ArrayList;

/**
 * Cette classe de l'interface graphique permet d'afficher le fichier transcrit et d'intéragir avec le fichier
 * à mettre en forme (édition, mode perkins, etc.).
 * @author Bruno Mascret
 */
public class EditeurTan extends EditeurBraille implements MouseListener, FileChangeListener, KeyListener
{
	/** Textual contents */
	private static Language texts = new Language("EditeurTan");
	
	/** identifiant par défaut pour la sérialisation (non utilisé dans NAT)*/ 
	private static final long serialVersionUID = 1L;
	/** nom du fichier braille généré pour obtenir l'apercu */
	public static final String tmpApercu = ConfigNat.getUserTempFolder()+"tmpApercuTan.tan";
	/** nom du fichier xhtml réalisé en détranscrivant {@link #tmpApercu} */
	public static final String tmpXHTML = ConfigNat.getUserTempFolder()+"tmpTan.xhtml";
	
	/** Constante de style: emphase */
	private static final int STYLE_EM = 1;
	/** Constante de style: exposant */
	private static final int STYLE_SUP = 2;
	/** Constante de style: indice */
	private static final int STYLE_SUB = 3;
	
	//private JPanel apercu = new JPanel();
	/** le ScrollPane associé au JTextPane apercu */
	private JScrollPane scrollAp;
	/** JButton aperçu dans un navigateur */
	private JButton btNavigateur = new JButton(texts.getText("navigator"), new ImageIcon("ui/icon/web-browser.png"));
	/** JButton aperçu en noir du texte braille saisi*/
	private JButton btApercuNoir = new JButton(texts.getText("refresh"), new ImageIcon("ui/icon/view-refresh.png"));
	/** JButton enregistrer fich noir */
	private JButton btEnregNoir = new JButton(texts.getText("saveas"),new ImageIcon("ui/icon/document-save-as.png"));
	/** JButton mettre à jour le texte braille*/
	private JButton btMajBraille = new JButton(texts.getText("refreshbraille"),new ImageIcon("ui/icon/go-up.png"));
	
	/** Liste des JMathComponent utilisés en noir */
	private ArrayList<JMathComponent> lMaths = new ArrayList<JMathComponent>();
	
	/** constante identifiant la base du nom du fichier temporaire d'édition des maths */
	private static String TMP_MATHML = ConfigNat.getUserTempFolder()+"tmp";
	/** adresse du script de lancement de l'éditeur d'openoffice */
	private String script = ConfigNat.getUserTempFolder()+"/scriptOOMath";

	
	/** instance de nat pour lancer les détranscriptions*/
	private Nat nat;
	
	/** ligne de commande à exécuter pour lancer OpenOffice */
	private String ldc = "soffice ";
	
	/** FileMonitor suit les modifications faites sur le ou les fichiers mathml édités par openoffice */
	private FileMonitor fMon = new FileMonitor();//.getInstance();
	
	/**
     * Afficheur graphique de logs
     * @since 2.0
     */
    private AfficheurJTASwing panneauLog;// = new JTextArea (15,40);
    
    /** Menu item mettre à jour le fichier braille */
	private JMenuItem jmiMAJBraille;
	/** Menu item mettre à jour le fichier noir */
	private JMenuItem jmiMAJNoir;
	
	/** Menu item enregistrer le fichier noir */
	private JMenuItem jmiEnregistrerNoir;
	
	/** Menu item apercu noir dans navigateur*/
	private JMenuItem jmiApercuNavigateur;
    
    /* indique si la zone de texte {@link #apercu} a été modifiée *
    private boolean modifNoir = false;
    
    /* indique si la zone de texte {@link #resultat} a été modifiée *
    private boolean modifBraille = false;*/
	
	/** 
	 * Construit un objet Editeur
	 * @param emb l'objet Embosseur à utiliser pour l'embossage
	 * @param f adresse du fichier édité
	 * @param n instance de nat pour lancer les détranscriptions
	 */
	public EditeurTan(Embosseur emb, String f,Nat n)
	{
	    super(texts.getText("taneditor"),emb,n.getGestionnaireErreur());
		nat=n;
		apercu.setBackground(Color.lightGray);
		apercu.getDocument().addDocumentListener(this);
		setSize(600,400);
		
		btNavigateur.addActionListener(this);
		Context cbtNavigateur = new Context("w","Button","navigator",texts);
		new ContextualHelp(btNavigateur,cbtNavigateur);
		btNavigateur.setMnemonic('w');
		btNavigateur.setEnabled(false);
		
		btApercuNoir.addActionListener(this);
		Context cbtApercuNoir = new Context("u","Button","refresh",texts);
		new ContextualHelp(btApercuNoir,cbtApercuNoir);
		btApercuNoir.setMnemonic('u');
		
		btMajBraille.addActionListener(this);
		Context cbtMajBraille = new Context("b","Button","refreshbraille",texts);
		new ContextualHelp(btMajBraille,cbtMajBraille);
		btMajBraille.setMnemonic('b');
		
		scrollAp = new JScrollPane (apercu);
		scrollAp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollAp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,scrollRes,scrollAp);
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation(ConfigNat.getCurrentConfig().getSplitPositionEditorTan());
		
		//Provide minimum sizes for the two components in the split pane
		Dimension minimumSize = new Dimension(100, 50);
		scrollRes.setMinimumSize(minimumSize);
		scrollAp.setMinimumSize(minimumSize);
		
		panneauLog = new AfficheurJTASwing(13, 25);
	    panneauLog.setEditable(false);
	    panneauLog.setLineWrap(true);
	    
	    n.getGestionnaireErreur().addAfficheur(panneauLog);
	    
	    JScrollPane scrollLog = new JScrollPane (panneauLog);
	    scrollLog.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	    scrollLog.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		Context cscrollLog = new Context("","Scrollpane","pane",texts);
		new ContextualHelp(scrollLog,cscrollLog);
		
	    btEnregNoir.addActionListener(this);
	    Context cbtEnregNoir = new Context("i","Button","saveas",texts);
	    new ContextualHelp(btEnregNoir,cbtEnregNoir);
		btEnregNoir.setMnemonic('i');
		
		//focus sur les zones de texte
		resultat.addFocusListener(this);
		apercu.addFocusListener(this);

		//menus
		jmiEnregistrerNoir= new JMenuItem(texts.getText("saveas"),new ImageIcon("ui/icon/document-save-as.png"));
		jmiEnregistrerNoir.setMnemonic('i');
		jmiEnregistrerNoir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiEnregistrerNoir.addActionListener(this);
		
		jmiMAJNoir= new JMenuItem(texts.getText("refresh"),new ImageIcon("ui/icon/view-refresh.png"));
		jmiMAJNoir.setMnemonic('u');
		jmiMAJNoir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiMAJNoir.addActionListener(this);
		
		jmiMAJBraille= new JMenuItem(texts.getText("refreshbraille"),new ImageIcon("ui/icon/go-up.png"));
		jmiMAJBraille.setMnemonic('b');
		jmiMAJBraille.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiMAJBraille.addActionListener(this);
		
		jmiApercuNavigateur= new JMenuItem(texts.getText("navigator"),new ImageIcon("ui/icon/web-browser.png"));
		jmiApercuNavigateur.setMnemonic('w');
		jmiApercuNavigateur.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		jmiApercuNavigateur.addActionListener(this);
		
		
		
		jmFichier.add(jmiEnregistrer);
	    jmFichier.add(jmiEnregistrerSous);
	    jmFichier.addSeparator();
	    //fichier noir
	    jmFichier.add(jmiEnregistrerNoir);
	    jmFichier.addSeparator();
	    jmFichier.add(jmiEmboss);
	    
	    jmFichier.addSeparator();
	    jmFichier.add(jmiQuitter);
	    
	    jmEdition.add(jmiUndo);
	    jmEdition.add(jmiRedo);
	    jmFichier.addSeparator();
	    
	    jmEdition.add(jmiPerkins);
	    
	    jmEdition.addSeparator();
	    
	    jmView.addSeparator();
	    jmView.add(jmiMAJNoir);
	    jmView.add(jmiMAJBraille);
	    jmView.addSeparator();
	    jmView.add(jmiApercuNavigateur);
		/*
		 * Mise en page
		 */
		GridBagConstraints c = new GridBagConstraints();
		
		JToolBar jtbBraille=new JToolBar();
		jtbBraille.setLayout(new GridBagLayout());
		JToolBar jtbNoir=new JToolBar();
		jtbNoir.setLayout(new GridBagLayout());
		JToolBar jtbE=new JToolBar();
		jtbE.setLayout(new GridBagLayout());
		
		//fabrication tool bar
		c.gridwidth=1;
		c.gridx=0;
		jtbBraille.add(btMajBraille,c);
		c.gridx++;
		jtbBraille.add(btEnregistrer,c);
		c.gridx++;
		jtbBraille.add(btEnregistrersous,c);
		//c.gridx++;
		//jtbBraille.add(btEmbosser,c);
		jmb.add(jtbBraille);
		
		c.gridx=0;
		jtbNoir.add(btApercuNoir,c);
		c.gridx++;
		jtbNoir.add(btEnregNoir,c);
		c.gridx++;
		jtbNoir.add(btNavigateur,c);
		jmb.add(jtbNoir);
		
		c.gridx=0;
		jtbE.add(jcbPerkins,c);
		c.gridx++;
		jtbE.add(btUndo,c);
		c.gridx++;
		jtbE.add(btRedo,c);
		c.gridx++;
		jmb.add(jtbE);
		
		panneauAffichage.setLayout(new BorderLayout());
		panneauAffichage.add("Center",splitPane);
		//panneauAffichage.add("South",scrollAp);
		
		/*JPanel pLateral = new JPanel();
		LineBorder l = new LineBorder(scrollLog.getBackground(),12);
		scrollLog.setBorder(l);
		
		pLateral.setLayout(new GridBagLayout());
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.VERTICAL;
		pLateral.add(scrollLog,c);*/
		
		/*
		 * Mise en page générale
		 */
		JPanel panneau = new JPanel();
		panneau.setLayout(new BorderLayout());
		panneau.add("North",lFichier);
		panneau.add("Center",panneauAffichage);
		panneau.add("East",scrollLog);
		JPanel p = new JPanel();
		p.setPreferredSize(new Dimension(10,10));
		panneau.add("West",p);
		setContentPane(panneau);
		if(ConfigNat.getCurrentConfig().getMemoriserFenetre())
		{
			int x= ConfigNat.getCurrentConfig().getWidthEditeur();
			int y=ConfigNat.getCurrentConfig().getHeightEditeur();
			if(x+y != 0){setPreferredSize(new Dimension(x,y));}
		}
		if(ConfigNat.getCurrentConfig().getCentrerFenetre())
		{
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension size = this.getPreferredSize();
			screenSize.height = screenSize.height/2;
			screenSize.width = screenSize.width/2;
			size.height = size.height/2;
			size.width = size.width/2;
			int y = screenSize.height - size.height;
			int x = screenSize.width - size.width;
			setLocation(x, y);
		}
		encodage=ConfigNat.getCurrentConfig().getBrailleEncoding();
        afficheFichier(f, ConfigNat.getCurrentConfig().getPoliceEditeur(), ConfigNat.getCurrentConfig().getTaillePolice());
        if(resultat.getText().length()> 0)
        {
        	afficheNoir();
        }
        pack();
	}
	/**
	 * Demande aussi s'il faut effacer le fichier temporaire mémorisé
	 * @see java.awt.Window#setVisible(boolean)
	 */
	@Override
    public void setVisible(boolean v)
	{
		super.setVisible(v);
		if(v && resultat.getText().length()>0 && getFichier().equals(ConfigNat.fichTmpTan) &&
	        	JOptionPane.showConfirmDialog(this,texts.getText("deletepreviousfile"),texts.getText("cleareditor"), JOptionPane.YES_NO_OPTION)==JOptionPane.OK_OPTION)
		{
	        	resultat.setText("");
	        	enregistrerFichier();
	    }
	}
	
	/**
	 * Enregistre aussi le fichier noir (en XHTML)
	 * @param fichierNoir adresse du fichier noir
	 * @param changeNom vrai si il faut enregistrer le fichier sous un autre nom
	 * TODO: paramétrage pour ajouter ou non un doctype; pour l'instant, la variable est toujours à faux
	 */
	protected void enregistrerFichierNoir(String fichierNoir, boolean changeNom)
	{
		//TODO utiliser paramètre
		boolean useDocType = false;
		String texte="";
		Element [] elem = apercu.getDocument().getRootElements();
		for(Element e : elem)
		{
			int numeq = 0;
			try
            {
				for(int i = 0;i<e.getElementCount();i++)
				{
					Element f = e.getElement(i);
					//System.out.println("****"+f.getName()+":"+f.getDocument().getText(0, f.getDocument().getLength()));
					for(int j = 0;j<f.getElementCount();j++)
					{
						Element g = f.getElement(j);
						if(g.getName()=="component")
						{
							System.out.println("**** MATHS:");
							
							DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
					    	fabrique.setValidating(false);
							DocumentBuilder constructeur;

				            constructeur = fabrique.newDocumentBuilder();
				            org.w3c.dom.Document doc = constructeur.newDocument();
						    // Propriétés de docParam
						    doc.setXmlVersion("1.0");
						    doc.setXmlStandalone(true);
						    
						    //racine
						    Node racine = doc.importNode(lMaths.get(numeq).getDocument(),true);
						    Node attr = doc.createAttribute("xmlns:m");
						    attr.setNodeValue("http://www.w3.org/1998/Math/MathML");
						    Node attr2 = doc.createAttribute("xmlns:maths");
						    attr.setNodeValue("http://www.w3.org/1998/Math/MathML");
						    racine.getAttributes().setNamedItem(attr);
						    racine.getAttributes().setNamedItem(attr2);
						    
						    doc.appendChild(racine);		    
						    /* Sauvegarde de document dans un fichier */
					        Source source = new DOMSource(doc);
					        
					        // Création de la sortie
					        StringWriter sr = new StringWriter();
					        Result out = new StreamResult(sr);
					        
					        // Configuration du transformer
					        TransformerFactory tfabrique = TransformerFactory.newInstance();
					        Transformer transformer = tfabrique.newTransformer();
					        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
					        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");  
					        transformer.setOutputProperty(OutputKeys.VERSION, "1.1");
					        //transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, ConfigNat.getCurrentConfig().getDTD());
					        // Transformation
					        transformer.transform(source, out);
					        texte+=sr.getBuffer();
					        numeq++;
						}
						else if(g.getName().equals("content"))
						{
							System.out.println("NOM:"+g.getName());
							int startOffset = g.getStartOffset();
				            int endOffset = g.getEndOffset();
				            int length = endOffset - startOffset;
				            AttributeSet childAttributes = g.getAttributes();
				            /*java.util.Enumeration enu = childAttributes.getAttributeNames();
				            while(enu.hasMoreElements())
				            {
				            	System.out.println(enu.nextElement());
				            }*/
				            String noeud="";
				            boolean [] ferme = {false,false,false};
				            if(StyleConstants.isBold(childAttributes))
				            {
				            	ferme[0]=true;
				            	noeud+="<em>";
				            }
				            if(StyleConstants.isSubscript(childAttributes))
				            {
				            	ferme[1]=true;
				            	noeud+="<sub>";
				            }
				            if(StyleConstants.isSuperscript(childAttributes))
				            {
				            	ferme[2]=true;
				            	noeud+="<sup>";
				            }		
				            String contenu = apercu.getDocument().getText(startOffset,length);
				            noeud+= contenu;
				            if(ferme[0]){noeud+="</em>";}
				            if(ferme[1]){noeud+="</sub>";}
				            if(ferme[2]){noeud+="</sup>";}
				            System.out.println("****"+noeud);
				            if(contenu.endsWith("\n"))
				            {
				            	noeud+="</p>\n\t<p>";
				            }
				            texte+=noeud;
						}
						else
						{
							System.out.println(texts.getText("name")+g.getName());
						}
					}
				}
				
	            //System.out.println(e.getName()+":"+e.getDocument().getText(0, e.getDocument().getLength()));
				
            }
            catch (BadLocationException e1)
            {
	            gestErreur.afficheMessage(texts.getText("badLocationError"), Nat.LOG_SILENCIEUX);
	            //e1.printStackTrace();
            }
            catch (ParserConfigurationException pce)
            {
	            gestErreur.afficheMessage(texts.getText("parserError"), Nat.LOG_SILENCIEUX);
	            //pce.printStackTrace();
            }
            catch (TransformerConfigurationException tce)
            {
            	gestErreur.afficheMessage(texts.getText("transConfError"), Nat.LOG_SILENCIEUX);
	            //tce.printStackTrace();
            }
            catch (TransformerException te)
            {
            	gestErreur.afficheMessage(texts.getText("transError"), Nat.LOG_SILENCIEUX);
	            //te.printStackTrace();
            }
		}
		JFileChooser selectionneFichier = new JFileChooser();
		FileFilter xhtmlFilter = new FileNameExtensionFilter("XHTML 1.1 (*.xhtml)", "xhtml");
		selectionneFichier.addChoosableFileFilter(xhtmlFilter);
		selectionneFichier.setApproveButtonText(texts.getText("chosethatfile"));
		//TODO déplacer ce if au tout début
		texte = texte.replaceAll("<[?]xml.*[?]>", "");
		String doctype = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN\" "+
			"\"http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd\">\n";
		if(!useDocType){doctype="";}
		texte = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
			doctype +
			"<html xmlns:m=\"http://www.w3.org/1998/Math/MathML\""+
			" xmlns=\"http://www.w3.org/1999/xhtml\" " +
			" xmlns:math=\"http://www.w3.org/1998/Math/MathML\">"+
			"<head></head>" +
			"<body>"+
			"\t<p>"+ texte + "\n</p></body></html>";
		if (!changeNom)
		{
			FileToolKit.saveStrToFile(texte, fichierNoir, "UTF-8");
		}
		else if(selectionneFichier.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {    
 			//si un fichier est selectionné, récupérer le fichier puis son path
			fichierNoir = selectionneFichier.getSelectedFile().getAbsolutePath();
			
			FileToolKit.saveStrToFile(texte, fichierNoir, "UTF-8");		
        }
		System.out.println("Texte:\n" + texte);
		//else{message.setText("<html><p color=\"red\">Erreur lors de l'enregistrement du fichier noir</p></html>");}
	}
	
	/**
	 * Affiche le fichier dans le JTextPane resultat et configure la ligne secondaire
	 * @param nomFichier nom du fichier transcrit
	 * @param police police principale
	 * @param taillePolice taille de la police principale
	 * @param police2 police secondaire
	 * @param taillePolice2 taille de la police secondaire
	 */
	public void afficheFichier(String nomFichier,String police, int taillePolice, String police2, int taillePolice2)
	{
		afficheFichier(nomFichier, police, taillePolice);
	}
	
	/**
	 * Affiche le fichier dans le JTextPane
	 * @param nomFichier nom du fichier transcrit
	 * @param police police principale
	 * @param taillePolice taille de la police principale
	 */
	public void afficheFichier(String nomFichier,String police, int taillePolice)
    {
		setTitle("NAT " + nomFichier);
	    setFichier(nomFichier);
	    lFichier.setText(texts.getText("file") + getFichier());
		//Préparation des styles pour le JTextPane
	    StyledDocument doc = resultat.getStyledDocument();
	    //pour éviter que l'affichage prenne 3 plombes à chaque fois...
	    Document blank = new DefaultStyledDocument();
	    resultat.setDocument(blank);
	    
	    MutableAttributeSet attrs = resultat.getInputAttributes();
	    Font fonteBraille = new Font(police, Font.PLAIN, taillePolice);

	    StyleConstants.setFontFamily(attrs, fonteBraille.getFamily());
	    StyleConstants.setFontSize(attrs,taillePolice);
	    
		resultat.setPreferredSize(new Dimension(400,280));
		apercu.setPreferredSize(new Dimension(400,100));
		//this.pack();
		
	    try
	    {
	    	BufferedReader raf = new BufferedReader(new InputStreamReader(new FileInputStream(nomFichier),encodage));
	    	String ligne;
	    	while ( (ligne = raf.readLine()) != null )
	    	{
	    		doc.insertString(doc.getLength(), ligne + "\n", attrs);
	    	}
	    	raf.close();
	     }
	    catch (BadLocationException ble){System.err.println(texts.getText("cantdisplaytext"));ble.printStackTrace();}
	    catch (IOException e){System.err.println(texts.getText("errorin")+": " + e.getMessage() + " ");e.printStackTrace();}
			
		// bru initialiseMap();
		ajouteListenerDoc(doc);
		jcbPerkins.doClick();
    }	  
	
	/**
	 * Détranscrit le texte contenu dans {@link EditeurBraille#resultat}
	 * @return true si détranscription sans erreur
	 */
	private boolean detranscrit()
	{
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		boolean retour = false;
		//apercu.repaint();
		ArrayList<String>noir = new ArrayList<String>();
		ArrayList<String>braille = new ArrayList<String>();
		//création d'un fichier temporaire d'aperçu
		FileToolKit.saveStrToFile(resultat.getText(), tmpApercu,ConfigNat.getCurrentConfig().getBrailleEncoding());
		noir.add(tmpXHTML);
		braille.add(tmpApercu);
		if (nat.fabriqueTranscriptions(noir, braille, true))
	    {			
		//lancement de la détranscription
		   btNavigateur.setEnabled(nat.lanceScenario());
		   retour = true;
	    }
		setCursor(Cursor.getDefaultCursor());
		return retour;
	}
	
	/**
	 * Transcrit le fichier d'aperçu {@link #tmpXHTML}
	 * @return true si transcription sans erreur
	 */
	private boolean transcrit()
	{
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		boolean retour = false;
		
		//apercu.repaint();
		ArrayList<String>noir = new ArrayList<String>();
		ArrayList<String>braille = new ArrayList<String>();
		//création d'un fichier temporaire d'aperçu
		enregistrerFichierNoir(tmpXHTML,false);
		noir.add(tmpXHTML);
		braille.add(tmpApercu);
		String noirEncoding = ConfigNat.getCurrentConfig().getNoirEncoding();
		ConfigNat.getCurrentConfig().setNoirEncoding("UTF-8");
		if (nat.fabriqueTranscriptions(noir, braille, false)&& nat.lanceScenario())
	    {	
			try
            {
	            resultat.getDocument().remove(0, resultat.getDocument().getLength());
            }
            catch (BadLocationException e)
            {
            	gestErreur.afficheMessage(texts.getText("badLocationError"), Nat.LOG_SILENCIEUX);
	            //e.printStackTrace();
            }
			afficheFichier(tmpApercu, ConfigNat.getCurrentConfig().getPoliceEditeur(), ConfigNat.getCurrentConfig().getTaillePolice());
			retour = true;
	    }
		ConfigNat.getCurrentConfig().setNoirEncoding(noirEncoding);
		setCursor(Cursor.getDefaultCursor());
		return retour;
	}
	/**
	 * Affiche en noir dans {@link #apercu} la détranscription du texte de {@link #resultat}
	 */
	private void afficheNoir()
	{		
		try
        {
	        apercu.getDocument().remove(0, apercu.getDocument().getLength());
        }
        catch (BadLocationException ble)
        {
        	gestErreur.afficheMessage(texts.getText("badLocationError"), Nat.LOG_SILENCIEUX);
	        //ble.printStackTrace();
        }
		lMaths.clear();
		//création du DOM
		org.w3c.dom.Document doc;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG);
        dbf.setNamespaceAware(false);
    
        DocumentBuilder db;
		try
		{
			dbf.setXIncludeAware(false);
			db = dbf.newDocumentBuilder();
			doc = db.parse(new File(tmpXHTML));
			
			NodeList phrases = doc.getElementsByTagName("p");
			MutableAttributeSet attrs = apercu.getInputAttributes();
			StyleConstants.setFontSize(attrs, ConfigNat.getCurrentConfig().getTaillePolice2());
			//MathDocHandler handler = new MathDocHandler();
			//ArrayList<MathPanel> lMathPan = new ArrayList<MathPanel>();
			for(int i=0;i<phrases.getLength();i++)
			{	
				//apercu.setLayout(null);
				NodeList fils = phrases.item(i).getChildNodes();
				for(int j=0;j<fils.getLength();j++)
				{
					Node f = fils.item(j);
					if(f.getNodeName().equals("m:math")|| f.getNodeName().equals("math:math"))
					{
						//System.out.println(f.getNodeName()+" "+f.getNodeValue());

						JMathComponent jmc = new JMathComponent();
						jmc.addMouseListener(this);
						jmc.addKeyListener(this);
						lMaths.add(jmc);
						
						jmc.setDocument(f);
						jmc.setFontSize(ConfigNat.getCurrentConfig().getTaillePolice2());
						
						jmc.setAlignmentX(Component.LEFT_ALIGNMENT);
						jmc.setAlignmentY(Component.BOTTOM_ALIGNMENT);
						apercu.insertComponent(jmc);
						StyleConstants.setFontSize(attrs, ConfigNat.getCurrentConfig().getTaillePolice2());
						apercu.replaceSelection(" ");
					}
					else
					{
						int style = getStyle(phrases.item(i));
						drawLit(f, style);
					}
				}
				apercu.replaceSelection("\n");//fin de la phrase
			}
		}
		catch (ParserConfigurationException e)
		{
			gestErreur.afficheMessage(texts.getText("parserError"), Nat.LOG_SILENCIEUX);
			//e.printStackTrace();
		}
		catch (SAXException e)
		{
			gestErreur.afficheMessage(texts.getText("saxError"), Nat.LOG_SILENCIEUX);
			//e.printStackTrace();
		}
		catch (IOException e)
		{
			gestErreur.afficheMessage(texts.getText("ioError"), Nat.LOG_SILENCIEUX);
			//e.printStackTrace();
		}
		apercu.repaint();
		btNavigateur.setEnabled(true);
	}
	
	/**
	 * Renvoie le code du style pour le noeud item
     * @param item le noeud à tester
     * @return code du style
     */
    private int getStyle(Node item)
    {
	    int retour = 0;
	    if(item != null)
	    {
		    String nom = item.getNodeName();
		    if(nom != null)
		    {
			    if(nom.equals("em")){retour=STYLE_EM;}
			    else if(nom.equals("sup")){retour = STYLE_SUP;}
			    else if(nom.equals("sub")){retour = STYLE_SUB;}
		    }
	    }
	    return retour;
    }

	/**
	 * Ecrit récursivement les contenus littéraires en fonction de leur attributs de mise en forme 
	 * @param f le noeud courant
	 * @param pere le type de pere du noeud courant
	 */
	private void drawLit(Node f, int pere)
	{
		MutableAttributeSet attrs = apercu.getInputAttributes();
		if(f.getNodeType()==Node.TEXT_NODE)
		{
			//System.out.println(f.getNodeName()+" TEXT "+f.getNodeValue());
			apercu.replaceSelection(f.getNodeValue().replaceAll("\n", "").trim()+" ");
			//apercu.replaceSelection("***"+f.getNodeValue().replaceAll("\n|\t|  ", "")+"***");
			switch(pere)
			{
				case STYLE_EM:
				{
					//System.out.println("suppr EM");
					StyleConstants.setBold(attrs, false);
					break;
				}
				case STYLE_SUP:
				{
					//System.out.println("suppr SUP");
					StyleConstants.setSuperscript(attrs, false);
					break;
				}
				case STYLE_SUB:
				{
					//System.out.println("suppr SUB");
					StyleConstants.setSubscript(attrs, false);
					break;
				}
			}
		}
		else
		{
			switch(getStyle(f))
			{
				case STYLE_EM:
				{
					StyleConstants.setBold(attrs, true);
					//System.out.println(f.getNodeName()+" +EM "+f.getNodeValue());
					break;
				}
				case STYLE_SUP:
				{
					StyleConstants.setSuperscript(attrs, true);
					//System.out.println(f.getNodeName()+" +SUP "+f.getNodeValue());
					break;
				}
				case STYLE_SUB:
				{
					StyleConstants.setSubscript(attrs, true);
					//System.out.println(f.getNodeName()+" +SUB "+f.getNodeValue());
					break;
				}
			}
			NodeList fils = f.getChildNodes();
			for(int i=0;i<fils.getLength();i++)
			{
				drawLit(fils.item(i), getStyle(f));
			}
		}
	}
	
	/**
	 * Renvoie le texte contenu dans {@link #resultat}
	 * @return Le texte contenu dans {@link #resultat}
	 */
	@Override
	public String getText(){return resultat.getText();}
	
	/** 
	 * Implémente la méthode actionPerformed d'ActionListener
	 * Gère les actions des boutons et met à jour l'InputMap du JTextPane resultat en fonction de
	 * l'état du JCheckBox jcbPerkins
	 * @param evt l'objet ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent evt)
	{
		super.actionPerformed(evt);
		if ((evt.getSource()==btApercuNoir) && detranscrit()){afficheNoir();}
		else if(evt.getSource()==jmiMAJNoir){btApercuNoir.doClick();}
		else if(evt.getSource()==jmiMAJBraille){btMajBraille.doClick();}
		else if (evt.getSource()==btNavigateur || evt.getSource()==jmiApercuNavigateur)
		{
			if(Desktop.getDesktop().isSupported(Desktop.Action.BROWSE))
			{
				
				try 
				{
					File f = new File(ConfigNat.getUserTempFolder()+"/tmpTan.xhtml");
					Desktop.getDesktop().browse(f.toURI());
				}
				catch (IOException e){e.printStackTrace();}
			}
			else
			{
				JOptionPane.showMessageDialog(this,texts.getText("notsaved1") +
						texts.getText("notsaved2") +
						texts.getText("notsaved3"),texts.getText("notsaved4"),JOptionPane.INFORMATION_MESSAGE);
			}
		}
		else if (evt.getSource()==btEnregNoir || evt.getSource()==jmiEnregistrerNoir)
		{
			enregistrerFichierNoir("", true);
		}
		else if (evt.getSource()==btMajBraille || evt.getSource()==jmiMAJBraille)
		{
			setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			transcrit();
			setCursor(Cursor.getDefaultCursor());
		}
	}
	/**
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(MouseEvent me)
    {
    	if(me.getClickCount()==2)
	    {
	    	editFormule((JMathComponent)(me.getSource()));
	    }
	    
    }

	/**
	 * Edite dans openoffice le composant mathématique passé en paramètre
     * @param jmc le JMathComponent à éditer
     */
    private void editFormule(JMathComponent jmc)
    {
    	//mathEdit = lMaths.indexOf(jmc);
    	//System.out.println("Clic sur " + lMaths.indexOf(jmc));
    	Node n = jmc.getDocument();
    	DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
    	fabrique.setValidating(false);
		DocumentBuilder constructeur;
        try
        {
            constructeur = fabrique.newDocumentBuilder();
            org.w3c.dom.Document doc = constructeur.newDocument();
		    // Propriétés de docParam
		    doc.setXmlVersion("1.0");
		    doc.setXmlStandalone(true);
		    
		    //racine
		    Node racine = doc.importNode(n,true);
		    Node attr = doc.createAttribute("xmlns:m");
		    attr.setNodeValue("http://www.w3.org/1998/Math/MathML");
		    Node attr2 = doc.createAttribute("xmlns:maths");
		    attr.setNodeValue("http://www.w3.org/1998/Math/MathML");
		    racine.getAttributes().setNamedItem(attr);
		    racine.getAttributes().setNamedItem(attr2);
		    
		    doc.appendChild(racine);		    
		    /* Sauvegarde de document dans un fichier */
	        Source source = new DOMSource(doc);
	        
	        // Création du fichier de sortie
	        File f = new File(TMP_MATHML+"."+lMaths.indexOf(jmc)+".mml");
	        f.delete();
	        Result out = new StreamResult(f);
	        
	        // Configuration du transformer
	        TransformerFactory tfabrique = TransformerFactory.newInstance();
	        Transformer transformer = tfabrique.newTransformer();
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");  
	        transformer.setOutputProperty(OutputKeys.VERSION, "1.1");
	        //transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, ConfigNat.getCurrentConfig().getDTD());
	        // Transformation
	        transformer.transform(source, out);
	        
	        out = null;
	        f.setWritable(true, false);
	        /* Bruno */////////////////////// virer les namespaces
	        String xmlString ="";
			try {
				xmlString = FileToolKit.loadFileToStr(f.getCanonicalPath(), "UTF-8");
				System.out.println(xmlString);
				xmlString = xmlString.//replaceAll("(<\\?[^<]*\\?>)?", ""). /* remove preamble */
	        	  replaceAll("(<)(\\w+:)(.*?>)", "$1$3") /* remove opening tag prefix */
	        	  .replaceAll("(</)(\\w+:)(.*?>)", "$1$3") /* remove closing tags prefix */
				  .replaceAll("<math .*>","<math xmlns=\"http://www.w3.org/1998/Math/MathML\">");
				System.out.println(xmlString);
				FileToolKit.saveStrToFile(xmlString, f.getCanonicalPath(), "UTF-8");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
	        
	        
	        //f.setLastModified(System.currentTimeMillis()-2000);
	        //transformer = null; out = null;
	        
	        // que pour Linux fMon.addFileChangeListener(this, f.getAbsolutePath(), 20);
	        
	        //ouverture de l'éditeur openoffice
	        //Path p = new Path(gestErreur);
	        
	        gestErreur.afficheMessage(texts.getText("ooeditordetection"), Nat.LOG_NORMAL);		        
	        boolean OO_Ok;
			//if(System.getProperty("os.name").startsWith("Linux")) et macos ?
	        String osName = System.getProperty("os.name");
	        if(osName.startsWith("Linux")||osName.startsWith("Mac"))
			{
				fMon.addFileChangeListener(this, f.getAbsolutePath(), 20);
				gestErreur.afficheMessage(texts.getText("scriptwriting"), Nat.LOG_VERBEUX);
				int osss;
				if (osName.startsWith("Linux")) {osss = Convertisseur2ODT.OS_LINUX ;}
				else {osss = Convertisseur2ODT.OS_MAC ;}
	            OO_Ok = fabriqueExec(osss,f.getAbsolutePath());
	            if (OO_Ok)
	            {
	            	gestErreur.afficheMessage(texts.getText("ooeditorstart"), Nat.LOG_VERBEUX);
					Runtime runTime = Runtime.getRuntime();
					int resu = 0;//à vérifier
					
		            try
					{
						//Process p = runTime.exec(script);
		            	runTime.exec(script);
						//res = p.waitFor();
					}
					catch (Exception e) {gestErreur.afficheMessage(texts.getText("iobug")+e.getMessage(), Nat.LOG_NORMAL); OO_Ok = false;}
					//catch (InterruptedException e) {gestErreur.afficheMessage("\nErreur de communication avec openoffice", Nat.LOG_NORMAL); OO_Ok = false;}
					if (resu != 0)
					{//le processus p ne s'est pas terminé normalement
						gestErreur.afficheMessage(texts.getText("ooeditorbug"), Nat.LOG_NORMAL);
						OO_Ok = false;
					}
	            }
			}
			else if(osName.startsWith("Windows"))
			{
				Runtime runTime = Runtime.getRuntime();
				int resu = 0;//TODO à vérifier
				gestErreur.afficheMessage(texts.getText("oostartscript"), Nat.LOG_VERBEUX);
				//obligé de copier le fichier mml sinon lock par openoffice
				File f2 = new File(TMP_MATHML+"_."+lMaths.indexOf(jmc)+".mml");
				FileToolKit.copyFile(f.getAbsolutePath(), f2.getAbsolutePath());
				fMon.addFileChangeListener(this, f2.getAbsolutePath(), 20);
				OO_Ok = fabriqueExec(Convertisseur2ODT.OS_WINDOWS,f2.getAbsolutePath());
				if (OO_Ok)
				{
					gestErreur.afficheMessage(texts.getText("ooeditorstart2"), Nat.LOG_VERBEUX);
					TextConverter tc = new TextConverter (script);
					try
					{
						tc.convert(); //conversion des lf en crlf pour impression
					}
					catch (Exception e) {gestErreur.afficheMessage(texts.getText("testconvertererror"), Nat.LOG_NORMAL);}
					try
					{
						// la ligne de commande est lancée directement pour éviter les problèmes d'encoding dans un fichier .bat
						//System.out.println(ldc);
						runTime.exec(ldc);
					}
					catch (IOException e) {gestErreur.afficheMessage(texts.getText("iobug2")+" "+e.getLocalizedMessage()+" "+ldc, Nat.LOG_NORMAL);}
					if (resu != 0)
					{//le processus p ne s'est pas terminé normalement
						gestErreur.afficheMessage(texts.getText("oostarterror"), Nat.LOG_NORMAL);
						OO_Ok = false;
					}
				}
			}
			else{gestErreur.afficheMessage(texts.getText("unknownos"), Nat.LOG_NORMAL); OO_Ok = false;}
        }
        catch (ParserConfigurationException pce)
        {
            gestErreur.afficheMessage(texts.getText("parserError"), Nat.LOG_SILENCIEUX);
            //pce.printStackTrace();
        }
        catch (TransformerConfigurationException tce)
        {
        	gestErreur.afficheMessage(texts.getText("transConfError"), Nat.LOG_SILENCIEUX);
            //tce.printStackTrace();
        }
        catch (TransformerException te)
        {
        	gestErreur.afficheMessage(texts.getText("transError"), Nat.LOG_SILENCIEUX);
            //te.printStackTrace();
        }
        catch (FileNotFoundException e)
        {
        	gestErreur.afficheMessage(texts.getText("fileNotFoundError"), Nat.LOG_SILENCIEUX);
            e.printStackTrace();
        }
	    
    }
	/**
	 * Fabrique le script de lancement de l'éditeur mathématique d'OOo en fonction de l'OS
	 * @param os indique le système d'exploitation
	 * @param fichMath nom du fichier temporaire mathml
	 * @return true si OO a été détecté et que le script a bien été créé
	 */
	public boolean fabriqueExec(int os, String fichMath)
	{
		Boolean exec_ok = true;
		
        String pathOO = "";
        
		if (os == Convertisseur2ODT.OS_MAC)
		{
	        
			gestErreur.afficheMessage(texts.getText("lookingforoo")+" ", Nat.LOG_VERBEUX);
            File OfficePath = new File ("/Applications/LibreOffice.app");
            if (!OfficePath.exists()) {OfficePath = new File ("/Applications/OpenOffice.org.app");}
            if (!OfficePath.exists())
            {
                gestErreur.afficheMessage(texts.getText("oonotinstalled1") +
                		texts.getText("oonotinstalled2") +
                        " "+texts.getText("oonotinstalled3")+" " +
                        texts.getText("oonotinstalled4") +
                        " "+texts.getText("oonotinstalled5")+
                        texts.getText("oonotinstalled6"), Nat.LOG_SILENCIEUX);
                exec_ok = false;
                	
            }
            else
            {
            	try {
					String cmd = "open "+OfficePath.getCanonicalPath()+" "+fichMath+" --args -nolockcheck";
					gestErreur.afficheMessage(texts.getText("osmac"), Nat.LOG_VERBEUX);
	    			gestErreur.afficheMessage(texts.getText("scriptcreation"), Nat.LOG_VERBEUX);
	    			BufferedWriter fich = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(script),"UTF-8"));
	    			fich.write("#!/bin/bash\n#"+texts.getText("filegeneratedby")+" NatBraille\n");
	    			fich.write("exec " + cmd+"\n");
    				fich.close();
                    Runtime.getRuntime().exec("chmod 755 " + script);
				} catch (Exception e) {
					gestErreur.afficheMessage(texts.getText("startscripterror") + e.getMessage(),Nat.LOG_NORMAL);
					exec_ok = false;
				}
            }
		}
		else if(os == Convertisseur2ODT.OS_LINUX)
		{
	        BufferedReader bufferedReader = null;
	        InputStream out = null;
	
	        try {
	            /**recherche d'une instance d'openOffice déjà lancé**/
	            gestErreur.afficheMessage(texts.getText("lookingforoo")+" ", Nat.LOG_VERBEUX);
	            out = Runtime.getRuntime().exec("which soffice").getInputStream();
	            bufferedReader = new BufferedReader(new InputStreamReader(out));
	            pathOO = bufferedReader.readLine();
	            if (pathOO==null)
	            {
	                gestErreur.afficheMessage(texts.getText("oonotinstalled1") +
	                		texts.getText("oonotinstalled2") +
	                        " "+texts.getText("oonotinstalled3")+" " +
	                        texts.getText("oonotinstalled4") +
	                        " "+texts.getText("oonotinstalled5")+
	                        texts.getText("oonotinstalled6"), Nat.LOG_SILENCIEUX);
	                exec_ok = false;
	                	
	            }
	            else
	            {
	                String cmd =ldc.replaceFirst("soffice",pathOO + " -nolockcheck");
	                cmd+= " "+fichMath;
	                gestErreur.afficheMessage(texts.getText("oslinux"), Nat.LOG_VERBEUX);
	    			gestErreur.afficheMessage(texts.getText("scriptcreation"), Nat.LOG_VERBEUX);
	    			try
	    			{
	    				BufferedWriter fich = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(script),"UTF-8"));
	    				//FileWriter fichierXSL = new FileWriter(filtre);
	    				fich.write("#!/bin/sh\n#"+texts.getText("filegeneratedby")+" NatBraille\n");
	    				fich.write("exec=exec\n");
	    				fich.write("exec " + cmd);
	                    Runtime.getRuntime().exec("chmod 755 " + script);
	    				fich.close();
	    			}
	    			catch (IOException e)
	    			{
	    				gestErreur.afficheMessage(texts.getText("oostartingscripterror") + e,Nat.LOG_NORMAL);
	    				exec_ok = false;
	    			}
	            }
	        } 
	        catch (Exception officeException) 
	        {
	            officeException.printStackTrace();
	            gestErreur.afficheMessage(texts.getText("cantfindoo"), Nat.LOG_NORMAL);
	            exec_ok = false;
	        }	
		}
		else if(os == Convertisseur2ODT.OS_WINDOWS)
		{
	
	        if (!script.endsWith(".bat")){script+=".bat";} //ajout de l'extension afin que le script tourne sous Windows
			gestErreur.afficheMessage(texts.getText("oswindows"), Nat.LOG_VERBEUX);
			gestErreur.afficheMessage(texts.getText("scriptcreation"), Nat.LOG_VERBEUX);
	        Path path = new Path(gestErreur);
	        pathOO = path.getOOPath();
	        if (pathOO.isEmpty())
	        {
	        	gestErreur.afficheMessage(texts.getText("oonotinstalled1") +
	                		texts.getText("oonotinstalled2") +
	                        " "+texts.getText("oonotinstalled3")+" " +
	                        texts.getText("oonotinstalled4") +
	                        " "+texts.getText("oonotinstalled5")+
	                        texts.getText("oonotinstalled6"), Nat.LOG_SILENCIEUX);
	            exec_ok = false;
	        }
	        //gest.afficheMessage(pathOO,Nat.LOG_VERBEUX);
			if (exec_ok) try
			{
				ldc = "soffice";
				BufferedWriter fich = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(script), "windows-1252"));
	            String cmd = ldc.replaceFirst("soffice","\""+pathOO.trim()+"\"");
	            gestErreur.afficheMessage("path oo:"+pathOO+":",Nat.LOG_DEBUG);
	            cmd += " -o \"" + fichMath + "\"";//-o pas en readonly
				fich.write(cmd);
				// le script ne sert pas (problème encoding sous windows)
				ldc=cmd;
				fich.close();
			}
			catch (IOException e)
			{
				gestErreur.afficheMessage(texts.getText("startscripterror") + e,Nat.LOG_NORMAL);
				exec_ok = false;
			}
		}
		else
		{
			gestErreur.afficheMessage(texts.getText("unknownos2"),Nat.LOG_SILENCIEUX);
			exec_ok = false;
		}
		
		return exec_ok;
	}
	/**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseEntered(MouseEvent arg0){/*rien*/}
	/**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseExited(MouseEvent arg0){/*rien*/}
	/**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent arg0){/*rien*/}
	/**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent arg0){/*rien*/}
	/**
	 * Le fichier a été modifié
     * @see ui.listener.FileChangeListener#fileChanged(java.lang.String)
     */
    @Override
    public void fileChanged(String fileName)
    {
    	//création du DOM
		org.w3c.dom.Document doc;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG);
    
        DocumentBuilder db;
		try
		{
			dbf.setNamespaceAware(true);
			db = dbf.newDocumentBuilder();
			db.setEntityResolver(new EntityResolverNull());
			File f=new File(fileName);
			doc = db.parse(f);
			System.out.println(texts.getText("tempfilename") + fileName);
			String [] decoupNom = fileName.split("\\.");
			int indice = Integer.parseInt(decoupNom[decoupNom.length-2]);
			Node math = doc.getElementsByTagName("math").item(0);
			//en cas de problème avec office...
			if(!math.hasChildNodes()){math = doc.getElementsByTagName("math:math").item(0);}
			if(!math.hasChildNodes()){math = doc.getElementsByTagName("m:math").item(0);}
			
			lMaths.get(indice).setDocument(math);
			apercu.repaint();
			//suppression de l'écouteur
			fMon.removeFileChangeListener(this, fileName);
			//f.setWritable(true);
		}
		catch (ParserConfigurationException e)
		{
			gestErreur.afficheMessage(texts.getText("parserError"), Nat.LOG_SILENCIEUX);
			//e.printStackTrace();
		}
		catch (SAXException e)
		{
			gestErreur.afficheMessage(texts.getText("saxError"), Nat.LOG_SILENCIEUX);
			//e.printStackTrace();
		}
		catch (IOException e)
		{
			gestErreur.afficheMessage(texts.getText("ioError"), Nat.LOG_SILENCIEUX);
			//e.printStackTrace();
		}
	    
    }
    
    /**
     * Prend en charge l'utilisation de ENTER sur un JMathComponent
     */
    @Override
    public void keyPressed(KeyEvent ke)
    {
    	if(ke.getKeyCode()==KeyEvent.VK_ENTER && ke.getSource() instanceof JMathComponent)
    	{
    		this.editFormule((JMathComponent)(ke.getSource()));
    	}/*
    	else
    	{
    		super.keyPressed(ke);
    	}*/
    }
    
    /**
	 * Vérifie si il faut demander l'enregistrement
	 * Enregistre la position de la barre de division
	 * Appelle {@link EditeurBraille#windowClosing(WindowEvent)}
	 * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosing(WindowEvent arg0)
	{
		ConfigNat.getCurrentConfig().setSplitPositionEditorTan(splitPane.getDividerLocation());
		super.windowClosing(arg0);
	}
	
    
    /**
     * Supprime aussi {@link #panneauLog} des écouteurs de logs
     * @see java.awt.Window#dispose()
     */
    @Override
    public void dispose()
    {
    	nat.getGestionnaireErreur().removeAfficheur(panneauLog);
    	super.dispose();
    }
	/**
     * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
     */
    @Override
    public void keyReleased(KeyEvent e)
    {
	    // TODO Auto-generated method stub
	    
    }
	/**
     * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
     */
    @Override
    public void keyTyped(KeyEvent e)
    {
	    // TODO Auto-generated method stub
	    
    }
    
    /* Se lance trop trop souvent sous windows... à voir si on conserve ou pas
     * Lance les mises à jour des deux zones d'édition si il y a eu des modifs
     * @see ui.EditeurBraille#focusLost(java.awt.event.FocusEvent)
     *
    @Override
    public void focusLost(FocusEvent fe)
    {
    	super.focusLost(fe);
    	if(fe.getSource()== resultat && modifBraille)
    	{
    		if(detranscrit()){afficheNoir();}
    		modifBraille = false;
    		modifNoir = false;
    	}
    	else if(fe.getSource()==apercu && modifNoir)
    	{
    		transcrit();
    		modifNoir = false;
    		modifBraille = false;
    	}
    }*/
    
    /*
     * @see ui.EditeurBraille#insertUpdate(javax.swing.event.DocumentEvent)
     *
    @Override
    public void insertUpdate(DocumentEvent de)
    {
    	if(de.getDocument()==resultat.getDocument()){modifBraille = true;}
    	else if(de.getDocument()==apercu.getDocument()){modifNoir = true;}
    }*/
}

