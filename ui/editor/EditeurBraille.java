/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui.editor;

import gestionnaires.GestionnaireErreur;
import gestionnaires.GestionnaireExporter;

import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.UndoManager;

import nat.ConfigNat;
import outils.FileToolKit;
import outils.TextSender;
import outils.emboss.Embosseur;
import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;
import ui.listener.PerkinsNotifier;

/**
 * Classe décrivant un éditeur braille dans nat
 * @author bruno
 *
 */
public abstract class EditeurBraille extends JFrame implements WindowListener, ActionListener, ComponentListener, TextSender, DocumentListener, UndoableEditListener,FocusListener
{
	/** Textual contents */
	protected static Language texts = new Language("EditeurBraille");

	/** Objet embosseur, pour l'embossage bien entendu...*/
	protected Embosseur embosseur;
	/** identifiant par défaut pour la sérialisation (non utilisé dans NAT)*/ 
	private static final long serialVersionUID = 1L;
	/** le label contenant le nom du fichier résultat */
	protected JLabel lFichier = new JLabel(texts.getText("file")+" ");
	/** le panneau contenant les éléments à afficher et le scrollPane*/
	protected JPanel panneauAffichage = new JPanel();
	/** la zone d'affichage principale du fichier */
	protected BrailleTextPane resultat = new BrailleTextPane(this);
	/** second panneau d'affichage*/
	protected JTextPane apercu = new JTextPane();
	/** le ScrollPane associé au JTextPane resultat */
	protected JScrollPane scrollRes;
	/** JButton pour enregistrer le fichier */
	protected JButton btEnregistrer = null;
	/** JButton pour enregistrer le fichier */
	protected JButton btEnregistrersous = null;
    /** JButton pour fermer la fenêtre */
    protected JButton btFermer = null;
	/** L'adresse du fichier produit */
	private String fichierOut = null;
	/** encodage du fichier transcrit */
	protected String encodage ="UTF-8";
	/** Jlabel d'information pour l'enregistrement */
	protected JLabel message= new JLabel("");
	/** taille du caractère de la police principale en point (ne fonctionne qu'avec les polices à
	 * chasse fixe */
	protected int tailleCaractere;
	/** JCheckBoxpour l'activation du mode perkins */
	protected JCheckBox jcbPerkins = new JCheckBox(texts.getText("perkins"));
	/** JButton pour lancer l'embossage */
	protected JButton btEmbosser = null;
	/** splitpane entre les deux panneaux d'affichage */
	protected JSplitPane splitPane;
	
	/** JMenu manipulation fichier */
	protected JMenu jmFichier = new JMenu(texts.getText("fileMenu"));
    /** JMenu édition */
    protected JMenu jmEdition = new JMenu(texts.getText("editMenu"));
    /** JMenu affichage */
    protected JMenu jmView = new JMenu(texts.getText("viewMenu"));
    /** Menu bar de la fenetre */
	protected JMenuBar jmb = new JMenuBar();
	/** Menu item enregister*/
	protected JMenuItem jmiEnregistrer;
	/** Menu item enregister*/
	protected JMenuItem jmiEnregistrerSous;
	/** Menu item quitter*/
	protected JMenuItem jmiQuitter;
	/** Menu item undo*/
	protected JMenuItem jmiUndo;
	/** Menu item redo*/
	protected JMenuItem jmiRedo;
	/** Menu item activer perkins*/
	protected JMenuItem jmiPerkins;
	/** Menu item embosser*/
	protected JMenuItem jmiEmboss;
	/** Menu item afficher uniquement le panneau de gauche*/
	protected JMenuItem jmiShowLeft;
	/** Menu item uniquement le panneau de droite*/
	protected JMenuItem jmiShowRight;
	/** Menu item revenir à la position de départ du splitpane*/
	protected JMenuItem jmiResetPos;
	/** Menu item pour maximiser la fenêtre */
	private JMenuItem jmiMaximizeWindow;
	
	
	
	//private boolean resizing = false;
	
	//fonctionalités pour l'édition
	/** Undo manager */
	protected UndoManager undoMng = new UndoManager();
	/** JButton pour annuler frappe */
	protected JButton btUndo = new JButton(new ImageIcon("ui/icon/edit-undo.png"));
	/** JButton pour répéter frappe */
	protected JButton btRedo = new JButton(new ImageIcon("ui/icon/edit-redo.png"));
	
	/** Position du curseur **/
	protected int positionCurseur = 0;
	/** Position de la barre de division du splitpane */
	private int divLoc = 0;
	
	/** Indique si des modifications sont en cours */
	protected boolean enModif = false;
	/** Indique si il y a des modification non enregistrées */
	protected boolean modif=false;
	
	/** Instance du gestionnaire d'erreur */
	protected GestionnaireErreur gestErreur;
	
	/** The Perkins notifier*/
	protected PerkinsNotifier pn = null;
	
	
	/** 
	 * Construit un objet EditeurBraille avec la table braille UTF8
	 * @param nom le nom de la fenêtre
	 * @param emb l'objet Embosseur à utiliser pour l'embossage
	 * @param g instance de GestionnaireErreur
	 * @param table table braille utilisée pour {@link #resultat} et {@link #pn}
	 */
	public EditeurBraille(String nom, Embosseur emb, GestionnaireErreur g, String table)
	{
		super(nom);
		fabriqueEditeur(emb,g,table);
		fichierOut = ConfigNat.getCurrentConfig().getFBraille();
	}
	
	/** 
	 * Construit un objet EditeurBraille avec la table braille de la configuration
	 * @param nom le nom de la fenêtre
	 * @param emb l'objet Embosseur à utiliser pour l'embossage
	 * @param g instance de GestionnaireErreur
	 */
	public EditeurBraille(String nom, Embosseur emb, GestionnaireErreur g)
	{
		super(nom);
		String table="";
		if(ConfigNat.getCurrentConfig().getIsSysTable()){table="xsl/tablesBraille/";}
		else{ConfigNat.getUserBrailleTableFolder();}
		table += ConfigNat.getCurrentConfig().getTableBraille();
		fabriqueEditeur(emb,g,table);
	}
	
	/** 
	 * Fabrique un objet EditeurBraille
	 * @param emb l'objet Embosseur à utiliser pour l'embossage
	 * @param g instance de GestionnaireErreur
	 * @param table table braille utilisée pour {@link #resultat} et {@link #pn}
	 */
	private void fabriqueEditeur(Embosseur emb, GestionnaireErreur g, String table)
	{
		try
	    {
	        pn = new PerkinsNotifier(FileToolKit.getSysDepPath(new File(table).getAbsolutePath()));
	    }
	    catch (NumberFormatException e1)
	    {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
	    }
	    catch (IOException e1)
	    {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
	    }
		//pn.addPerkinsObserver(this);
		embosseur = emb;
		gestErreur = g;
		
		addComponentListener(this);
		addWindowListener(this);
		
		/*
		 * Menus
		 */
		jmFichier.setMnemonic('F');
		jmEdition.setMnemonic('E');
		jmView.setMnemonic('A');
		setJMenuBar(jmb);
		jmb.add(jmFichier);
		jmb.add(jmEdition);
		jmb.add(jmView);
		
		jmiEnregistrer= new JMenuItem(EditeurBraille.texts.getText("save"), new ImageIcon("ui/icon/document-save.png"));
	    jmiEnregistrer.setMnemonic('s');
	    jmiEnregistrer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiEnregistrer.addActionListener(this);
	    
	    jmiEnregistrerSous= new JMenuItem(EditeurBraille.texts.getText("saveas"),new ImageIcon("ui/icon/document-save-as.png"));
	    jmiEnregistrerSous.setMnemonic('r');
	    jmiEnregistrerSous.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiEnregistrerSous.addActionListener(new GestionnaireExporter(this,this,GestionnaireExporter.EXPORTER_BRF,g));
	    
	    jmiEmboss= new JMenuItem(EditeurBraille.texts.getText("emboss"), new ImageIcon("ui/icon/document-print.png"));
	    jmiEmboss.setMnemonic('e');
	    jmiEmboss.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiEmboss.addActionListener(this);
	    
	    jmiQuitter= new JMenuItem(EditeurBraille.texts.getText("close"), new ImageIcon("ui/icon/exit.png") );
	    jmiQuitter.setMnemonic('q');
	    jmiQuitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiQuitter.addActionListener(this);
	    
	    jmiUndo = new JMenuItem(EditeurBraille.texts.getText("undo"), new ImageIcon("ui/icon/edit-undo.png"));
	    jmiUndo.setMnemonic('n');
	    jmiUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiUndo.addActionListener(this);
	    jmiUndo.setEnabled(false);
	    
	    jmiRedo = new JMenuItem(EditeurBraille.texts.getText("redo"), new ImageIcon("ui/icon/edit-redo.png"));
	    jmiRedo.setMnemonic('i');
	    jmiRedo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiRedo.addActionListener(this);
	    jmiRedo.setEnabled(false);
	    
	    jmiPerkins = new JMenuItem(EditeurBraille.texts.getText("perkins"));
	    jmiPerkins.setMnemonic('p');
	    jmiPerkins.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
	    jmiPerkins.addActionListener(this);
	    
	    
	    jmiShowRight = new JMenuItem(EditeurBraille.texts.getText("showRight"));
	    jmiShowRight.setMnemonic('g');
	    //jmiShowRight.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK+ActionEvent.ALT_MASK, false));
	    jmiShowRight.addActionListener(this);
	    
	    jmiShowLeft = new JMenuItem(EditeurBraille.texts.getText("showLeft"));
	    jmiShowLeft.setMnemonic('d');
	    //jmiShowLeft.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.CTRL_MASK+ActionEvent.ALT_MASK, false));
	    jmiShowLeft.addActionListener(this);
	    
	    jmiResetPos = new JMenuItem(EditeurBraille.texts.getText("restoreView"));
	    jmiResetPos.setMnemonic('r');
	    jmiResetPos.addActionListener(this);
	    //jmiResetPos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK+ActionEvent.ALT_MASK,false));
	    
	    jmiMaximizeWindow = new JMenuItem(texts.getText("maxwin"));
	    jmiMaximizeWindow.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F11,0,false));
	    jmiMaximizeWindow.addActionListener(this);
	   
	    jmView.add(jmiShowRight);
	    jmView.add(jmiShowLeft);
	    jmView.add(jmiResetPos);
	    jmView.add(jmiMaximizeWindow);
	    
		/* 
		 * Boutons 
		 */
	    if(ConfigNat.getCurrentConfig().getShowIconText()){btEnregistrer = new JButton(texts.getText("save"),new ImageIcon("ui/icon/document-save.png"));}
	    else{btEnregistrer=new JButton(new ImageIcon("ui/icon/document-save.png"));}
		btEnregistrer.addActionListener(this);
	    Context cbtEnregistrer = new Context("s","Button","save",texts);
	    new ContextualHelp(btEnregistrer,cbtEnregistrer);
		btEnregistrer.setMnemonic('s');
		
		if(ConfigNat.getCurrentConfig().getShowIconText()){ btEnregistrersous = new JButton(texts.getText("saveas"),new ImageIcon("ui/icon/document-save-as.png"));}
		else{btEnregistrersous = new JButton(new ImageIcon("ui/icon/document-save-as.png"));}
		btEnregistrersous.addActionListener(new GestionnaireExporter(this,this,GestionnaireExporter.EXPORTER_BRF,g));
		Context cbtEnregistrersous = new Context("n","Button","saveas",texts);
		new ContextualHelp(btEnregistrersous,cbtEnregistrersous);
		btEnregistrersous.setMnemonic('n');
		
		if(ConfigNat.getCurrentConfig().getShowIconText()){ btFermer =  new JButton(texts.getText("close"),new ImageIcon("ui/icon/exit.png"));}
		else{btFermer =  new JButton(new ImageIcon("ui/icon/exit.png"));}
		btFermer.addActionListener(this);
		Context cbtFermer = new Context("q","Button","close",texts);
		new ContextualHelp(btFermer,cbtFermer);
		btFermer.setMnemonic('q');
		
		if(ConfigNat.getCurrentConfig().getShowIconText()){btEmbosser = new JButton(texts.getText("emboss"),new ImageIcon("ui/icon/document-print.png"));}
		else{btEmbosser = new JButton(new ImageIcon("ui/icon/document-print.png"));}
		btEmbosser.addActionListener(this);
		Context cbtEmbosser = new Context("","Button","emboss",texts);
		new ContextualHelp(btEmbosser,cbtEmbosser);
		//btEmbosser.setMnemonic('e');
		if(embosseur==null){btEmbosser.setEnabled(false);}
		
		Context cjcbPerkins = new Context("p","CheckBox","perkins",texts);
		new ContextualHelp(jcbPerkins,cjcbPerkins);
		jcbPerkins.setMnemonic('p');
		jcbPerkins.addActionListener(this);
		
		btUndo.addActionListener(this);
		Context cbtUndo = new Context("z","Button","undo",texts);
		new ContextualHelp(btUndo,cbtUndo);
		btUndo.setMnemonic('z');
		btUndo.setEnabled(false);
		
		btRedo.addActionListener(this);
		Context cbtRedo = new Context("y","Button","redo",texts);
		new ContextualHelp(btRedo,cbtRedo);
		btRedo.setMnemonic('y');
		btRedo.setEnabled(false);
		
		scrollRes = new JScrollPane (resultat);
		scrollRes.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollRes.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		lFichier.setLabelFor(resultat);
		//lFichier.setDisplayedMnemonic('t');
		
		//jcPerkinsObserver.add(resultat);
		//resultat.addKeyListener(pn);
	}
	/*
	 * Assesseurs
	 * 
	 */
	/** Méthode d'accès, modifie la valeur de l'encodage 
	 * @param enc valeur pour {@link #encodage}*/
	public void setEncodage(String enc){encodage = enc;}

	
	/*
	 * Méthodes	 * 
	 */
	/** Vérifie l'état du manager undo et active ou désactive les boutons undo/redo */
	private void verifBtEdit() 
	{
		btUndo.setEnabled(undoMng.canUndo());
		jmiUndo.setEnabled(undoMng.canUndo());
		btRedo.setEnabled(undoMng.canRedo());
		jmiRedo.setEnabled(undoMng.canRedo());
	}
	/** 
	 *	Enregistre le fichier contenu dans {@link #resultat} dans {@link #fichierOut}
	 */
	protected void enregistrerFichier()
	{
		if (FileToolKit.saveStrToFile(getText(), fichierOut))
		{
			message.setText(texts.getText("filesaved"));
			modif=false;
		}	
		else{message.setText("<html><p color=\"red\">"+texts.getText("bugwhilesaving")+"</p></html>");}
	}
	/** 
	 *	Enregistre le fichier contenu dans {@link #resultat} dans {@link #fichierOut}
	 *	Utilise <code>encoding</code> comme encodage
	 *	@param encoding encodage à utiliser
	 */
	protected void enregistrerFichier(String encoding)
	{
		if (FileToolKit.saveStrToFile(getText(), fichierOut, encoding))
		{
			message.setText(texts.getText("filesaved"));
			modif=false;
		}	
		else{message.setText("<html><p color=\"red\">"+texts.getText("bugwhilesaving")+"</p></html>");}
	}
	
	
	/**
	 * Ajoute au document <code>doc</code> les listeners nécéssaires
	 * @param doc le document
	 */
	protected void ajouteListenerDoc(Document doc)
	{
		resultat.setDocument(doc);
		resultat.setCaretPosition(0);
		resultat.getDocument().addDocumentListener(this);
		resultat.getDocument().addUndoableEditListener(this);
	}
	
	/**
	 * Vérifie si il faut enregistrer le fichier
	 */
	private void verifDoc()
	{
		if(modif)
		{
			if(JOptionPane.showConfirmDialog( this,texts.getText("wanttosave"),texts.getText("saving"), JOptionPane.YES_NO_OPTION)==JOptionPane.OK_OPTION)
			{
				enregistrerFichier();
			}
		}
		dispose();		
	}
	
	/*
	 * 
	 * Implémentations
	 */
	/**
	 * Envoie l'adresse du fichier d'origine ({@link #fichierOut}
	 * @see outils.TextSender#getOrigine()
	 */
	@Override
    public String getOrigine() {return getFichier();}
	
	/** Non implémentée ici
	 * @see outils.TextSender#getText()
	 */
	@Override
    public abstract String getText();
	
	/** 
	 * Implémente la méthode actionPerformed d'ActionListener
	 * Gère les actions des boutons et met à jour l'InputMap du JTextPane resultat en fonction de
	 * l'état du JCheckBox jcbPerkins
	 * @param evt l'objet ActionEvent
	 */
	@Override
    public void actionPerformed(ActionEvent evt)
	{
		if(evt.getSource()==btEnregistrer || evt.getSource()==jmiEnregistrer)
		{
			/* autre fichier que fichier? encodage en utf-8 */
			if(getFichier()==fichierOut){enregistrerFichier();}
			else{enregistrerFichier("UTF-8");}
			resultat.grabFocus();
		}
		else if (evt.getSource() == btFermer || evt.getSource()==jmiQuitter){verifDoc();}
		else if ((evt.getSource() == btUndo|| evt.getSource()== jmiUndo) && undoMng.canUndo())
		{
			undoMng.undo();
			verifBtEdit();
			resultat.grabFocus();
		}
		else if ((evt.getSource() == btRedo || evt.getSource()==jmiRedo)&& undoMng.canRedo())
		{
			undoMng.redo();
			verifBtEdit();
			resultat.grabFocus();
		}
		else if(evt.getSource()==jmiPerkins)
		{
			//on rend le focus à celui qui l'avait avant
			JComponent currentFocus = (JComponent) this.getFocusOwner();
			jcbPerkins.doClick(); 
			currentFocus.grabFocus();
		}
		else if(evt.getSource()==jcbPerkins)
		//cet évènement est géré ici pour editeurtan et editeur2;
		//il y a un écouteur perkins de plus pour editeur2
		{
			if(jcbPerkins.isSelected()){
				pn.addPerkinsObserver(resultat);
				}
			else{
				pn.removePerkinsObserver(resultat);
				}
		}
		else if(evt.getSource()==jmiShowRight)
		{
			if (splitPane.getDividerLocation()> splitPane.getMinimumDividerLocation() &&
				splitPane.getDividerLocation()< splitPane.getMaximumDividerLocation())
				{divLoc = splitPane.getDividerLocation();}
			splitPane.grabFocus();
			/*try{ c'était fait comme ça avant mais c'est pô propre du tout !
			 * pour que ça marche, il faut juste initialiser le setminimumsize à 0 des composants du splitpane. 
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_F8);
			r.keyRelease(KeyEvent.VK_F8);
			r.keyPress(KeyEvent.VK_HOME);
			r.keyRelease(KeyEvent.VK_HOME);
			}
            catch (AWTException e){gestErreur.afficheMessage(texts.getText("robotError"), Nat.LOG_SILENCIEUX);}*/
			splitPane.setDividerLocation(splitPane.getMinimumDividerLocation());
		}
		else if(evt.getSource()==jmiShowLeft)
		{
			if (splitPane.getDividerLocation()> splitPane.getMinimumDividerLocation() &&
				splitPane.getDividerLocation()< splitPane.getMaximumDividerLocation())
				{divLoc = splitPane.getDividerLocation();}
			splitPane.grabFocus();
			/*try{
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_F8);
			r.keyRelease(KeyEvent.VK_F8);
			r.keyPress(KeyEvent.VK_END);
			r.keyRelease(KeyEvent.VK_END);
			}
            catch (AWTException e){gestErreur.afficheMessage(texts.getText("robotError"), Nat.LOG_SILENCIEUX);}*/
			splitPane.setDividerLocation(splitPane.getMaximumDividerLocation());
		}
		else if(evt.getSource()==jmiResetPos)
		{
			//cas où un des deux panneaux est déjà masqué par défaut
			if(divLoc >splitPane.getMinimumDividerLocation() && divLoc < splitPane.getMaximumDividerLocation())
				{splitPane.setDividerLocation(divLoc);}
			else
			{
				splitPane.resetToPreferredSizes();
				divLoc = splitPane.getDividerLocation();
			}
		}
		else if (evt.getSource() == jmiMaximizeWindow)
	    	{if (this.getExtendedState()==Frame.MAXIMIZED_BOTH)
	    	{this.setExtendedState(Frame.NORMAL);}
	    	else {this.setExtendedState(Frame.MAXIMIZED_BOTH);}
	    	}
	}

	/**
	 * Implémente removeUpdate de DocumentListener
	 * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
	 */
	@Override
    public void removeUpdate(DocumentEvent de) 
	{
		message.setText(texts.getText("filemodified"));		
		verifBtEdit();
	}
	/**
	 * Implémente undoableEditHappened de UndoableEditListener
	 * @see javax.swing.event.UndoableEditListener#undoableEditHappened(javax.swing.event.UndoableEditEvent)
	 */
	@Override
    public void undoableEditHappened(UndoableEditEvent uee) 
	{
		undoMng.addEdit(uee.getEdit());
		verifBtEdit();
	}
	/**
	 * Implémente focusGained de Focus Listener
	 * positionne le curseur sur le text area
	 * @see java.awt.event.FocusListener#focusGained(java.awt.event.FocusEvent)
	 */
	@Override
    public void focusGained(FocusEvent arg0){resultat.setCaretPosition(positionCurseur);}
	/**
	 * Ne fait rien de plus
	 * @see java.awt.event.FocusListener#focusLost(java.awt.event.FocusEvent)
	 */
	@Override
    public void focusLost(FocusEvent arg0) 
		{
		//par Fred : je le vire pour voir si ctrlF6 marche mieux
		positionCurseur=resultat.getCaretPosition();
		}
	
	/**
	 * Ne fait rien
	 * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowActivated(WindowEvent arg0) {/*rien*/}
	/**
	 * ne fait rien
	 * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosed(WindowEvent arg0) {/*rien*/}
	/**
	 * Vérifie si il faut demander l'enregistrement
	 * Appelle {@link #verifDoc()}
	 * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowClosing(WindowEvent arg0) {verifDoc();/*rien*/}
	/**
	 * Ne fait rien
	 * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowDeactivated(WindowEvent arg0) {/*rien*/}
	/**
	 * Ne fait rien
	 * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowDeiconified(WindowEvent arg0) {/*rien*/}
	/**
	 * Ne fait rien
	 * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowIconified(WindowEvent arg0) {/*rien*/}
	/**
	 * Ne fait rien
	 * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
	 */
	@Override
	public void windowOpened(WindowEvent arg0) {/*rien*/}
	
		
	
	/** Méthode redéfinie de DocumentListener
	 * Affiche un message si le test area est modifié
	 * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
	 */
	@Override
    public void insertUpdate(DocumentEvent de) 
	{
		if(de.getDocument()==resultat.getDocument())
		{
			message.setText("Fichier modifié");
			modif=true;
			positionCurseur=  resultat.getCaretPosition();
			verifBtEdit();
		}
	}
	/**
	 * Ne fait rien de plus
	 * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
	 */
	@Override
    public void changedUpdate(DocumentEvent de){/*rien*/}
	
	/** Méthode redéfinie de ComponentListener
	 * Ne fait rien
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentHidden(ComponentEvent arg0){/*do nothing*/}
	/** Méthode redéfinie de ComponentListener
	 * Ne fait rien
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentMoved(ComponentEvent arg0){/*do nothing*/}
	/** Méthode redéfinie de ComponentListener
	 * Ne fait rien
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentShown(ComponentEvent arg0){/*do nothing*/}
	
	/** Méthode redéfinie de ComponentListener
	 * Ne fait rien
	 * @param arg0 Le ComponentEvent
	 */
	@Override
    public void componentResized(ComponentEvent arg0){/*do nothing*/}

	/**
	 * Change le nom du fichier produit
     * @param nf le nouveau nom du fichier
     */
    public void setFichier(String nf)
    {
    	fichierOut = nf;
    	lFichier.setText(texts.getText("file")+fichierOut);
    	setTitle("NAT" + fichierOut);
    }

	/**
	 * Renvoie {@link #fichierOut}
     * @return le nom du fichier produit
     */
    public String getFichier(){return fichierOut;}
    
    /**
     * Manage errors sent by PerkinsObservers
     * @param code error code
     * @param m error message
     */
    public void manageError(int code, String m)
    {
	    switch(code)
		{
			case PerkinsNotifier.FILE_NOT_VALID:
				System.err.println(texts.getText("thefile") +" "+ m +" "+ texts.getText("isnotvalid"));
				message.setText("<html><br><p color=\"red\">"+texts.getText("thefile") +" "+ m + " "+ texts.getText("isnotvalid")+"</p></html>");
				break;
			
		}
    }
	
}
