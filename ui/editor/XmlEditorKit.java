/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* Original licence
 * Copyright 2006-2008 Kees de Kooter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ui.editor;

import javax.swing.text.StyledEditorKit;
import javax.swing.text.ViewFactory;


/**
 * @author kees
 * date: 12-jan-2006
 *
 */
public class XmlEditorKit extends StyledEditorKit {

	/** for serialisation */
    private static final long serialVersionUID = 2969169649596107757L;
    /** The EditorKit view Factory*/
    private ViewFactory xmlViewFactory;

    /** Constructor
     * Build the {@link #xmlViewFactory}
     */
    public XmlEditorKit() {
        xmlViewFactory = new XmlViewFactory();
    }
    
    /**
     * @see javax.swing.text.StyledEditorKit#getViewFactory()
     */
    @Override
    public ViewFactory getViewFactory() {
        return xmlViewFactory;
    }

    /**
     * @see javax.swing.text.DefaultEditorKit#getContentType()
     * return "text/xml"
     */
    @Override
    public String getContentType() {
        return "text/xml";
    }

    
}
