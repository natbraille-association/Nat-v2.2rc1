/*
 * Trace assistant
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ui.dialog;

import java.awt.Font;
import java.awt.GridLayout;
import java.io.File;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 * information sur la légende utilisée
 * @author bruno
 *
 */
public class DialogueLegende extends JDialog
{

	/** Pour la sérialisation, non utilisé */
    private static final long serialVersionUID = 1L;

    /**
     * Affiche la légende des caractères spéciaux
     * @param f la fenêtre appelante
     * @param carSpec tableau des caractères utilisés
     */
    public DialogueLegende(JFrame f,String[] carSpec)
    {
    	super(f, "Signification des caractères spéciaux");
    	setLayout (new GridLayout(carSpec.length,1));
    	Font fonte;
        try
        {
            fonte = Font.createFont(Font.PLAIN, new File("ui/fontes/DejaVuSans.ttf"));
            fonte = fonte.deriveFont(Font.PLAIN, 11);
        }
        catch (Exception e){fonte = new Font("DejaVu Sans",Font.PLAIN,11);}
    	
        String[] texts = {"coupure",
        		"coupure esthétique en mathématique",
        		"début d'expression mathématique",
        		"fin d'expression mathématique",
        		"espace insécable à générer",
        		"pour l'espace sécable à générer",
        		"début de table",
        		"fin de table",
        		"saut de ligne à générer",
        		"ne plus couper à partir de là"};
    	if(carSpec.length == texts.length)
    	{
    		for(int i=0;i<carSpec.length;i++)
    		{
    			JLabel l = new JLabel(carSpec[i]+": "+texts[i]);
    			l.setFont(fonte);
    			add(l);
    		}
    	}
    	else
    	{
    		add(new JLabel("erreur: pas d'informations"));
    	}
    	
    	setModal(false);
    	pack();
    	setVisible(true);
    	
    }
}
