/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ui.dialog;
import gestionnaires.GestionnaireErreur;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
//import java.util.Collections;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import outils.FileToolKit;
import outils.ProperNounDetector;
import ui.FiltreFichier;
import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;

import nat.ConfigNat;
import nat.Nat;

/**
 * Cette classe de dialogue permet l'édition et l'enregistrement des listes de mots pour l'abrégé:
 * liste des mots en intégral ()
 * liste des mots avec ivb ()
 * @author bruno
 *
 */
public class DialogueListe extends JDialog implements WindowListener, ActionListener, KeyListener
{
	/** Constante représentant la liste de mot avec IVB */ 
	public final static int IVB_LIST = 1;
	/** Constante représentant la liste de mot en intégral */
	public final static int INTEGRAL_LIST = 2;
	/** Textual contents */
	private Language texts = new Language("DialogueIntegral");
	
	/** pour la sérialisation (non utilisé)*/
    private static final long serialVersionUID = 1L;
    /** données de la table*/
	private ArrayList<ArrayList<Object>> donnees = new ArrayList<ArrayList<Object>>();
    /** table */
    private JTable table;
    /** Modèle pour la table */
	private TableModeleIntegral tm;
    /** Valeur de retour */
    private static int retour=JOptionPane.YES_OPTION;
    /** indique si la table a été modifiée */
    private boolean modif=false;
    /** nom du fichier contenant la liste de mots à garder en intégral */
    private String listeFich = ConfigNat.getUserTempFolder()+"listeIntegral.txt";
    /** Bouton continuer */
    private JButton btContinuer = new JButton(texts.getText("continue"),new ImageIcon("ui/icon/go-next.png"));
    /** Bouton mettre à jour le fichier */
    private JButton btSaveList = new JButton(texts.getText("save"),new ImageIcon("ui/icon/document-save.png"));
    /** Bouton annuler */
    private JButton btClose = new JButton(texts.getText("close"),new ImageIcon("ui/icon/exit.png"));
    /** Bouton charger une liste */
    private JButton btCharger = new JButton(texts.getText("load"),new ImageIcon("ui/icon/document-open.png"));
    /** Bouton faire une copie de sauvegarde de la liste */
    private JButton btCopyList = new JButton(texts.getText("copy"),new ImageIcon("ui/icon/edit-copy.png"));
    /** Bouton extraire une liste candidate*/
    private JButton btExtract = new JButton(texts.getText("extract"),new ImageIcon("ui/icon/edit-find.png"));
    
    /** vrai si une transcription est en cours */
    private boolean running;
    /** vrai si le dialogue peut être affiché */
    private boolean affichable = true;
    /** Gestionnaire d'erreur */
    private GestionnaireErreur gest = null;
    /** identifie le type de liste manipulé, voir {@link #IVB_LIST} et {@link #INTEGRAL_LIST}*/
    private int liste = 1;

    /**
     * Constructor
     * 
     * @param fichier nom du fichier contenant la liste
     * @param l le type de liste utilisé ({@link #IVB_LIST} ou {@link #INTEGRAL_LIST})
     * @param g Error manager instance
     */
    public DialogueListe(String fichier, int l, GestionnaireErreur g)
    {
    	liste = l;
    	if (liste == IVB_LIST){texts = new Language("DialogueIVB");}
    	gest = g;
    	running=true;
    	listeFich = fichier;
    	setModal(true);
    	fabriqueDialogue();
    }
    /**
     * Constructeur
     * @param frame la fenêtre appelante
     * @param r vrai si affichage du dialog pendant une transcription, valuer pour {@link #running}
     * @param g instance du gestionnaire d'erreurs
     * @param p l'objet Language à utiliser
     * @param fichier nom du fichier contenant la liste
     * @param l le type de liste utilisé ({@link #IVB_LIST} ou {@link #INTEGRAL_LIST})
     */
    private DialogueListe(JFrame frame, boolean r, GestionnaireErreur g, Language p, String fichier, int l)
    {
    	super(frame, true);
    	liste = l;
    	texts = p;
    	listeFich = fichier;
    	
		gest = g;
    	running=r;
    	fabriqueDialogue();
    	
    	int x=0;
		int y=0;
		Dimension parentSize = frame.getSize();
		Dimension size = getSize();
		
		y = frame.getY() + parentSize.height/2 - size.height/2;
		x = frame.getX() + parentSize.width/2 - size.width/2;
		setLocation(x, y);
		setVisible(affichable);
    }
    
    /**
     * Constructeur public
     * Ne permet pas l'affichage d'un dialogue, juste la manipulation des données
     * @param fichier adresse du fichier contenant la liste
     */
    public DialogueListe(String fichier)
    {
    	listeFich = fichier;
    	affichable = false;
    	donnees = creerDonneesTable();
    }
    
    /**
     * Créer et affiche un DialogueIntegral 
     * @param frame la fenêtre appelante
     * @param running vrai si affichage du dialog pendant une transcription
     * @param g instance du gestionnaire d'erreur
     * @param l instance de Language pour les textes
     * @param fichier adresse du fichier contenant la liste
     * @param liste le type de liste utilisé ({@link #IVB_LIST} ou {@link #INTEGRAL_LIST})
     * @return réponse de l'utilisateur
     */
    public static int showDialog(JFrame frame, boolean running, GestionnaireErreur g, Language l, String fichier,int liste)
    {
    	new DialogueListe(frame,running,g,l,fichier,liste);
    	return retour;
    }

    /**
     * fabrique le jdialog
     * Si running est vrai, propose de modifier le fichier pour la transcription uniquement, ou aussi le fichier de référence
     * Si running est faux, ne propose que l'enregistrement du fichier de référence
     */
    private void fabriqueDialogue()
    {
    	addWindowListener(this);
    	
    	/*texte d'explication/ explaining text*/
    	JLabel info = new JLabel();
    	switch (liste)
    	{
    		case INTEGRAL_LIST:
    			info.setText(texts.getText("infosInt"));
    			break;
    		case IVB_LIST:
    			info.setText(texts.getText("infosIVB"));
    			break;
    	}
    	setTitle((texts.getText("wordslist")));
    	info.setIcon(new ImageIcon("ui/icon/dialog-information.png"));
    	info.setIconTextGap(20);
    	/*
    	 * récupération des données
    	 */
    	donnees = creerDonneesTable(listeFich);
    	tm = new TableModeleIntegral(donnees);
		table = new JTable(tm);
		table.addKeyListener(this);
		table.setAutoCreateRowSorter(true);
		//bug de sun pour la maj de la table
		table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		//taille des colonnes
		TableColumnModel modelesColonnes = table.getColumnModel();
	    TableColumn modelColonne = modelesColonnes.getColumn(0);
		//modelColonne.setMaxWidth(150);
		modelColonne.setMinWidth(200);
		modelColonne =  modelesColonnes.getColumn(1);
		modelColonne.setMaxWidth(70);
		modelColonne.setMinWidth(70);
		modelColonne =  modelesColonnes.getColumn(2);
		modelColonne.setMaxWidth(70);
		modelColonne.setMinWidth(70);
		table.setPreferredScrollableViewportSize(new Dimension(200,150));
		
		btContinuer.setMnemonic('c');
		Context cbtContinuer = new Context("c","Button","continue",texts);
		new ContextualHelp(btContinuer,cbtContinuer);
		btContinuer.addActionListener(this);
		
		btSaveList.setMnemonic('s');
		Context cbtContinuerMaj = new Context("s","Button","save",texts);
		new ContextualHelp(btSaveList,cbtContinuerMaj);
		btSaveList.addActionListener(this);
		
		btClose.setMnemonic('f');
		Context cbtAnnuler = new Context("f","Button","close",texts);
		new ContextualHelp(btClose,cbtAnnuler);
		btClose.addActionListener(this);
	
		btCharger.addActionListener(this);
		btCharger.setMnemonic('h');
		Context cbtCharger = new Context("h","Button","load",texts);
		new ContextualHelp(btCharger,cbtCharger);
		
		btCopyList.addActionListener(this);
		btCopyList.setMnemonic('i');
		Context cbtCopyList = new Context("i","Button","copy",texts);
		new ContextualHelp(btCopyList,cbtCopyList);
		
		btExtract.addActionListener(this);
		btExtract.setMnemonic('e');
		Context cbtExtract = new Context("e","Button","extract",texts);
		new ContextualHelp(btExtract,cbtExtract);
		
        JPanel pan = new JPanel(new GridBagLayout());
        JScrollPane sp = new JScrollPane();
    	setContentPane(pan);
    	
    	GridBagConstraints c = new GridBagConstraints();
    	c.fill = GridBagConstraints.HORIZONTAL;
    	c.gridx = 0;
    	c.gridy = 0;
    	c.gridwidth=3;
    	c.insets = new Insets(10,3,3,3);
    	
        pan.add(info,c);
        
        c.gridy++;
        c.gridwidth=4;
    	pan.add(sp, c);
    	sp.getViewport().add(table, null);
    	
    	c.gridwidth=1;
    	c.gridy++;
    	int mem = c.gridy;
    	pan.add(btCharger,c);
    	c.gridy++;
    	
    	pan.add(btCopyList,c);
    	
    	c.gridy++;
    	pan.add(btSaveList,c);
    	
    	c.gridy=mem;
    	c.gridx+=2;
    	if(liste == INTEGRAL_LIST)
    	{
    		c.anchor=GridBagConstraints.WEST;
    		pan.add(btExtract,c);
    		c.gridx++;
    	}
    	c.anchor=GridBagConstraints.EAST;
    	if(running)
    	{
			pan.add(btContinuer,c);
    	}
    	c.gridy++;
    	pan.add(btClose,c);
    	
    	pack();
    	//setVisible(affichable);
    	this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
    
    /**
     * N'appelle {@link JDialog#setVisible(boolean)} que si {@link #affichable} est vrai
     * @see java.awt.Dialog#setVisible(boolean)
     */
    @Override
    public void setVisible(boolean b){super.setVisible(affichable);}
    
    /**
	 * Crée les données pour la JTable {@link #table} contenues dans le 
	 * fichier d'exceptions <code>dico</code>
	 * @return les données pour la table {@link #table}
	 */
	private ArrayList<ArrayList<Object>> creerDonneesTable()
	{
		return creerDonneesTable(listeFich);
	}
    /**
	 * Crée les données pour la JTable {@link #table} contenues dans le 
	 * fichier d'exceptions <code>dico</code>
	 * @param dico adresse du dictionnaire de règle à charger
	 * @return les données pour la table {@link #table}
	 */
	private ArrayList<ArrayList<Object>> creerDonneesTable(String dico)
	{
		ArrayList<ArrayList<Object>> d = new ArrayList<ArrayList<Object>>();
		try
        {
	        BufferedReader raf = new BufferedReader(new InputStreamReader(new FileInputStream(dico),"UTF-8"));
	        String ligne=raf.readLine();
			while(ligne!=null)
			{
				String[] exep = ligne.split(" ");
				if(exep.length==3)
				{
					String mot = exep[0];
					Boolean auto = new Boolean(Boolean.parseBoolean(exep[1]));
					Boolean ask = new Boolean(Boolean.parseBoolean(exep[2]));
					ArrayList<Object> a = new ArrayList<Object>();
					
					a.add(mot);
					a.add(auto);
					a.add(ask);
					
					d.add(a);
				}
				ligne=raf.readLine();
			}

			ArrayList<Object> a = new ArrayList<Object>();
			a.add("");
			a.add(Boolean.FALSE);
			a.add(Boolean.FALSE);
			d.add(a);
			
			
			raf.close();
        }
        catch (UnsupportedEncodingException e){e.printStackTrace();}
        catch (FileNotFoundException e)
        {
        	System.out.println(texts.getText("filenotfound"));
        	ArrayList<Object> a = new ArrayList<Object>();
        	a.add("");
        	a.add(Boolean.FALSE);
        	a.add(Boolean.FALSE);
        	d.add(a);
        	e.printStackTrace();
        }
        catch (IOException e){e.printStackTrace();}
		return d;
		
	}
	/** @return l'instance de Language {@link #texts}*/
    public Language getTexts(){return texts;}
	/**
	 * Méthode d'accès en écriture à {@link #modif}
	 * @param m valeur pour modif
	 */
	public void setModif(boolean m) {modif = m;}

	
	/**
     * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
     */
    @Override
    public void windowActivated(WindowEvent arg0){/*do nothing*/}
	/**
     * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
     */
    @Override
    public void windowClosed(WindowEvent arg0){/*do nothing*/}

	/**
	 * demande l'enregistrement de la table si besoin
     * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
     */
    @Override
    public void windowClosing(WindowEvent arg0)
    {
    	if(!modif){retour = JOptionPane.CANCEL_OPTION;}
    	/*
    	{
    		System.out.println(texts.getText("modifs"));
    	}
    	else
    	{
    		System.out.println(texts.getText("nomodifs"));
    		retour = JOptionPane.CANCEL_OPTION;
    	}*/
    }

	/**
     * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
     */
    @Override
    public void windowDeactivated(WindowEvent arg0){/*do nothing*/}

	/**
     * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
     */
    @Override
    public void windowDeiconified(WindowEvent arg0){/*do nothing*/}

	/**
     * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
     */
    @Override
    public void windowIconified(WindowEvent arg0){/*do nothing*/}

	/**
     * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
     */
    @Override
    public void windowOpened(WindowEvent arg0){/*do nothing*/}

	/**
	 * Gère les actions sur les boutons
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent ae)
    {
    	retour = JOptionPane.YES_OPTION;
	    if(ae.getSource()==btClose)
	    {
	    	retour = JOptionPane.CANCEL_OPTION;
	    	dispose();
	    }
	    else if(ae.getSource()==btContinuer){continuer();}
	    else if(ae.getSource()==btSaveList){maj();}
	    else if(ae.getSource()==btCharger){charger();}
	    else if(ae.getSource()==btCopyList){copyList();}
	    else if(ae.getSource()==btExtract){extractList();}
    }
    
    /**
     * Try to discover proper nouns in the document
     */
    private void extractList()
    {
	    donnees = ProperNounDetector.extractProperNouns(false);
	    if(donnees.size()>1)
	    {
		    tm.setArrayListOfData(donnees);
			tm.fireTableDataChanged();
	    }
	    else
	    {
	    	JOptionPane.showMessageDialog(this, texts.getText("noresultmsg"),
    				texts.getText("noresult"), JOptionPane.INFORMATION_MESSAGE);
	    }
    }

	/**
     * Copie la liste de mots actuelle dans un fichier de sauvegarde
     * destiné à être rechargé si besoin
     */
    private void copyList()
    {
    	JFileChooser jfc = new JFileChooser();
    	jfc.setCurrentDirectory(new File(ConfigNat.getUserConfigFolder()));
		// ajout des filtres au JFileChooser
		jfc.addChoosableFileFilter(new FiltreFichier( new String[]{"txt"}, texts.getText("textfiles")+" (*.txt)"));
		jfc.setDialogTitle(texts.getText("copyfileload"));
 		if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {    
 			StringBuffer s = new StringBuffer("");
 			ArrayList<ArrayList<Object>> d = tm.getArrayListOfData();
 			for(ArrayList<Object> al:d)
 			{
 				if(!al.get(0).equals(""))
 				{
	 				for(Object o : al){s.append(o.toString()+" ");}
	 				s.append("\n");
 				}
 			}
 			FileToolKit.saveStrToFile(s.toString(), jfc.getSelectedFile().getAbsolutePath());
        }
    }

	/**
     * Charge une liste de mots et d'expressions dans la liste des mots à 
     * transcription personnalisées( Noms propres,...)
     */
    private void charger()
    {
    	JFileChooser jfc = new JFileChooser();
    	jfc.setCurrentDirectory(new File(ConfigNat.getUserConfigFolder()));
		// ajout des filtres au JFileChooser
		jfc.addChoosableFileFilter(new FiltreFichier( new String[]{"txt"}, texts.getText("textfiles")+" (*.txt)"));
		jfc.setDialogTitle(texts.getText("fileload"));
 		if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {    
 			//si un fichier est selectionné, récupérer le fichier puis son path
			donnees = creerDonneesTable(jfc.getSelectedFile().getAbsolutePath());
			tm.setArrayListOfData(donnees);
			tm.fireTableDataChanged();
        }
    }

	/**
     * Enregistre le fichier d'exception intégral pour la transcription en cours et ferme la fenêtre
     */
    private void continuer()
    {
    	String sequence ="";
    	String sequenceAmb ="";
    	for(ArrayList<Object> al : donnees)
    	{
    		if(((Boolean)(al.get(1))).booleanValue())
    		{
    			String chaine = ((String)(al.get(0))).trim().replaceAll("'", "''");
    			if(((Boolean)(al.get(2))).booleanValue())
    			{
    				sequenceAmb+="'"+chaine+"',";
    				sequenceAmb+="'"+chaine.toUpperCase()+"',";
    			}
    			else
    			{
    				sequence+="'"+chaine+"',";
    				sequence+="'"+chaine.toUpperCase()+"',";
    			}
    		}
    	}
    	//suppression des dernières virgules
    	if(sequenceAmb.length()>0){sequenceAmb = sequenceAmb.substring(0,sequenceAmb.length()-1);}
    	if(sequence.length()>0){sequence = sequence.substring(0,sequence.length()-1);}
    	switch (liste)
    	{
    		case INTEGRAL_LIST:
    			ConfigNat.getCurrentConfig().setListeIntegral(sequence);
    			ConfigNat.getCurrentConfig().setListeIntegralAmb(sequenceAmb);
    			break;
    		case IVB_LIST:
    			ConfigNat.getCurrentConfig().setListeIVB(sequence);
    			ConfigNat.getCurrentConfig().setListeIVBAmb(sequenceAmb);
    			break;
    	}
    	dispose();
    }
    
    /**
     * Met à jour le fichier de référence des mots en intégral}
     */
    private void maj()
    {
    	try
        {
	        BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(listeFich),"UTF8"));
	        for(ArrayList<Object> al : donnees)
	        {
	        	String chaine = ((String)(al.get(0))).trim();
	        	if(chaine.length()>0){fcible.write(chaine+" "+al.get(1)+ " "+al.get(2) +"\n");}
	        }
	        fcible.close();
        }
        catch (UnsupportedEncodingException e)
        {
        	gest.afficheMessage(texts.getText("err_encoding"), Nat.LOG_SILENCIEUX);
        }
        catch (FileNotFoundException e)
        {
        	gest.afficheMessage(texts.getText("err_fileNotFound"), Nat.LOG_SILENCIEUX);
        }
        catch (IOException e)
        {
        	gest.afficheMessage(texts.getText("err_IO"), Nat.LOG_SILENCIEUX);
        }
	    //continuer();
    }
	/**
	 * Manages line and combo check 
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
    @Override
    public void keyPressed(KeyEvent ke)
    {
    	if(ke.isControlDown() && ke.isShiftDown()){
    		if(ke.getKeyCode() == KeyEvent.VK_D)
    		{
	    		for(int i :table.getSelectedRows())
	    		{
	    			donnees.get(i).set(2, new Boolean(false));
	    		}
	    		table.updateUI();
    		}
    		if(ke.getKeyCode() == KeyEvent.VK_G)
    		{
	    		for(int i :table.getSelectedRows())
	    		{
	    			donnees.get(i).set(1, new Boolean(false));
	    		}
	    		table.updateUI();
    		}
    	}
    	else if(ke.isControlDown()){
    		if(ke.getKeyCode() == KeyEvent.VK_D)
    		{
	    		for(int i :table.getSelectedRows())
	    		{
	    			donnees.get(i).set(2, new Boolean(true));
	    		}
	    		table.updateUI();
    		}
    		if(ke.getKeyCode() == KeyEvent.VK_G)
    		{
	    		for(int i :table.getSelectedRows())
	    		{
	    			donnees.get(i).set(1, new Boolean(true));
	    		}
	    		table.updateUI();
    		}
    	}
    	else if(ke.getKeyCode() == KeyEvent.VK_DELETE)
    	{
    		ArrayList<ArrayList<Object>> al = new ArrayList<ArrayList<Object>>();
    		for(int i :table.getSelectedRows())
    		{
    			
    			al.add(donnees.get(i));
    		}
    		donnees.removeAll(al);
    		table.updateUI();
    	}
	    // TODO Auto-generated method stub
	    
    }
	/**
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
    @Override
    public void keyReleased(KeyEvent arg0)
    {
	    // TODO Auto-generated method stub
	    
    }
	/**
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
    @Override
    public void keyTyped(KeyEvent arg0)
    {
	    // TODO Auto-generated method stub
	    
    }
    /* ***********************************************************************************
     * Internal classes
     *************************************************************************************/
    
    /**
	 * Classe interne décrivant le modèle de JTable utilisé pour {@link DialogueListe}
	 * @author bruno
	 *
	 */
	class TableModeleIntegral extends DefaultTableModel
	{
		/** Pour la sérialisation, non utilisé */
		private static final long serialVersionUID = 1L;
		/** Les données de la table */
		private ArrayList<ArrayList<Object>> data = new ArrayList<ArrayList<Object>>();
		/** Tableau contenant les classes des colonnes d'objets*/
		private Class<?>[] colClass = new Class<?>[]{String.class,Boolean.class,Boolean.class};
		/** Tableau conteannt les noms des colonnes */
		private String[] columnNames = new String[]{getTexts().getText("columnword"), getTexts().getText("columnactivate"),getTexts().getText("columnask")};
		/** 
		 * Constructeur
		 * @param d les données de la table
		 */
		public TableModeleIntegral(ArrayList<ArrayList<Object>> d)
		{
			super();
			data = d;
		}
		/**
		 * Retourne les données sous forme d'ArrayList double
		 * @return {@link #data}
		 */
		public ArrayList<ArrayList<Object>> getArrayListOfData() {return data;}
		/**
		 * MAJ des données sous forme d'ArrayList double
		 * @param d l'arraylist avec les nouvelles données
		 */
		public void setArrayListOfData(ArrayList<ArrayList<Object>> d) {data = d;}
		/**
		 * Stocke les données passées en paramètre dans la structure {@link #data}
		 * @param d liste
		 * @see javax.swing.table.DefaultTableModel#setDataVector(java.lang.Object[][], java.lang.Object[])
		 */
		public void setDataVector(ArrayList<ArrayList<Object>> d)
		{
			data = d;
		}
		/**
		 * Ajoute une ligne à {@link #data}
		 * @see javax.swing.table.DefaultTableModel#addRow(java.lang.Object[])
		 */
		@Override
		public void addRow(Object[] o)
		{
			ArrayList<Object> al = new ArrayList<Object>();
			for(int i=0;i<o.length;i++){al.add(o[i]);}
			data.add(al);
			this.fireTableRowsInserted(data.size()-1,data.size()-1);
			setModif(true);
		}
		/**
		 * Renvoie le nom de la colonne <code>col</code>
		 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
		 */
		@Override
		public String getColumnName(int col) {return columnNames[col].toString();}
		/**
		 * Affecte <code>value</code> à cellule (<code>row</code>,<code>col</code>) de {@link #data}
		 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
		 */
		@Override
		public void setValueAt(Object value, int row, int col)
		{
			data.get(row).set(col, value);
			if(row==data.size()-1 && !((String)(data.get(row).get(0))).trim().equals(""))
			{
				Object [] ligne = {"",Boolean.FALSE,Boolean.FALSE};
				addRow(ligne);
			}
	        fireTableCellUpdated(row, col);
	        setModif(true);
	    }
		/**
		 * Supprime la ligne <code>row</code>
		 * @see javax.swing.table.DefaultTableModel#removeRow(int)
		 */
		@Override
		public void removeRow(int row)
		{
			data.remove(row);
			this.fireTableRowsDeleted(row,row);
			setModif(true);
		}
		/**
		 * Renvoie le nombre de colonnes de {@link #data}
		 * @see javax.swing.table.TableModel#getColumnCount()
		 */
		@Override
		public int getColumnCount() {return data.get(0).size();}
		/**
		 * Renvoie le nombre de lignes de {@link #data}
		 * @see javax.swing.table.TableModel#getRowCount()
		 */
		@Override
		public int getRowCount()
		{
			int ret=0;
			if(data!=null){ret= data.size();}
			return ret;
		}
		/**
		 * Renvoie l'objet de la cellule (<code>row</code>,<code>col</code>) de {@link #data}
		 * @see javax.swing.table.TableModel#getValueAt(int, int)
		 */
		@Override
		public Object getValueAt(int row, int col) {return data.get(row).get(col);}
		/**
		 * Redéfinition indiquant que toutes les cellules, sauf celles de la
		 * première colonne (n°), sont éditables
		 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
		 */
		@Override
		public boolean isCellEditable(int i, int j)
		{
			return true;
		}
		/**
		 * Renvoie la classe des objets de la colonne <code>col</code>
		 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
		 */
 		@Override
 		public Class<?> getColumnClass(int col) {return colClass[col];}
	 }


}

