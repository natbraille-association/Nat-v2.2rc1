/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ui.dialog;
import gestionnaires.GestionnaireErreur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;
import ui.config.Configuration;
import nat.ConfigNat;
import nat.Nat;
import nat.NullEntityResolver;

/**
 * Cette classe de dialogue permet l'édition et l'enregistrement des styles
 * @author bruno
 *
 */
public class ConfStyle extends JDialog implements WindowListener, ActionListener
{
	/** Textual contents */
	private Language texts = new Language("ConfStyle");
	
	/** pour la sérialisation (non utilisé)*/
    private static final long serialVersionUID = 1L;
    /** données de la table*/
	private ArrayList<ArrayList<Object>> donnees = new ArrayList<ArrayList<Object>>();
	/** ArrayList containing the names of the system styles*/
	private ArrayList<String> styleRef = new ArrayList<String>();
    /** table */
    private JTable table;
    /** Modèle pour la table */
	private TableModeleStyle tm;
    /** Valeur de retour */
    private static int retour=JOptionPane.YES_OPTION;
    /** indique si la table a été modifiée */
    private boolean modif=false;
    /** Style file name */
    private String fStyle = ConfigNat.getCurrentConfig().getStylist();
    /** Style user file name */
    private String fUserStyle = ConfigNat.getCurrentConfig().getUserStylist();
    /** Bouton mettre à jour le fichier */
    private JButton btSave = new JButton(texts.getText("save"),new ImageIcon("ui/icon/document-save.png"));
    /** Bouton annuler */
    private JButton btAnnuler = new JButton(texts.getText("cancel"),new ImageIcon("ui/icon/exit.png"));
    /** supprime l'étape */
    private JButton btRemoveStyle = new JButton(new ImageIcon("ui/icon/list-remove.png"));
    /** ajoute une étape */
    private JButton btAddStyle = new JButton(new ImageIcon("ui/icon/list-add.png"));
    /** Gestionnaire d'erreur */
    private GestionnaireErreur gest = null;
    /** data for structure column */
    public static String[] arMode = {"aucun","paragraphe","titre"};
    /** data for special column */
	public static String[] arSpecial = {"g0","g1","chimie", "chemistry","normal"};
    /**
     * Constructeur
     * @param cfg la fenêtre appelante
     */
    private ConfStyle(Configuration cfg)
    {
    	super(cfg, true);
    	setTitle(texts.getText("title"));
		gest = cfg.getGestErreur();
    	fabriqueDialogue();
    	
    	int x=0;
		int y=0;
		Dimension parentSize = cfg.getSize();
		Dimension size = getSize();
		
		y = cfg.getY() + parentSize.height/2 - size.height/2;
		x = cfg.getX() + parentSize.width/2 - size.width/2;
		setLocation(x, y);
		setVisible(true);
    }
    
    /**
     * Créer et affiche un DialogueIntegral 
     * @param cfg la fenêtre Configuration appelante
     * @return réponse de l'utilisateur
     */
    public static int showDialog(Configuration cfg)
    {
    	new ConfStyle(cfg);
    	return retour;
    }

    /**
     * fabrique le jdialog
     * Si running est vrai, propose de modifier le fichier pour la transcription uniquement, ou aussi le fichier de référence
     * Si running est faux, ne propose que l'enregistrement du fichier de référence
     */
    private void fabriqueDialogue()
    {
    	addWindowListener(this);

		
    	/*
    	 * récupération des données
    	 */
    	donnees = creerDonneesTable();
    	tm = new TableModeleStyle(donnees);
		table = new JTable(tm);
		table.setAutoCreateRowSorter(true);
		//bug de sun pour la maj de la table
		table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		table.setRowHeight(20);
		TableBrailleRenderer tbr =  new TableBrailleRenderer();
		
		//taille des colonnes
		TableColumnModel modelesColonnes = table.getColumnModel();
	    TableColumn modelColonne = modelesColonnes.getColumn(0);
		//modelColonne.setMaxWidth(150);
		modelColonne.setMinWidth(100);
		modelColonne =  modelesColonnes.getColumn(1);
		modelColonne.setCellEditor(new DefaultCellEditor(new JComboBox<String>(arMode)));
		modelColonne.setMaxWidth(100);
		modelColonne.setMinWidth(100);
		modelColonne =  modelesColonnes.getColumn(2);
		modelColonne.setMaxWidth(100);
		modelColonne.setMinWidth(100);
		modelColonne =  modelesColonnes.getColumn(3);
		//modelColonne.setCellEditor(new DefaultCellEditor(new JCheckBox()));
		modelColonne.setMaxWidth(100);
		modelColonne.setMinWidth(100);
		modelColonne =  modelesColonnes.getColumn(4);
		modelColonne.setMaxWidth(100);
		modelColonne.setMinWidth(100);
		modelColonne =  modelesColonnes.getColumn(5);
		modelColonne.setMaxWidth(100);
		modelColonne.setMinWidth(100);
		modelColonne =  modelesColonnes.getColumn(6);
		modelColonne.setCellEditor(new DefaultCellEditor(new JComboBox<String>(arSpecial)));
		modelColonne.setMaxWidth(100);
		modelColonne.setMinWidth(100);
		table.setPreferredScrollableViewportSize(new Dimension(700,300));
		for(int i=0;i<table.getColumnCount();i++)
		{
			table.getColumnModel().getColumn(i).setCellRenderer(tbr);
		}
		
		table.getRowSorter().toggleSortOrder(0);
		
		Context cbtAddStyle = new Context("n","Button","addStyle",texts);
		new ContextualHelp(btAddStyle,cbtAddStyle);
		btAddStyle.setMnemonic('n');
		btAddStyle.addActionListener(this);
		
		Context cbtRemoveStyle = new Context("d","Button","removeStyle",texts);
		new ContextualHelp(btRemoveStyle,cbtRemoveStyle);
		btRemoveStyle.setMnemonic('d');
		btRemoveStyle.addActionListener(this);
		
		btSave.setMnemonic('s');
		Context cbtContinuerMaj = new Context("s","Button","save",texts);
		new ContextualHelp(btSave,cbtContinuerMaj);
		btSave.addActionListener(this);
		
		btAnnuler.setMnemonic('a');
		Context cbtAnnuler = new Context("a","Button","cancel",texts);
		new ContextualHelp(btAnnuler,cbtAnnuler);
		btAnnuler.addActionListener(this);
		
        JPanel pan = new JPanel();
        JScrollPane sp = new JScrollPane();
    	pan.setLayout(new BorderLayout());
    	getContentPane().add(pan);
    	pan.add(sp, BorderLayout.CENTER);
    	sp.getViewport().add(table, null);
    	JPanel pBoutons = new JPanel(new GridBagLayout());
    	GridBagConstraints gbc = new GridBagConstraints();
    	gbc.fill = GridBagConstraints.HORIZONTAL;
    	gbc.gridx = 3;
    	gbc.gridy = 0;
    	gbc.insets = new Insets(10,3,3,3);

    	pBoutons.add(btAddStyle,gbc);
    	gbc.gridx++;
    	pBoutons.add(btRemoveStyle,gbc);
    	
    	gbc.gridx = 0;
    	gbc.gridy++;
    	
    	
    	pBoutons.add(btSave,gbc);
    	
    	gbc.gridx+=2;
    	
    	pBoutons.add(btAnnuler,gbc);
    	pan.add(pBoutons, BorderLayout.SOUTH);
    	pack();
    }
    
    
    /*
	 * Crée les données pour la JTable {@link #table} contenues dans le 
	 * fichier d'exceptions <code>dico</code>
	 * @return les données pour la table {@link #table}
	 *
	private ArrayList<ArrayList<Object>> creerDonneesTable()
	{
		return creerDonneesTable(listeFich);
	}*/
    /**
	 * Create JTable data from style file <code>fS</code>
	 * @return les données pour la table {@link #table}
	 */
	private ArrayList<ArrayList<Object>> creerDonneesTable()
	{
		ArrayList<ArrayList<Object>> d = new ArrayList<ArrayList<Object>>();
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(false);
			factory.setValidating(false);
			factory.setIgnoringComments(true);
			factory.setIgnoringElementContentWhitespace(false);
			factory.setXIncludeAware(false);
 
			// sinon, génère parfois des null pointer exp au parsage (problème avec les simples quote)
			factory.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			builder.setErrorHandler(gest);
			//pour les problèmes de résolution d'entités des dtd public, si il n'y a pas de réseau:
			Document doc;
			Document ref;
			try
			{
				doc = builder.parse(new File(fUserStyle));
				ref = builder.parse(new File(fStyle));
			}
			catch(UnknownHostException uoe)
			{
				builder.setEntityResolver(new NullEntityResolver(gest));
				doc = builder.parse(new File(fUserStyle));
				ref = builder.parse(new File(fStyle));
			}     
			doc.setStrictErrorChecking(false);
			ref.setStrictErrorChecking(false);
			
			//loading reference names
			NodeList nl = ref.getElementsByTagName("style");		
			for(int i=0; i<nl.getLength();i++)
			{
				Node n = nl.item(i);				
				getStyleRef().add(n.getAttributes().getNamedItem("name").getNodeValue());
			}
			
			//loading xml data into JTable
			nl = doc.getElementsByTagName("style");
			
			for(int i=0; i<nl.getLength();i++)
			{
				Node n = nl.item(i);
				ArrayList<Object> a = new ArrayList<Object>();
				
				a.add(n.getAttributes().getNamedItem("name").getNodeValue());
				String mode ="aucun";
				try{mode = n.getAttributes().getNamedItem("mode").getNodeValue();}
				catch(NullPointerException npe){mode="";}
				
				if(mode.startsWith("title")){a.add("titre");}
				else if(mode.equals("")||mode.equals("aucun")){a.add("aucun");}
				else{a.add("paragraphe");}
				
				a.add(mode);
				
				String upsidedown = "false";
				try
				{
					upsidedown = n.getAttributes().getNamedItem("upsidedown").getNodeValue();
					if (upsidedown.equals("yes")){upsidedown = "true";}
				}
				catch(NullPointerException npe){upsidedown="false";}
				a.add(new Boolean(upsidedown));
				
				String prefixe = "";
				try{prefixe = n.getAttributes().getNamedItem("prefixe").getNodeValue();}
				catch(NullPointerException npe){/*do nothing*/}
				a.add(prefixe);
				
				String suffixe = "";
				try{suffixe = n.getAttributes().getNamedItem("suffixe").getNodeValue();}
				catch(NullPointerException npe){/*do nothing*/}	
				a.add(suffixe);
				
				String special = "normal";
				try{special = n.getAttributes().getNamedItem("special").getNodeValue();}
				catch(NullPointerException npe){/*do nothing*/}			
				a.add(special);
				
				d.add(a);
				
			}
		}
		catch (SAXException e)
        {
			gest.afficheMessage(texts.getText("errorLoad"), Nat.LOG_SILENCIEUX);
	        if(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG){e.printStackTrace();}
        }
        catch (IOException e)
        {
            gest.afficheMessage(texts.getText("errorFile"), Nat.LOG_SILENCIEUX);
	        if(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG){e.printStackTrace();}
        }
        catch (ParserConfigurationException e)
        {
        	gest.afficheMessage(texts.getText("errorLoad"), Nat.LOG_SILENCIEUX);
	        if(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG){e.printStackTrace();}
        }
		return d;
		
	}
	/** @return l'instance de Language {@link #texts}*/
    public Language getTexts(){return texts;}
	/**
	 * 
	 * @return the #styleRef
	 */
    public ArrayList<String> getStyleRef(){return styleRef;}

	/**
	 * Méthode d'accès en écriture à {@link #modif}
	 * @param m valeur pour modif
	 */
	public void setModif(boolean m) {modif = m;}


	/**
     * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
     */
    @Override
    public void windowActivated(WindowEvent arg0){/*do nothing*/}
	/**
     * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
     */
    @Override
    public void windowClosed(WindowEvent arg0){/*do nothing*/}

	/**
	 * demande l'enregistrement de la table si besoin
     * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
     */
    @Override
    public void windowClosing(WindowEvent arg0)
    {
    	if(!modif){retour = JOptionPane.CANCEL_OPTION;}
    }

	/**
     * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
     */
    @Override
    public void windowDeactivated(WindowEvent arg0){/*do nothing*/}

	/**
     * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
     */
    @Override
    public void windowDeiconified(WindowEvent arg0){/*do nothing*/}

	/**
     * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
     */
    @Override
    public void windowIconified(WindowEvent arg0){/*do nothing*/}

	/**
     * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
     */
    @Override
    public void windowOpened(WindowEvent arg0){/*do nothing*/}

	/**
	 * Gère les actions sur les boutons
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent ae)
    {
    	retour = JOptionPane.YES_OPTION;
	    if(ae.getSource()==btAnnuler)
	    {
	    	retour = JOptionPane.CANCEL_OPTION;
	    	dispose();
	    }
	    else if(ae.getSource()==btSave){maj();}
	    else if(ae.getSource()==btAddStyle){addStyle();}
	    else if(ae.getSource()==btRemoveStyle){removeStyle();}
    }
    
    /**
	 * 
	 */
    private void removeStyle()
    {
    	int r = table.getSelectedRow();
    	if(r>=0)
    	{
	    	int row = table.getRowSorter().convertRowIndexToModel(r);
	    	tm.removeRow(row);
    	}
    }

	/**
	 * 
	 */
    private void addStyle()
    {
	    ArrayList<Object> al = new ArrayList<Object>();
	    al.add("");
	    al.add("paragraphe");
	    al.add("3-1");
	    al.add(Boolean.FALSE);
	    al.add("");
	    al.add("");
	    al.add("normal");
	    tm.addRow(al.toArray());
    }

    
    /**
     * Met à jour le fichier de style
     */
    private void maj()
    {
	    try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(false);
			factory.setValidating(false);
			factory.setIgnoringComments(true);
			factory.setIgnoringElementContentWhitespace(false);
			factory.setXIncludeAware(false);
	
			// sinon, génère parfois des null pointer exp au parsage (problème avec les simples quote)
			factory.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			builder.setErrorHandler(gest);
			Document doc;
			doc = builder.newDocument();
			doc.setStrictErrorChecking(false);
			
			Element styles = doc.createElement("styles");
			
			//writing styles
			for(ArrayList<Object> al:donnees)
			{
				Element style =doc.createElement("style");
				style.setAttribute("name", (String)al.get(0));
				
				String mode = (String) al.get(2);
				if(!mode.equals("")){style.setAttribute("mode", mode);}
				
				if(al.get(3).equals(Boolean.TRUE)){	style.setAttribute("upsidedown", "yes");}
				
				String pref = (String)al.get(4);
				if(!pref.equals("")){style.setAttribute("prefixe", pref);}
				
				String suffixe = (String)al.get(5);
				if(!suffixe.equals("")){style.setAttribute("suffixe", suffixe);}
				
				String special = (String)al.get(6);
				if(!special.equals("normal")){style.setAttribute("special", special);}
				
				styles.appendChild(style);			
			}
			doc.appendChild(styles);
			//save the file
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			Result output = new StreamResult(new File(ConfigNat.getCurrentConfig().getUserStylist()));
			Source input = new DOMSource(doc);

			transformer.transform(input, output);
		}
	    catch (ParserConfigurationException e)
	    {
	        gest.afficheMessage(texts.getText("errorData"), Nat.LOG_SILENCIEUX);
	        if(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG){e.printStackTrace();}
	    }
        catch (TransformerConfigurationException e)
        {
        	gest.afficheMessage(texts.getText("errorSave"), Nat.LOG_SILENCIEUX);
	        if(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG){e.printStackTrace();}
        }
        catch (TransformerFactoryConfigurationError e)
        {
        	gest.afficheMessage(texts.getText("errorSave"), Nat.LOG_SILENCIEUX);
	        if(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG){e.printStackTrace();}
        }
        catch (TransformerException e)
        {
        	gest.afficheMessage(texts.getText("errorSave"), Nat.LOG_SILENCIEUX);
	        if(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG){e.printStackTrace();}
        }
	    dispose();
    }

	/**
	 * Classe interne décrivant le modèle de JTable utilisé pour {@link DialogueListe}
	 * @author bruno
	 *
	 */
	class TableModeleStyle extends DefaultTableModel
	{
		/** Pour la sérialisation, non utilisé */
		private static final long serialVersionUID = 1L;
		/** Les données de la table */
		private ArrayList<ArrayList<Object>> data = new ArrayList<ArrayList<Object>>();
		/** Tableau contenant les classes des colonnes d'objets*/
		private Class<?>[] colClass = new Class<?>[]{
				String.class,
				ArrayList.class,
				String.class,
				Boolean.class,
				String.class,
				String.class,
				ArrayList.class
				};
		/** Tableau contenant les noms des colonnes */
		private String[] columnNames = new String[]{getTexts().getText("colName"),getTexts().getText("colStructure"), getTexts().getText("colMode"), getTexts().getText("colUpside"),getTexts().getText("colPrefixe"), getTexts().getText("colSuffixe"), getTexts().getText("colSpecial")};
		/** 
		 * Constructeur
		 * @param d les données de la table
		 */
		public TableModeleStyle(ArrayList<ArrayList<Object>> d)
		{
			super();
			data = d;
		}
		/**
		 * Retourne les données sous forme d'ArrayList double
		 * @return {@link #data}
		 */
		public ArrayList<ArrayList<Object>> getArrayListOfData() {return data;}
		/**
		 * MAJ des données sous forme d'ArrayList double
		 * @param d l'arraylist avec les nouvelles données
		 */
		public void setArrayListOfData(ArrayList<ArrayList<Object>> d) {data = d;}
		/**
		 * Stocke les données passées en paramètre dans la structure {@link #data}
		 * @param d liste
		 * @see javax.swing.table.DefaultTableModel#setDataVector(java.lang.Object[][], java.lang.Object[])
		 */
		public void setDataVector(ArrayList<ArrayList<Object>> d)
		{
			data = d;
		}
		/**
		 * Ajoute une ligne à {@link #data}
		 * @see javax.swing.table.DefaultTableModel#addRow(java.lang.Object[])
		 */
		@Override
		public void addRow(Object[] o)
		{
			ArrayList<Object> al = new ArrayList<Object>();
			for(int i=0;i<o.length;i++){al.add(o[i]);}
			data.add(al);
			this.fireTableRowsInserted(data.size()-1,data.size()-1);
			setModif(true);
		}
		/**
		 * Renvoie le nom de la colonne <code>col</code>
		 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
		 */
		@Override
		public String getColumnName(int col) {return columnNames[col].toString();}
		
		/**
		 * Affecte <code>value</code> à cellule (<code>row</code>,<code>col</code>) de {@link #data}
		 * Check if the value is well formated according to the row
		 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
		 */
		@Override
		public void setValueAt(Object value, int row, int col)
		{
			//controls
			boolean ok = true;
			String mode = "";
			
			switch(col)
			{
				case 0:
					String name = (String)value;
					for(int i = 0; i<data.size();i++)
					{
						ArrayList<Object> al = data.get(i);
						if(al.get(0).equals(name) && i!= row)
						{
							ok = false;
							JOptionPane.showMessageDialog(null, getTexts().getText("errorName"), getTexts().getText("errorInput"), JOptionPane.ERROR_MESSAGE);
							break;
						}
					}
					break;
				case 1:	
					mode = (String)value;
					String valeurMode = (String)data.get(row).get(2);
					//if (mode.equals("aucun"
					if(!checkModeValue(mode,valeurMode,false))
					{
						if(mode.equals("paragraphe")){data.get(row).set(2,"3-1");}
						else if(mode.equals("titre")){data.get(row).set(2,"title1");}
						else {data.get(row).set(2,"");}
						//System.err.println("bijour !");
						fireTableCellUpdated(row, 2);
					}
					break;
				case 2://mode value
					mode = (String)data.get(row).get(1);
					ok = checkModeValue(mode,(String)value,true);
					if(mode.equals("titre")&&((String)value).matches("^[12345]$")){value = "title" + value;}
					break;
				case 4:
				case 5:
					String str = (String) value;
					if((!str.matches("^((p0|p(1?2?3?4?5?6?)))+$") || str.length()==1)&&str.length()>0)//>1 because "p" is not ok
					{
						JOptionPane.showMessageDialog(null, getTexts().getText("errorPoints"), getTexts().getText("errorInput"), JOptionPane.ERROR_MESSAGE);
						ok = false;
					}
					break;
			}
			if(ok)
			{
				data.get(row).set(col, value);
		        fireTableCellUpdated(row, col);
		        setModif(true);
			}
			
	    }
		/**
		 * Check if the value <code>value</code> is a well formated braille mode string for the structure <code>mode</code>
		 * @param mode the structure (paragraphe or titre)
		 * @param value the value to check
		 * @param gui displays warning and error message if true
		 * @return true if <code>value</code> is a well formated string
		 */
        private boolean checkModeValue(String mode, String value, boolean gui)
        {
        	boolean ok = true;
        	if(mode.equals("titre"))
			{
				if(!(value).matches("^(title)?([12345])?$")||value.equals(""))
				{
					if(gui){JOptionPane.showMessageDialog(null, getTexts().getText("errorTitle"), getTexts().getText("errorInput"), JOptionPane.ERROR_MESSAGE);}
					ok = false;
				}
				if(!gui && value.matches("^[12345]$")) {ok = false;}
			}
			else if(mode.equals("paragraphe"))
			{
				if(!value.matches("^[\\d]+([-][\\d]+)?$"))
				{
					if(gui){JOptionPane.showMessageDialog(null, getTexts().getText("errorMode"), getTexts().getText("errorInput"), JOptionPane.ERROR_MESSAGE);}
					ok = false;
				}
				else
				{
					String[] nbs = value.split("-");
					for(String s: nbs)
					{
						if(Integer.parseInt(s)>ConfigNat.getCurrentConfig().getLongueurLigne()-5)
						{
							if(gui){JOptionPane.showMessageDialog(null, getTexts().getText("warningMode"), getTexts().getText("warningInput"), JOptionPane.WARNING_MESSAGE);}
							break;//on sort dès la première erreur
						}
					}
				}
			}
			else //mode.equals("aucun")
			{ ok = value.equals("");}
	        return ok;
        }
		/**
		 * Supprime la ligne <code>row</code>
		 * @see javax.swing.table.DefaultTableModel#removeRow(int)
		 */
		@Override
		public void removeRow(int row)
		{
			data.remove(row);
			this.fireTableRowsDeleted(row,row);
			setModif(true);
		}
		/**
		 * Renvoie le nombre de colonnes de {@link #data}
		 * @see javax.swing.table.TableModel#getColumnCount()
		 */
		@Override
		public int getColumnCount() {return columnNames.length;}//data.get(0).size();}
		/**
		 * Renvoie le nombre de lignes de {@link #data}
		 * @see javax.swing.table.TableModel#getRowCount()
		 */
		@Override
		public int getRowCount()
		{
			int ret=0;
			if(data!=null){ret= data.size();}
			return ret;
		}
		/**
		 * Renvoie l'objet de la cellule (<code>row</code>,<code>col</code>) de {@link #data}
		 * @see javax.swing.table.TableModel#getValueAt(int, int)
		 */
		@Override
		public Object getValueAt(int row, int col) {return data.get(row).get(col);}
		/**
		 * Redéfinition indiquant que toutes les cellules, sauf celles de la
		 * première colonne (n°), sont éditables
		 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
		 */
		@Override
		public boolean isCellEditable(int i, int j)
		{
			boolean ret = true;
			if(getStyleRef().contains(data.get(i).get(0))){ret = false;}
			if((j==2) && ((String)data.get(i).get(1)).equals("aucun")){ret = false;}
			return ret;
		}
		/**
		 * Renvoie la classe des objets de la colonne <code>col</code>
		 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
		 */
 		@Override
 		public Class<?> getColumnClass(int col) {return colClass[col];}
	 }
	
	/**
	 * Classe interne pour l'affichage de la JTable
	 * Affiche la 1ère colonne avec une teinte beige
	 * Affiche les cellules des autres colonnes sur fond rouge si leur donnée est non valide, en blanc sinon
	 * @author bruno
	 *
	 */
	public class TableBrailleRenderer extends DefaultTableCellRenderer 
	{
		/** Pour la sérialisation (non utilisé) */
		private static final long serialVersionUID = 1L;

		/**
		 * Méthode redéfinie de DefaultTableCellRenderer
		 * <p>Affiche la 1ère colonne avec une teinte beige</p>
		 * <p>Affiche les cellules des autres colonnes sur fond rouge si leur donnée est non valide, en blanc sinon</p>
		 * <p>Renvoie un Component correspondant à une cellule de la Table</p>
		 * @return un Component correspondant à une cellule de la Table
		 * @see javax.swing.table.DefaultTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
		 */
		@Override
		public Component getTableCellRendererComponent(JTable tbl, Object value, boolean isSelected, boolean hasFocus, int row,int column)
		{
			//valeurs par défaut
			JComponent c = (JComponent) super.getTableCellRendererComponent(tbl, value, isSelected, hasFocus, row, column);
			//int colIndex = table.getColumnModel().getColumnIndex(sportColumn);
			if(column == 3){c= new JCheckBox("",((Boolean)value).booleanValue());}
			else if(column == 1)
			{
				JComboBox<String>cb = new JComboBox<String>(ConfStyle.arMode);
				cb.setSelectedItem(value);
				c = cb;
			}
			else if(column == 6)
			{
				JComboBox<String>cb = new JComboBox<String>(ConfStyle.arSpecial);
				cb.setSelectedItem(value);
				c = cb;
			}
			c.setToolTipText(getTexts().getText("col"+(column+1)+"ttt"));
			c.getAccessibleContext().setAccessibleDescription(getTexts().getText("col"+(column+1)+"desc"));
			c.setBackground(Color.WHITE);
			//Coloration de la première colonne		
			if(getStyleRef().contains(tbl.getValueAt(row, 0)))
			{
				c.setBackground(Color.LIGHT_GRAY);
				c.setToolTipText(getToolTipText()+" ("+getTexts().getText("noLineModif")+")");
			}
			return c;
		}
	}
}

