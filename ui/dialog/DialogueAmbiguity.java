/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ui.dialog;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import outils.FileToolKit;

import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;
import ui.editor.BrailleTextField;
import ui.listener.PerkinsNotifier;

import nat.ConfigNat;
import nat.Nat;
import nat.transcodeur.Ambiguity;

/**
 * @author bruno
 *
 */
public class DialogueAmbiguity extends JDialog implements ActionListener, ItemListener, DocumentListener
{
	/** Textual contents */
	static Language texts = new Language("DialogueAmbiguity");

	/** Pour la sérialisation (non utilisé) */
    private static final long serialVersionUID = 1L;
    /** ambiguité à résoudre */
    private Ambiguity amb;
    /** ambiguïtés résolues à mémoriser*/
    private ArrayList<Ambiguity> lAmbAuto;
    /** liste des possibilités */
    private ArrayList<JRadioButton> choix = new ArrayList<JRadioButton>();
    /** BrailleTextField saisie personnalisée */
    private BrailleTextField jtfPerso = new BrailleTextField(20);
    /** bouton valider le choix */
    private JButton btValider= new JButton(texts.getText("validthis"));
    /** bouton valider le choix */
    private JButton btValiderTout= new JButton(texts.getText("validall"));
    /** radio button custom */
    private JRadioButton custom;
    /** groupe des boutons de choix */ 
    private ButtonGroup bgSol;
    /** Checkbox for perkins mode*/
    private JCheckBox jcbPerkins = new JCheckBox(texts.getText("perkins"));
    /** Notifier for perkins input */
    private PerkinsNotifier pn = null;

    
	/**
	 * Constructeur
	 * @param a valeur pour {@link #amb}
	 * @param laa liste des ambiguïtés résolues
	 */
	public DialogueAmbiguity(Ambiguity a, ArrayList<Ambiguity> laa)
	{
		super();
		setTitle(texts.getText("title"));
		getAccessibleContext().setAccessibleName(texts.getText("titlename"));
		try
        {
	        pn = new PerkinsNotifier(FileToolKit.getSysDepPath(new File("xsl/tablesBraille/brailleUTF8.ent").getAbsolutePath()));
        }
        catch (NumberFormatException e1)
        {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
        }
        catch (IOException e1)
        {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
        }
		lAmbAuto = laa;
		setModal(true);
		amb = a;
		fabriqueDialogue();
	}
	
	
	/**
	 * Fabrique le dialogue pour résoudre {@link #amb}
	 */
	private void fabriqueDialogue()
	{
		if(Nat.LOG_DEBUG==ConfigNat.getCurrentConfig().getNiveauLog()){amb.afficheAmbiguity();}
		setSize(new Dimension(400,250));
		
		Font fBraille; 
    	try
    	{
    		fBraille = Font.createFont(Font.PLAIN, new File("ui/fontes/DejaVuSans.ttf"));
    		fBraille = fBraille.deriveFont(Font.PLAIN, 12);
		}
    	catch (Exception e)
    	{
    		System.err.println(texts.getText("unknown police"));
    		fBraille = new Font("DejaVu Sans",Font.PLAIN,12);
	        	
    	}
    	
    	//titre
    	JLabel titre = new JLabel("<html><body><h2>"+texts.getText("ambiguityabout")+" \"" + amb.getExpression()+"\"</h2></body></html>");
    	
    	JTextPane jtp = new JTextPane();
    	jtp.setContentType("text/html");
    	jtp.setText("<html><body><p>"+amb.getAvant()+
    			" <b>"+amb.getExpression()+"</b> "+amb.getApres()+"</p></body></html>");
    	jtp.setEditable(false);
    	
    	JLabel lDesc = new JLabel("<html><body><p><b>"+amb.getDescription()+"\"</b><p></body></html>");
    	
    	jtfPerso.getDocument().addDocumentListener(this);
    	//fonte pour l'affichage des caractère spéciaux
		Font fonte;
        try
        {
            fonte = Font.createFont(Font.PLAIN, new File("ui/fontes/DejaVuSans.ttf"));
            fonte = fonte.deriveFont(Font.PLAIN, jtfPerso.getFont().getSize());
        }
        catch (Exception e){fonte = new Font("DejaVu Sans",Font.PLAIN,ConfigNat.getCurrentConfig().getTaillePolice());}
        jtfPerso.setFont(fonte);
    	
    	btValiderTout.addActionListener(this);
    	btValider.addActionListener(this);
    	new ContextualHelp(btValider,new Context("","Button","validthis",texts));
    	new ContextualHelp(btValiderTout,new Context("","Button","validall",texts));
    	new ContextualHelp(jtfPerso,new Context("","TextField","custom",texts));
    	
    	jcbPerkins.addActionListener(this);
		jcbPerkins.setMnemonic('p');
		Context cjcbPerkins = new Context("p","CheckBox","perkins",texts);
		new ContextualHelp(jcbPerkins,cjcbPerkins);
    	
		//création des radios boutons
		bgSol = new ButtonGroup();
		JPanel pSol = new JPanel(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.anchor=GridBagConstraints.LINE_START;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 3;
		pSol.add(titre,gbc);
		
		gbc.gridy++;
		pSol.add(lDesc,gbc);
		gbc.gridy++;
		gbc.gridheight=2;
		pSol.add(jtp,gbc);
		
		gbc.gridheight=1;
		gbc.gridy+=2;
		
		int i=1;
		for(String s : amb.getPropositions())
		{
			JRadioButton jrbSol =  new JRadioButton(s);
			jrbSol.setFont(fBraille);
			jrbSol.addItemListener(this);
			choix.add(jrbSol);
			bgSol.add(jrbSol);
			gbc.gridwidth = 2;
			gbc.gridx=0;
			pSol.add(jrbSol,gbc);
			gbc.gridwidth = 1;
			gbc.gridx+=2;
			pSol.add(new JLabel(" ("+i+")"),gbc);
			gbc.gridy++;
			i++;
		}
		gbc.gridx=0;
		gbc.gridwidth = 1;
		//ajout du custom radio button
		custom = new JRadioButton(texts.getText("other"));
		new ContextualHelp(custom,new Context("","other",texts));
		custom.setFont(fBraille);
		custom.addItemListener(this);
		bgSol.add(custom);
		pSol.add(custom,gbc);
		gbc.gridx++;
		pSol.add(jtfPerso,gbc);
		gbc.gridx++;
		pSol.add(jcbPerkins,gbc);
		
		gbc.gridx=0;
		gbc.gridy++;
		pSol.add(btValider,gbc);
		gbc.gridx++;
		pSol.add(btValiderTout,gbc);
		
		setContentPane(pSol);
		
		//sélection du premier choix
		if(choix.size()>0){choix.get(0).setSelected(true);}
		else{custom.setSelected(true);}
		//setVisible(true);
	}


	/**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent ae)
    {
	    if(ae.getSource()==btValider){dispose();}
	    else if (ae.getSource()==btValiderTout)
	    {
	    	lAmbAuto.add(amb);
	    	dispose();
	    }
	    else if(ae.getSource()==jcbPerkins)
	    {
	    	if(jcbPerkins.isSelected()){pn.addPerkinsObserver(jtfPerso);}
	    	else{pn.removePerkinsObserver(jtfPerso);}
	    }
	    
    }


	/**
     * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
     */
    @Override
    public void itemStateChanged(ItemEvent ie)
    {
	    if(ie.getSource()==custom)
	    {
	    	amb.setSolution(jtfPerso.getText());
	    	//System.out.println("Sol:"+jtfPerso.getText());
	    }
	    else
	    {	    	
	    	//System.out.println("Sol:"+((JRadioButton)(ie.getSource())).getText());
	    	amb.setSolution(((JRadioButton)(ie.getSource())).getText());
	    }
	    
    }

	/**
     * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
     */
    @Override
    public void changedUpdate(DocumentEvent arg0)
    {
    	amb.setSolution(jtfPerso.getText());
    	custom.setSelected(true);
    }


	/**
     * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
     */
    @Override
    public void insertUpdate(DocumentEvent arg0)
    {
    	amb.setSolution(jtfPerso.getText());
    	custom.setSelected(true);
    }


	/**
     * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
     */
    @Override
    public void removeUpdate(DocumentEvent arg0)
    {
    	amb.setSolution(jtfPerso.getText());
    	custom.setSelected(true);	    
    }

}
