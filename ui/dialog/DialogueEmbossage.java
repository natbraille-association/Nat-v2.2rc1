/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui.dialog;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import outils.emboss.Embossage;

import ui.accessibility.Context;
import ui.accessibility.ContextualHelp;
import ui.accessibility.Language;

import nat.ConfigNat;

/**
 * Dialogue permettant la sélection des pages à imprimer
 * @author bruno
 *
 */
public class DialogueEmbossage extends JDialog implements ActionListener, KeyListener
{
	/** Textual contents */
	static Language texts = new Language("DialogueEmbossage");
	
	/** pour la sérialisation (ne sert pas)*/
	private static final long serialVersionUID = 1L;
	/** Bouton radio sélectionner toutes les pages */
	private JRadioButton jrbToutes = new JRadioButton(texts.getText("all"));
	/** Bouton radio sélectionner la page courante */
	private JRadioButton jrbActu = new JRadioButton();
	/** Bouton radio sélection particulière des pages */
	private JRadioButton jrbSpec = new JRadioButton(texts.getText("select"));
	/** JTextField sélection des pages */
	private JTextField jtfPages = new JTextField(8);
	/** bouton embosser */
	private JButton jbEmbosser = new JButton(texts.getText("emboss"));
	/** bouton annuler */
	private JButton jbAnnuler = new JButton(texts.getText("cancel"));
	/** spinner exemplaire */
	private JSpinner jsExemplaires  = new JSpinner(new SpinnerNumberModel(1, 1, 10000, 1));	
	/** Instance d'embossage à réaliser */
	private Embossage emb = null;
	
	/**
	 * Constructeur
	 * @param parent La fenêtre appelante
	 * @param e instance d'Embossage contenant les informations d'embossage justement
	 */
	public DialogueEmbossage(JFrame parent, Embossage e)
	{
		super(parent, texts.getText("title"));
		getAccessibleContext().setAccessibleName(texts.getText("titlename"));
		setModal(true);
		emb = e;
		fabriqueFenetre();
		jrbActu.addKeyListener(this);
		jrbToutes.addKeyListener(this);
		jrbSpec.addKeyListener(this);
		jbEmbosser.addKeyListener(this);
		jtfPages.addKeyListener(this);
		jsExemplaires.addKeyListener(this);
		setVisible(true);
	}

	/**
	 * Fabrique la fenêtre de dialogue
	 */
	private void fabriqueFenetre()
	{		
		jrbActu.setText(texts.getText("current")+" (" + (emb.getPageActu()) + ")");
		ButtonGroup bg = new ButtonGroup();
		bg.add(jrbToutes);
		bg.add(jrbActu);
		bg.add(jrbSpec);
		
		jrbToutes.setSelected(true);
		jrbToutes.setMnemonic('t');
		Context cjrbToutes = new Context("t","RadioBox","all",texts);
		new ContextualHelp(jrbToutes,cjrbToutes);
		
		jrbActu.setMnemonic('p');
		Context cjrbActu = new Context("p","RadioBox","current",texts);
		new ContextualHelp(jrbActu,cjrbActu);
		
		jrbSpec.setMnemonic('u');
		Context cjrbSpec = new Context("u","RadioBox","select",texts);
		new ContextualHelp(jrbSpec,cjrbSpec);
		
		jbEmbosser.addActionListener(this);
		Context cjbEmbosser = new Context("e","Button","emboss",texts);
		new ContextualHelp(jbEmbosser,cjbEmbosser);
		jbEmbosser.setMnemonic('e');
		
		jbAnnuler.addActionListener(this);
		Context cjbAnnuler = new Context("a","Button","cancel",texts);
		new ContextualHelp(jbAnnuler,cjbAnnuler);
		jbAnnuler.setMnemonic('a');
		
		Context cjtfPages = new Context("","TextField","pages",texts);
		new ContextualHelp(jtfPages,cjtfPages);
		
		Context cjspEx = new Context("x","","exemplaires",texts);
		new ContextualHelp(jsExemplaires,cjspEx);
		JLabel lExemplaire = new JLabel(texts.getText("exemplaires"));
		lExemplaire.setDisplayedMnemonic('x');
		lExemplaire.setLabelFor(jsExemplaires);
		
		JLabel lJtfPages = new JLabel(texts.getText("pageslabel1") +
			" "+texts.getText("pageslabel2")+
			" "+texts.getText("pageslabel3"));
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		JLabel lTitre = new JLabel(texts.getText("title2"));

		c.gridx=0;
		c.gridy=0;
		c.insets = new Insets(3,3,3,3);
		c.anchor = GridBagConstraints.WEST;
		add(lTitre,c);
		
		c.gridy++;
		add(jrbToutes,c);
		
		c.gridy++;
		add(jrbActu,c);
		
		c.gridy++;
		add(jrbSpec,c);
		c.gridx++;
		add(jtfPages,c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=2;
		add(lJtfPages,c);
		
		c.gridy++;
		c.gridwidth=1;
		add(lExemplaire,c);
		c.gridx++;
		add(jsExemplaires,c);
		
		c.gridy++;
		c.gridx=0;
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(10,3,3,3);
		add(jbEmbosser,c);
		c.gridx++;
		add(jbAnnuler,c);
		
		if(ConfigNat.getCurrentConfig().getDoubleSpace())
		{
			c.gridy++;
			c.gridwidth=3;
			c.gridx=0;
			add(new JLabel("<html><font color='red'>"+texts.getText("warningDoubleSpace")+"</font></html>"),c);
		}
		
		
		pack();
		
	}

	/**
	 * Gère les actions sur les boutons {@link #jbAnnuler} et {@link #jbEmbosser}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
    public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==jbEmbosser)
		{
			majPages();
			dispose();
		}
		else if (ae.getSource()==jbAnnuler)
		{
			//mise à false
			for(int i=0;i<emb.getPages().length;i++){emb.getPages()[i]=false;}
			dispose();
		}
	}

	/**
	 * Met à jour du tableau des pages à embosser;
	 * si une page n'est pas sélectionnée, mets false comme valeur
	 * Indique le nombre d'exemplaire
	 */
	private void majPages()
	{
		if(jrbActu.isSelected()){emb.select(Embossage.CURRENT);}
		else if(jrbToutes.isSelected()){emb.select(Embossage.ALL);}
		else if(jrbSpec.isSelected()){emb.select(jtfPages.getText());}
		emb.setNbExemplaires(((Integer)(jsExemplaires.getValue())).intValue());
	}

	/**
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode()==KeyEvent.VK_ESCAPE)
			{jbAnnuler.doClick();}
	}

	/**
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
