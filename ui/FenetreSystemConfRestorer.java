/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import outils.SystemConfRestorer;
import ui.accessibility.Context;
import ui.accessibility.Language;


/** Classe pour restaurer les confs système
 * @author Frédéric
 *
 */
public class FenetreSystemConfRestorer extends JFrame implements ActionListener
{
   
    /** pour la sérialisation */
    private static final long serialVersionUID = 1L;

	/** Language instance containing the strings to use */
	private Language texts = new Language("FenetreSystemConfRestorer");
	/** message field */
    private JTextField  jTextFieldMsg = new JTextField();
    /** validate JButton */
    private JButton     jButtonValidate = new JButton();
    /** force validate JButton */
    private JButton     jButtonValidateForce = new JButton();
    /** cancel JButton */
    private JButton     jButtonCancel = new JButton();

    /** Context for {@link #jButtonCancel}*/
    private Context cjButtonCancel = new Context("","Button", "cancelRestore", texts);
    /** Context for {@link #jButtonValidateForce}*/
    private Context cjButtonValidateForce = new Context("","Button","validateForce",texts);
    /** Context for {@link #jButtonValidate}*/
    private Context cjButtonValidate = new Context("","Button","validate",texts);
    /** Context for {@link #jTextFieldMsg}*/
    private Context cjTextFieldMsg = new Context("","TextField","Message",texts);
    
    /** Instance de la fenêtre principale de NAT qui a construit l'instance de Configuration */
    private FenetrePrinc fPrinc;

    /** Fenêtre de dialogue pour restaurer les confs système
     * @param fp = fenêtre principale de NAT
     */
    public FenetreSystemConfRestorer(FenetrePrinc fp)
    {	 
    
    fPrinc = fp;

	setLayout(new GridBagLayout());
        GridBagConstraints gBC = new GridBagConstraints();

	gBC.insets=new Insets(10,0,0,0);  //Padding

	gBC.fill = GridBagConstraints.BOTH;
	gBC.gridwidth = 3;
	gBC.gridx = 0;
        gBC.gridy = 0;
	add(jTextFieldMsg,gBC);

	gBC.fill = GridBagConstraints.HORIZONTAL;
	gBC.gridwidth = 1;

	gBC.gridx = 0;
        gBC.gridy = 1;
	add(jButtonValidate,gBC);

	gBC.gridx = 1;
        gBC.gridy = 1;
	add(jButtonValidateForce,gBC);

	gBC.gridx = 2;
        gBC.gridy = 1;
	add(jButtonCancel,gBC);


	jButtonCancel.setText(texts.getText("cancel"));
	cjButtonCancel.treat();
	cjButtonCancel.setContextualHelp(jButtonCancel,"mainbuttons");

	jButtonValidate.setText(texts.getText("validate"));
	cjButtonValidate.treat();
	cjButtonValidate.setContextualHelp(jButtonValidate,"mainbuttons");

	jButtonValidateForce.setText(texts.getText("validateforce"));
	cjButtonValidateForce.treat();
	cjButtonValidateForce.setContextualHelp(jButtonValidateForce,"mainbuttons");

	jButtonCancel.setText(texts.getText("cancel"));
	cjButtonCancel.treat();
	cjButtonCancel.setContextualHelp(jButtonCancel,"mainbuttons");

	jTextFieldMsg.setText(texts.getText("Avertissement"));
	cjTextFieldMsg.treat();
	cjTextFieldMsg.setContextualHelp(jTextFieldMsg,"mainbuttons");



 	setSize(200,200);

	jButtonValidate.addActionListener(this);
	jButtonValidateForce.addActionListener(this);
	jButtonCancel.addActionListener(this);

	setVisible(true); 		
    }   
   
    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent evt)
    {
	boolean force = (evt.getSource() == jButtonValidateForce);
	boolean perform = (evt.getSource() ==jButtonValidate)
	    ||(evt.getSource() == jButtonValidateForce);
	
	if (perform) {
	    SystemConfRestorer.restoreNonInteractive(force);
	    fPrinc.chargeConfigurations(); //raffraîchissement de la liste des confs
	}
	dispose();
    }
	    
}
	
 
	

