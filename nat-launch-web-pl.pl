#!/usr/bin/perl

#
# wrapper for the mozilla firefox plugin
#

use strict;
use warnings;

$natLaunchWeb="/home/bruno/developpement/nat/nat-launch-web";

print "**********************\n";
my ($from,$to,$config) = ('_fromdefault','_todefault','_configdefault');
foreach (@ARGV){
    if ($_ =~ /from\s+(.+)/){
	$from = $1;
    }
    if ($_ =~ /to\s+(.+)/){
	$to = $1;
    }
    if ($_ =~ /config\s+(.+)/){
	$config = $1;
    }
}
die "from file ?" unless $from;
die "to file ?" unless $to;
die "config file ?" unless $config;

my $tidyoutfilename = $from."tidy.html";
my $tidyerrfilename = $from."tidy.err.html";

my $cmdtidy = join(" ",
		   'tidy -asxml -utf8 --quote-nbsp no',
		   "--force-output yes",
#		   "--show-body-only yes",
		   "--doctype omit",
		   "-f",$tidyerrfilename,
		   "-o",$tidyoutfilename,
		   $from);

print $cmdtidy."\n";
print `$cmdtidy`;
print "\n\n<ERREUR TIDY>\n";
open A, $tidyerrfilename;
foreach (<A>){
    print $_;
}
close A;
print "</ERREUR TIDY>\n\n\n";

my $cmdnat = join(" ",
	       $natLaunchWeb,
	       "-f",$tidyoutfilename,
	       "-t",$to,
	       "-c",$config,
	       "-m");

print $cmdnat."\n";
print "\n\n<SORTIE NAT>\n";
print `$cmdnat`;
print "</SORTIE NAT>\n\n\n";
 
      

# open A, $from;
# my @input = <A>;
# close A;

# my @resu;
# foreach (@input){
#     my $l = $_;
#     $l=~ s/Mission Handicap/Mission PROUTE/i;
#     $l=~ s/étudiant/chien/i;
#     push @resu, $l;   
# }
# sleep 3;
# open B, ">$to";
# foreach (@resu){
#     print B $_;
# }
# close B;
# 1;
