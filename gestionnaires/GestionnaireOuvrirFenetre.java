/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package gestionnaires;

import java.awt.Frame;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import nat.ConfigNat;

import ui.APropos;
import ui.FenetrePrinc;
import ui.accessibility.HelpAction;
import ui.config.Configuration;

/**
 * Gestionnaire ouvrant les fenêtres secondaire
 * @author bruno
 */
public class GestionnaireOuvrirFenetre implements ActionListener
{
	//constantes
	/** Constante représentant l'ouverture de la fenêtre d'option */ 
	public static final int OUVRIR_OPTIONS = 1;
	/** Constante représentant l'ouverture de la fenêtre d'aide */
	public static final int OUVRIR_AIDE = 2;
	/** Constante représentant l'ouverture de la fenêtre d'information*/
	public static final int OUVRIR_APROPOS = 3;
	//attributs
	/** entier représentant la fenêtre à ouvrir*/
	private int action;
	/** instance de la fenêtre principale créant cette instance */
	private FenetrePrinc fPrinc;
	/** fenêtre à créer*/
	private JFrame fenetre;
	//private ActionListener al;//lui il va servir pour la mise à jour du combo configurations de la fenêtre principale
	
	/**
	 * COnstructeur
	 * @param a code de la fenêtre à ouvrir
	 * @param fp instance de la fenêtre principale parente
	 */
	public GestionnaireOuvrirFenetre( int a, FenetrePrinc fp)
	{
	    action = a;
	    fPrinc = fp;
	}
	
	/**
	 * Implémentation. Ouvre la fenêtre correspondant à {@link #action}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
    public void actionPerformed(ActionEvent evt) 
	 {    
		 switch (action)
		 {
		 	case OUVRIR_OPTIONS :
		 		if(!fPrinc.getOptionsOuvertes())
		 		{
			 		fenetre = new Configuration(fPrinc);
		 			fPrinc.setOptionsOuvertes(true);
		 			fenetre.pack();
		 			//fenetre.addKeyListener(fPrinc);
		 			fenetre.setVisible(true);
		 			if (ConfigNat.getCurrentConfig().getMaximizedOptions())
		 				{fenetre.setExtendedState(Frame.MAXIMIZED_BOTH);}
		 		}
		 		else
		 		{
		 			fenetre.toFront();
		 		}
		 		break;
		 		
		 	case OUVRIR_AIDE :
		 		new HelpAction("top",null,false);
		 		break;
		 		
		 	case OUVRIR_APROPOS:
		 		fenetre = new APropos();
		 		//apropos.pack();
		 		fenetre.setVisible(true);
		 		break;
		 }
	 }



}
