/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package gestionnaires;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import nat.Nat;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import java.io.FileNotFoundException;
import java.util.ArrayList;


/**
 * Gestionnaire pour les messages d'erreurs générés par NAT
 * <p>Utilise un pattern <code>Ecouteur</code> pour la diffusion des messages.</p>
 * @author bruno
 *
 */
public class GestionnaireErreur extends DefaultHandler
{
	/** La dernière exception transmise au gestionnaire d'erreur*/
	private Exception exception;
	/** Vrai si NAT est en mode debug */
	private boolean debug = false;
	/** Niveau de verbosité des logs */
	private int niveauLog=1;
	/** Liste des afficheurs à notifier */
	private ArrayList<Afficheur> afficheurs = new ArrayList<Afficheur>();
	/** true si diffusion des messages, false sinon*/
	private boolean deliver = true;

	//constructeurs
	/**
	 * Constructeur
	 * @param e exception à traiter (<code>null</code> si aucune)
	 * @param al liste des afficheurs abonnés aux notifications 
	 * @param niveau niveau de verbosité des logs
	 */
	public GestionnaireErreur(Exception e, ArrayList<Afficheur> al, int niveau)
	{
		afficheurs = al;
		exception = e;
		niveauLog = niveau;
	}
	
	/**
	 * Constructeur
	 * @param e exception à traiter (<code>null</code> si aucune)
	 * @param niveau niveau de verbosité des logs
	 */
	public GestionnaireErreur(Exception e, int niveau)
	{
		exception = e;
		niveauLog = niveau;
		if (niveauLog == Nat.LOG_DEBUG){debug = true;}
	}

	//méthodes d'accès
	/** @return {@link #exception} */
	public Exception getException(){return  exception;}
	/** @param e valeur pour {@link #exception} */
	public void setException(Exception e){exception = e;}
	/**
	 * Si le niveau est debug, passe debug à vrai 
	 * @param niveau valeur pour {@link #exception} */
	public void setNiveauLog(int niveau)
	{
		niveauLog = niveau;
		if (niveauLog == Nat.LOG_DEBUG){debug = true;}
		else{debug = false;}
	}
	/** @param isDebugging valeur pour {@link #debug}*/
	public void setModeDebugage(boolean isDebugging){debug = isDebugging;}
	/**
	 * Ajoute l'afficheur <code>a</code> à la liste {@link #afficheurs}
	 * @param a afficheur à ajouter
	 * @see Afficheur
	 */
	public void addAfficheur(Afficheur a){afficheurs.add(a);}
	/**
	 * Supprime l'afficheur <code>a</code> à la liste {@link #afficheurs}
	 * @param a afficheur à supprimer
	 * @see Afficheur
	 */	
	public void removeAfficheur(Afficheur a){afficheurs.remove(a);}
	
	/**
	 * @return renvoie une liste des Afficheurs de type AfficheurLog
	 */
	public ArrayList<AfficheurLog> getAfficheursLog()
	{
		ArrayList<AfficheurLog> retour = new ArrayList<AfficheurLog>();
		for(Afficheur a : afficheurs)
		{
			if(a instanceof AfficheurLog){retour.add((AfficheurLog)a);}
		}
		return retour;
	}
	//méthodes
	/**
	 * fait afficher le message <code>message</code> par les afficheurs abonnés ({@link #afficheurs}
	 * <p><code>niveau</code> doit être inférieur à {@link #niveauLog} pour lancer la diffusion
	 * @param message le message à diffuser
	 * @param niveau le niveau de log requis pour être diffusé
	 */
	public void afficheMessage(String message, int niveau)
	{
		if (niveau <= niveauLog && deliver)
		{
			for(int i=0; i<afficheurs.size();i++)
			{
				afficheurs.get(i).afficheMessage(message);
			}
		}
	}
	
	/**
	 * Méthode préparant le message d'erreur des SAXParseException
	 * @param e l'instance de SAXParseException
	 * @return le message généré
	 */
	private String messageParser(SAXParseException e)
	{
		String message = "  " + e.getMessage() + "\n";
        message += "  Ligne " + e.getLineNumber()+", colonne "+e.getColumnNumber()+"\n";
        message += "  Public id : "+e.getPublicId()+"\n";
        message += "  System id : "+e.getSystemId()+"\n";
        return message;
    }
	/**
	 * Affiche le message provenant d'une SAXParseException
	 * @param e l'instance de SAXParseException
	 */
	private void afficheSAXException(SAXParseException e)
	{
		afficheMessage(messageParser(e), Nat.LOG_VERBEUX);
        if(e.getException() != null)
        {
        	e.getException().printStackTrace();
        }
    }
	/**
	 * Gère l'affichage de l'exception {@link #exception}
	 */
	public void gestionErreur()
	{
		if(deliver)
		{
			if (exception.getClass()==FileNotFoundException.class)
	        {
				afficheMessage("not ok\nERREUR: Problème lors du chargement d'un fichier: " + exception.getMessage(),Nat.LOG_SILENCIEUX);
	        }
			if (exception.getClass()==TransformerConfigurationException.class) 
	        {
				afficheMessage("not ok\nERREUR: Problème lors du chargement de la configuration du convertisseur: " + exception.getMessage(),Nat.LOG_SILENCIEUX );
	        } 
			if (exception.getClass()==TransformerException.class) 
	        {
				afficheMessage("not ok\nERREUR: Erreur du convertisseur: " + exception.getMessage(),Nat.LOG_SILENCIEUX );
	         }
			if (exception.getClass()==SAXException.class)
	        {
				afficheMessage("not ok\nERREUR: Erreur du parser SAX: " + exception.getMessage(),Nat.LOG_SILENCIEUX );
	        } 
			if (exception.getClass()==SAXParseException.class)
			{
				afficheMessage("not ok\nERREUR: Problème lors du parsage du document d'entrée avec SAX : " + exception.getMessage(),Nat.LOG_SILENCIEUX );
			}
			if (exception.getClass()==ParserConfigurationException.class)
	        {
				afficheMessage("not ok\nERREUR: Problème de configuration du parser: " + exception.getMessage(),Nat.LOG_SILENCIEUX );
	        }
			if (exception.getCause()!=null && exception.getCause().getClass()==OutOfMemoryError.class)
			{
				afficheMessage("\n*** Erreur: dépassement de la capacité mémoire\n", Nat.LOG_SILENCIEUX);
			}
			if(exception.getClass()==net.sf.saxon.trans.XPathException.class)
			{
				afficheMessage("not ok\nERREUR: Erreur lors de la transformation du document: "+ exception.getMessage(),Nat.LOG_SILENCIEUX);
			}
			if (exception.getClass()==NullPointerException.class)
	        {
				afficheMessage("not ok\nERREUR: Erreur de pointeur: " + exception.getMessage(),Nat.LOG_SILENCIEUX );
	        }
			if (exception.getClass()==Exception.class)  
	        {
				afficheMessage("not ok\nERREUR: Problème non identifié " + exception.getMessage(),Nat.LOG_SILENCIEUX);
	        }
			exception.printStackTrace();
		}
	}
	
	//méthodes redéfinies pour le DefaultHandler
	/**
	 * Si fonctionnement en mode debugage, affiche l'avertissement 
	 * @see org.xml.sax.helpers.DefaultHandler#warning(org.xml.sax.SAXParseException)
	 */
	@Override
	public void warning(SAXParseException spe) throws SAXException
	 {
		 if (debug)
		 {
			 afficheMessage("\n-Avertissement\n",Nat.LOG_VERBEUX);
			 afficheSAXException(spe);
		 }
     }
	/**
	 * Si fonctionnement en mode debugage, affiche l'erreur non bloquante 
	 * @see org.xml.sax.helpers.DefaultHandler#error(org.xml.sax.SAXParseException)
	 */
	@Override
	public void error(SAXParseException spe) throws SAXException
	 {
		 if (debug)
		 {
			 afficheMessage("\n-Erreur non bloquante\n",Nat.LOG_VERBEUX);
			 afficheSAXException(spe);
		 }
	 }
	/** 
	 * Lance la demande d'affichage pour l'erreur fatale <code>exception</code>
	 * @see org.xml.sax.helpers.DefaultHandler#fatalError(org.xml.sax.SAXParseException)
	 */
	@Override
	public void fatalError(SAXParseException spe) throws SAXException
	 {
		 afficheMessage("*\n** Erreur fatale ***",Nat.LOG_SILENCIEUX);
         throw spe;
     }

	/**
	 * Actibe ou désactive la diffusion des messages
	 * @param b true si diffusion de messages, false sinon
	 * @see #deliver
	 */
	public void deliver(boolean b) {deliver=b;}

}
