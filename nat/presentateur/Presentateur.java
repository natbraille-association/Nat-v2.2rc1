/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.presentateur;

import gestionnaires.GestionnaireErreur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
//import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
//import java.nio.charset.Charset;

import nat.ConfigNat;
import nat.Nat;

//import nat.Nat;
/**
 * Classe abstraite de présentation
 * <p>Les classes <code>Presentateur</code> sont généralement utilisées en fin de traitement,
 * une fois la conversion et la transcription réalisées, afin d'assurer le rendu final du document
 * transcrit</p>
 * <p>Chaque présentateur est tenu d'implémenter la méthode {@link #presenter()} afin de garantir la
 * généricité du code</p>
 */
public abstract class Presentateur
{
	/** L'adresse de la sortie */
	protected String sortie;
	/** le nom de la table braille de présentation */
	protected String tableBraille;
	/** l'adresse du fichier transcrit à présenter*/
	protected String source;
	/** temps d'exécution de la présentation en millisecondes */
	protected long tempsExecution;
	/** une instance de {@link GestionnaireErreur}*/
	protected GestionnaireErreur gest;
	/**
	 * Constructeur
	 * @param g une instance de {@link GestionnaireErreur}
	 * @param src L'adresse du fichier transcrit à présenter
	 * @param sor l'adresse de la sortie
	 * @param tab la table braille de sortie
	 */
	public Presentateur(GestionnaireErreur g,String src, String sor, String tab)
	{
		source= src;
		sortie = sor;
		gest = g;
		if(ConfigNat.getCurrentConfig().getIsSysTable())
		{
			tableBraille = ConfigNat.getInstallFolder()+"xsl/tablesEmbosseuse/"+tab;
		}
		else{tableBraille = ConfigNat.getUserEmbossTableFolder()+"/"+tab ;}
		if (!(tableBraille.endsWith(".ent"))){tableBraille=tableBraille+".ent";}
	}
	/** Méthode d'accès,
	 * @return {@link #tempsExecution}*/
	public long donneTempsExecution(){return tempsExecution;}
	/**
	 * Recopie le fichier source dans le fichier sortie en changeant l'encodage
	 * @param charsetSource encodage du fichier source
	 * @param charsetCible encodage du fichier de sortie
	 */
	protected void encode(String charsetSource, String charsetCible)
	{
		try 
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(source),charsetSource));
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(sortie),charsetCible));
			String ligne;
			while ((ligne = br.readLine()) != null)
			{
				bw.write(ligne+"\n");
				//System.err.println(ligne);
			}
			br.close();
			bw.close();
		}
		catch (FileNotFoundException fnfe)
		{
			gest.afficheMessage("Erreur, file not found", Nat.LOG_SILENCIEUX);
			fnfe.printStackTrace();
		}
		catch(IOException ioe)
		{
			gest.afficheMessage("Erreur entrée sortie", Nat.LOG_SILENCIEUX);
			ioe.printStackTrace();
		}
	finally
		{
			//TODO voir le truc pour fermer proprement dans tous les cas
		}
	}
	/**
	 * Méthode de présentation (rendu) de la transcription
	 * @return true si la présentation s'est déroulée correctement, false sinon
	 */
	public abstract boolean presenter();
}