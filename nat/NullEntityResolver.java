/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat;

import gestionnaires.GestionnaireErreur;

import java.io.IOException;
import java.io.StringReader;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * This class is used if no internet connection allow to resolve entities during parsing
 * @author bruno
 *
 */
public class NullEntityResolver implements EntityResolver
{
	/** Instance of error manager*/
	private GestionnaireErreur gest;
	
	/**
	 * Constructor
	 * @param g the error maager
	 */
	public NullEntityResolver(GestionnaireErreur g){gest = g;}
	/**
     * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String, java.lang.String)
     */
    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException
    {
    	gest.afficheMessage("Ignoring " + publicId +";" +systemId, Nat.LOG_DEBUG);
	    return new InputSource(new StringReader(""));
    }

}
