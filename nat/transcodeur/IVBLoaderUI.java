/*
 * Nat Braille
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * http://natbraille.free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat.transcodeur;

import gestionnaires.GestionnaireErreur;

/**
 * This interface describes a IVBLoader behaviour
 * @author bruno
 *
 */
public interface IVBLoaderUI
{
	/**
	 * run the ivb loader UI
	 */
	public void launch();

	/**
	 * set the error manager
	 * @param g the error manager instance
	 */
    void setGestionnaireErreur(GestionnaireErreur g);
}
