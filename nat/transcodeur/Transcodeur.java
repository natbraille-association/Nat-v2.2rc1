/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.transcodeur;

import nat.ConfigNat;
import gestionnaires.GestionnaireErreur;
/**
 * <p>La classe <code>Transcodeur</code> est une classe abstraite décrivant ce que doit faire un
 * Transcodeur</p>
 * @author  Bruno Mascret
 */
public abstract class Transcodeur
{
	/** Temps d'exécution de la transcription en milliseconde */
	protected long tempsExecution;
	/**  La feuille de style xsl à utiliser pour la transcription    */
	protected String filtre=ConfigNat.getCurrentConfig().getXSL();
	/**  Adresse du fichier d'entrée au format XML interne    */
	protected String entree;
	/**  Adresse du fichier de sortie xml  */
	protected String cible;
	/**  Encodage du fichier de sortie    */
	protected String sortieEncoding = "UTF-8";
	/** Le gestionnaire d'erreur */
	protected GestionnaireErreur gest;
	/**  Valeur du parametre xsl "abrege"    */
	protected boolean abrege = true;
	/** true si transcription du noir vers le Braille, false sinon
	 * à implémenter proprement
	 *   */
	protected boolean sens = false; /* TAN = true, NAT = false*/
	/**
     * Construit un objet Transcodeur
     *
     *  @param	e			<code>String</code> adresse du fichier d'entrée (format XML interne)
     *  @param	s			<code>String</code> adresse du fichier de sortie
     *  @param	se			<code>String</code> encodage du fichier de sortie
	 *  @param  g 			instance de <code>GestionnaireErreur</code>
     */
		
	public Transcodeur(String e, String s, String se, GestionnaireErreur g)
	{
		entree = e;
		cible = s;
		sortieEncoding = se;
		gest =g;
	}
	
	/**
	 * Set the transcription's direction
	 * @param s true si transcription du noir vers le braille
	 */
	public void setSens(boolean s){sens = s;}
	/**
	 * Get the transcription's direction
	 * @return the ranscription direction, true if from black to braille text
	 */
	public boolean getSens(){return sens;}
	
	/**Méthode d'accès à <code>tempsExecution</code>
	 * @return le temps mis pour transcire */
	public long donneTempsExecution(){return tempsExecution;}
	
	/**
	 * Réalise la transcription du fichier d'entrée vers le fichier de sortie 
	 * @param	gestErreur	Un objet <code>GestionnaireErreur</code> pour l'affichage et la gestion
	 * des improbables erreurs.
	 * @return true si la tarnscription s'est déroulée normallement
	 */
	public abstract boolean transcrire(GestionnaireErreur gestErreur);
		
}
