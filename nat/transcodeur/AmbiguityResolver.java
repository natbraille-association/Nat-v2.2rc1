/*
 * Nat Braille
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * http://natbraille.free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat.transcodeur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import outils.FileToolKit;

import nat.ConfigNat;
import nat.Nat;
import gestionnaires.GestionnaireErreur;

/**
 * Résoud les ambiguïtés détectée lors de la transcription
 * @author bruno
 *
 */
public class AmbiguityResolver
{
	/** Nom du fichier transcrit contenant des ambiguïtés */
	private String fTransAmb ="";
	/** Nom du fichier transcrit contenant les ambiguïtés résolues*/
	private String fTransAmbResol ="";
	/** Instance du gestionnaire d'erreurs */
	private GestionnaireErreur gest = null;
	/** liste des ambiguités */
	private ArrayList<Ambiguity> ambiguities = new ArrayList<Ambiguity>();
	/** instance de AmbiguityResolverUI */
	private AmbiguityResolverUI arUI;
	
	/**
	 * Crée une instance de AmbiguityResolver
	 * @param fTrAmb adresse du fichier à analyser
	 * @param g instance du gestionnaire d'erreur
	 */
	public AmbiguityResolver(String fTrAmb, GestionnaireErreur g)
	{
		fTransAmb = fTrAmb;
		fTransAmbResol = fTransAmb + ".amb";
		gest = g;
		arUI = ConfigNat.getAmbiguityResolverUI();
		resolveAmbiguity();
	}
	
	/**
	 * Charge les ambiguités
	 * @return true si analyse terminée correctement
	 */
	private boolean resolveAmbiguity()
	{
		boolean retour = true;
		
		gest.afficheMessage("Chargement du fichier transcrit pour analyse", Nat.LOG_VERBEUX);
		/*Document doc;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG);
    
        DocumentBuilder db;
		try
		{
			db = dbf.newDocumentBuilder();
			doc = db.parse(new File(fTransAmb));
			
			NodeList ambs = doc.get
			
			for(int i=0;i<ambs.getLength();i++)
			{	
				ambiguities.add(new Ambiguity(ambs.item(i)));
			}
		}
		catch (ParserConfigurationException e) {e.printStackTrace();}//TODO
		catch (SAXException e) {e.printStackTrace();}//TODO
		catch (IOException e) {e.printStackTrace();}//TODO*/
		try
        {
			BufferedReader raf = new BufferedReader(new InputStreamReader(new FileInputStream(fTransAmb),"UTF-8"));
	        BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fTransAmbResol),"UTF8"));
	        String ligne = raf.readLine();
	        while(ligne!=null)
	        {
	        	if(ligne.contains("[Amb:"))
	        	{
	        		//récupération de l'ambiguïté
	        		String [] decoup = ligne.split("\\[Amb:|\\]");
	        		for(int i=0;i<decoup.length;i++)
	        		{
	        			if(i%2==0){fcible.write(decoup[i]);}
	        			else
	        			{
	        				Ambiguity amb = new Ambiguity(decoup[i]);
	        				boolean all = true;//TODO
	        				if(!ambiguities.contains(amb)||all)
	        				{
	        					arUI.resolve(amb);
	        				}
	        				ambiguities.add(amb);
	        				fcible.write(amb.getSolution());
	        			}
	        		}
	        		fcible.write("\n");
	        	}
	        	else
	        	{
	        		fcible.write(ligne+"\n");
	        	}
	        	ligne=raf.readLine();
	        }
	        raf.close();
	        fcible.close();
	        //copie du nouveau fichier avec les bonnes ambiguïtés résolues
	        FileToolKit.copyFile(fTransAmbResol, fTransAmb);
        }
        catch (UnsupportedEncodingException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (FileNotFoundException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (IOException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
		return retour;		
	}
}
