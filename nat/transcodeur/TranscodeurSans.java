/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.transcodeur;

import gestionnaires.GestionnaireErreur;
import nat.transcodeur.Transcodeur;
import outils.FileToolKit;
/**
 * <p>La classe <code>TranscodeurSans</code> se contente de recopier le fichier d'entrée dans le fichier de sortie.
 * Elle ne réalise pas de transcription.</p>
 * @author  Bruno Mascret
 */
public class TranscodeurSans extends Transcodeur {

	/**
	 * Constructeur
	 * @param e fichier entrée
	 * @param s fichier sortie
	 * @param se encodage entrée
	 * @param g instance de GestionnaireErreur
	 */
	public TranscodeurSans(String e, String s, String se, GestionnaireErreur g)
	{
		super(e, s, se, g);
	}

	/**
	 * Copy le fichier d'entrée dans le fichier de sortie
	 * @see nat.transcodeur.Transcodeur#transcrire(gestionnaires.GestionnaireErreur)
	 */
	@Override
	public boolean transcrire(GestionnaireErreur gestErreur) {
		return FileToolKit.copyFile(entree, cible);
	}

}
