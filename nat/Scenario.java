/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat;

import gestionnaires.GestionnaireErreur;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import outils.FileToolKit;

/**
 * @author bruno
 * Cette classe permet la manipulation d'un scénario composé de configuration différentes
 *
 */
public class Scenario
{
	/** */
	public static final String CONTENT_FILE = "content.scen";
	/** les adresses des différentes configurations */
	private ArrayList<String> etapes = new ArrayList<String>();
	
	/** numéro de l'étape actuelle */
	private int step = 0;
	
	/** nom du scénario */
	private String name ="";
	
	/** chemin du répertoire contenant le scénario */
	private String path="";
	
	/** petit nom joli du scénario */
	private String prettyName = "";
	
	/** Gestionnaire d'erreur */
	private GestionnaireErreur gest = null;
	
	/**
	 * @param p adresse du répertoire contenant le scénario
	 * @param g instance du gestionnaire d'erreur à utiliser
	 *
	 * @return une instance vierge de Scenario 
	 */
	public static Scenario createNewScen(String p, GestionnaireErreur g)
	{
		File f = new File(p);
		if (f.exists())
		{
			FileToolKit.saveStrToFile("", p+File.separator+CONTENT_FILE, "UTF-8");
		}
		return new Scenario(p,g);
	}
	/**
	 * @param p adresse du répertoire contenant le scénario
	 * @param g instance du gestionnaire d'erreur à utiliser
	 */
	public Scenario(String p, GestionnaireErreur g)
	{
		gest = g;
		path = p;
		//nom du scenario: le nom de son répertoire
		name = new File(path).getName();
		
		try
		{
			//chargements des configurations présentes
			ArrayList<String> lPresent = new ArrayList<String>();
			for(String f : new File(path).list()){lPresent.add(f);}
			//chargement des étapes déclarées dans le fichier
			for(String f : FileToolKit.loadFileToStr(path+File.separator+CONTENT_FILE, "UTF-8").split("\n"))
			{
				if(lPresent.contains(f)){etapes.add(f);}
				else{gest.afficheMessage("Fichier de configuration " + f + 
						" (étape "+ etapes.size()+ " introuvable, étape supprimée", Nat.LOG_SILENCIEUX);
				}
			}
			//chargement de l'étape actuelle et du fameux pretty name
			
			Properties scenarii = new Properties();
			String fichScen = ConfigNat.getScenariiFile();
			if(new File(fichScen).exists())
			{
			    try
			    {
			    	scenarii.load(new FileInputStream(fichScen));
			    	step = Integer.parseInt(scenarii.getProperty(name, "1"));
			    	prettyName = scenarii.getProperty(name+"-pretty", name);
			    }
			    catch (IOException ioe) 
		        {
				    ioe.printStackTrace(); 
				    gest.afficheMessage("Exception while reading scenario configuration file", Nat.LOG_SILENCIEUX);
		        }
			}
			else
			{
				//création du fichier
				scenarii.setProperty(name, "1");//étape 1 par défaut
				try
	            {
		            scenarii.store(new FileOutputStream(fichScen), null);
	            }
	            catch (FileNotFoundException e)
	            {
		            gest.afficheMessage("Fichier " + fichScen + " non trouvé, arrêt", Nat.LOG_SILENCIEUX);
	            }
	            catch (IOException e)
	            {
	            	gest.afficheMessage("Erreur d'entrée sortie, arrêt", Nat.LOG_SILENCIEUX);
	            }	
			}
		}
		catch(NullPointerException npe)
		{
			System.out.println("erreur ");
			npe.printStackTrace();
			//scénario vide
			step = 0;
			prettyName = name;
		}
	}
	
	/**
	 * Insère une copie de la configuration <code>confName</code> dans le scénario à la position <code>pos</code>
	 * Si pos > last position, alors pos = last position;
	 * Si pos < 1, alors pos = 1
	 * @param confName adresse de la configuration à copier
	 * @param pos position pour l'insertion
	 * @return true si insertion réalisée, false si erreur
	 */
	public boolean insertStep(String confName, int pos)
	{
		boolean retour = true;
		if (pos > etapes.size()){pos = etapes.size();}
		else if(pos < 1){pos = 1;}
		try
		{
			//on ajoute la conf
			etapes.add(pos-1, confName);
			//décalage des fichiers de configuration
			for(int i = etapes.size() -1 ; i >= pos; i--)
			{
				File f = new File(path+File.separator+etapes.get(i));
				File c = new File(path+File.separator+name+(i+1)+".cfg");
				FileToolKit.copyFile(f.getCanonicalPath(), c.getCanonicalPath());
				//f.renameTo(c);
				etapes.set(i, name+(i+1)+".cfg");
			}
			//la conf à insérer
			String rename = path+File.separator+name+pos+".cfg";
			FileToolKit.copyFile(confName, rename);
			etapes.set(pos-1, name+pos+".cfg");
			//mise à jour du fichier content
			String liste = "";
			for(String s : etapes)
			{
				//System.out.println(s);
				liste+=s+"\n";
				//on rappelle à tout le monde qu'ils ont un scénario
				Properties conf = new Properties();
				conf.load(new FileInputStream(path+File.separator+s));
				conf.setProperty(OptNames.sc_name, name);
				
				conf.store(new FileOutputStream(path+File.separator+s), null);
			}
			FileToolKit.saveStrToFile(liste, path+File.separator+CONTENT_FILE,"UTF-8");
		}
		catch(Exception e)
		{
			gest.afficheMessage("Erreur lors de l'insertion d'une étape", Nat.LOG_SILENCIEUX);
			retour = false;
		}
		return retour;
	}
	
	/**
	 * Retire du scénario la configuration de la position <code>pos</code>
	 * Si pos n'est pas valable (<0 ou > lastSptep), ne fait rien
	 * @param pos position pour l'insertion
	 * @return true si insertion réalisée, false si erreur
	 */
	public boolean removeStep(int pos)
	{
		boolean retour = true;
		if (pos > etapes.size()){pos = etapes.size();}
		else if(pos < 1){pos = 1;}
		try
		{
			//décalage des fichiers de configuration
			for(int i = pos -1 ; i < etapes.size()-1; i++)
			{
				File f = new File(path+File.separator+etapes.get(i));
				File c = new File(path+File.separator+etapes.get(i+1));
				c.renameTo(f);
			}
			//on enleve la denière conf (doublon)
			etapes.remove(etapes.size()-1);
			//mise à jour du fichier content
			String liste = "";
			for(String s : etapes){liste+=s+"\n";}
			FileToolKit.saveStrToFile(liste, path+File.separator+CONTENT_FILE,"UTF-8");
		}
		catch(Exception e)
		{
			gest.afficheMessage("Erreur lors de l'insertion d'une étape", Nat.LOG_SILENCIEUX);
			retour = false;
		}
		//maj éventuelle de l'étape
		setStep(step);
		return retour;
	}
	
	/** @return the step */
    public int getStep(){return step;}

	/** @param s the step to set */
    public void setStep(int s)
    {
    	step = s;
    	if(step > etapes.size()){step = etapes.size();}
    	else if(step<1){step=1;}
    	Properties scenarii = new Properties();
    	try
        {
    		scenarii.load(new FileInputStream(ConfigNat.getScenariiFile()));
    		scenarii.setProperty(name, ""+step);
            scenarii.store(new FileOutputStream(ConfigNat.getScenariiFile()), null);
        }
        catch (FileNotFoundException e)
        {
            gest.afficheMessage("Fichier " + ConfigNat.getScenariiFile() + " non trouvé, arrêt", Nat.LOG_SILENCIEUX);
        }
        catch (IOException e)
        {
        	gest.afficheMessage("Erreur d'entrée sortie, arrêt", Nat.LOG_SILENCIEUX);
        }
    }
    
    /**
     * 
     * @param i numéro de l'étape
     * @return adresse de la configuration
     */
    public String getEtape(int i)
    {
    	String retour = "";
    	if(i>0 && i<= etapes.size()){retour =path+File.separator+ etapes.get(i-1);}
    	return retour;
    }
    
    /**
     * @return the current step path
     */
    public String getCurrentStepPath(){return path+File.separator+etapes.get(step-1);}
    /**
     * @return the current step number (starts at 1)
     */
    public int getCurrentStep(){return step;}
    /**
     * @return the number of steps
     */
    public int getSize(){return etapes.size();}
    /**
     * @return the scenario name: {@link #name}
     */
    public String getName(){return name;}
    /**
     * @return the scenario pretty name: {@link #prettyName}
     */
    public String getPrettyName(){return prettyName;}
    /**
     * @param pn the scenario pretty name: {@link #prettyName}
     */
    public void setPrettyName(String pn)
    {
    	prettyName=pn;
    	Properties scenarii = new Properties();
    	try
        {
    		scenarii.load(new FileInputStream(ConfigNat.getScenariiFile()));
    		scenarii.setProperty(name+"-pretty", prettyName);
            scenarii.store(new FileOutputStream(ConfigNat.getScenariiFile()), null);
        }
        catch (FileNotFoundException e)
        {
            gest.afficheMessage("Fichier " + ConfigNat.getScenariiFile() + " non trouvé, arrêt", Nat.LOG_SILENCIEUX);
        }
        catch (IOException e)
        {
        	gest.afficheMessage("Erreur d'entrée sortie, arrêt", Nat.LOG_SILENCIEUX);
        }
    }
    
    /** redéfinition de toString() */
    @Override
    public String toString()
    {
    	return "nom: " + name +
    		"\npretty: "+ prettyName +
    		"\nstep: "+ step + "/" + etapes.size()+", "+getCurrentStepPath();
    }
	/**
	 * ajoute l'étape etape (en fin de liste)
	 * maj des fichiers
	 * step = last index + 1
     * @param etape adresse de l'étape à ajouter
     */
    public void addStep(String etape)
    {
    	String rename = path+File.separator+name+(getSize()+1)+".cfg";
		FileToolKit.copyFile(etape, rename);
	    etapes.add(name+(getSize()+1)+".cfg");
	    setStep(etapes.indexOf(rename)+1);
	    //mise à jour du fichier content
		String liste = "";
		for(String s : etapes){liste+=s+"\n";}
		FileToolKit.saveStrToFile(liste, path+File.separator+CONTENT_FILE,"UTF-8");
    }
	
	/*
	 * TODO A virer, uniquement pour les test
	 *
	public void test()
	{
		for (String s : etapes){System.out.println(s);}
		System.out.println("étape:" + step);
		insertStep("/home/bruno/.nat-braille/filters/5-dbt",6);
		removeStep(3);
		System.out.println("étape:" + step);
		insertStep("/home/bruno/.nat-braille/filters/5-dbt",6);
		insertStep("/home/bruno/.nat-braille/filters/5-dbt",6);
		removeStep(1);
		
		
	}
	/*
	 * TODO A virer, uniquement pour les test
	 * @param a à virer
	 *
	public static void main(String[]a)
	{
		Scenario s = new Scenario("configurations/testScen",null);
		s.test();
	}*/
}
