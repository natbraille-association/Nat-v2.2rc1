/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat.saxFuncts;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net2.sf.saxon.functions.ExtensionFunctionCall;
import net2.sf.saxon.functions.ExtensionFunctionDefinition;
import net2.sf.saxon.om.StructuredQName;
import net2.sf.saxon.value.SequenceType;


/**
 * Cette classe est destinée à fournir une version modifiée du translate de xsl pour les grosses opérations
 * de conversion entre tables
 * @author bruno
 *
 */
public class SaxHyphenatesDef extends ExtensionFunctionDefinition
{
	
	/** */
    private static final long serialVersionUID = 1L;

	/**
	 * Cette méthode fournie une version modifiée du translate de xsl pour les grosses opérations
	 * de conversion entre tables braille
	 * @param template la chaine à convertir
	 * @param in la chaine de conversion d'entrée
	 * @param out la chaine de conversion de sortie
	 * @return la chaine convertie
	 */
	
	public static String translate(String template, String in, String out)
	{
		Map<String,String> tokens = new HashMap<String,String>();
		//String template = "cat really needs some beverage.";
	
		// Create pattern of the format "%(cat|beverage)%"
		String patternString ="(";
		for(int i=0;i<in.length();i++)
		{
			String s = ""+in.charAt(i);
			tokens.put(s, ""+out.charAt(i));
			patternString = patternString + s + "|";
		}
		patternString = patternString.substring(0, patternString.length()-1) + ")";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(template);
		//System.out.println(patternString);
		StringBuffer sb = new StringBuffer();
		while(matcher.find()) {
		    matcher.appendReplacement(sb, tokens.get(matcher.group(1)));
		}
		matcher.appendTail(sb);	
		//System.out.println(sb.toString());
		return sb.toString();
	}
	



	/**
     * @see net2.sf.saxon.functions.ExtensionFunctionDefinition#getArgumentTypes()
     */
    @Override
    public SequenceType[] getArgumentTypes()
    {
	    SequenceType [] st = new SequenceType[5];
	    st[0] = SequenceType.SINGLE_STRING;
	    st[1] = SequenceType.SINGLE_STRING;
	    st[2] = SequenceType.ATOMIC_SEQUENCE;
	    st[3] = SequenceType.ATOMIC_SEQUENCE;
	    st[4] = SequenceType.ATOMIC_SEQUENCE;
	    
	    return st;
    }

	/**
     * @see net2.sf.saxon.functions.ExtensionFunctionDefinition#getFunctionQName()
     */
    @Override
    public StructuredQName getFunctionQName()
    {
	    StructuredQName sqn = new StructuredQName("", "", "hyphenates");
	    return sqn;
    }

	/**
     * @see net2.sf.saxon.functions.ExtensionFunctionDefinition#getMinimumNumberOfArguments()
     */
    @Override
    public int getMinimumNumberOfArguments()
    {
	    return 5;
    }

	/**
     * @see net2.sf.saxon.functions.ExtensionFunctionDefinition#getResultType(net2.sf.saxon.value.SequenceType[])
     */
    @Override
    public SequenceType getResultType(SequenceType[] arg0)
    {
	    // TODO Auto-generated method stub
	    return SequenceType.SINGLE_STRING;
    }

	/**
     * @see net2.sf.saxon.functions.ExtensionFunctionDefinition#makeCallExpression()
     */
    @Override
    public ExtensionFunctionCall makeCallExpression()
    {
	    // TODO Auto-generated method stub
	    return new SaxHyphenatesCall();
    }

}
