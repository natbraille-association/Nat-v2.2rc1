/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat.saxFuncts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Cette classe est destinée à fournir une version modifiée du translate de xsl pour les grosses opérations
 * de conversion entre tables
 * @author bruno
 *
 */
public class SaxFuncts
{
	
	/**
	 * Cette méthode fournie une version modifiée du translate de xsl pour les grosses opérations
	 * de conversion entre tables braille
	 * @param template la chaine à convertir
	 * @param in la chaine de conversion d'entrée
	 * @param out la chaine de conversion de sortie
	 * @return la chaine convertie
	 */
	
	public static String translate(String template, String in, String out)
	{
		Map<String,String> tokens = new HashMap<String,String>();
		//String template = "cat really needs some beverage.";
	
		// Create pattern of the format "%(cat|beverage)%"
		String patternString ="(";
		for(int i=0;i<in.length();i++)
		{
			String s = ""+in.charAt(i);
			tokens.put(s, ""+out.charAt(i));
			patternString = patternString + s + "|";
		}
		patternString = patternString.substring(0, patternString.length()-1) + ")";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(template);
		//System.out.println(patternString);
		StringBuffer sb = new StringBuffer();
		while(matcher.find()) {
		    matcher.appendReplacement(sb, tokens.get(matcher.group(1)));
		}
		matcher.appendTail(sb);	
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
	/**
	 * Test methode
	 * @param arg param strings
	 */
	public static void main(String arg[])
	{
		/*ArrayList<String> t1 = new ArrayList<String>();
		ArrayList<String> t2 = new ArrayList<String>();
		t1.add("cat");
		t2.add("felix");
		t1.add("beverage");
		t2.add("milk");
		new BrailleTranslate(t1,t2);*/
		SaxFuncts.translate("Salut les copains, ça roule bien ici?","abc", "bBC");
	}
	
	/**
	 * Test
	 * @param mot le mot à couper
	 * @param coupe le char de coupe
	 * @param patterns tableau des patterns à tester
	 * @param patterns0 tableau des patterns avec 0 inséré
	 * @param patterns0nd tableau des patterns de remplacement (0 partout)
	 * @return le mot avec les coupes possibles
	 */
	public static String hyphenates(String mot,String coupe,String [] patterns,String [] patterns0,String []patterns0nd)
	{
		StringBuffer retour = new StringBuffer();
		//System.out.println("{mot: " + mot +"} -------");
		if(!mot.matches(".*[1|2|3|4].*"))
		{
			StringBuffer motCoup = new StringBuffer("");
			for(int i=0; i<mot.length();i++)
			{
				motCoup.append(mot.charAt(i));
				motCoup.append(0);
			}
			String s = motCoup.toString();
			ArrayList<String> list = new ArrayList<String>();
			for(int i=0; i<patterns.length;i++)
			{
				String s2 = s.replaceAll(patterns0[i], patterns0nd[i]);
				if(!s2.equals(s))
				{
					String s3 = s2.replaceAll("0(1|2|3|4)", "$1");
					list.add(s3);
					//System.out.println(s3);
				}
			}
			for(int i=0;i<motCoup.length();i++)
			{
				char ch = (char)motCoup.codePointAt(i);
				if(ch=='0')
				{
					char code = '0';
					int j=0;
					while(j<list.size() && code < '4')
					{
						char lu = list.get(j).charAt(i);
						if(lu > code){code=lu;}
						j++;
					}
					if(code=='1' || code=='3'){retour.append(coupe);}
				}
				else{retour.append(ch);}
			}
			//System.out.println("----------------");
			//System.out.println(retour.toString());
		}
		return retour.toString();
	}

}
