/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat.saxFuncts;


import net2.sf.saxon.expr.XPathContext;
import net2.sf.saxon.functions.ExtensionFunctionCall;
import net2.sf.saxon.om.SequenceIterator;
import net2.sf.saxon.om.SingletonIterator;
import net2.sf.saxon.trans.XPathException;
import net2.sf.saxon.value.ObjectValue;

/**
 * @author bruno
 *
 */
public class SaxHyphenatesCall extends ExtensionFunctionCall
{

	/** */
    private static final long serialVersionUID = 1L;

	/**
	 * @see net2.sf.saxon.functions.ExtensionFunctionCall#call(net2.sf.saxon.om.SequenceIterator[], net2.sf.saxon.expr.XPathContext)
	 */
	@Override
	public SequenceIterator call(SequenceIterator[] arg0, XPathContext arg1)
	        throws XPathException
	{
		//GroundedValue va = ((GroundedIterator)arg0[2].next()).materialize();
		
		//String retour = hyphenates(arg0[0].current().getStringValueCS(),arg0[1].current().getStringValue(),a,b,c);
		String retour="";
		ObjectValue ov = new ObjectValue(retour);
		return SingletonIterator.makeIterator(ov);
	}
	
	

}
