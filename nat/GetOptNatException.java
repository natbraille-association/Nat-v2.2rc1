/*
 * NAT - An universal Translator
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package nat;

/**
 * Exception for use with GetOptNat
 * raised unless command line is correct
 * @author Vivien
 *
 */
public class GetOptNatException extends Exception {  
    /** Numéro de sérialisation, not used */
    private static final long serialVersionUID = 1L;
    /** 
     * Crée une nouvelle instance de GetOptNatException
     * @param message Le message détaillant exception 
     */  
    public GetOptNatException(String message) {  
	super(message); 
    }  
    /** 
     * Crée une nouvelle instance de GetOptNatException
     * @param cause L'exception à l'origine de cette exception 
     */  
    public GetOptNatException(Throwable cause) {  
	super(cause); 
    }  
    /** 
     * Crée une nouvelle instance de GetOptNatException 
     * @param message Le message détaillant exception 
     * @param cause L'exception à l'origine de cette exception 
     */  
    public GetOptNatException(String message, Throwable cause) {  
	super(message, cause); 
    } 
}
