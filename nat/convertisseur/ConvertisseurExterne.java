/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.convertisseur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import outils.FileToolKit;
import nat.ConfigNat;
import nat.Nat;
import nat.Transcription;
import gestionnaires.GestionnaireErreur;

/**
 * Permet l'exécution d'un convertisseur externe (ligne de commande et paramètre)
 * Contient également, si besoin, le convertisseur suivant à utiliser
 * @author bruno
 *
 */
public class ConvertisseurExterne extends Convertisseur
{

	/** Constante indiquant qu'il ne faut pas utiliser de convertisseur supplémentaire après la conversion */
	public static final int CONV_AUCUN = 0;
	/** Constante indiquant qu'il faut utiliser le convertisseur XHTML après la conversion */
	public static final int CONV_XHTML = 1;
	/** 
	 * ligne de commande à exécuter
	 * $i sera remplacé par le fichier source
	 * $o sera remplacé par le fichier cible
	 **/
	private String ldc = "";
	/** code pour le convertisseur supplémentaire */
	private int code = 0; 
	/**
	 * @param l ligne de commande à exécuter (voir format pour {@link #ldc})
     * @param src fichier source
     * @param tgt fichier produit
     */
    public ConvertisseurExterne(String src, String tgt, String l)
    {
	    super(src, tgt);
	    ldc = l;
    }
    
    /**
	 * @param l ligne de commande à exécuter (voir format pour {@link #ldc})
     * @param src fichier source
     * @param tgt fichier produit
     * @param c valeur pour {@link #code}
     */
    public ConvertisseurExterne(String src, String tgt, String l, int c)
    {
	    super(src, tgt);
	    ldc = l;
	    code = c;
    }

	/**
	 * @see nat.convertisseur.Convertisseur#convertir(gestionnaires.GestionnaireErreur)
	 */
	@Override
	public boolean convertir(GestionnaireErreur gest)
	{
		boolean retour = true;
		
		/* si besoin, appel du convertisseur supplémentaire */
		switch(code)
		{
			case CONV_AUCUN:
				retour = execCommande(cible,gest);
				break;
			case CONV_XHTML:
				retour = execCommande(Transcription.fTempXHTML,gest);
				/* Le fichier généré contient une dtd qui doit être résolue avec une connexion
				 * internet: on supprime cette ligne du fichier
				 */
				if(retour)
				{
					String s = FileToolKit.loadFileToStr(Transcription.fTempXHTML);
					//s=s.replaceFirst(".*DOCTYPE.*", "").replaceFirst(".*xhtml-math11-f.dtd.*", "");
					s=s.replaceFirst("\".*xhtml-math11-f.dtd", "\""+ConfigNat.getCurrentConfig().getDTD());
					//System.out.println(s);
					FileToolKit.saveStrToFile(s,Transcription.fTempXHTML);
					//conversion de XHTML vers le format interne
					ConvertisseurXML convXML = new ConvertisseurXML(Transcription.fTempXHTML, cible);
					retour = convXML.convertir(gest);
				}
				break;
		}
		tempsExecution = System.currentTimeMillis() - tempsExecution;
		return retour;
	}
	
	/**
	 * Exécute le programme externe avec ses paramètres
	 * @param c la cible du convertisseur (fichier produit)
	 * @param gest the error manager in use
	 * @return true si conversion réalisée
	 */
	private boolean execCommande(String c,GestionnaireErreur gest)
	{
		String ldcExec = ldc.replaceFirst("[$]i", source).replaceFirst("[$]o", c);
		System.out.println("******** LDC ****\n"+ldcExec+"\n");
		Runtime runTime = Runtime.getRuntime();
		int res = 0;
		try
		{
			Process p = runTime.exec(ldcExec);
			System.out.println("lancé");
	         // read the standard output of the command
	        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
	        BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
	        String s ="";
	        while (!procDone(p))
	        {
	        	while((s=stdInput.readLine()) !=null){gest.afficheMessage(s, Nat.LOG_DEBUG);}
	        	while((s=stdError.readLine()) !=null){gest.afficheMessage(s, Nat.LOG_SILENCIEUX);}
	        }
	        stdInput.close();
	        stdError.close();
			res = p.waitFor();
		}
		catch (IOException e) {e.printStackTrace();}
		catch (InterruptedException e) {e.printStackTrace();}
		if (res != 0)
		{//le processus p ne s'est pas terminé normalement
			System.out.println("erreur");
		}
		return true;
	}
	/**
	 * Return true if the process p is done
	 * @param p the running process
	 * @return true when process p is done
	 */
	private boolean procDone(Process p)
	{
		boolean retour = true;
		try{p.exitValue();}
		catch(IllegalThreadStateException e){retour=false;}
		return retour;
    }

}
