/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.convertisseur;

import gestionnaires.GestionnaireErreur;
import nat.Nat;
import nat.Transcription;
// *** writer2latex ***
/*import writer2latex.Writer2XHTML;
import writer2latex.office.MIMETypes;*/
//import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
//import java.util.Enumeration;
import writer2latex.api.*;
import writer2latex.office.MIMETypes;

/**
 * Convertisseur de documents odt; utilise writer2xhtml pour convertir en xhtml puis {@link ConvertisseurXML}
 * pour convertir au format interne
 * @author bruno
 *
 */
public class ConvertisseurOpenOffice extends ConvertisseurXML
{
	/**
	 * Constructeur
	 * @param src l'adresse du fichier source
	 * @param tgt l'adresse du fichier cible
	 */
	public ConvertisseurOpenOffice(String src, String tgt){super(src, tgt);}
	/**
	 * Redéfinition de {@link Convertisseur#convertir(GestionnaireErreur)}
	 * <p>Convertit d'abord le fichier odt en fichier xhtml avec <code>writer2xhtml</code> (création du fichier temporaire 
	 * {@link Transcription#fTempXHTML}</p>.
	 * <p>Convertit ensuite le fichier {@link Transcription#fTempXHTML} au format interne.
	 */
	@Override
	public boolean convertir(GestionnaireErreur gest)
	{
		tempsExecution = System.currentTimeMillis();
		boolean retour;
		gest.afficheMessage("** Conversion en XHTML avec Writer2XHTML...",Nat.LOG_VERBEUX);
		
		//Create a XHTML converter
		Converter converter = ConverterFactory.createConverter(MIMETypes.XHTML_MATHML);

		//Create a configuration
		Config config = converter.getConfig();
		try
		{
			config.read(new FileInputStream("writer2latex/xhtml/config/cleanxhtml.xml"));
			config.setOption("inputencoding","utf-8");
			config.setOption("use_named_entities", "true");
			//Convert the document
			//gest.afficheMessage(Transcription.fTempXHTML+" hdcd\n",Nat.LOG_SILENCIEUX);
			String t=Transcription.fTempXHTML; //il ne faut que le nom de fichier sans le path pour la conversion et seulement le path pour result.write
			ConverterResult result = converter.convert(new FileInputStream(source),t.substring(t.lastIndexOf("/")+1));
			//gest.afficheMessage(t.substring(t.lastIndexOf("/"))+" hsdsdsdsd\n",Nat.LOG_SILENCIEUX);
			//gest.afficheMessage(t.substring(0,t.lastIndexOf("/"))+" FFFFFFFFFFd\n",Nat.LOG_SILENCIEUX);
			result.write(new File(t.substring(0,t.lastIndexOf("/"))));
			/*Enumeration docEnum = result.getMasterDocument().;

			while (docEnum.hasMoreElements()) 
			{
				OutputFile docOut = (OutputFile) docEnum.nextElement();
			    FileOutputStream fos = new FileOutputStream(docOut.getFileName());
			    docOut.write(fos);
			    fos.flush();
			    fos.close();
			}*/
		}
		catch(Exception e)
		{
			gest.afficheMessage("Problème lors de la conversion avec Writer2XHTML "+e.getLocalizedMessage(),Nat.LOG_SILENCIEUX); e.printStackTrace();
			e.printStackTrace();
		}
		/*Write the files
		Enumeration docEnum = dataOut.getDocumentEnumeration();
		while (docEnum.hasMoreElements())
		{
			OutputFile docOut = (OutputFile) docEnum.nextElement();
		    FileOutputStream fos = new FileOutputStream(docOut.getFileName());
		    docOut.write(fos);
		    fos.flush();
		    fos.close();
		}*/
		//conversion de xhtml vers interne
		ConvertisseurXML convXML = new ConvertisseurXML(Transcription.fTempXHTML, cible);
		retour = convXML.convertir(gest);
		tempsExecution = System.currentTimeMillis() - tempsExecution;
		return retour;
	}
}