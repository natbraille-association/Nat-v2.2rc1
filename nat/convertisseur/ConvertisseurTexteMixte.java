/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.convertisseur;

//Package maisons
import nat.Nat;
import gestionnaires.GestionnaireErreur;

//***  java.io ***
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.io.FileInputStream;
/**
 * Convertit un fichier texte contenant du MathMl au format interne
 * @author bruno
 *
 */
public class ConvertisseurTexteMixte extends ConvertisseurTexte
{
	/**
	 * Constructeur
	 * <p>Par défaut, utilise l'encodage UTF-8</p>
	 * @param src l'adresse du fichier source
	 * @param tgt l'adresse du fichier cible
	 */
	public ConvertisseurTexteMixte(String src, String tgt){super(src, tgt);}
	/**
	 * Constructeur
	 * @param src l'adresse du fichier source
	 * @param tgt l'adresse du fichier cible
	 * @param sEncoding encodage du fichier source
	 */
	public ConvertisseurTexteMixte(String src, String tgt,String sEncoding){super(src, tgt,sEncoding);}
	/**
	 * Redéfinition de {@link ConvertisseurTexte#convertir(GestionnaireErreur)}
	 * <p>Convertit le fichier {@link Convertisseur#source} au format interne</p>
	 * <p>Supprime les fils semantics des tags <code>math</code> pour le MathML</p>
	 */
	@Override
	public boolean convertir(GestionnaireErreur gest)
	{
		tempsExecution = System.currentTimeMillis();
		boolean retour=true;
		nbCars = 0;
		nbMots = 0;
		nbPhrases = 0;
		try
		{
			gest.afficheMessage("** Ouverture du fichier source: " + source + " ...",Nat.LOG_VERBEUX);
			//RandomAccessFile raf = new RandomAccessFile(source, "r");
			BufferedReader raf = new BufferedReader(new InputStreamReader(new FileInputStream(source),sourceEncoding));
			gest.afficheMessage("ok\n** Conversion du fichier source ...",Nat.LOG_NORMAL);
			BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(cible),"UTF8"));

			//on met les entêtes au fichier xml
			fcible.write("<?xml version=\"1.1\" encoding=\"UTF-8\"?>");
			fcible.write("\n<!DOCTYPE doc:doc SYSTEM \"" + DTD +"\">");
			fcible.write("\n<doc:doc xmlns:doc=\"espaceDoc\">");
	
			String ligne;

			boolean recopie = false;
			ligne =  raf.readLine();
			while ( ligne != null )
			{
				//Ajout du namespace xmlns="http://www.w3.org/1998/Math/MathML"si il n' en a pas pour les maths
				ligne = ligne.replaceAll("<semantics>", "");
				ligne = ligne.replaceAll("</semantics>", "");
				if (ligne.indexOf("<math ")!=-1 && ligne.indexOf("xmlns")==-1)
				{
					ligne=ligne.replace("<math ","\n<math xmlns=\"http://www.w3.org/1998/Math/MathML\" ");
				}
				else if (ligne.indexOf("<math>")!=-1)
				{
					ligne=ligne.replace("<math>","\n<math xmlns=\"http://www.w3.org/1998/Math/MathML\">");
				}
				if (recopie==true)
				{
					if (ligne.indexOf("</math>")!=-1 || ligne.indexOf("</m:math>")!=-1)
					{
						fcible.write("\n\t\t" + ligne.substring(0,ligne.indexOf("</math>")+7));
						//System.out.println(ligne.substring(0,ligne.indexOf("</math>")+7));
						ligne = ligne.substring(ligne.indexOf("</math>")+7);
						recopie = false;
						if (ligne.length() ==0)
						{
							fcible.write("\n\t</phrase>\n");
							ligne = raf.readLine();
						}
						else
						{
							if (ligne.indexOf("<math")!=-1 || ligne.indexOf("<m:math")!=-1)
							{
								ligneLit(ligne.substring(0,ligne.indexOf("<math")), gest, fcible);
								ligne=ligne.substring(ligne.indexOf("<math"));
								recopie = true;
							}
							else
							{
								ligneLit(ligne, gest, fcible);
								fcible.write("\n\t</phrase>\n");
								ligne =  raf.readLine();
							}
						}
					}
					else
					{
						fcible.write("\t\t" + ligne);
						ligne =  raf.readLine();
					}
				}
				else //(recopie==false)
				{
					if (ligne.indexOf("<math")!=-1 || ligne.indexOf("<m:math")!=-1)
					{
						//fcible.print(ligne.substring(0,ligne.indexOf("<math")+5) + "\n");
						fcible.write("\n\t<phrase>");
						ligneLit(ligne.substring(0,ligne.indexOf("<math")), gest, fcible);
						ligne=ligne.substring(ligne.indexOf("<math"));
						recopie = true;
					}
					if (recopie==false)
					{
						nbPhrases++;
						fcible.write("\n\t<phrase>\n");
						ligneLit(ligne, gest, fcible);
						fcible.write("\n\t</phrase>\n");
						ligne =  raf.readLine();
					}
				}
			}
			fcible.write("\n</doc:doc>");
			fcible.close();
			raf.close();
			gest.afficheMessage("\nLe document contient " + nbPhrases +" paragraphes, " + nbMots + " mots et " + nbCars +" caractères.",Nat.LOG_VERBEUX);
			tempsExecution = System.currentTimeMillis() - tempsExecution;
			gest.afficheMessage("\n----Conversion texte mixte terminée en " + tempsExecution + " msec.\n",Nat.LOG_VERBEUX);
			retour = true;
		}
		catch (java.io.IOException e)
		{
			gest.setException(e);
			gest.gestionErreur();
			retour = false;
		}
		catch (Exception e)  
		{
			gest.setException(e);
			gest.gestionErreur();
			retour = false;
		}		
		return retour;//retour;
	}
}