/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.convertisseur;

import nat.ConfigNat;
import nat.Nat;
import nat.NullEntityResolver;
import nat.Transcription;
//***  java.io ***
import java.io.File;
import java.net.UnknownHostException;
//import java.io.FileReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;

// *** gestionnaires ***
import gestionnaires.GestionnaireErreur;
/**
 * Convertit un fichier XML au format interne
 * Appelle les feuilles de styles correspondant au format xml détecté
 * @author bruno
 *
 */
public class ConvertisseurXML extends Convertisseur
{
	/** adress of the XHTML to Internal format xsl stylesheet*/
	private static final String XHTML_FILTER = "xsl/xhtml2interne.xsl";
	/** adress of the XHTML to Internal format xsl stylesheet*/
	private static final String DTBOOK_FILTER = "xsl/dtbook2interne.xsl";
	/** adress of the XHTML to Internal format xsl stylesheet preserving XML tags*/
	private static final String XHTML_FILTER_PRESERVE = "xsl/xhtml2internePreserve.xsl";
	/**
	 * Constructeur
	 * @param src l'adresse du fichier source
	 * @param tgt l'adresse du fichier cible
	 */
	public ConvertisseurXML(String src, String tgt){super(src, tgt);}
	/**
	 * Convertit le fichier XML/XHTML {@link Convertisseur#source} au format interne de nat
	 * en appelant {@link #xml2FormatInterne(GestionnaireErreur)}
	 * @see nat.convertisseur.Convertisseur#convertir(gestionnaires.GestionnaireErreur)
	 */
	@Override
	public boolean convertir(GestionnaireErreur gest)
	{
		tempsExecution = System.currentTimeMillis();
		boolean retour = false;
		gest.afficheMessage("ok\n** Conversion au format interne...",Nat.LOG_VERBEUX);
		retour = xml2FormatInterne(gest); 
		tempsExecution = System.currentTimeMillis() - tempsExecution;
		//gest.AfficheMessage("ok\n----Conversion terminée en " + tempsExecution + " msec.\n",Nat.LOG_SILENCIEUX);
		return retour;
	}
	/**
	 * Méthode réalisant la conversion via appel à la feuille xsl <code>xsl/xhtml2interne.xsl</code>
	 * @param gestErreur une instance de {@link GestionnaireErreur}
	 * @return true si la conversion s'est bien passé, false sinon
	 */
	private boolean xml2FormatInterne(GestionnaireErreur gestErreur)
	{
		gestErreur.afficheMessage("ok\n*** Création de la fabrique (DocumentBuilderFactory) ...",Nat.LOG_VERBEUX);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//configuration de la fabrique
		factory.setNamespaceAware(true);/*
		try{
			factory.setAttribute("indent-number", new Integer(6));
		}
		catch(IllegalArgumentException iae){}*/
		factory.setValidating(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG);
		//factory.setIgnoringElementContentWhitespace(true);
		factory.setIgnoringComments(true);
		factory.setIgnoringElementContentWhitespace(false);
		factory.setXIncludeAware(false);
		try 
		{
			// sinon, génère parfois des null pointer exp au parsage (problème avec les simples quote)
			factory.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			gestErreur.afficheMessage("ok\n*** Parsage du document d'entrée XML avec SAX ...",Nat.LOG_VERBEUX);
			builder.setErrorHandler(gestErreur);
			//pour les problèmes de résolution d'entités des dtd public, si il n'y a pas de réseau:
			Document doc;
			try{doc = builder.parse(new File(source));}
			catch(UnknownHostException uoe)
			{
				builder.setEntityResolver(new NullEntityResolver(gestErreur));
				doc = builder.parse(new File(source));
			}
			doc.setStrictErrorChecking(false);
			//determining wich xml is used
			DocumentType doctype = doc.getDoctype();
			//System.out.println(doctype.getPublicId());
			
			String filtre = XHTML_FILTER;
			if(doctype!=null && (doctype.getPublicId().contains("dtbook")||doctype.getSystemId().contains("dtbook")))
			{
				filtre = DTBOOK_FILTER;
			}
			else if(ConfigNat.getCurrentConfig().getPreserveTag())
			{
				filtre = XHTML_FILTER_PRESERVE;
			}
				
				
			gestErreur.afficheMessage("ok\n*** Initialisation et lecture de la feuille de style de conversion...",Nat.LOG_VERBEUX);
			TransformerFactory transformFactory = TransformerFactory.newInstance();
			StreamSource styleSource = new StreamSource(new File(filtre));
			// lire le style
			
			Transformer transform = transformFactory.newTransformer(styleSource);
			transform.setParameter("dtd",ConfigNat.getCurrentConfig().getDTD());
			transform.setParameter("processImage", ConfigNat.getCurrentConfig().getTranscrireImages());
			transform.setParameter("chemistryStyle", ConfigNat.getCurrentConfig().getStyleChimie());
			transform.setParameter("indexTableName", ConfigNat.getCurrentConfig().getIndexTableName());
			transform.setParameter("insertIndexBraille", ConfigNat.getCurrentConfig().getIndexTable());
			transform.setParameter("stylistFile", ConfigNat.getCurrentConfig().getUserStylist());
			// conformer le transformeur au style
			DOMSource in = new DOMSource(doc);
			gestErreur.afficheMessage("ok\n*** Création du fichier au format interne ...",Nat.LOG_VERBEUX);
			// Création du fichier de sortie
			//File file = new File("tmpEntites.xhtml");
			File file = new File(Transcription.fTempXML);
			///Result resultat = new StreamResult(fichier);
			StreamResult out = new StreamResult(file);
			gestErreur.afficheMessage("ok\n*** Transformation du document interne...",Nat.LOG_VERBEUX);
			transform.transform(in, out);
			return true;
		}
		catch (Exception e)  
		{
			e.printStackTrace();
			gestErreur.setException(e);
			gestErreur.gestionErreur();
			return false;
		}
	}
}