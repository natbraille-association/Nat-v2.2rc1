/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat.convertisseur;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import nat.ConfigNat;
import nat.Nat;

import org.natbraille.soffice.*;
import org.natbraille.soffice.executor.TimeoutException;

import gestionnaires.GestionnaireErreur;

import org.apache.commons.exec.ExecuteException;
import org.artofsolving.jodconverter.OfficeDocumentConverter;
import org.artofsolving.jodconverter.office.DefaultOfficeManagerConfiguration;
import org.artofsolving.jodconverter.office.OfficeManager;
/**
 * <p>Convertit en odt un document doc, rtf, ou tout autre en faisant 
 * appel à l'API d'openoffice via <code>JODConverter</p>
 * <p>Convertit ensuite cet odt au format interne</p>
 * @author Bruno Mascret, Raphaël Mina
 *
 */
public class Convertisseur2ODT extends Convertisseur
{
	/** Constante pour représenter l'OS LINUX*/
	public static final int OS_LINUX = 1;
	/** Constante pour représenter l'OS WINDOWS*/
	public static final int OS_WINDOWS = 2;
	/** Constante pour représenter l'OS MAC*/
	public static final int OS_MAC = 3;
	/** adresse du fichier odt généré */
	private String tmpOdt = ConfigNat.getUserTempFolder()+"/tmp.odt";
	/** le gestionnaire d'erreur */
	private GestionnaireErreur gest;

	/**
	 * Constructeur
	 * @param src adresse du fichier source
	 * @param tgt adresse du fichier cible
	 */
	public Convertisseur2ODT(String src, String tgt)
	{
		super(src, tgt);
	}

	/**
	 * Convertit en odt un document doc, rtf, ou tout autre en faisant appel à l'API d'openoffice
	 * Convertit ensuite cet odt au format interne
	 * @param g une instance de GestionnaireErreur
	 * @return true si la conversion s'est bien passée
	 */
	@Override
	public boolean convertir(GestionnaireErreur g)
	{
		tempsExecution = System.currentTimeMillis();
		gest = g;

		boolean retour = false;
		File inputFile = new File(source);
		File outputFile = new File(tmpOdt);

		gest.afficheMessage("\n** Conversion de " + source + " au format ODT...", Nat.LOG_NORMAL);
		gest.afficheMessage("\n*** Lancement du convertisseur... ", Nat.LOG_SILENCIEUX);
		
		// configure soffice
        try {
        	SofficeConfig sofficeConfig = new AutoConfigurator().getSofficeConfig();
        	if (sofficeConfig == null) {
        		gest.afficheMessage("[configuration done] no suitable soffice found",Nat.LOG_VERBEUX);
        	} else if (sofficeConfig.isLibreOffice())
        		{ //pour LibreOffice, on lance en ligne de commande avec --convert-to
            	gest.afficheMessage("[configuration done] using " + sofficeConfig,Nat.LOG_VERBEUX);
            	sofficeConfig.setTimeout(120000L); //2 minutes for timeout
            	SofficeDisposableConverter loDc = new SofficeDisposableConverter(sofficeConfig, inputFile, outputFile);
                // execute the converter
                loDc.execute();
                gest.afficheMessage("[ok] successfully converted '" + inputFile + "' to '" + outputFile + "'", Nat.LOG_SILENCIEUX);
    			retour=true;
            }
        	else if (sofficeConfig.isOpenOffice())
        	{ // pour OpenOffice, c'est JODConverter
        		OfficeManager officeManager = null;
        		//OfficeDocumentConverter converter = new OfficeDocumentConverter(officeManager);
        		try {			
        			DefaultOfficeManagerConfiguration omc = new DefaultOfficeManagerConfiguration() ;
        			File oh = new File(sofficeConfig.getPath());
        			omc.setOfficeHome (oh.getParent());
        			omc.setTaskQueueTimeout(120000L);
        			omc.setTaskExecutionTimeout(120000L);
        			gest.afficheMessage(omc.toString(), Nat.LOG_VERBEUX);
        			officeManager = omc.buildOfficeManager();
        			gest.afficheMessage(" ok\n*** conversion en odt avec JODConverter...", Nat.LOG_VERBEUX);
        			officeManager.start();
        			//gest.afficheMessage(officeManager.toString(),Nat.LOG_VERBEUX);
        			OfficeDocumentConverter converter = new OfficeDocumentConverter(officeManager);
        			// convert		
        			converter.convert(inputFile, outputFile);
        			retour=true;
        		} catch (Exception e) {
        			if (officeManager != null){
        				gest.afficheMessage("\nERREUR: Impossible de se connecter à openoffice", Nat.LOG_SILENCIEUX);			
        				e.printStackTrace();
        			} else {
        				gest.afficheMessage("\nERREUR: Impossible de trouver openoffice", Nat.LOG_SILENCIEUX);
        				e.printStackTrace();
        			}
        		} finally {
        			if (officeManager==null) 
    				{
    					gest.afficheMessage("\nProblème LibreOffice ou OpenOffice : 2solutions:\n1. Lancer \"nat pour gros documents\" dans le menu démarrer et réessayez.\n2.Faire la conversion en odt en ouvrant votre document avec votre Office\n", Nat.LOG_SILENCIEUX);
    					//gest.afficheMessage(String.valueOf(retour), Nat.LOG_VERBEUX);
    				}
    			else {
    			try {
    					officeManager.stop();
    				} catch (Exception e) {
    					gest.afficheMessage("\nERREUR: Impossible de fermer openoffice", Nat.LOG_SILENCIEUX);
    					//gest.afficheMessage(e.toString()+" "+e.getStackTrace().toString()+" "+e.getLocalizedMessage()+" "+e.getMessage(), Nat.LOG_VERBEUX);
    				}					
    				}
    			}
        	}
        	else
        	{	gest.afficheMessage("[error] openoffice version not supported", Nat.LOG_SILENCIEUX) ;}
        } catch (TimeoutException e) {
        	gest.afficheMessage("[user timeout] " + e.getLocalizedMessage(), Nat.LOG_SILENCIEUX);
        } catch (FileNotFoundException e) {
        	gest.afficheMessage("[missing or empty output file?] "+e.getLocalizedMessage(),Nat.LOG_SILENCIEUX);
        } catch (ExecuteException e) {
        	gest.afficheMessage("[error] while running process" + e.getLocalizedMessage(),Nat.LOG_SILENCIEUX);
        } catch (IOException e) {
        	gest.afficheMessage("[io error]" + e.getLocalizedMessage(), Nat.LOG_SILENCIEUX);
        }

		if (retour){
			gest.afficheMessage("conversion ok\n", Nat.LOG_VERBEUX);

			// utilisation de ConvertisseurOO
			ConvertisseurOpenOffice c = new ConvertisseurOpenOffice(tmpOdt, cible);
			c.convertir(gest);

			tempsExecution = System.currentTimeMillis() - tempsExecution;			
		}
		else { 
				gest.afficheMessage("conversion PAS ok\n", Nat.LOG_VERBEUX);
		}
		return retour;
	}
}