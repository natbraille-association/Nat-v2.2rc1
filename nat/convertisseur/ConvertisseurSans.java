/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.convertisseur;

import gestionnaires.GestionnaireErreur;
import nat.convertisseur.Convertisseur;
import outils.FileToolKit;
/**
 * <p>La classe <code>ConvertisseurSans</code> se contente de recopier le fichier d'entrée dans le fichier de sortie.
 * Elle ne réalise pas de conversion.</p>
 * @author  Bruno Mascret
 */
public class ConvertisseurSans extends Convertisseur
{

	/**
	 * Constructeur
	 * @param fs adresse du fichier source
	 * @param tempXML adresse du fichier cible
	 */
	public ConvertisseurSans(String fs, String tempXML){super(fs,tempXML);}

	/**
	 * Convertit le fichier {@link Convertisseur#source}
	 * @see nat.convertisseur.Convertisseur#convertir(gestionnaires.GestionnaireErreur)
	 */
	@Override
	public boolean convertir(GestionnaireErreur gest)
	{
		return FileToolKit.copyFile(source, cible);
	}

}
