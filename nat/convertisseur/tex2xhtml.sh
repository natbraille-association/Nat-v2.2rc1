#!/bin/sh
#Exécute l'appel à tex4ht; doit être placé dans le répertoire temporaire de l'utilisateur
echo "lancement..."
cd ~/.nat-braille/tmp
if [ -f "tmp.xhtml" ]
then rm tmp.xhtml
fi
if [ -f "tmp.html" ]
then rm tmp.html
fi
if [ -f "tmp.dvi" ]
then rm tmp.dvi
fi
if [ -f "tmp.aux" ]
then rm tmp.aux
fi
echo X | echo X | echo X | htlatex tmp.tex "xhtml,mathml" " -cunihtf" "-cvalidate"
mv tmp.html tmp.xhtml