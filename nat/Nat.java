/*
 * NAT - An universal Translator
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package nat;
import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import outils.ConfConv;
import nat.ConfigNat;
import nat.GetOptNat;
import nat.convertisseur.Convertisseur;
import nat.convertisseur.ConvertisseurChaine;
import nat.presentateur.PresentateurMEP;
import nat.transcodeur.Transcodeur;
import nat.transcodeur.TranscodeurNormal;
import nat.presentateur.Presentateur;

//import joptsimple.OptionSet;
//import joptsimple.OptionException;

import gestionnaires.AfficheurLog;
import gestionnaires.GestionnaireErreur;
import ui.AfficheurConsole;
import ui.FenetrePrinc;
//import ui.Language;

/**
 * Classe principale de l'application
 * @author bruno
 *
 */
public class Nat 
{
  
    //constantes
    /** Représente un niveau de verbosité des logs muet */
    public final static int LOG_AUCUN = 0;
    /** Représente un niveau de verbosité des logs très faible */
    public final static int LOG_SILENCIEUX = 1;
    /** Représente un niveau de verbosité des logs normal */
    public final static int LOG_NORMAL = 2;
    /** Représente un niveau de verbosité des logs verbeux */
    public final static int LOG_VERBEUX = 3;
    /** Représente un niveau de verbosité des logs verbeux avec les informations de débuggage */
    public final static int LOG_DEBUG = 4;
    /** Représente la génération de version de configuration */
    public final static String CONFS_VERSION = "3" ;
    /** adresse web du fichier contenant le n° de la dernière version en ligne */
    private static final String CURRENT_VERSION_ADDRESS = "http://natbraille.free.fr/current-version.txt";
    /** Represents the use of an unknown or not implemented screen reader */
    public final static int SR_DEFAULT = 1;
    /** Represents the use of JAWS */
    public final static int SR_JAWS = 2;
    /** Represents the use of NVDA */
    public final static int SR_NVDA = 3;
    /** Represents the use of NVDA */
    public final static int SR_WEYES = 4;
 
    /** String contenant la licence de NAT (GPL) */
    private static String licence;
    /** Une instance de gestionnaire d'erreur */
    private GestionnaireErreur gest;
    /** true si pas de transcriptions en cours */
    private boolean ready = true;
    /** true si nouvelle version disponible */
    private int updateAvailable = 0;
   

    /** Liste d'instances de transcription représentant les transcription à réaliser */
    private ArrayList<Transcription> transcriptions = new ArrayList<Transcription>();
    //TODO raph a remplacer par ArrayList<Transcription> transcriptions = new ArrayList<Transcription>(); ???
    /**
     * Constructeur
     * @param g Une instance de GestionnaireErreur
     */
    public Nat(GestionnaireErreur g) 
    {
	licence = getLicence("","");
	gest = g;
	
    }

    /* méthodes d'accès */
    /**
     * renvoie le nom du fichier de configuration
     * @return le nom du fichier de configuration
     */
    public String getFichierConf(){return ConfigNat.getCurrentConfig().getFichierConf();}	
    /**
     * Renvoie une chaine contenant le numéro long de la version de NAT
     * @return une chaine contenant le numéro long de version
     */
    public String getVersionLong(){return ConfigNat.getVersionLong();}
    /**
     * Renvoie une chaine contenant le nom de version de NAT
     * @return une chaine contenant le nom de version
     */
    public String getVersion(){return ConfigNat.getVersion();}
    /**
     * @param ua the updateAvailable to set
     * @see #updateAvailable
     */
    public void setUpdateAvailable(int ua){updateAvailable = ua;}

    /**
     * @return the updateAvailable value
     * @see #updateAvailable
     */
    public int isUpdateAvailable(){return updateAvailable;}

    /**
     * Renvoie l'instance de GestionnaireErreur
     * @return l'instance de GestionnaireErreur
     * @see Nat#gest
     */
    public GestionnaireErreur getGestionnaireErreur() {return gest;}
	
    /**
     * Renvoie la licence de nat préfixée par prefixe et terminée par suffixe
     * @param prefixe préfixe à insérer avant la licence (/* ou <!-- par exemple)
     * @param suffixe suffixe à insérer après la licence (* / ou --> par exemple)
     * @return la licence de NAT
     */
    public static String getLicence(String prefixe, String suffixe)
    {
	licence =  prefixe + " * NAT - An universal Translator\n" +
	    "* Copyright (C) 2009-2014 Bruno Mascret\n" +
	    "* Contact: bmascret@free.fr\n" +
	    "* \n" +
	    "* This program is free software; you can redistribute it and/or\n" +
	    "* modify it under the terms of the GNU General Public License\n" +
	    "* as published by the Free Software Foundation; either version 2\n" +
	    "* of the License.\n" +
	    "* \n" +
	    "* This program is distributed in the hope that it will be useful,\n" +
	    "* but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
	    "* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
	    "* GNU General Public License for more details.\n" +
	    "* \n" +
	    "* You should have received a copy of the GNU General Public License\n" +
	    "* along with this program; if not, write to the Free Software\n" +
	    "* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.\n" +
	    suffixe;	
	return licence;
    }
    /**
     * Fait appel à la fabrique Transcription pour obtenir les instances de transcription à réaliser
     * Utilise le booléen <code>reverse</code> pour contraindre le sens de transcription 
     * @param noirs les adresses des fichiers noir
     * @param brailles les adresses des fichiers braille
     * @param reverse indique le sens de transcription: true si inverse, false sinon
     * @return <code>true</code> si la fabrication a réussi
     * @see Transcription#fabriqueTranscription(String, String, GestionnaireErreur, boolean)
     */
    public boolean fabriqueTranscriptions(ArrayList<String> noirs, ArrayList<String> brailles, boolean reverse)
    {
	boolean retour = true;
	//on vide la liste
	transcriptions.removeAll(transcriptions);
	for(int i=0;i<noirs.size();i++)
	    {
		String noir = noirs.get(i);
		String braille = brailles.get(i);
		Transcription t = Transcription.fabriqueTranscription(noir,braille,gest,reverse);
		if(t!=null){transcriptions.add(t);}
		else{retour=false;}
	    }
	return retour;
    }
    /**
     * Fait appel à la fabrique Transcription pour obtenir les instances de transcription à réaliser
     * Ne détermine pas le sens de la transcription, qui sera établit dans {@link Transcription#fabriqueTranscription(String, String, GestionnaireErreur)}
     * @param noirs les adresses des fichiers noirs
     * @param brailles les adresses des fichiers braille
     * @return <code>true</code> si la fabrication a réussi
     * @see Transcription#fabriqueTranscription(String, String, GestionnaireErreur)
     */
    public boolean fabriqueTranscriptions(ArrayList<String> noirs, ArrayList<String> brailles)
    {
	boolean retour = true;
	//on vide la liste
	transcriptions.removeAll(transcriptions);
	for(int i=0;i<noirs.size();i++)
	    {
		String noir = noirs.get(i);
		String braille = brailles.get(i);
		Transcription t = Transcription.fabriqueTranscription(noir,braille,gest);
		if(t!=null){transcriptions.add(t);}
		else{retour=false;}
	    }
	return retour;
    }
    /**
     * Lance le processus complet de transcription des instances de <code>transcription</code>
     * Attends éventuellement si une transcription est en cours
     * @return true si le scénario s'est déroulé normallement
     * @see Nat#transcriptions
     */
    public boolean lanceScenario()
    {
    	if(!ready)
	    {
    		gest.afficheMessage("\nLa transcription commencera dès la fin de la transcription en cours\n", Nat.LOG_NORMAL);
	    	while(!ready)
		    {
	    		try {Thread.sleep(1000);}
			catch (InterruptedException e) {e.printStackTrace();}
		    }
	    }
    	ready=false;
    	gest.setException(null);
    	boolean retour = true;
    	for(Transcription t : transcriptions)
	    {
	    	try
		    {
	    		retour = retour & t.transcrire();
		    }
	    	catch(OutOfMemoryError oome)
		    {
	    		gest.setException(new Exception("mémoire",oome));
	    		gest.gestionErreur();
		    }
	    }
    	ready =true;
    	return retour;
    }
    
    /**
     * Met {@link #ready} à true
     * Indique à l'objet que la transcription en cours a été interrompue
     */
    public void transStopped(){ready = true;}


    /**
     * Vérifie si une nouvelle version est disponible en ligne
     * Met à jour {@link #updateAvailable}
     * @return true si vérification effectuée, false si vérification impossible
     */
    public boolean checkUpdate()
    {
        boolean retour = true;
    	gest.afficheMessage("Recherche d'une mise à jour de NAT...\n", LOG_VERBEUX);
    	URL url;
		try 
		{
			url = new URL(CURRENT_VERSION_ADDRESS);
			URLConnection urlCon = url.openConnection();
		        
			BufferedReader br = new BufferedReader(new InputStreamReader(urlCon.getInputStream()));
		        
	        String ligne= br.readLine();
	        br.close();
	        if(Integer.parseInt(ligne) > ConfigNat.getSvnVersion())
		    {
	        	updateAvailable = 1;
		    }
	        else if(Integer.parseInt(ligne) < ConfigNat.getSvnVersion())
	        {
	        	updateAvailable = -1;
	        }
	        else {updateAvailable = 0; }
	    }
		catch (NumberFormatException nfe){gest.afficheMessage("\n** pas de connexion web pour vérifier la présence de mise à jour", Nat.LOG_SILENCIEUX);retour=false;}
		catch (MalformedURLException e) {gest.afficheMessage("\n** adresse internet " + CURRENT_VERSION_ADDRESS +" non valide", Nat.LOG_SILENCIEUX);retour=false;}
		catch (IOException e) {gest.afficheMessage("\n**pas de connexion internet pour vérifier si vous avez la dernière version", Nat.LOG_SILENCIEUX);retour=false;}
		return retour;
    }


    /**
     * Appel à la méthode touveEncodingSource de Transcription
     * @param source le fichier source
     * @return une chaîne correspondant à l'encodage du fichier source
     * @see Transcription#trouveEncodingSource(String, GestionnaireErreur)
     */
    public String trouveEncodingSource(String source){return Transcription.trouveEncodingSource(source, gest);}
 
    /**
     * Lecture de l'entrée standard
     * @return chaine lue dans l'entrée standard, terminée par un '\n'
     */
    static private String getOneStdinLine()
    {
		char c = 0;
		String buf = "";
		try
		{
		    while (c != '\n')
		    {
		    	c = (char)(System.in.read());
		    	buf += c;
		    }
		}
		catch(IOException e) {e.printStackTrace();}
		return buf;
    }

    /**
     * Méthode main
     * Analyse la chaine de paramètres, lance ou non l'interface graphique, la transcription, etc
     * @param argv les paramètres de la méthode main
     */
    
    public static void main (String argv[])
    {	
		System.setProperty("javax.xml.transform.TransformerFactory",
				   "net.sf.saxon.TransformerFactoryImpl");
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
				   "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
	
	    ConfigNat.charger(null);  
		GestionnaireErreur gestErreur = new GestionnaireErreur(null,ConfigNat.getCurrentConfig().getNiveauLog());
		//Language texts = new Language("CommandLineInterface");//non utilisé
		
		Nat nat = new Nat(gestErreur);
		AfficheurConsole ac = new AfficheurConsole();
		gestErreur.addAfficheur(ac);
		gestErreur.addAfficheur(new AfficheurLog());
		
		ConfConv.convert(gestErreur);
		
		try
		{
		    GetOptNat cl = new GetOptNat(argv);
	
		    if (cl.useTheGui){
				/* use the gui ; ignore other options */
				ConfigNat.setGUI(true);
				try {
				    UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
				} catch(Exception e) {
				    System.out.println("Error setting Java LAF: " + e);
				}
				FenetrePrinc fenetre = new FenetrePrinc(nat);
				fenetre.pack();
				if (cl.fromFiles.size()==1){
				    fenetre.setEntree(cl.fromFiles.get(0));
				    fenetre.setSortie(cl.fromFiles.get(0));
				    fenetre.setSortieAuto(false);
				}
				fenetre.setVisible(true);
		    }
		    else {
				/* don't use the gui */
				ConfigNat.charger(cl.getConfigFile());
				if (cl.quiet) {gestErreur.removeAfficheur(ac);}
				ConfigNat.getCurrentConfig().setReverseTrans(cl.reverseTranscription);
	
				/* on inverse les from et to si on est en reverse trans */
				ArrayList<String> noirFiles = new ArrayList<String>();
				ArrayList<String> brailleFiles = new ArrayList<String>();
				
				if (cl.reverseTranscription) {
					noirFiles.addAll(cl.toFiles);
					brailleFiles.addAll(cl.fromFiles);
				} else {
					noirFiles.addAll(cl.fromFiles);
					brailleFiles.addAll(cl.toFiles);
				}
				
				if(cl.preserveTag){ConfigNat.getCurrentConfig().setPreserveTag(true);}
				
				/* stream input from stdin */
				if (cl.fromFiles.get(0).equals("-"))
				{
				    while (true)
				    {
						String line = getOneStdinLine();
						String txml = ConfigNat.getUserTempFolder()+"/tmp.xml";
						String ttxt = ConfigNat.getUserTempFolder()+"/tmp.txt";
						String tout = ConfigNat.getUserTempFolder()+"/out.txt";
						
						Convertisseur c = new ConvertisseurChaine(line,txml,"UTF-8");
						Transcodeur   t = new TranscodeurNormal(txml,ttxt,"UTF-8",gestErreur);
						Presentateur  p = new PresentateurMEP(gestErreur,"UTF-8",ttxt,tout,"brailleUTF8");
									
						c.convertir(gestErreur);
						t.transcrire(gestErreur);
						p.presenter();
						try
						{
						    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(tout)));
						    System.out.println(br.readLine());
						    br.close();
						}
						catch (FileNotFoundException e) {e.printStackTrace();}
						catch (IOException e) {e.printStackTrace();}
				    }
				}
				
				//sinon (pas besoin de else)
				
				/* convert from and to files */
			    for (int i=0;i<cl.fromFiles.size();i++)
			    {
					gestErreur.afficheMessage("conversion de "
								  + cl.fromFiles.get(i)
								  + " vers "
								  + cl.toFiles.get(i),Nat.LOG_NORMAL);
			    }
			    if (nat.fabriqueTranscriptions(noirFiles, brailleFiles)){nat.lanceScenario();}
			    else
			    {
			    	gestErreur.afficheMessage("\n**ERREUR: certains fichiers n'existent pas "
							  + "et ne pourront être transcrits", Nat.LOG_SILENCIEUX);
			    	nat.lanceScenario();
			    }
		   }
		}
		catch (GetOptNatException e)
		{
	    	System.out.println(e.getMessage());
	    	System.exit(0);
		}
    }
}

	

	
