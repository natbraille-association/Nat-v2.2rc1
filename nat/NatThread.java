/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat;

import java.util.ArrayList;

/**
 * Lance une transcription vide en arrière plan (utilisé au lancement de NAT)
 * @author bruno
 *
 */
public class NatThread extends Thread
{
	/** Instance de nat */
	Nat nat;
	/**
	 * Constructeur
	 * @param n instance de Nat
	 */
	public NatThread(Nat n){nat=n;}

	/** Redéfinition du run de Thread
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run()
	{
		nat.getGestionnaireErreur().deliver(false);
		String fileName = "ui/vide.odt"; 
    	ArrayList<String> al = new ArrayList<String>() , al2 = new ArrayList<String>();
    	al.add(fileName);
    	al2.add(ConfigNat.getUserTempFolder()+"tmp.txt");
    	nat.fabriqueTranscriptions(al,al2);
    	nat.lanceScenario();
    	nat.getGestionnaireErreur().deliver(true);
    	nat.getGestionnaireErreur().afficheMessage("ok\nNAT est prêt\n", Nat.LOG_SILENCIEUX);
	}
}
