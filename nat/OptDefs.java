/*
 * NAT - An universal Translator
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/**
 * Container and loader for {OptDefs} options definitions found in the xml option definition file
 * @author vivien
 *
 */
public class OptDefs {    
	/**
	 * relative path to the xml option definition file
	 */
	private static final String xmlDefinition = "nat/options.xml";
	/**
	 * static options, loaded from xmlDefinition file
	 */
	private static Hashtable<String,OptDef> options=loadXML(new File(xmlDefinition));

	/**
	 * Loads Options from an xml option definition file
	 * @param xmlFile the xml option definition file
	 * @return an Hashtable instance of string and OptDef
	 */
	private static Hashtable<String,OptDef> loadXML (File xmlFile){
		Hashtable<String,OptDef> opts = new Hashtable<String,OptDef>();
		try
		{
			DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
			DocumentBuilder constructeur= fabrique.newDocumentBuilder();
			Document doc = constructeur.parse(xmlFile);    
			NodeList xmlProperties = doc.
			getElementsByTagName("options").
			item(0).
			getChildNodes();		

			for(int i= 0; i< xmlProperties.getLength();i++)
			{
				Node xmlProperty = xmlProperties.item(i);
				if (xmlProperty.getNodeName().equals("property"))
				{
					OptDef o = new OptDef(xmlProperty);
					opts.put(o.getId(),o);
				}
			}
		}	
		catch (NullPointerException e){e.printStackTrace();}
		catch (ParserConfigurationException e) {e.printStackTrace();} 
		catch (IOException ioe) {ioe.printStackTrace();}
		catch (SAXException e){e.printStackTrace();}
		return opts;
	}	

	/**
	 * get option definition of a given id
	 * @param id the given id
	 * @return the OptDef corresponding to the id
	 */
	public static OptDef getOptDefById(String id){return options.get(id);}
	/**
	 * get all option definitions ids
	 * @return all option definitions ids
	 */
	public static Enumeration<String> getOptDefsIds(){return options.keys();}
	
	/**
	 * for test purposes only
	 * @param a command line arguments
	 */
	public static void main(String [] a)
	{	
		for (	Enumeration<String> allIds = getOptDefsIds(); 
		allIds.hasMoreElements();){
			System.out.println(allIds.nextElement());
		}

		System.out.println("oioi\n");
		System.out.println(options);
		System.out.println(getOptDefById("tr-litt-use-mixed-upper-lower-rules").getClass());
	}
}