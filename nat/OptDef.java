/*
 * NAT - An universal Translator
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
/**
 * Definition of an option, as read in options.xml
 * <property 
 *     id="fi-abr-ask-exceptions"
 *     class="detrans_basic" 
 *     description="fi-abr-ask-exceptions-desc"
 *     default-value="true" >
 *  <gui-renderer component="booleanRenderer" />
 *  </property> @author vivien
 *
 */

public class OptDef {    
   
    /** The xml node containing the property*/
    private Node xmlNode =null;
    
    /**
     * constructs an OptDef from an <option> node in the xml file
     * @param node the xml option node
     */
    public OptDef(Node node){xmlNode = node;}
    
    /**
     * get the value of an option attribute in a xml option node
     * @param node the xml option node
     * @param attrName the attribute name
     * @return the attribute value
     */
    public static String nodeAttributeValue(Node node, String attrName)
    {
		String attributeValue = node.getAttributes().getNamedItem(attrName).getFirstChild().getTextContent();
		return attributeValue;
    }
    /**
     * get xml option node 'id' attribute
     * @return the option node id
     */
    public String getId() {return nodeAttributeValue(xmlNode,"id");}
    /**
     * get xml option node 'class' attribute
     * @return the class attribute value
     */
    public String getCclass() {return nodeAttributeValue(xmlNode,"class");}
    /**
     * get xml option node 'default-value' attribute
     * @return the default-value attribute value
     */
    public  String getDefaultValue() {return nodeAttributeValue(xmlNode,"default-value");}
    /**
     * get xml option node 'description' attribute
     * @return the description attribute value
     */
    public String getDescription(){return nodeAttributeValue(xmlNode,"description");}
    /**
     * check xml option node 'gui-renderer' attribute (
     * describing the interface component used to set this option).
     * existence
     * @return true if existing
     */
    public Node getGuiRendererNode()
    {
		NodeList childs = xmlNode.getChildNodes();
		Node maybeGuiRenderer = null;
		for(int i= 0; i< childs.getLength();i++)
		{
		    maybeGuiRenderer = childs.item(i);
		    /*
		     * Possible RETURN here 
		     */
		    if (maybeGuiRenderer.getNodeName().equals("gui-renderer")){return maybeGuiRenderer;}
		}
		return null;
    }
    /**
     * get xml option node gui-renderer 'component' attribute
     * describing the interface component used to set this option.
     * @return the gui-renderer 'component' attribute value
     */
    public String getGuiRendererName(){return nodeAttributeValue(getGuiRendererNode(),"component");}
}