@echo off
echo *** effacement des .class **
del /s *.class
mkdir natclasses
echo *** Compilation complete de NAT ***
echo --- Package Writer2latex ---
javac -source 1.7 -target 1.7 -d natclasses -O -encoding utf8 writer2latex/xhtml/*.java
echo.
echo --- Packages de Nat ---
javac -source 1.7 -target 1.7 -d natclasses -classpath .;./lib/* -source 6 -target 6 -encoding utf8 gnu/getopt/*.java gestionnaires/*.java ui/*.java nat/transcodeur/*.java nat/saxFuncts/*.java nat/presentateur/*.java nat/convertisseur/*.java nat/*.java outils/*.java org/im4java/core/*.java org/im4java/process/*.java org/im4java/utils/*.java"
echo.
rem pause
echo *** Fabrication de natbraille.jar... ***
rem jar cvfe natbraille.jar nat.Nat writer2latex/xhtml/*.class gestionnaires/*.class ui/*.class nat/transcodeur/*.class nat/saxFuncts/*.class nat/presentateur/*.class nat/convertisseur/*.class nat/*.class outils org/im4java/core/*.class org/im4java/process/*.class org/im4java/utils/*.class
jar cvmf natbraille.manifest natbraille.jar -C natclasses .
rem jar i natbraille.jar
rem pause
echo *** effacement des .class **
rmdir /s /q natclasses
pause
